# awake-event-preview
Preview of the data stored in the AWAKE event files. 

To execute, run `python src.py`. Path to the event files and output directories should be specified in `src.py` by modifying `DATA_PATH`, `SAVE_PATH`, `PLOT_FILE`, `TIMELINE_PLOT_PATH`. 
source /user/awakeop/gzevi/env/setup_awakeop_env1.sh