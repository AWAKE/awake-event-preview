#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  8 09:53:31 2022

@author: vasylhafyc, francescaelverson, gzevi

"""

import time, os, h5py
import matplotlib.pyplot as plt
import numpy as np
import shutil
from scipy.optimize import curve_fit
import sys
from scipy.ndimage import gaussian_filter
import copy
from datetime import datetime, timedelta
import matplotlib.dates as mdates
import matplotlib.gridspec as gridspec
import math



def constr_ref():
    
    """
    Dictionary constructor for the reference trajectory data
    
    """
    return {
        'is1_core' : {'x': [], 'y': []},
        'is2_core' :  {'x': [], 'y': []},
        'btv_54' :  {'x': [], 'y': []},
        'bpm_electron_1': {'x': [], 'y': []},
        'bpm_electron_2': {'x': [], 'y': []},
        'bpm_proton_1': {'x': [], 'y': []},
        'bpm_proton_2': {'x': [], 'y': []},
        'bpm_proton_3': {'x': [], 'y': []},
        'bpm_proton_4': {'x': [], 'y': []},
        'bpm_proton_5': {'x': [], 'y': []},
        'laser_3' : {'x': [], 'y': []},
        'laser_4' :{'x': [], 'y': []},
        'laser_5' : {'x': [], 'y': []},
        }
      

def update_ref_alignment(ref):
    
    return {'proton' :  {'detector_1' : ref['bpm_proton_1'], 
                         'detector_2' : ref['bpm_proton_3']},
            'electron' : {'detector_1' : ref['bpm_electron_1'], 
                          'detector_2' : ref['bpm_electron_2']}, 
            'laser': {'detector_1' : ref['laser_3'], 
                      'detector_2' : ref['laser_5']}, }
    
def constr_timeline_data():
    """
    Dictionary constructor for the timeline data
    """
    
    return {
        'btv_50' : {'x': np.zeros(1), 'y': np.zeros(1), 'sigma_x': np.zeros(1), 'sigma_y': np.zeros(1), 'time' : np.zeros(1) },
        'btv_53' : {'x': np.zeros(1), 'y': np.zeros(1), 'sigma_x': np.zeros(1), 'sigma_y': np.zeros(1), 'time' : np.zeros(1) },
        'btv_54' : {'x': np.zeros(1), 'y': np.zeros(1),'time' : np.zeros(1), },
        'btv_26' : {'x': np.zeros(1), 'y': np.zeros(1), 'time' : np.zeros(1), },
        'laser_3' : {'x': np.zeros(1), 'y': np.zeros(1),'time' : np.zeros(1), },
        'laser_4' : {'x': np.zeros(1), 'y': np.zeros(1),'time' : np.zeros(1), },
        'laser_5' : {'x': np.zeros(1), 'y': np.zeros(1),'time' : np.zeros(1), },
        'is1_core' : {'x': np.zeros(1), 'y': np.zeros(1), 'sigma_x': np.zeros(1), 'sigma_y': np.zeros(1), 'time' : np.zeros(1), },
        'is1_halo' : {'x': np.zeros(1), 'y': np.zeros(1), 'time' : np.zeros(1), },
        'is2_core' : {'x': np.zeros(1), 'y': np.zeros(1), 'sigma_x': np.zeros(1), 'sigma_y': np.zeros(1), 'time' : np.zeros(1), },
        'is2_halo' : {'x': np.zeros(1), 'y': np.zeros(1), 'sigma_x': np.zeros(1), 'sigma_y': np.zeros(1), 'time' : np.zeros(1), },
        'bpm_electron_1': {'x': np.zeros(1), 'y': np.zeros(1), 'time' : np.zeros(1)},
        'bpm_electron_2': {'x': np.zeros(1), 'y': np.zeros(1), 'time' : np.zeros(1)},
        'bpm_proton_1': {'x': np.zeros(1), 'y': np.zeros(1), 'time' : np.zeros(1)},
        'bpm_proton_2': {'x': np.zeros(1), 'y': np.zeros(1), 'time' : np.zeros(1)},
        'bpm_proton_3': {'x': np.zeros(1), 'y': np.zeros(1), 'time' : np.zeros(1)},
        'bpm_proton_4': {'x': np.zeros(1), 'y': np.zeros(1), 'time' : np.zeros(1)},
        'bpm_proton_5': {'x': np.zeros(1), 'y': np.zeros(1), 'time' : np.zeros(1)},
        'MPP_STREAK' : {'x': np.zeros(1), 'y': np.zeros(1),'time' : np.zeros(1), },
        'BTV_STREAK' : {'x': np.zeros(1), 'y': np.zeros(1),'time' : np.zeros(1), },
        #'otr_angle' : {'x': np.zeros(1), 'y': np.zeros(1),'time' : np.zeros(1), },
        'proton_pop_bctf' : {'pop': np.zeros(1), 'time': np.zeros(1), },
        'emeter_02' : {'energy' : np.zeros(1), 'time' : np.zeros(1), },
        'emeter_03' : {'energy' : np.zeros(1), 'time' : np.zeros(1), },
        'emeter_04' : {'energy' : np.zeros(1), 'time' : np.zeros(1), },
        'emeter_05' : {'energy' : np.zeros(1), 'time' : np.zeros(1), },
        'plasma_light' : {'upstream': np.zeros(1), 'downstream' : np.zeros(1), 'time' : np.zeros(1), },
        'proton_pop_bctfi' :{'charge' : np.zeros(1), 'time' : np.zeros(1), },

    }

def constr_alignment_tl(): 
    """
    Dictionary constructor for the alignment timeline data
    """
    return {'alignment_p_e' : {'upstream' : {'rel_pos': {'x': np.zeros(1), 'y' : np.zeros(1)},
                                             'angle' :{'x': np.zeros(1), 'y' : np.zeros(1)} }, 
                               'downstream' : {'rel_pos': {'x': np.zeros(1), 'y' : np.zeros(1)},
                                               'angle' :{'x': np.zeros(1), 'y' : np.zeros(1)} },
                               'time' : np.zeros(1)
                               },
            'alignment_p_l' : {'upstream' : {'rel_pos': {'x': np.zeros(1), 'y' : np.zeros(1)},
                                             'angle' :{'x': np.zeros(1), 'y' : np.zeros(1)} }, 
                               'downstream' : {'rel_pos': {'x': np.zeros(1), 'y' : np.zeros(1)},
                                               'angle' :{'x': np.zeros(1), 'y' : np.zeros(1)} }, 
                               'time' : np.zeros(1)
                               },
            'deltas_p_e' : {'detector_1' : {'x': np.zeros(1), 'y' : np.zeros(1),  'time' : np.zeros(1) },
                            'detector_2' :  {'x': np.zeros(1), 'y' : np.zeros(1),  'time' : np.zeros(1) },
                            'detector_3' : {'x': np.zeros(1), 'y' : np.zeros(1),  'time' : np.zeros(1) },
                            'detector_4' :  {'x': np.zeros(1), 'y' : np.zeros(1),  'time' : np.zeros(1) },                 
                            },
        
            'deltas_p_l' : {'detector_1' : {'x': np.zeros(1), 'y' : np.zeros(1),  'time' : np.zeros(1) },
                            'detector_2' :  {'x': np.zeros(1), 'y' : np.zeros(1),  'time' : np.zeros(1) },
                            'detector_3' : {'x': np.zeros(1), 'y' : np.zeros(1),  'time' : np.zeros(1) },
                            'detector_4' :  {'x': np.zeros(1), 'y' : np.zeros(1),  'time' : np.zeros(1) },   
                            }
            }

def update_alignment_tl(event_data, delta_curr, alignment_tl):

    """
    Update timeline for raw position differences at single detectors and for 
    trajectory.
    """
    # Proton-electron alignment
    try:
        get_alignment(delta_curr['proton'], delta_curr['electron'], 'proton', 'electron',  alignment_tl['alignment_p_e'], alignment_tl['deltas_p_e'])
    except:
        print('missing proton-electron detector data to get alignment')
   
    # Proton-laser alignment
    try:
        get_alignment(delta_curr['proton'], delta_curr['laser'], 'proton', 'laser', alignment_tl['alignment_p_l'], alignment_tl['deltas_p_l'])
    except:
        print('missing proton-laser detector data to get alignment')
    
   

def update_timeline(timeline_data, eventdata):
    """
    Append the data from `eventdata` to `timeline_data`
    """
    # N.B: set restictions here on the data quality thresholds for each detector
    # i.e. proton population, ensuring that the shot is not late

    # timeline is1_core

    dttmp = eventdata['is1_core']['data_1d']
    sigma = eventdata['is1_core']['sigma']
    timeline_tmp = copy.deepcopy(timeline_data['is1_core'])
    if np.isfinite(dttmp['x']) & np.isfinite(dttmp['y']) & (eventdata['is1_core']['timediff'] < 10) & (eventdata['proton_pop_bctf'] > 0.0) :
        tmp_1 = np.append(timeline_tmp['x'], np.array(dttmp['x'] ) )
        tmp_2 = np.append(timeline_tmp['y'], np.array(dttmp['y'] ) )
        tmp_3 = np.append(timeline_tmp['sigma_x'], np.array(sigma['x']) )
        tmp_4 = np.append(timeline_tmp['sigma_y'], np.array(sigma['y']) )
        timeadd = np.append(timeline_tmp['time'],datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S'))
        timeline_tmp.update({'x' : tmp_1})
        timeline_tmp.update({'y' : tmp_2})
        timeline_tmp.update({'sigma_x' : tmp_3})
        timeline_tmp.update({'sigma_y' : tmp_4})
        timeline_tmp.update({'time': timeadd})
        timeline_data.update({'is1_core' : timeline_tmp})
    
    # timeline is2_core

    dttmp = eventdata['is2_core']['data_1d']
    sigma = eventdata['is2_core']['sigma']
    timeline_tmp = copy.deepcopy(timeline_data['is2_core'])
    if np.isfinite(dttmp['x']) & np.isfinite(dttmp['y'])& (eventdata['is2_core']['timediff'] < 10) & (eventdata['proton_pop_bctf'] > 0.0) :
        tmp_1 = np.append(timeline_tmp['x'], np.array(dttmp['x'] ) )
        tmp_2 = np.append(timeline_tmp['y'], np.array(dttmp['y'] ) )
        tmp_3 = np.append(timeline_tmp['sigma_x'], np.array(sigma['x']) )
        tmp_4 = np.append(timeline_tmp['sigma_y'], np.array(sigma['y']) )
        timeadd = np.append(timeline_tmp['time'],datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S') )  
        timeline_tmp.update({'x' : tmp_1})
        timeline_tmp.update({'y' : tmp_2})
        timeline_tmp.update({'sigma_x' : tmp_3})
        timeline_tmp.update({'sigma_y' : tmp_4})
        timeline_tmp.update({'time': timeadd})
        timeline_data.update({'is2_core' : timeline_tmp})
    
    # timeline btv_54

    dttmp = eventdata['btv_54']['data_1d']
    timeline_tmp = copy.deepcopy(timeline_data['btv_54'])
    if np.isfinite(dttmp['x']) & np.isfinite(dttmp['y']) & (eventdata['btv_54']['timediff'] < 10) & (eventdata['proton_pop_bctf'] > 0.0) :
        tmp_1 = np.append(timeline_tmp['x'], np.array(dttmp['x'] ) )
        tmp_2 = np.append(timeline_tmp['y'], np.array(dttmp['y'] ) )
        timeadd = np.append(timeline_tmp['time'],datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S') )
        timeline_tmp.update({'x' : tmp_1})
        timeline_tmp.update({'y' : tmp_2})
        timeline_tmp.update({'time': timeadd})
        timeline_data.update({'btv_54' : timeline_tmp})
    
    
    # # timeline Awake Cam 05

    # dttmp = eventdata['AWAKECAM_05']['data']
    # timeline_tmp = copy.deepcopy(timeline_data['AWAKECAM_05'])
    # if (np.shape(dttmp) != (1,0)) & (eventdata['AWAKECAM_05']['timediff'] < 10) & (eventdata['proton_pop_bctf'] > 0.0):
    #     centr_params = def_centroid(dttmp)[0]
    #     if np.prod(np.invert(np.isnan(centr_params))):
    #         tmp_1 = np.append(timeline_tmp['x'], np.array(centr_params[0]) )
    #         tmp_2 = np.append(timeline_tmp['y'], np.array(centr_params[1]) )
    #         timeadd = np.append(timeline_tmp['time'],datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S') )
    #         timeline_tmp.update({'x' : tmp_1})
    #         timeline_tmp.update({'y' : tmp_2})
    #         timeline_tmp.update({'time': timeadd})
    #     timeline_data.update({'AWAKECAM_05' : timeline_tmp})
   
    #timeline laser_3

    dttmp = eventdata['laser_3']['data_1d']
    #print('timeline:', dttmp, eventdata['laser_3']['timediff'], eventdata['proton_pop_bctf'])
    timeline_tmp = copy.deepcopy(timeline_data['laser_3'])
    if np.isfinite(dttmp['x']) & np.isfinite(dttmp['y']) & (eventdata['laser_3']['timediff'] < 10) & (eventdata['proton_pop_bctf'] > 0.0) :
        tmp_1 = np.append(timeline_tmp['x'], np.array(dttmp['x'] ) )
        tmp_2 = np.append(timeline_tmp['y'], np.array(dttmp['y'] ) )
        timeadd = np.append(timeline_tmp['time'],datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S') )
        timeline_tmp.update({'x' : tmp_1})
        timeline_tmp.update({'y' : tmp_2})
        timeline_tmp.update({'time': timeadd})
        timeline_data.update({'laser_3' : timeline_tmp})
        #print('done', timeline_tmp)
    #timeline laser_4
        
    dttmp = eventdata['laser_4']['data_1d']
    timeline_tmp = copy.deepcopy(timeline_data['laser_4'])
    if np.isfinite(dttmp['x']) & np.isfinite(dttmp['y']) & (eventdata['laser_4']['timediff'] < 10) & (eventdata['proton_pop_bctf'] > 0.0) :
        tmp_1 = np.append(timeline_tmp['x'], np.array(dttmp['x'] ) )
        tmp_2 = np.append(timeline_tmp['y'], np.array(dttmp['y'] ) )
        timeadd = np.append(timeline_tmp['time'],datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S') )
        timeline_tmp.update({'x' : tmp_1})
        timeline_tmp.update({'y' : tmp_2}) 
        timeline_tmp.update({'time': timeadd})
        timeline_data.update({'laser_4' : timeline_tmp})
    
    #timeline laser_5
     
    dttmp = eventdata['laser_5']['data_1d']
    timeline_tmp = copy.deepcopy(timeline_data['laser_5'])
    if np.isfinite(dttmp['x']) & np.isfinite(dttmp['y']) & (eventdata['laser_5']['timediff'] < 10) & (eventdata['proton_pop_bctf'] > 0.0) :
        tmp_1 = np.append(timeline_tmp['x'], np.array(dttmp['x'] ) )
        tmp_2 = np.append(timeline_tmp['y'], np.array(dttmp['y'] ) )
        timeadd = np.append(timeline_tmp['time'],datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S') )
        timeline_tmp.update({'x' : tmp_1})
        timeline_tmp.update({'y' : tmp_2})
        timeline_tmp.update({'time': timeadd})
        timeline_data.update({'laser_5' : timeline_tmp})

    # timeline bpm proton 1

    dttmp = eventdata['bpm_proton_1']['data_1d']
    if np.isfinite(dttmp['x']):
        timeline_tmp = copy.deepcopy(timeline_data['bpm_proton_1'])
        tmp_1 = np.append(timeline_tmp['x'],np.array(dttmp['x']))
        tmp_2 = np.append(timeline_tmp['y'],np.array(dttmp['y']))
        timeadd = np.append(timeline_tmp['time'], datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S'))
        timeline_tmp.update({'x' : tmp_1})
        timeline_tmp.update({'y' : tmp_2})
        timeline_tmp.update({'time': timeadd})
        timeline_data.update({'bpm_proton_1' : timeline_tmp})
 

    # timeline bpm proton 2

    dttmp = eventdata['bpm_proton_2']['data_1d']
    timeline_tmp = copy.deepcopy(timeline_data['bpm_proton_2'])
    if np.isfinite(dttmp['x']) & np.isfinite(dttmp['y']):
        timeline_tmp = copy.deepcopy(timeline_data['bpm_proton_2'])
        tmp_1 = np.append(timeline_tmp['x'],np.array(dttmp['x']))
        tmp_2 = np.append(timeline_tmp['y'],np.array(dttmp['y']))
        timeadd = np.append(timeline_tmp['time'], datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S'))
        timeline_tmp.update({'x' : tmp_1})
        timeline_tmp.update({'y' : tmp_2})
        timeline_tmp.update({'time': timeadd})
        timeline_data.update({'bpm_proton_2' : timeline_tmp})
   

    # timeline bpm proton 3

    dttmp = eventdata['bpm_proton_3']['data_1d']
    timeline_tmp = copy.deepcopy(timeline_data['bpm_proton_3'])
    if np.isfinite(dttmp['x']) & np.isfinite(dttmp['y']):
        tmp_1 = np.append(timeline_tmp['x'],np.array(dttmp['x']))
        tmp_2 = np.append(timeline_tmp['y'],np.array(dttmp['y']))
        timeadd = np.append(timeline_tmp['time'], datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S'))
        timeline_tmp.update({'x' : tmp_1})
        timeline_tmp.update({'y' : tmp_2})
        timeline_tmp.update({'time': timeadd})
        timeline_data.update({'bpm_proton_3' : timeline_tmp})

    # timeline bpm proton 4

    dttmp = eventdata['bpm_proton_4']['data_1d']
    timeline_tmp = copy.deepcopy(timeline_data['bpm_proton_4'])
    if np.isfinite(dttmp['x']) & np.isfinite(dttmp['y']):
        tmp_1 = np.append(timeline_tmp['x'],np.array(dttmp['x']))
        tmp_2 = np.append(timeline_tmp['y'],np.array(dttmp['y']))
        timeadd = np.append(timeline_tmp['time'], datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S'))
        timeline_tmp.update({'x' : tmp_1})
        timeline_tmp.update({'y' : tmp_2})
        timeline_tmp.update({'time': timeadd})
        timeline_data.update({'bpm_proton_4' : timeline_tmp})


    # timeline bpm proton 5

    dttmp = eventdata['bpm_proton_5']['data_1d']
    timeline_tmp = copy.deepcopy(timeline_data['bpm_proton_5'])
    if np.isfinite(dttmp['x']) & np.isfinite(dttmp['y']):
        tmp_1 = np.append(timeline_tmp['x'],np.array(dttmp['x']))
        tmp_2 = np.append(timeline_tmp['y'],np.array(dttmp['y']))
        timeadd = np.append(timeline_tmp['time'], datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S'))
        timeline_tmp.update({'x' : tmp_1})
        timeline_tmp.update({'y' : tmp_2})
        timeline_tmp.update({'time': timeadd})
        timeline_data.update({'bpm_proton_5' : timeline_tmp})
        
        
    # timeline bpm electron 1

    dttmp = eventdata['bpm_electron_1']['data_1d']
    timeline_tmp = copy.deepcopy(timeline_data['bpm_electron_1'])

    if np.isfinite(dttmp['x']) & np.isfinite(dttmp['y']):
        tmp_1 = np.append(timeline_tmp['x'],np.array(dttmp['x']))
        tmp_2 = np.append(timeline_tmp['y'],np.array(dttmp['y']))
        timeadd = np.append(timeline_tmp['time'], datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S'))
        timeline_tmp.update({'x' : tmp_1})
        timeline_tmp.update({'y' : tmp_2})
        timeline_tmp.update({'time': timeadd})
        timeline_data.update({'bpm_electron_1' : timeline_tmp})

    # timeline bpm electron 2

    dttmp = eventdata['bpm_electron_2']['data_1d']
    timeline_tmp = copy.deepcopy(timeline_data['bpm_electron_2'])

    if np.isfinite(dttmp['x']) & np.isfinite(dttmp['y']):
        tmp_1 = np.append(timeline_tmp['x'],np.array(dttmp['x']))
        tmp_2 = np.append(timeline_tmp['y'],np.array(dttmp['y']))
        timeadd = np.append(timeline_tmp['time'], datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S'))
        timeline_tmp.update({'x' : tmp_1})
        timeline_tmp.update({'y' : tmp_2})
        timeline_tmp.update({'time': timeadd})
        timeline_data.update({'bpm_electron_2' : timeline_tmp})


    # timeline emeter 2

    dttmp = eventdata['emeter_02']['energy']
    timeline_tmp = copy.deepcopy(timeline_data['emeter_02'])
    if np.isfinite(dttmp):
        tmp = np.append(timeline_tmp['energy'],np.array(dttmp))
        timeadd = np.append(timeline_tmp['time'], datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S'))
        timeline_tmp.update({'energy' : tmp})
        timeline_tmp.update({'time': timeadd})
        timeline_data.update({'emeter_02' : timeline_tmp})

    # timeline emeter 3

    dttmp = eventdata['emeter_03']['energy']
    timeline_tmp = copy.deepcopy(timeline_data['emeter_03'])
    if np.isfinite(dttmp):
        tmp = np.append(timeline_tmp['energy'],np.array(dttmp))
        timeadd = np.append(timeline_tmp['time'], datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S'))
        timeline_tmp.update({'energy' : tmp})
        timeline_tmp.update({'time': timeadd})
        timeline_data.update({'emeter_03' : timeline_tmp})

    # timeline emeter 4

    dttmp = eventdata['emeter_04']['energy']
    timeline_tmp = copy.deepcopy(timeline_data['emeter_04'])
    if np.isfinite(dttmp):
        tmp = np.append(timeline_tmp['energy'],np.array(dttmp))
        timeadd = np.append(timeline_tmp['time'], datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S'))
        timeline_tmp.update({'energy' : tmp})
        timeline_tmp.update({'time': timeadd})
        timeline_data.update({'emeter_04' : timeline_tmp})

    # timeline emeter 5

    dttmp = eventdata['emeter_05']['energy']
    timeline_tmp = copy.deepcopy(timeline_data['emeter_05'])
    if np.isfinite(dttmp):
        tmp = np.append(timeline_tmp['energy'],np.array(dttmp))
        timeadd = np.append(timeline_tmp['time'], datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S'))
        timeline_tmp.update({'energy' : tmp})
        timeline_tmp.update({'time': timeadd})
        timeline_data.update({'emeter_05' : timeline_tmp})
    
    # timeline plasma light

    dttmp = eventdata['plasma_light']['point']
    timeline_tmp = copy.deepcopy(timeline_data['plasma_light'])
    if np.isfinite(dttmp['upstream']) and np.isfinite(dttmp['downstream']):
        tmp_1 = np.append(timeline_tmp['upstream'],np.array(dttmp['upstream']))
        tmp_2 = np.append(timeline_tmp['downstream'],np.array(dttmp['downstream']))
        timeadd = np.append(timeline_tmp['time'], datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S'))
        timeline_tmp.update({'upstream' : tmp_1})
        timeline_tmp.update({'downstream' : tmp_2})
        timeline_tmp.update({'time': timeadd})
        timeline_data.update({'plasma_light' : timeline_tmp})


    # timeline proton beam intensity: BCTF
    
    timeline_tmp = copy.deepcopy(timeline_data)['proton_pop_bctf']
    if type(dttmp) is not bool:
        tmp = np.append(timeline_tmp['pop'], np.array(eventdata['proton_pop_bctf']))
        timeadd = np.append(timeline_tmp['time'], datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S'))
    else:
        tmp = np.append(timeline_tmp['pop'], np.array(np.nan))
        timeadd = np.append(timeline_tmp['time'], np.array(np.nan))
    timeline_data['proton_pop_bctf'].update({'pop' : tmp})
    timeline_data['proton_pop_bctf'].update({'time': timeadd})
  
    # timeline proton beam intensity: BCTFI
    dttmp = eventdata['proton_pop_bctfi']['charge']
    timeline_tmp = copy.deepcopy(timeline_data['proton_pop_bctfi'])
    if np.size(dttmp) > 0:
        if np.isfinite(dttmp):
            tmp = np.append(timeline_tmp['charge'], np.array(dttmp))
            timeadd = np.append(timeline_tmp['time'],datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S') )
            timeline_tmp.update({'charge' : tmp})
            timeline_tmp.update({'time': timeadd})
            timeline_data.update({'proton_pop_bctfi' : timeline_tmp})



def update_timeline_average(timeline_average, timeline_data, eventdata):    
    
    # N.B: timeline_data is zeroed at the start
    
    def average_array(data, avg_data, start_index, end_index):
        sample_data = data[start_index:end_index] # slice out latest 'sample_size' number of events 
        average = np.median(sample_data) # use mean... alternatively use median
        new_data = np.append(avg_data, average)    
        return new_data

    sample_size = 20 
    
    for detector, data in timeline_data.items():
        # Do not average the proton_pop_bctf
        if detector == 'proton_pop_bctf': 
            timeline_average['proton_pop_bctf'] = timeline_data['proton_pop_bctf'] 
            continue
        
        # Streamline all the naming conventions
        if detector.startswith('bpm'): x = 'x'; y = 'y'
        elif detector.startswith('emeter'): data_1D = 'energy'
        elif detector == 'proton_pop_bctfi':  data_1D = 'charge'
        elif detector == 'plasma_light': x = 'upstream'; y = 'downstream'
        else: x = 'x'; y = 'y'
            

        # One dimensional data
        if detector.startswith('emeter') or detector == 'proton_pop_bctfi':
            total_number_events = data[data_1D].size
            # Only update if there is a new entry on the raw data timeline array
            if timeline_data[detector]['time'][-1] != timeline_average[detector]['time'][-1]:
    
                end_index = total_number_events # Index of most recent event
                start_index = end_index - sample_size # Index of event used as the start of the average 
                
                if total_number_events < 21:# Wait until we have 20 non-zeroed data points before averaging - copy the raw data for now  
                    timeline_average[detector] = copy.deepcopy(timeline_data[detector])
                
                if total_number_events > 20: # Once we have 20 non-zeroed data points - start averaging 
                    timeline_tmp = copy.deepcopy(timeline_data[detector]) 
                    timeline_avg = copy.deepcopy(timeline_average[detector])
                    tmp = average_array(timeline_tmp[data_1D], timeline_avg[data_1D], start_index, end_index)
                    timeline_tmp.update({data_1D : tmp})
                    timeline_average.update({detector : timeline_tmp})

        elif detector.startswith('alignment') : continue
        # Two dimensional data
        elif detector.startswith('deltas') : continue
        else:  
            total_number_events = data[x].size
            if timeline_data[detector]['time'][-1] != timeline_average[detector]['time'][-1]:

                end_index = total_number_events # Index of most recent event
                start_index = end_index - sample_size # Index of event used as the start of the average
                
                if total_number_events < 21: # Wait until we have 20 non-zeroed data points before averaging - copy the raw data for now
                    timeline_average[detector] = copy.deepcopy(timeline_data[detector])
                
                if total_number_events > 20: # Once we have 20 non-zeroed data points - start averaging
                    timeline_tmp = copy.deepcopy(timeline_data[detector])
                    timeline_avg = copy.deepcopy(timeline_average[detector])
                    tmp_1 = average_array(timeline_tmp[x], timeline_avg[x], start_index, end_index)
                    tmp_2 = average_array(timeline_tmp[y], timeline_avg[y], start_index, end_index)
                    timeline_tmp.update({x : tmp_1})
                    timeline_tmp.update({y : tmp_2})
                    # Update average sigma for proton beam at IS1 Core and IS2 Core
                    if detector == 'is1_core' or detector == 'is2_core':
                        tmp_3 = average_array(timeline_tmp['sigma_x'],timeline_avg['sigma_x'], start_index, end_index)
                        tmp_4 = average_array(timeline_tmp['sigma_y'],timeline_avg['sigma_y'], start_index, end_index)                    
                        timeline_tmp.update({'sigma_x':tmp_3})
                        timeline_tmp.update({'sigma_y':tmp_4})
                    timeline_average.update({detector : timeline_tmp})


def Gauss(x, a, x0, sigma, p):
    return a * np.exp(-(x - x0)**2 / (2 * sigma**2)) + p

def fit_gaussian(xdata, ydata): # xdata are 1D coordinates, ydata is rough gaussian

    mean = np.nansum(xdata * ydata) / np.nansum(ydata) # average coord using weighted coords
    sigma = np.sqrt(np.nansum(ydata * (xdata - mean)**2) / np.nansum(ydata))

    try:
        popt, pcov = curve_fit(Gauss, xdata, ydata, p0=[np.nanmax(ydata), mean, sigma, 0.2])
        
        if (popt[1] > len(ydata)) or (popt[1] <= 0) or (np.abs(popt[2]) > 0.5 * len(ydata)):
            popt = [np.nan, np.nan, np.nan, np.nan]
            
    except :
        popt = [np.nan, np.nan, np.nan, np.nan] 
        sigma = np.nan

    return (popt, sigma)

def def_centroid(data):
    """
    Fit centroid to the data from IS1 Core. Customize certain data range for a better fit if needed.
    Returns x and y coordinates and σ. 
    """

    xpr = np.nansum(data, 0) #sum of data points vertically (y direction) stored in horizontal array for sum of each column
    xpr = xpr / np.nanmax(xpr) # normalising to most populated column to form 1D gaussian expressing vertical density of points
    xpr = xpr
    params = fit_gaussian(np.arange(len(xpr)), xpr)
    best_parm_x = params[0][1]
    best_sigma_x = params[1]

    ypr = np.nansum(data, 1) #sum of data points horizontally (x direction) stored in horizontal array for sum of each row
    ypr = ypr / np.nanmax(ypr) # normalising to most populated row to form 1D gaussian expressing horizontal density of points
    ypr = ypr
    params =fit_gaussian(np.arange(len(ypr)), ypr)
    best_parm_y = params[0][1]
    best_sigma_y =  params[1]

    return ((best_parm_x, best_parm_y), (best_sigma_x, best_sigma_y))


def plot_timeline_xy(leftdata, rightdata, ax1, ax2, ax3, camname, dx, dy, 
                     ref_pos, ref_times_all, starttime, endtime, delta=1):
    """
    Plots timeline for two-dimensional data
    
    ---
    leftdata: raw data is displayed on the 'most recent 20 events, x-y cross section' plot
    rightdata: either raw data or averaged data is displayed on the timeline plot
    delta: sets the axes limits i.e. smaller delta increases the visual precision
    
    """

    
    # Convert the timeline data (e.g. pixels) to mm 
    leftxdata = leftdata['x']*dx
    leftydata = leftdata['y']*dy
    rightxdata = rightdata['x']*dx
    rightydata = rightdata['y']*dy 
   
    # Format the axis to transform datetime objects correctly
    ax2.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    ax3.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    ax2.set_xlim(starttime, endtime+timedelta(seconds=1))
    ax3.set_xlim(starttime, endtime+timedelta(seconds=1))
    
    # Plot the last 20 events only on the 2D plot
    if np.size(leftxdata) != 1: # ignore zeroed data 
    
        xhist = np.histogram(leftxdata[1:][-20:], bins=200)
        yhist = np.histogram(leftydata[1:][-20:], bins=200)

        # Get mean values
        xmean = xhist[1][np.argmax(xhist[0])]
        ymean = yhist[1][np.argmax(yhist[0])]
        
        ax1.scatter(leftxdata[1:][-20:], leftydata[1:][-20:], alpha=0.4, s=15,)
        
        # Plot histogram on 2D plot
        ax111 = ax1.twinx() 
        ax112 = ax1.twiny()
        ax111.set_yticks([])
        ax112.set_xticks([])
        hist111 = ax111.hist(leftxdata[-20:], bins = np.linspace(xmean-delta*dx*2.5, xmean+delta*dx*2.5, 20), histtype='step',)
        hist222 = ax112.hist(leftydata[-20:], bins = np.linspace(ymean-delta*dy*2.5, ymean+delta*dy*2.5, 20), histtype='step', orientation='horizontal', )
        ax111.set_ylim(0, 4*max(hist111[0]))
        ax112.set_xlim(0, 4*max(hist222[0]))
        
        # Set limits for axes, position limits
        ax1.set_xlim(xmean-delta*dx*2.5, xmean+delta*dx*2.5)
        ax1.set_ylim(ymean-delta*dy*2.5, ymean+delta*dy*2.5)
        ax2.set_ylim(xmean-delta*dx, xmean+delta*dx)
        ax3.set_ylim(ymean-delta*dy, ymean+delta*dy)
        
        
    # Plot all events on the 1D plot
    ax2.scatter(rightdata['time'][1:], rightxdata[1:], s=15,)
    ax3.scatter(rightdata['time'][1:], rightydata[1:], s= 15)
   
    # # Plot reference line corresponding to 20th event

    # if np.size(leftxdata)>20 and np.size(leftydata)>20: 
    #     ax1.axhline(y = rightydata[20])
    #     ax1.axvline(x = rightxdata[20])
    #     ax2.axhline(y = rightxdata[20])
    #     ax3.axhline(y = rightydata[20])
    #     #  Get mean values
    #     xmean = rightxdata[20]
    #     ymean = rightydata[20]
        
    #     ax2.set_ylim(xmean-delta*dx, xmean+delta*dx)
    #     ax3.set_ylim(ymean-delta*dy, ymean+delta*dy)


    # Plot reference line...
    if [] not in list(ref_pos.values()):
        
        ax2.axhline(y = ref_pos['x'], ls = 'dashed', alpha = 0.6)
        ax3.axhline(y = ref_pos['y'], ls = 'dashed', alpha = 0.6)
        ax2.set_ylim(ref_pos['x']-delta*dx, ref_pos['x']+delta*dx)
        ax3.set_ylim(ref_pos['y']-delta*dy, ref_pos['y']+delta*dy)

    # Plot vertical lines marking when reference times were changed
    try:
        for i in ref_times_all:
            ax2.axvline(x = i, alpha = 0.3, color = 'red')
            ax3.axvline(x = i, alpha = 0.3, color = 'red')
    except:
        pass 
        
    
    # Add jitter information                                                                                                                                                                                                                  
    camname2 = camname+', jitter: '+'{0:.2f}'.format(np.std(leftxdata[-20:]))+', '+'{0:.2f}'.format(np.std(leftydata[-20:]))
    text = ax1.text(0.03,0.9,camname2, weight="bold", fontsize=12, transform=ax1.transAxes, )

def plot_timeline_x(data_vals, data_time, ax1, starttime, endtime, yax_label, leg_label = None,
                    delta=1000000, bbox_params = None, color = 'turquoise', offset = 0.0):
    
    """
    Plots timeline for one-dimensional data
    ---
    dx: scaling factor if required, delta: specifies the axes limits
    """

    ax1.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    ax1.scatter(data_time[1:], data_vals[1:], s=15, c = color, label = leg_label)
    ax1.set_xlim(starttime, endtime+timedelta(seconds=1))
    ax1.set_ylabel(yax_label, bbox = bbox_params)
    ax1.set_xlabel("Time")
    # ax1.tick_params(axis='y', labelcolor='#feffb3')
    
    # ax1.set_ylim(ymean-delta*dy, ymean+delta*dy)
   

def _plot_figure_with_proj(ax, data):

    try:

        nx = data.shape[1]
        ny = data.shape[0]

        xpr = np.nansum(data, 0) ; xpr = xpr / np.nanmax(xpr)
        ypr = np.nansum(data, 1) ; ypr = ypr / np.nanmax(ypr)

        xrange = list(np.asarray(range(0, nx)) + 0.5)
        yrange =  list(np.asarray(range(0, ny)) + 0.5)
        
        max_amp = np.max(gaussian_filter(data, sigma=3))

        ax.pcolormesh(range(0,nx+1), range(0,ny+1), data, vmin=0, vmax=max_amp, rasterized=True, cmap="inferno")
        
        ax.text(0.21,0.78,"max. amp: " + str(round(max_amp, ndigits=2)), transform=ax.transAxes,  )

        ax2 = ax.twinx()
        ax3 = ax.twiny()
        ax2.fill_between(xrange, list(xpr),  step='mid', color="white", alpha=0.5, lw=0.0, zorder = 2)
        ax2.set_ylim(0, 4.5)
        ax3.fill_betweenx(yrange, list(ypr), step='mid', color="white", alpha=0.5,  lw=0.0,  zorder = 2)
        ax3.set_xlim(0, 4.5)
        ax2.set_yticks([])
        ax3.set_xticks([])

        ax.set_xlim(xrange[0], xrange[-1])
        ax.set_ylim(yrange[0], yrange[-1])
        ax3.set_ylim(yrange[0], yrange[-1])
        ax2.set_xlim(xrange[0], xrange[-1])

    except:
        ax.set_title("Error")

# =============================================================================
# Plot mini images of diagnostics
# =============================================================================

def plot_event_data(data, savepath, store=True, closeplot=True):
    """
    Plot the data from one event.
    """

    x_text = 0.21
    y_text_2 = 0.92
    y_text_1 = 0.85
    y_text_3 = 0.78

    fig, ax = plt.subplots(4,4, figsize=(19,10),)
    fig.subplots_adjust(hspace=0.23, wspace=0.23)

    if type(data) is not bool:

        # Plot BTV50 data
        _plot_figure_with_proj(ax[0,0], data['btv_50']['data_2d'])
        ax[0,0].set_title("BTV50", weight="bold",)
        ax[0,0].text(x_text,y_text_2,data['btv_50']['timestamp'], transform=ax[0,0].transAxes,  )
        ax[0,0].text(x_text,y_text_1,"dt: " + str(data['btv_50']['timediff']) + " [s]", transform=ax[0,0].transAxes,  )

        if data['btv_50']['timediff'] > 10 :
            ax[0,0].text(0.15,0.5,"Outdated Image!", transform=ax[0,0].transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['btv_50']['excep']:
            ax[0,0].text(0.15,0.3,"Except Error", transform=ax[0,0].transAxes, color="red", weight="bold", fontsize=12,)

        # Plot BTV53 data
        _plot_figure_with_proj(ax[0,1], data['btv_53']['data_2d'])
        ax[0,1].set_title("BTV53", weight="bold",)
        ax[0,1].text(x_text,y_text_2,data['btv_53']['timestamp'], transform=ax[0,1].transAxes,  )
        ax[0,1].text(x_text,y_text_1,"dt: " + str(data['btv_53']['timediff']) + " [s]", transform=ax[0,1].transAxes,  )

        if data['btv_53']['timediff'] > 10 :
            ax[0,1].text(0.15,0.5,"Outdated Image!", transform=ax[0,1].transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['btv_53']['excep']:
            ax[0,1].text(0.15,0.3,"Except Error", transform=ax[0,1].transAxes, color="red", weight="bold", fontsize=12,)   

        #Plot BTV54 data
        _plot_figure_with_proj(ax[0,2], data['btv_54']['data_2d'])
        ax[0,2].set_title("BTV54", weight="bold",)
        ax[0,2].text(x_text,y_text_2,data['btv_54']['timestamp'], transform=ax[0,2].transAxes,  )
        ax[0,2].text(x_text,y_text_1,"dt: " + str(data['btv_54']['timediff']) + " [s]", transform=ax[0,2].transAxes,  )

        if data['btv_54']['timediff'] > 10 :
            ax[0,2].text(0.15,0.5,"Outdated Image!", transform=ax[0,2].transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['btv_54']['excep']:
            ax[0,2].text(0.15,0.3,"Except Error", transform=ax[0,2].transAxes, color="red", weight="bold", fontsize=12,)   

        # Plot ExpVol01 data
        _plot_figure_with_proj(ax[0,3], data['exp_vol_1']['data_2d'])
        ax[0,3].set_title("ExpVol01", weight="bold",)
        ax[0,3].text(x_text,y_text_2,data['exp_vol_1']['timestamp'], transform=ax[0,3].transAxes,  )
        ax[0,3].text(x_text,y_text_1,"dt: " + str(data['exp_vol_1']['timediff']) + " [s]", transform=ax[0,3].transAxes,  )

        if data['exp_vol_1']['timediff'] > 10 :
            ax[0,3].text(0.15,0.5,"Outdated Image!", transform=ax[0,3].transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['exp_vol_1']['excep']:
             ax[0,3].text(0.15,0.3,"Except Error", transform=ax[0,3].transAxes, color="red", weight="bold", fontsize=12,)   
        
        # Plot BTV26 data
        _plot_figure_with_proj(ax[1,0], data['btv_26']['data_2d'])
        ax[1,0].set_title("BTV26", weight="bold",)
        ax[1,0].text(x_text,y_text_2,data['btv_26']['timestamp'], transform=ax[1,0].transAxes,  )
        ax[1,0].text(x_text,y_text_1,"dt: " + str(data['btv_26']['timediff']) + " [s]", transform=ax[1,0].transAxes,  )

        if data['btv_26']['timediff'] > 10 :
            ax[1,0].text(0.15,0.5,"Outdated Image!", transform=ax[1,0].transAxes, color="red", weight="bold", fontsize=12,)
        
        if data['btv_26']['excep']:
            ax[1,0].text(0.15,0.3,"Except Error", transform=ax[1,0].transAxes, color="red", weight="bold", fontsize=12,)

        # Plot IS1-Core data
        _plot_figure_with_proj(ax[1,1], data['is1_core']['data_2d'])
        ax[1,1].set_title("IS1-Core", weight="bold",)
        ax[1,1].text(x_text,y_text_2,data['is1_core']['timestamp'], transform=ax[1,1].transAxes,  )
        ax[1,1].text(x_text,y_text_1,"dt: " + str(data['is1_core']['timediff']) + " [s]", transform=ax[1,1].transAxes,  )

        if data['is1_core']['timediff'] > 10  :
            ax[1,1].text(0.15,0.5,"Outdated Image!", transform=ax[1,1].transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['is1_core']['excep']:
            ax[1,1].text(0.15,0.3,"Except Error", transform=ax[1,1].transAxes, color="red", weight="bold", fontsize=12,)


        # Plot IS1-Halo data
        _plot_figure_with_proj(ax[1,2], data['is1_halo']['data_2d'])
        ax[1,2].set_title("IS1-Halo", weight="bold",)
        ax[1,2].text(x_text,y_text_2,data['is1_halo']['timestamp'], transform=ax[1,2].transAxes,  )
        ax[1,2].text(x_text,y_text_1,"dt: " + str(data['is1_halo']['timediff']) + " [s]", transform=ax[1,2].transAxes,  )

        if data['is1_halo']['timediff'] > 10  :
            ax[1,2].text(0.15,0.5,"Outdated Image!", transform=ax[1,2].transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['is1_halo']['excep']:     
            ax[1,2].text(0.15,0.3,"Except Error", transform=ax[1,2].transAxes, color="red", weight="bold", fontsize= 12,)

        # PLot DMD 1
        _plot_figure_with_proj(ax[1,3], data['dmd_1']['data_2d'])
        ax[1,3].set_title("Plasma Light Basler", weight="bold",)
        ax[1,3].text(x_text,y_text_2,data['dmd_1']['timestamp'], transform=ax[1,3].transAxes,  )
        ax[1,3].text(x_text,y_text_1,"dt: " + str(data['dmd_1']['timediff']) + " [s]", transform=ax[1,3].transAxes,  )

        if data['dmd_1']['timediff'] > 10 :
            ax[1,3].text(0.15,0.5,"Outdated Image!", transform=ax[1,3].transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['dmd_1']['excep']:
            ax[1,3].text(0.15,0.3,"Except Error", transform=ax[1,3].transAxes, color="red", weight="bold", fontsize= 12, )

        # Plot DMD 2
        _plot_figure_with_proj(ax[2,0], data['dmd_2']['data_2d'])
        ax[2,0].set_title("CTR Screen", weight="bold",)
        ax[2,0].text(x_text,y_text_2,data['dmd_2']['timestamp'], transform=ax[2,0].transAxes,  )
        ax[2,0].text(x_text,y_text_1,"dt: " + str(data['dmd_2']['timediff']) + " [s]", transform=ax[2,0].transAxes,  )

        if data['dmd_2']['timediff'] > 10 :
            ax[2,0].text(0.15,0.5,"Outdated Image!", transform=ax[2,0].transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['dmd_2']['excep']:
            ax[2,0].text(0.15,0.3,"Except Error", transform=ax[2,0].transAxes, color="red", weight="bold", fontsize=12,)   

        # Plot MPP_STREAK
        _plot_figure_with_proj(ax[2,1], data['MPP_STREAK']['data_2d'])
        ax[2,1].set_title("MPP_STREAK", weight="bold",)
        ax[2,1].text(x_text,y_text_2,data['MPP_STREAK']['timestamp'], transform=ax[2,1].transAxes,  )
        ax[2,1].text(x_text,y_text_1,"dt: " + str(data['MPP_STREAK']['timediff']) + " [s]", transform=ax[2,1].transAxes,  )

        if data['MPP_STREAK']['timediff'] > 10 :
            ax[2,1].text(0.15,0.5,"Outdated Image!", transform=ax[2,1].transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['MPP_STREAK']['excep']:
            ax[2,1].text(0.15,0.3,"Except Error", transform=ax[2,1].transAxes, color="red", weight="bold", fontsize=12,)   

        # Plot BTV_STREAK
        _plot_figure_with_proj(ax[2,2], data['BTV_STREAK']['data_2d'])
        ax[2,2].set_title("BTV_STREAK", weight="bold",)
        ax[2,2].text(x_text,y_text_2,data['BTV_STREAK']['timestamp'], transform=ax[2,2].transAxes,  )
        ax[2,2].text(x_text,y_text_1,"dt: " + str(data['BTV_STREAK']['timediff']) + " [s]", transform=ax[2,2].transAxes,  )

        if data['BTV_STREAK']['timediff'] > 10 :
            ax[2,2].text(0.15,0.5,"Outdated Image!", transform=ax[2,2].transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['BTV_STREAK']['excep']:
            ax[2,2].text(0.15,0.3,"Except Error", transform=ax[2,2].transAxes, color="red", weight="bold", fontsize=12,)   
        

        # Plot IS2-Core data
        _plot_figure_with_proj(ax[2,3], data['is2_core']['data_2d'])
        ax[2,3].set_title("IS2-Core", weight="bold",)
        ax[2,3].text(x_text,y_text_2,data['is2_core']['timestamp'], transform=ax[2,3].transAxes, )
        ax[2,3].text(x_text,y_text_1,"dt: " + str(data['is2_core']['timediff']) + " [s]", transform=ax[2,3].transAxes,  )

        if data['is2_core']['timediff'] > 10  :
            ax[2,3].text(0.15,0.5,"Outdated Image!", transform=ax[2,3].transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['is2_core']['excep']:
            ax[2,3].text(0.15,0.3,"Except Error", transform=ax[2,3].transAxes, color="red", weight="bold", fontsize=12,)

        # Plot IS2-Halo data
        _plot_figure_with_proj(ax[3,0], data['is2_halo']['data_2d'])
        ax[3,0].set_title("IS2-Halo", weight="bold",)
        ax[3,0].text(x_text,y_text_2,data['is2_halo']['timestamp'], transform=ax[3,0].transAxes,  )
        ax[3,0].text(x_text,y_text_1,"dt: " + str(data['is2_halo']['timediff']) + " [s]", transform=ax[3,0].transAxes,  )

        if data['is2_halo']['timediff'] > 10 :
            ax[3,0].text(0.15,0.5,"Outdated Image!", transform=ax[3,0].transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['is2_halo']['excep']:
            ax[3,0].text(0.15,0.3,"Except Error", transform=ax[3,0].transAxes, color="red", weight="bold", fontsize=12,)


        # Plot Streak TT41 data
        _plot_figure_with_proj(ax[3,1], data['streak_2']['data_2d'])
        ax[3,1].set_title("Upstream Streak",  weight="bold",)
        ax[3,1].text(x_text,y_text_2,data['streak_2']['timestamp'], transform=ax[3,1].transAxes,  )
        ax[3,1].text(x_text,y_text_1,"dt: " + str(data['streak_2']['timediff']) + " [s]", transform=ax[3,1].transAxes,  )

        if data['streak_2']['timediff'] > 10 :
            ax[3,1].text(0.15,0.5,"Outdated Image!", transform=ax[3,1].transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['streak_2']['excep']:
            ax[3,1].text(0.15,0.3,"Except Error", transform=ax[3,1].transAxes, color="red", weight="bold", fontsize=12,)

        # Plot Streak XMPP data    
        _plot_figure_with_proj(ax[3,2], data['streak_1']['data_2d'])
        ax[3,2].set_title("Downstream Streak", weight="bold",)
        ax[3,2].text(x_text,y_text_2,data['streak_1']['timestamp'], transform=ax[3,2].transAxes,  )
        ax[3,2].text(x_text,y_text_1,"dt: " + str(data['streak_1']['timediff']) + " [s]", transform=ax[3,2].transAxes,  )

        if data['streak_1']['timediff'] > 10 :
            ax[3,2].text(0.15,0.5,"Outdated Image!", transform=ax[3,2].transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['streak_1']['excep']:
            ax[3,2].text(0.15,0.3,"Except Error", transform=ax[3,2].transAxes, color="red", weight="bold", fontsize=12,)

        # Plot Streak YMPP data    
        _plot_figure_with_proj(ax[3,3], data['streak_3']['data_2d'])
        ax[3,3].set_title("Downstream Y Streak", weight="bold",)
        ax[3,3].text(x_text,y_text_2,data['streak_3']['timestamp'], transform=ax[3,3].transAxes,  )
        ax[3,3].text(x_text,y_text_1,"dt: " + str(data['streak_3']['timediff']) + " [s]", transform=ax[3,3].transAxes,  )

        if data['streak_3']['timediff'] > 10 :
            ax[3,3].text(0.15,0.5,"Outdated Image!", transform=ax[3,3].transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['streak_3']['excep']:
            ax[3,3].text(0.15,0.3,"Except Error", transform=ax[3,3].transAxes, color="red", weight="bold", fontsize=12,)

#        # Plot SPECTRO 1
#        _plot_figure_with_proj(ax[3,3], data['spectro_1']['data_2d'])
#        ax[3,3].set_title("SPECTRO 1 (DPS light)", weight="bold",)
#        ax[3,3].text(x_text,y_text_2,data['spectro_1']['timestamp'], transform=ax[3,3].transAxes,  )
#        ax[3,3].text(x_text,y_text_1,"dt: " + str(data['spectro_1']['timediff']) + " [s]", transform=ax[1,3].transAxes,  )
#
#        if data['spectro_1']['timediff'] > 10 :
#            ax[3,3].text(0.15,0.5,"Outdated Image!", transform=ax[1,3].transAxes, color="red", weight="bold", fontsize=12,)
#            
#        if data['spectro_1']['excep']:
#            ax[3,3].text(0.15,0.3,"Except Error", transform=ax[1,3].transAxes, color="red", weight="bold", fontsize=12,)   

#        # Plot XUCL-SPECTRO
#        _plot_figure_with_proj(ax[3,2], data['spectro']['data_2d'])
#        ax[3,2].set_title("e- Spectrometer", weight="bold",)
#        ax[3,2].text(x_text,y_text_2,data['spectro']['timestamp'], transform=ax[3,2].transAxes,  )
#        ax[3,2].text(x_text,y_text_1,"dt: " + str(data['spectro']['timediff']) + " [s]", transform=ax[3,2].transAxes,  )
#
#        if data['spectro']['timediff'] > 10 :
#            ax[3,2].text(0.15,0.5,"Outdated Image!", transform=ax[3,2].transAxes, color="red", weight="bold", fontsize=12,)
#            
#        if data['spectro']['excep']:
#            ax[3,2].text(0.15,0.3,"Except Error", transform=ax[3,2].transAxes, color="red", weight="bold", fontsize=12,)

        # Plot plasma light        
#        ax[3,3].set_title("Plasma Light Diagnostics", weight="bold",)
#        try:
#            ax[3,3].text(x_text,y_text_2,data['plasma_light']['timestamp'], transform=ax[3,3].transAxes,  )
#            ax[3,3].text(x_text,y_text_1,"dt: " + str(data['plasma_light']['timediff']) + " [s]", transform=ax[3,3].transAxes,  )
#            ax[3,3].set_ylabel('counts')      
#            # ax[3,3].bar(np.linspace(0, np.size(data['plasma_light']['image']), num = np.size(data['plasma_light']['image']) ), data['plasma_light']['image'],1)
#            ax[3,3].hist( data['plasma_light']['image'], bins = 150) 
#            if data['plasma_light']['timediff'] > 10:
#                ax[3,3].text(0.15,0.5,"Outdated Image!", transform=ax[3,3].transAxes, color="red", weight="bold", fontsize=12,)
#
#            if data['plasma_light']['excep']:
#                ax[3,3].text(0.15,0.3,"Except Error", transform=ax[3,3].transAxes, color="red", weight="bold", fontsize=12,)              
#        except:
#            pass
        fig.suptitle("Event #: " + str(data['enumber']) + ". Timestamp: " + str(data['timestamp']) + ". Population: " + str(data['proton_pop_bctf']) + ". Time: " + str(data['time']), y=0.93, fontsize=12, weight="bold",)

    else:
        fig.suptitle("Error reading HDF5 file", y=0.93)

    if store: 
        print('saving to ', savepath)
        fig.savefig(savepath, bbox_inches = "tight")
    if closeplot:
        plt.close(fig)
        
        
def plot_proton_optics(data, savepath, closeplot=True, store=True):
    #print("PlotProtonOptics")
    fig = plt.figure(figsize=(10,4))

    gs = gridspec.GridSpec(1, 1, figure=fig,  hspace = 0.5,  wspace = 0.2)
    ax1 = plt.subplot(gs[0, 0]) # Plot
    
    x_text = 0.21
    y_text_2 = 0.92
    y_text_1 = 0.85
    y_text_3 = 0.78
    
    if type(data) is not bool:# Plot Proton Optics
          

        #print('BTV50 sigma: ', data['btv_50']['sigma'])
        #print('BTV53 sigma: ', data['btv_53']['sigma'])
        #print('IS1 sigma: ', data['is1_core']['sigma'])
        #print('IS2 sigma: ', data['is2_halo']['sigma'])

        btv50sigma =  data['btv_50']['sigma']
        btv53sigma =  data['btv_53']['sigma']
        is1sigma   =  data['is1_core']['sigma']
        is2sigma   =  data['is2_halo']['sigma']
        
        ##BTV350###BTV353###IS1###IS2
        #s_x = np.array([0.174,0.13,0.474,0.760]) *1e-3            
        #s_y = np.array([0.18,0.14,0.482,0.842])*1e-3
        try:
            s_x = np.array([btv50sigma['x'], btv53sigma['x'], is1sigma['x'], is2sigma['x']]) *1e-3            
            s_y = np.array([btv50sigma['y'], btv53sigma['y'], is1sigma['y'], is2sigma['y']]) *1e-3            
            z = np.array([-2.775,-1.2475,12.251,20.38])
            

            def env(z, s0, z0, em):
                return np.sqrt(s0**2 + (em/400)**2/s0**2*(z-z0)**2)
            popt_x, pcov_x = curve_fit(env,z, s_x ,maxfev=10000)
            #print(popt)
            popt_y, pcov_y = curve_fit(env,z, s_y ,maxfev=10000)
            #print('Y plane')
            #print('sigma* [mm], z* [m], em_N [um]')
            #print(popt)
	
            #%%
            zz=np.linspace(np.min(z)-1,np.max(z)+1) # z-vector for plotting
            
            ax1.axvline(0,label='Plasma entrance',linestyle=':',color='w')
            ax1.errorbar(z, s_x*1000,linestyle='None',marker='o',color='tab:blue')
            ax1.plot(zz, env(zz, *popt_x)*1000,label='H',color='tab:blue')
            ax1.set_ylabel('r [mm]')
            ax1.legend()
            
            ax1.errorbar(z, s_y*1000,linestyle='None',marker='o',color='tab:green')
            ax1.plot(zz, env(zz, *popt_y)*1000,label='V',color='tab:green')
            ax1.set_xlabel('z [m]')
            ax1.set_ylabel('r [mm]')
            ax1.axvline(0,linestyle=':',color='k')
            ax1.legend()
            
            ax1.set_title(r'H: $\sigma$*= '+str(np.round(popt_x[0]*1000,3))+' [um], z*= '+str(np.round(popt_x[1],1))+' [m], $\epsilon_N$= '+str(np.round(popt_x[2]*1e6,1))+' [mm mrad], \n V: $\sigma$*= '+str(np.round(popt_y[0]*1000,3))+' [um], z*= '+str(np.round(popt_y[1],1))+' [m], $\epsilon_N$ = '+str(np.round(popt_y[2]*1e6,1))+' [mm mrad]')
          
        except Exception as e:
            ax1.text(0.15,0.3,"Except Error: x sizes are "+str(np.round(btv50sigma['x'],3))+", "+str(np.round(btv53sigma['x'],3))
                     +", "+str(np.round(is1sigma['x'],3))+", "+str(np.round(is2sigma['x'],3)), transform=ax1.transAxes, color="red", weight="bold", fontsize=12,) 
            print(e)

    else:
        fig.suptitle("Error reading HDF5 file", y=1.02)

    if store: 
        fig.savefig(savepath, bbox_inches = "tight")
    if closeplot:
        plt.close(fig)

def plot_mini_imgs_laser(data, savepath, closeplot=True, store=True):
        
    fig = plt.figure(figsize=(19,4))
    gs = gridspec.GridSpec(1, 5, figure=fig,  hspace = 0.5,  wspace = 0.2)
    ax1 = plt.subplot(gs[0, 0]) # UV cathode
    ax2 = plt.subplot(gs[0, 1]) # IR laser room
    ax3 = plt.subplot(gs[0, 2]) # VLC 3
    ax4 = plt.subplot(gs[0, 3]) # VLC 4
    ax5 = plt.subplot(gs[0, 4]) # VLC 5
    
    x_text = 0.21
    y_text_2 = 0.92
    y_text_1 = 0.85
    y_text_3 = 0.78
    
    if type(data) is not bool:
    
        # Plot Laser 1 data
        _plot_figure_with_proj(ax1, data['laser_1']['data_2d'])
        ax1.set_title("UV Cathode", weight="bold",)
        ax1.text(x_text,y_text_2,data['laser_1']['timestamp'], transform=ax1.transAxes,  )
        ax1.text(x_text,y_text_1,"dt: " + str(data['laser_1']['timediff']) + " [s]", transform=ax1.transAxes,  )
    
        if data['laser_1']['timediff'] > 10 :
            ax1.text(0.15,0.5,"Outdated Image!", transform=ax1.transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['laser_1']['excep']:
            ax1.text(0.15,0.3,"Except Error", transform=ax1.transAxes, color="red", weight="bold", fontsize=12,)
    
        # Plot laser 2
        _plot_figure_with_proj(ax2, data['laser_2']['data_2d'])
        ax2.set_title("IR Laser Room", weight="bold",)
        ax2.text(x_text,y_text_2,data['laser_2']['timestamp'], transform=ax2.transAxes,  )
        ax2.text(x_text,y_text_1,"dt: " + str(data['laser_2']['timediff']) + " [s]", transform=ax2.transAxes,  )
    
        if data['laser_2']['timediff'] > 10 :
            ax2.text(0.15,0.5,"Outdated Image!", transform=ax2.transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['laser_2']['excep']:
            ax2.text(0.15,0.3,"Except Error", transform=ax2.transAxes, color="red", weight="bold", fontsize=12,)
    
        # Plot laser 3
        _plot_figure_with_proj(ax3, data['laser_3']['data_2d'])
        ax3.set_title("Virtual Line 3", weight="bold",)
        ax3.text(x_text,y_text_2,data['laser_3']['timestamp'], transform=ax3.transAxes,  )
        ax3.text(x_text,y_text_1,"dt: " + str(data['laser_3']['timediff']) + " [s]", transform=ax3.transAxes,  )
    
        if data['laser_3']['timediff'] > 10 :
            ax3.text(0.15,0.5,"Outdated Image!", transform=ax3.transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['laser_3']['excep']:
            ax3.text(0.15,0.3,"Except Error", transform=ax3.transAxes, color="red", weight="bold", fontsize=12,)
    
        if (data['laser_3']['max_hor_amp']< 500 and (data['laser_4']['max_hor_amp']>1000 or data['laser_5']['max_hor_amp']>1000)) or (data['laser_3']['max_ver_amp']< 500 and (data['laser_4']['max_ver_amp']>1000 or data['laser_5']['max_ver_amp']>1000)):
            ax3.text(0.15,0.5,"Wrong Shot!", transform=ax3.transAxes, color = "red", weight = "bold", fontsize=12,)
    
        # Plot laser 4
        _plot_figure_with_proj(ax4, data['laser_4']['data_2d'])
        ax4.set_title("Virtual Line 4",  weight="bold",)
        ax4.text(x_text,y_text_2,data['laser_4']['timestamp'], transform=ax4.transAxes,  )
        ax4.text(x_text,y_text_1,"dt: " + str(data['laser_4']['timediff']) + " [s]", transform=ax4.transAxes,  )
    
        if data['laser_4']['timediff'] > 10 :
            ax4.text(0.15,0.3,"Outdated Image!", transform=ax4.transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['laser_4']['excep']:
            ax4.text(0.15,0.3,"Except Error", transform=ax4.transAxes, color="red", weight="bold", fontsize=12,)
       
        if (data['laser_4']['max_hor_amp']< 500 and (data['laser_3']['max_hor_amp']>1000 or data['laser_5']['max_hor_amp']>1000)) or (data['laser_4']['max_ver_amp']< 500 and (data['laser_3']['max_ver_amp']>1000 or data['laser_5']['max_ver_amp']>1000)):
            ax4.text(0.15,0.5,"Wrong Shot!", transform=ax4.transAxes, color = "red", weight = "bold", fontsize=12,)
        
    
        # Plot laser 5
        _plot_figure_with_proj(ax5, data['laser_5']['data_2d'])
        ax5.set_title("Virtual Line 5", weight="bold",)
        ax5.text(x_text,y_text_2,data['laser_5']['timestamp'], transform=ax5.transAxes,  )
        ax5.text(x_text,y_text_1,"dt: " + str(data['laser_5']['timediff']) + " [s]", transform=ax5.transAxes,  )
    
        if data['laser_5']['timediff'] > 10 :
            ax5.text(0.15,0.5,"Outdated Image!", transform=ax5.transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['laser_5']['excep']:
            ax5.text(0.15,0.3,"Except Error", transform=ax5.transAxes, color="red", weight="bold", fontsize=12,)
        
        if (data['laser_5']['max_hor_amp']< 500 and (data['laser_3']['max_hor_amp']>1000 or data['laser_4']['max_hor_amp']>1000)) or (data['laser_5']['max_ver_amp']< 500 and (data['laser_3']['max_ver_amp']>1000 or data['laser_4']['max_ver_amp']>1000)):
            ax5.text(0.15,0.5,"Wrong Shot!", transform=ax5.transAxes, color = "red", weight = "bold", fontsize=12,) 
       
        fig.suptitle("Event #: " + str(data['enumber']) + ". Timestamp: " + str(data['timestamp']) + ". Population: " + str(data['proton_pop_bctf']) + ". Time: " + str(data['time']), y=1.02, fontsize=15, weight="bold",)

    else:
        fig.suptitle("Error reading HDF5 file", y=1.02)

    if store: 
        fig.savefig(savepath, bbox_inches = "tight")
    if closeplot:
        plt.close(fig)


def tri_subplot(gs, fig, string):
    ax1 = fig.add_subplot(gs[:, :2])
    ax2 = fig.add_subplot(gs[0, 2:])
    ax3 = fig.add_subplot(gs[1, 2:])   
    
    # Distinguish the spine info for the dataset plotted using color (when multiple
    # datsets are plotted on one axes)
    
    bbox_params = dict(boxstyle = 'square', edgecolor = '#8dd3c7', facecolor = 'black')
    
    ax1.set_xlabel("$x$, mm", bbox = bbox_params )  # the 2D plot always shows the raw data              
    ax1.set_ylabel("$y$, mm", bbox = bbox_params  )                 
    ax2.set_xlabel("Time")   
    ax2.set_ylabel("$x$, mm" + string, bbox = bbox_params )          
    ax3.set_xlabel("Time")                  
    ax3.set_ylabel("$y$, mm" + string, bbox = bbox_params )
    ax2.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=False, right=False, left=False, labelleft=False)

    return ax1, ax2, ax3


def plot_alignment(data, delta_curr, delta_avg, starttime, endtime, reftime, ref_times_all, store=False, savepath=[], closeplot=False,  ):

    fig = plt.figure(figsize=(15,12),) 
    
    gs0 = fig.add_gridspec(ncols = 1, nrows = 7, hspace = 0.2, height_ratios = [0.2,2,0.2,0.5,2.5,0.5,2.5])
    
    # Sub layout for individual diagnostic
    gs00 = gs0[0].subgridspec(2, 5, hspace = 0, wspace = 0.45) # Title of detector deltas
    gs01 = gs0[1].subgridspec(3, 15, hspace = 0, wspace = 0.45) # Detector deltas table
    gs02 = gs0[2].subgridspec(2, 5, hspace = 0, wspace = 0.45) # Title of alignment deltas
    gs03_t = gs0[3].subgridspec(2, 5, hspace = 0, wspace = 0.45) # Subtitle proton-electron
    gs03 = gs0[4].subgridspec(2, 5, hspace = 0, wspace = 0.45) # Proton-Electron alignment deltas 
    gs04_t = gs0[5].subgridspec(2, 5, hspace = 0, wspace = 0.45) # Subtitle proton-laser
    gs04 = gs0[6].subgridspec(2, 5, hspace = 0, wspace = 0.45) # Proton-Laser alignment deltas 
    
    """
    gs00: Title for the position deltas at each of the relevant detectors used 
    in the alignment calculations
    
    """
    ax01 = fig.add_subplot(gs00[:, :])
    ax01.text(x = 0, y = 0, s = f'Reference timestamp: {reftime}', ha = 'left', va = 'center', transform = ax01.transAxes, fontsize = 18, fontweight = 'bold', color = 'r')
    ax01.axis('off')
    
    """
    gs01: Position deltas
    
    """
    ax11 = fig.add_subplot(gs01[0, 1:14])
    ax11.axis('off')
    ax12 = fig.add_subplot(gs01[1, 1:14])
    ax12.axis('off')
    ax13 = fig.add_subplot(gs01[2, 1:14])
    ax13.axis('off')


    ax11.text(x = 0.22, y = 0.5, s = 'Proton', ha = 'left', va = 'bottom', transform = ax11.transAxes, fontsize = 14, weight = 'bold')
    ax11.text(x = 0.49, y = 0.5, s = 'Electron', ha = 'left', va = 'bottom', transform = ax11.transAxes, fontsize = 14, weight = 'bold')
    ax11.text(x = 0.76, y = 0.5, s = 'Laser', ha = 'left', va = 'bottom', transform = ax11.transAxes, fontsize = 14, weight = 'bold')


    # Table spines formatting
    ax11.axvline(x = 0.2,)
    ax11.axvline(x = 0.47,)
    ax11.axvline(x = 0.74,)
    
    ax12.axvline(x = 0.2,)
    ax12.axvline(x = 0.47,)
    ax12.axvline(x = 0.74,)
    
    ax13.axvline(x = 0.2,)
    ax13.axvline(x = 0.47, )
    ax13.axvline(x = 0.74, )
    
    ax13.set_ylim(0, 1)
    ax13.axhline(y = 1)

    ax12.text(x = 0, y = 0.5, s = 'Current Δ(pos.), mm', ha = 'left', va = 'center', transform = ax12.transAxes, fontsize = 13)

    
    try:
        ax12.text(x = 0.22, y = 0.5, s = 
                  (f'BPM 339: {delta_curr["proton"]["detector_1"]["x"]:.2f} $\it{{x}}$, {delta_curr["proton"]["detector_1"]["y"]:.2f} $\it{{y}}$\nBPM 411: {delta_curr["proton"]["detector_2"]["x"]:.2f} $\it{{x}}$, {delta_curr["proton"]["detector_2"]["y"]:.2f} $\it{{y}}$'), 
                  ha = 'left', va = 'center', transform = ax12.transAxes, fontsize = 13)
    except:
        ax12.text(x = 0.22, y = 0.5, s = f'BPM 339: error\nBPM 411: error', ha = 'left', va = 'center', transform = ax12.transAxes, fontsize = 13)

    try:
        ax12.text(x = 0.49, y = 0.5, s = 
                  (f'BPM 349: {delta_curr["electron"]["detector_1"]["x"]:.2f} $\it{{x}}$, {delta_curr["electron"]["detector_1"]["y"]:.2f} $\it{{y}}$\nBPM 351: {delta_curr["electron"]["detector_2"]["x"]:.2f} $\it{{x}}$, {delta_curr["eletron"]["detector_2"]["y"]:.2f} $\it{{y}}$'), 
                  ha = 'left', va = 'center', transform = ax12.transAxes, fontsize = 13)
    except:
        ax12.text(x = 0.49, y = 0.5, s = f'BPM 349: error\nBPM 351: error', ha = 'left', va = 'center', transform = ax12.transAxes, fontsize = 13)

    try:     
        ax12.text(x = 0.76, y = 0.5, s =
                  (f'VLC 3: {delta_curr["laser"]["detector_1"]["x"]:.2f} $\it{{x}}$, {delta_curr["laser"]["detector_1"]["y"]:.2f} $\it{{y}}$\nVLC 5: {delta_curr["laser"]["detector_2"]["x"]:.2f} $\it{{x}}$, {delta_curr["laser"]["detector_2"]["y"]:.2f} $\it{{y}}$'), 
                  ha = 'left', va = 'center', transform = ax12.transAxes, fontsize = 13)
    except:
        ax12.text(x = 0.76, y = 0.5, s = f'VLC 3: error\nVLC 5: error', ha = 'left', va = 'center', transform = ax12.transAxes, fontsize = 13)

    ax13.text(x = 0, y = 0.5, s = 'Average Δ(pos.), mm', ha = 'left', va = 'center', transform = ax13.transAxes, fontsize = 13)
    ax13.text(x = 0.22, y = 0.5, s = f'BPM 339: {delta_avg["proton"]["detector_1"]["x"]:.2f} $\it{{x}}$, {delta_avg["proton"]["detector_1"]["y"]:.2f} $\it{{y}}$ \nBPM 411: {delta_avg["proton"]["detector_2"]["x"]:.2f} $\it{{x}}$, {delta_avg["proton"]["detector_2"]["y"]:.2f} $\it{{y}}$',
              ha = 'left', va = 'center', transform = ax13.transAxes, fontsize = 13)
    ax13.text(x = 0.49, y = 0.5, s = f'BPM 349: {delta_avg["electron"]["detector_1"]["x"]:.2f} $\it{{x}}$, {delta_avg["electron"]["detector_1"]["y"]:.2f} $\it{{y}}$ \nBPM 351: {delta_avg["electron"]["detector_2"]["x"]:.2f} $\it{{x}}$, {delta_avg["electron"]["detector_2"]["y"]:.2f} $\it{{y}}$',
              ha = 'left', va = 'center', transform = ax13.transAxes, fontsize = 13)
    ax13.text(x = 0.76, y = 0.5, s = f'VLC 3: {delta_avg["laser"]["detector_1"]["x"]:.2f} $\it{{x}}$, {delta_avg["laser"]["detector_1"]["y"]:.2f} $\it{{y}}$ \nVLC 5: {delta_avg["laser"]["detector_2"]["x"]:.2f} $\it{{x}}$, {delta_avg["laser"]["detector_2"]["y"]:.2f} $\it{{y}}$',
              ha = 'left', va = 'center', transform = ax13.transAxes, fontsize = 13)


    """
    gs02: Title for the position deltas at the start of the plasma cell between
    the two specified beam species
    
    """
    ax21 = fig.add_subplot(gs02[:, :])
    # ax21.text(x = 0, y = 0, s = 'Δ(pos.) between beams at start of plasma cell',  ha = 'left', va = 'bottom', transform = ax21.transAxes, fontsize = 17, weight = 'bold')
    ax21.text(x = 0, y = 0, s = 'Beam alignment at start of plasma cell',  ha = 'left', va = 'bottom', transform = ax21.transAxes, fontsize = 17, weight = 'bold')
    ax21.axis('off')
    
    """
    gs03_t: Title for the position deltas at the start of the plasma cell between
    the two specified beam species
    
    """
    ax3_t1 = fig.add_subplot(gs03_t[0, :])
    ax3_t2 = fig.add_subplot(gs03_t[1, :1])
    ax3_t3 = fig.add_subplot(gs03_t[1, 1:])
    ax3_t1.text(x = 0, y = 0, s = 'Proton-Electron', fontsize = 15, weight = 'bold')
    ax3_t1.axis('off')
    ax3_t2.text(x = 0, y = 0, s = 'Current alignment', fontsize = 11, weight = 'bold')
    ax3_t2.axis('off')
    ax3_t3.text(x = 0, y = 0, s = 'Historical alignment', fontsize = 11, weight = 'bold')
    ax3_t3.axis('off')
    

    """
    gs03: Electron-Proton alignment timeline (start of plasma cell, position difference
    in x and y)
    
    """
    # ax11 = fig.add_subplot(gs00[0, :2])
    ax30 = fig.add_subplot(gs03[0, :1])
    ax30.axis('off')

    ax31= fig.add_subplot(gs03[1, :1])
    ax31.axis('off')

    ax32 = fig.add_subplot(gs03[0, 1:])
    ax33 = fig.add_subplot(gs03[1, 1:])
   
    x_pos = data['alignment_p_e']['upstream']['rel_pos']['x'][-1] 
    x_angle = data['alignment_p_e']['upstream']['angle']['x'][-1]
    y_pos = data['alignment_p_e']['upstream']['rel_pos']['y'][-1]
    y_angle = data['alignment_p_e']['upstream']['angle']['y'][-1]
   
    # Text
    ax30.text(x = 0, y = 0.5, s = f'Δ($pos._x$): {x_pos:.2f} mm', ha = 'left', va = 'bottom',  fontsize = 14,  )
    ax30.text(x = 0, y = 0.5, s = f'Δ($θ_x$): {x_angle:.2f} μrad', ha = 'left', va = 'top', fontsize = 14,   )
    ax31.text(x = 0, y = 0.5, s = f'Δ($pos._y$): {y_pos:.2f} mm', ha = 'left', va = 'bottom', fontsize = 14,)
    ax31.text(x = 0, y = 0.5, s = f'Δ($θ_y$): {y_angle:.2f} μrad', ha = 'left', va = 'top',  fontsize = 14, )

    
    ax32.set_xlabel("Time")   
    ax32.set_ylabel("Δ($pos._x$), mm")
    ax33.set_xlabel("Time")                  
    ax33.set_ylabel("Δ($pos._y$), mm") 

    
    # # Hide the ticks for the 1D x plot
    ax32.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=False, right=False, left=False, labelleft=False)

    # Plot the proton - electron alignment

    timedata = data['alignment_p_e']['time']


    if timedata.size> 1:

        xdata = data['alignment_p_e']['upstream']['rel_pos']['x']
        ax32.scatter(timedata[1:], xdata[1:])
    
        ydata = data['alignment_p_e']['upstream']['rel_pos']['y']
        ax33.scatter(timedata[1:], ydata[1:])
            
    ax32.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M')) # covert datetime format into 
    ax33.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M')) # covert datetime format into 
 
    ax32.set_xlim(starttime, endtime+timedelta(seconds=1))
    ax33.set_xlim(starttime, endtime+timedelta(seconds=1))
    ax32.set_ylim(-1, 1)
    ax33.set_ylim(-1, 1)
    
    
    
    # create markers for when reference time changed
    try:
        for i in ref_times_all:
            ax32.axvline(x = i, alpha = 0.3, color = 'red')
            ax33.axvline(x = i, alpha = 0.3, color = 'red')
    except:
        pass
    
    
    # Get zero at centre of y-axis as perfect alignment reference point
    yabs_max = abs(max(ax32.get_ylim(), key=abs))
    ax32.set_ylim(ymin=-yabs_max, ymax=yabs_max)
    ax32.axhline(y = 0, ls = 'dashed', alpha = 0.6)
    yabs_max = abs(max(ax33.get_ylim(), key=abs))
    ax33.set_ylim(ymin=-yabs_max, ymax=yabs_max)
    ax33.axhline(y = 0, ls = 'dashed', alpha = 0.6)

    
    """
    gs04_t: Title for the position deltas at the start of the plasma cell between
    the two specified beam species
    
    """
    ax4_t1 = fig.add_subplot(gs04_t[0, :])
    ax4_t2 = fig.add_subplot(gs04_t[1, :1])
    ax4_t3 = fig.add_subplot(gs04_t[1, 1:])

    ax4_t1.text(x = 0, y = 0, s = 'Proton-Laser', fontsize = 15, weight = 'bold')
    ax4_t1.axis('off')
    ax4_t2.text(x = 0, y = 0, s = 'Current alignment', fontsize = 11, weight = 'bold')
    ax4_t2.axis('off')
    ax4_t3.text(x = 0, y = 0, s = 'Historical alignment', fontsize = 11, weight = 'bold')
    ax4_t3.axis('off')
    
    
    
    """
    gs04: Proton-Laser alignment timeline (start of plasma cell, position difference
    in x and y)
    
    """
    
    # Plot the proton - laser alignment


    # ax21 = fig.add_subplot(gs01[0, :2])
    ax40= fig.add_subplot(gs04[0, :1])
    ax40.axis('off')

    ax41= fig.add_subplot(gs04[1, :1])
    ax41.axis('off')

    ax42 = fig.add_subplot(gs04[0, 1:])
    ax43 = fig.add_subplot(gs04[1, 1:])
    
    x_pos = data['alignment_p_l']['upstream']['rel_pos']['x'][-1]
    x_angle = data['alignment_p_l']['upstream']['angle']['x'][-1]
    y_pos = data['alignment_p_l']['upstream']['rel_pos']['y'][-1]
    y_angle = data['alignment_p_l']['upstream']['angle']['y'][-1]
    
    # Text
    ax40.text(x = 0, y = 0.5, s = f'Δ($pos._x$): {x_pos:.2f} mm', ha = 'left', va = 'bottom', transform = ax40.transAxes, fontsize = 14,  )
    ax40.text(x = 0, y = 0.5, s = f'Δ($θ_x$): {x_angle:.2f} μrad', ha = 'left', va = 'top', transform = ax40.transAxes, fontsize = 14,  )

    ax41.text(x = 0, y = 0.5, s = f'Δ($pos._y$): {y_pos:.2f} mm', ha = 'left', va = 'bottom', transform = ax41.transAxes, fontsize = 14,  )
    ax41.text(x = 0, y = 0.5, s = f'Δ($θ_y$): {y_angle:.2f} μrad', ha = 'left', va = 'top', transform = ax41.transAxes, fontsize = 14,  )
    ax42.set_xlabel("Time")   
    ax42.set_ylabel("Δ($pos._x$), mm")
               
    ax43.set_xlabel("Time")                  
    ax43.set_ylabel("Δ($pos._y$), mm") 
   
    ax42.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=False, right=False, left=False, labelleft=False)
    ax42.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M')) # covert datetime format into 
    ax43.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M')) # covert datetime format into 
 
    ax42.set_xlim(starttime, endtime+timedelta(seconds=1))
    ax43.set_xlim(starttime, endtime+timedelta(seconds=1))
    # ax42.set_ylim(-1, 1)
    # ax43.set_xlim(-1, 1)
    
    timedata = data['alignment_p_l']['time']
    
  
    if timedata.size> 1:
       
        xdata = data['alignment_p_l']['upstream']['rel_pos']['x']
        ax42.scatter(timedata[1:], xdata[1:])
        ydata = data['alignment_p_l']['upstream']['rel_pos']['y']
        ax43.scatter(timedata[1:], ydata[1:])
    
    # Get zero at centre of y-axis as perfect alignment reference point
    yabs_max = abs(max(ax42.get_ylim(), key=abs))
    ax42.set_ylim(ymin=-yabs_max, ymax=yabs_max)
    ax42.axhline(y = 0, ls = 'dashed', alpha = 0.6)
    yabs_max = abs(max(ax43.get_ylim(), key=abs))
    ax43.set_ylim(ymin=-yabs_max, ymax=yabs_max)
    ax43.axhline(y = 0, ls = 'dashed', alpha = 0.6)
    
    # create markers for when reference time changed
    try:
        for i in ref_times_all:
            ax42.axvline(x = i, alpha = 0.3, color = 'red')
            ax43.axvline(x = i, alpha = 0.3, color = 'red')
    except:
        pass
    
    if store:
        fig.savefig(savepath, bbox_inches = "tight")
    if closeplot:
        plt.close(fig)
    

# =============================================================================
# Plot timelines (raw data)
# =============================================================================

def plot_timeline_data_protons(data, starttime, endtime,  ref, ref_times_all, store=False, savepath=[], closeplot=False):
   
    """
    Plot the timeline data for proton BTVs.
    """

    fig = plt.figure(figsize=(13,13),) 

    gs0 = fig.add_gridspec(ncols = 1, nrows = 5, hspace = 0.25, height_ratios = [2,2,2,0.001,1.3])
    
    # Sub layout for individual diagnostic
    gs00 = gs0[0].subgridspec(2, 5, hspace = 0, wspace = 0.45)
    gs01 = gs0[1].subgridspec(2, 5, hspace = 0, wspace = 0.45)
    gs02 = gs0[2].subgridspec(2, 5, hspace = 0, wspace = 0.45)
    # gs04 = gs0[3].subgridspec(1, 5, hspace = 0, wspace = 0.4)
    gs03 = gs0[4].subgridspec(1, 5, hspace = 0, wspace = 0.4)
    
    # Make the tick parameters different to those
    bbox_params = dict(boxstyle = 'square', edgecolor = '#feffb3', facecolor = 'black')
    
    """
    BTV 54
    """  
    
    ax11, ax12, ax13 = tri_subplot(gs00, fig, '')

    plot_timeline_xy(data["btv_54"], data["btv_54"], ax11, ax12, ax13, "BTV 54", 1, 1, 
                     ref["btv_54"], ref_times_all, starttime = starttime, endtime = endtime)
    
    
    """
    IS1 Core
    """  
    
    ax21, ax22, ax23 = tri_subplot(gs01, fig, '')
    
#    # Plot the proton beam size (sigma)
#    
#    ax12_2 = ax12.twinx() 
#    ax13_2 = ax13.twinx()
#    time_data = data['is1_core']['time']
#    
#    
#    plot_timeline_x(data['is1_core']['sigma_x'], time_data, ax12_2,
#                    starttime, endtime, delta=1000000, bbox_params = bbox_params,
#                    color = '#feffb3', yax_label = '$\sigma_x$', leg_label = None, offset = 0.0)
#    plot_timeline_x(data['is1_core']['sigma_y'], time_data, ax13_2,
#                    starttime, endtime, delta=1000000, bbox_params = bbox_params,
#                    color = '#feffb3', yax_label = '$\sigma_y$',leg_label = None, offset = 0.0)
#    
    
    # # Plot beam size    
    # xdata = data['is1_core']['sigma_x']
    # ax12_2.set_ylabel('Beam Size', color='#feffb3')
    # ax12_2.scatter(data['is1_core']['time'][1:], xdata[1:], color='#feffb3', marker = 'x', s=12,)
    # ax12_2.tick_params(axis='y', labelcolor='#feffb3') 
    # ydata = data['is1_core']['sigma_y']

    # ax13_2.set_ylabel('Beam Size', color='#feffb3') 
    # ax13_2.scatter(data['is1_core']['time'][1:], ydata[1:], color='#feffb3', marker = 'x', s=12,)
    # ax13_2.tick_params(axis='y', labelcolor='#feffb3')   

  
    
    # Plot beam position
    plot_timeline_xy(data["is1_core"], data["is1_core"], ax21, ax22, ax23, "IS1 Core", 1, 1,
                     ref["is1_core"], ref_times_all, starttime = starttime, endtime = endtime) 

    """
    IS2 Core
    """  
    
    ax31, ax32, ax33 = tri_subplot(gs02, fig, '')

 #   # Plot the proton beam size (sigma)
 #
 #   ax22_2 = ax22.twinx() 
 #   ax23_2 = ax23.twinx()
 #   time_data = data['is2_core']['time']
 #   
 #   plot_timeline_x(data['is2_core']['sigma_x'], time_data, ax22_2,
 #                   starttime, endtime, delta=1000000, bbox_params = bbox_params,
 #                   color = '#feffb3', yax_label = '$\sigma_x$', leg_label = None, offset = 0.0)
 #   plot_timeline_x(data['is2_core']['sigma_y'], time_data, ax23_2,
 #                   starttime, endtime, delta=1000000, bbox_params = bbox_params,
 #                   color = '#feffb3', yax_label = '$\sigma_x$', leg_label = None, offset = 0.0)
    
    # # Plot beam size        
    # xdata = data['is2_core']['sigma_x']
    # ax22_2 = ax22.twinx()  # instantiate a second axes that shares the same x-axis
    # ax22_2.set_ylabel('Beam Size', color='#feffb3')
    # ax22_2.scatter(data['is2_core']['time'][1:], xdata[1:], color='#feffb3', marker = 'x', s=12,)
    # ax22_2.tick_params(axis='y', labelcolor='#feffb3')
    # ydata = data['is2_core']['sigma_y']
    # ax23_2 = ax23.twinx()
    # ax23_2.set_ylabel('Beam Size', color='#feffb3') 
    # ax23_2.scatter(data['is2_core']['time'][1:], ydata[1:], color='#feffb3', marker = 'x', s=12,)
    # ax23_2.tick_params(axis='y', labelcolor='#feffb3')
    
 

    
    # Plot beam position
    plot_timeline_xy(data["is2_core"], data["is2_core"], ax31, ax32, ax33, "IS2 Core", 1, 1, 
                     ref["is2_core"], ref_times_all, starttime = starttime, endtime = endtime)

    
    """
    Proton beam intensity
    """  

    ax41 = fig.add_subplot(gs03[:, :])
    ax41.set_title('Proton Bunch Population', fontsize = 14, weight = 'bold', loc = 'left')
    ax41_1 = ax41.twinx()

    # BCTF - what is going on here???
    
    bbox_params = dict(boxstyle = 'square', edgecolor = '#8dd3c7', facecolor = 'black')

    
    data_pop = [x for x in data['proton_pop_bctf']['pop'] if (np.isnan(x) == False) and x > 0.0]
    indices = [i for i, n in enumerate(data['proton_pop_bctf']['pop']) if (np.isnan(n) == False) and n > 0.0]
    data_time = [data['proton_pop_bctf']['time'][i] for i in indices]
    
    plot_timeline_x(data_pop, data_time, ax41,
                    starttime, endtime, delta=1000000, bbox_params = bbox_params,
                    color = '#8dd3c7', yax_label = 'TT41.BCTF.412340', offset = 0.0)
    # BCTFI
    
    bbox_params = dict(boxstyle = 'square', edgecolor = '#feffb3', facecolor = 'black')

    
    data_pop = data['proton_pop_bctfi']['charge']
    data_time = data['proton_pop_bctfi']['time']
    
    plot_timeline_x(data_pop, data_time, ax41_1,
                    starttime, endtime, delta=1000000, bbox_params = bbox_params,
                    color = '#feffb3', yax_label = 'TT40.BCTFI.400344', offset = 0.0)
   
    
    # ax41_1.tick_params(axis='y', labelcolor='#feffb3')
        
    # print(f'BCTF_time: {data_time[1:]}\n BCTF_pop: {data_pop[1:],}\n BCTFI_time: {data["proton_pop_bctfi"]["time"][1:]}\n BCTFI_pop: {data["proton_pop_bctfi"]["charge"][1:]}')

    if store:
        fig.savefig(savepath, bbox_inches = "tight")
    if closeplot:
        plt.close(fig)   

        
def plot_timeline_data_laser(data, starttime, endtime, ref, ref_times_all, store=False, savepath=[], closeplot=False):
    """
    Plot the timeline data for the laser cameras.
    """

    fig = plt.figure(figsize=(13,15),)

    gs0 = fig.add_gridspec(ncols = 1, nrows = 4, hspace = 0.18, height_ratios = [1,1,1,1])

    # Sub layout for individual diagnostic
    gs00 = gs0[0].subgridspec(2, 5, hspace = 0, wspace = 0.45)
    gs01 = gs0[1].subgridspec(2, 5, hspace = 0, wspace = 0.45)
    gs02 = gs0[2].subgridspec(2, 5, hspace = 0, wspace = 0.45)
    gs03 = gs0[3].subgridspec(1, 10, hspace = 0, wspace = 0.4)   
    
    """
    VLC 3
    """  
    
    ax11, ax12, ax13 = tri_subplot(gs00, fig, '')

    ax12.set_ylabel("x, mm", color = 'black', bbox = dict(boxstyle = 'square', edgecolor = '#8dd3c7', facecolor = 'black'))
    ax13.set_ylabel("y, mm", color = 'black', bbox = dict(boxstyle = 'square', edgecolor = '#8dd3c7', facecolor = 'black'))
    
    plot_timeline_xy(data["laser_3"], data["laser_3"], ax11, ax12, ax13, "VLC3",
                     1, 1, ref['laser_3'], ref_times_all, delta=0.5, starttime = starttime, endtime = endtime)

    """
    VLC 4
    """     
    
    ax21, ax22, ax23 = tri_subplot(gs01, fig, '')

    ax22.set_ylabel("x, mm", color = 'black', bbox = dict(boxstyle = 'square', edgecolor = '#8dd3c7', facecolor = 'black'))
    ax23.set_ylabel("y, mm", color = 'black', bbox = dict(boxstyle = 'square', edgecolor = '#8dd3c7', facecolor = 'black'))

    plot_timeline_xy(data["laser_4"], data["laser_4"], ax21, ax22, ax23, "VLC4",
                     1, 1, ref["laser_4"], ref_times_all, delta=0.5, starttime = starttime, endtime = endtime)

    """
    VLC 5
    """  
        
    ax31, ax32, ax33 = tri_subplot(gs02, fig, '')

    ax32.set_ylabel("x, mm", color = 'black', bbox = dict(boxstyle = 'square', edgecolor = '#8dd3c7', facecolor = 'black'))
    ax33.set_ylabel("y, mm", color = 'black', bbox = dict(boxstyle = 'square', edgecolor = '#8dd3c7', facecolor = 'black'))
    
    plot_timeline_xy(data["laser_5"], data["laser_5"], ax31, ax32, ax33, "VLC5",
                     1, 1, ref["laser_5"], ref_times_all, delta=0.5, starttime = starttime, endtime = endtime)
    
    """
    Energy Meters
    """  
    
    ax41 = fig.add_subplot(gs03[:, :9])
    ax42 = ax41.twinx()
    ax43 = ax41.twinx()
    ax44 = ax41.twinx()

    # Multiple axes formatting
    ax43.spines['right'].set_position(("axes", 1.05))
    ax44.spines['right'].set_position(("axes", 1.1))
    ax41.yaxis.label.set_color('w')
    ax44.yaxis.label.set_color('w')
    ax41.tick_params(axis='y', colors='#00FFFF')
    ax42.tick_params(axis='y', colors='#FFFF00')
    ax43.tick_params(axis='y', colors='#FF0000')
    ax44.tick_params(axis='y', colors='#00FF00')
    
    # Set labels
    ax41.set_xlabel("Time")
    ax41.set_ylabel("Energy, mJ")
    ax44.set_ylabel("Energy, mJ")

    # Format the time on x axis
    ax41.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    ax42.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    ax43.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    ax44.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
  
    # Plot the data for each of the 4 emeters
    c1 = ax41.scatter(data['emeter_02']['time'][1:], data['emeter_02']['energy'][1:], s=15, c = '#00FFFF', label = 'Laser Room')
    c2 = ax42.scatter(data['emeter_03']['time'][1:], data['emeter_03']['energy'][1:], s=15, c = '#FFFF00', label = 'Compressor')
    c3 = ax43.scatter(data['emeter_04']['time'][1:], data['emeter_04']['energy'][1:], s=15, c = '#FF0000', label = 'Merging Point')
    c4 = ax44.scatter(data['emeter_05']['time'][1:], data['emeter_05']['energy'][1:], s=15, c = '#00FF00', label = 'Virtual Cathode (UV)')

    ax41.text(0.2,0.9, 'Energy Meters', weight="bold", fontsize=12, transform=ax41.transAxes, ) 
    
    ax41.set_xlim(starttime, endtime+timedelta(seconds=1))
    ax41.legend(handles=[c1, c2, c3, c4], loc = 'upper left')

    if store:
        fig.savefig(savepath, bbox_inches = "tight")
    if closeplot:
        plt.close(fig)

 
def plot_timeline_bpm_protons(data, starttime, endtime, ref, ref_times_all, store = False, savepath=[], closeplot = False):

    """
    Plot the timeline data for proton BPMs
    """

    fig = plt.figure(figsize=(13,13),)
    
    gs0 = fig.add_gridspec(ncols = 1, nrows = 4, hspace = 0.18, height_ratios = [2,2,2,2])

    # Sub layout for individual diagnostic
    gs00 = gs0[0].subgridspec(2, 5, hspace = 0, wspace = 0.45)
    gs01 = gs0[1].subgridspec(2, 5, hspace = 0, wspace = 0.45)
    gs02 = gs0[2].subgridspec(2, 5, hspace = 0, wspace = 0.45)
    gs03 = gs0[3].subgridspec(2, 5, hspace = 0, wspace = 0.45)

    """
    BPM Proton 1
    """  

    ax11, ax12, ax13 = tri_subplot(gs00, fig, '')

    plot_timeline_xy(data["bpm_proton_1"], data["bpm_proton_1"], ax11, ax12, ax13,
                     "TT41.BPM.412311", 1, 1, ref["bpm_proton_1"], ref_times_all, 
                     starttime = starttime, endtime = endtime, delta = 0.1)
    
    """
    BPM Proton 3
    """ 

    ax21, ax22, ax23 = tri_subplot(gs01, fig, '')
    
    plot_timeline_xy(data["bpm_proton_3"], data["bpm_proton_3"], ax21, ax22, ax23, 
                     "TT41.BPM.412339", 1, 1, ref["bpm_proton_3"], ref_times_all,
                     starttime = starttime, endtime = endtime, delta = 0.1) 
    
    """
    BPM Proton 4
    """ 
    
    ax31, ax32, ax33 = tri_subplot(gs02, fig, '')
    
    plot_timeline_xy(data["bpm_proton_4"], data["bpm_proton_4"], ax31, ax32, ax33,
                     "TT41.BPM.412352", 1, 1,  ref["bpm_proton_4"], ref_times_all, 
                     starttime = starttime, endtime = endtime, delta = 0.1)

    """
    BPM Proton 5
    """ 

    ax41, ax42, ax43 = tri_subplot(gs03, fig, '')
    
    plot_timeline_xy(data["bpm_proton_5"], data["bpm_proton_5"], ax41, ax42, ax43, 
                     "TT41.BPM.412425", 1, 1, ref["bpm_proton_5"], ref_times_all,
                     starttime = starttime, endtime = endtime, delta = 0.1)

   
    if store:
        fig.savefig(savepath, bbox_inches = "tight")
    if closeplot:
        plt.close(fig)    

def plot_timeline_bpm_electrons(data, starttime, endtime, ref, ref_times_all, store = False, savepath=[], closeplot = False):

    """
    Plot the timeline data for electron BPMs
    """
    fig = plt.figure(figsize=(13,7),)
    
    gs0 = fig.add_gridspec(ncols = 1, nrows = 2, hspace = 0.18, height_ratios = [2,2])

    # Sub layout for individual diagnostic
    gs00 = gs0[0].subgridspec(2, 5, hspace = 0, wspace = 0.45)
    gs01 = gs0[1].subgridspec(2, 5, hspace = 0, wspace = 0.45)


    """
    BPM Electron 1
    """  
    ax11, ax12, ax13 = tri_subplot(gs00, fig, '')

    plot_timeline_xy(data["bpm_electron_1"], data["bpm_electron_1"], ax11, ax12, ax13, 
                     "TT41.BPM.412349", 1, 1, ref["bpm_electron_1"], ref_times_all, 
                     starttime = starttime, endtime = endtime, delta = 0.1)

    # ax12.set_ylim(-0.1,+0.1)
    # ax13.set_ylim(-0.1,+0.1)

    """
    BPM Electron 2
    """  

    ax21, ax22, ax23 = tri_subplot(gs01, fig, '')

    plot_timeline_xy(data["bpm_electron_2"], data["bpm_electron_2"], ax21, ax22, ax23,
                     "TT41.BPM.412351", 1, 1, ref["bpm_electron_2"], ref_times_all,
                     starttime = starttime, endtime = endtime, delta = 0.1)
    
    # ax12.set_ylim(-0.1,+0.1)
    # ax13.set_ylim(-0.1,+0.1)

    if store:
        fig.savefig(savepath, bbox_inches = "tight")
    if closeplot:
        plt.close(fig)

def plot_timeline_plasma(data, starttime, endtime, ref, ref_times_all, store = False, savepath=[], closeplot = False):


    """
    Plot the timeline average for plasma diagnostics
    """

    
    fig = plt.figure(figsize=(10,3),)
    fig.subplots_adjust(hspace=0.4, wspace=0.4)
    ax2 = plt.subplot2grid((2, 10), (0, 0), rowspan=2, colspan=10,)
    

    
    time_data = data["plasma_light"]['time']
    

    plot_timeline_x(data["plasma_light"]['upstream'], time_data,  ax2,  
                    starttime, endtime, color = 'turquoise', 
                    yax_label = "Integrated counts",  leg_label = "Upstream")
    plot_timeline_x(data["plasma_light"]['downstream'],time_data,  ax2, 
                    starttime, endtime, color = 'm',
                    yax_label = "Integrated counts",leg_label = "Downstream")
   
    
    ax2.legend(loc = 'best')
    fig.suptitle('Plasma Light Diagnostics', x=0.24, y = 0.95, weight="bold", fontsize=12,)

    if store:
        fig.savefig(savepath, bbox_inches = "tight")
    if closeplot:
        plt.close(fig)

        
# =============================================================================
# Plot timelines (averaged data)        
# =============================================================================
        

def plot_timeline_average_protons(rawdata, avgdata, starttime, endtime, ref, ref_times_all, store=False, savepath=[], closeplot=False):
    """
    Plot the timeline average data for BTVs.
    """   
        
    fig = plt.figure(figsize=(13,13),) 

    gs0 = fig.add_gridspec(ncols = 1, nrows = 5, hspace = 0.25, height_ratios = [2,2,2,0.001,1.3])
    
    # Sub layout for individual diagnostic
    gs00 = gs0[0].subgridspec(2, 5, hspace = 0, wspace = 0.45)
    gs01 = gs0[1].subgridspec(2, 5, hspace = 0, wspace = 0.45)
    gs02 = gs0[2].subgridspec(2, 5, hspace = 0, wspace = 0.45)
    # gs04 = gs0[3].subgridspec(1, 5, hspace = 0, wspace = 0.4)
    gs03 = gs0[4].subgridspec(1, 5, hspace = 0, wspace = 0.4)
    
    """
    IS1 Core
    """  
    
    ax11, ax12, ax13 = tri_subplot(gs00, fig, ' (avg.)')
    
    # Plot beam size (averaged data)  
    xdata = avgdata['is1_core']['sigma_x']
    ax12_2 = ax12.twinx()  # instantiate a second axes that shares the same x-axis
    ax12_2.set_ylabel('Beam Size', color='#feffb3')
    ax12_2.scatter(avgdata['is1_core']['time'][1:], xdata[1:], color='#feffb3', marker = 'x', s=12,)
    ax12_2.tick_params(axis='y', labelcolor='#feffb3') 
    ydata = avgdata['is1_core']['sigma_y']
    ax13_2 = ax13.twinx()
    ax13_2.set_ylabel('Beam Size', color='#feffb3') 
    ax13_2.scatter(avgdata['is1_core']['time'][1:], ydata[1:], color='#feffb3', marker = 'x', s=12,)
    ax13_2.tick_params(axis='y', labelcolor='#feffb3')   
    
    # Plot beam position (x-y plot shows raw data, 1D plots show averaged data)
    plot_timeline_xy(rawdata["is1_core"], avgdata["is1_core"], ax11, ax12, ax13, "IS1 Core", 1, 1,
                     ref["is1_core"], ref_times_all, starttime = starttime, endtime = endtime) 

    """
    IS2 Core
    """  
    
    ax21, ax22, ax23 = tri_subplot(gs01, fig, ' (avg.)')

    # Plot beam size (averaged data)   
    xdata = avgdata['is2_core']['sigma_x']
    ax22_2 = ax22.twinx()  # instantiate a second axes that shares the same x-axis
    ax22_2.set_ylabel('Beam Size', color='#feffb3')
    ax22_2.scatter(avgdata['is2_core']['time'][1:], xdata[1:], color='#feffb3', marker = 'x', s=12,)
    ax22_2.tick_params(axis='y', labelcolor='#feffb3')
    ydata = avgdata['is2_core']['sigma_y']
    ax23_2 = ax23.twinx()
    ax23_2.set_ylabel('Beam Size', color='#feffb3') 
    ax23_2.scatter(avgdata['is2_core']['time'][1:], ydata[1:], color='#feffb3', marker = 'x', s=12,)
    ax23_2.tick_params(axis='y', labelcolor='#feffb3')
    
    
    
    # Plot beam position
    plot_timeline_xy(rawdata["is2_core"], avgdata["is2_core"], ax11, ax12, ax13, "IS1 Core", 1, 1,
                     ref["is2_core"], ref_times_all, starttime = starttime, endtime = endtime) 



    """
    BTV 54
    """  
    
    ax31, ax32, ax33 = tri_subplot(gs02, fig, ' (avg.)')
    
    plot_timeline_xy(rawdata["btv_54"], avgdata["btv_54"], ax31, ax32, ax33, "BTV 54", 1, 1, 
                     ref["btv_54"], ref_times_all, starttime = starttime, endtime = endtime)
    
    
    
    
    """
    Proton beam intensity
    """  
    
    ax41 = fig.add_subplot(gs03[:, :])
    ax41.set_title('Proton Bunch Population', fontsize = 14, weight = 'bold', loc = 'left')

    # BCTF
    data_pop = [x for x in avgdata['proton_pop_bctf']['pop'] if (np.isnan(x) == False) and x > 0.0]
    indices = [i for i, n in enumerate(avgdata['proton_pop_bctf']['pop']) if (np.isnan(n) == False) and n > 0.0]
    data_time = [avgdata['proton_pop_bctf']['time'][i] for i in indices]
   
    ax41.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    ax41.scatter(data_time[1:], data_pop[1:], s=15,)
    ax41.set_xlim(starttime, endtime)
    ax41.set_ylabel("TT41.BCTF.412340")
    ax41.set_xlabel("Time")
    
    # BCTFI
    ax41_1 = ax41.twinx()
    ax41_1.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    ax41_1.scatter(avgdata['proton_pop_bctfi']['time'][1:], avgdata['proton_pop_bctfi']['charge'][1:], s = 15, c = '#feffb3')
    ax41_1.set_xlim(starttime, endtime)
    ax41_1.set_ylabel("TT40.BCTFI.400344", color = '#feffb3')
    ax41_1.tick_params(axis='y', labelcolor='#feffb3')
    
    
    
    ax41.set_title('Proton bunch no. charges')
    if store:
        fig.savefig(savepath, bbox_inches = "tight")
    if closeplot:
        plt.close(fig)        

def plot_timeline_average_laser(rawdata, avgdata, starttime, endtime, ref, ref_times_all, store=False, savepath=[], closeplot=False):
   
      
    """
    Plot the timeline average for the laser cameras.
    """      
        
    fig = plt.figure(figsize=(13,15),)

    gs0 = fig.add_gridspec(ncols = 1, nrows = 4, hspace = 0.18, height_ratios = [1,1,1,1])

    # Sub layout for individual diagnostic
    gs00 = gs0[0].subgridspec(2, 5, hspace = 0, wspace = 0.45)
    gs01 = gs0[1].subgridspec(2, 5, hspace = 0, wspace = 0.45)
    gs02 = gs0[2].subgridspec(2, 5, hspace = 0, wspace = 0.45)
    gs03 = gs0[3].subgridspec(1, 10, hspace = 0, wspace = 0.4)   
    
    """
    VLC 3
    """  
    
    ax11, ax12, ax13 = tri_subplot(gs00, fig, ' (avg.)')

    ax12.set_ylabel("x, mm", color = 'black', bbox = dict(boxstyle = 'square', facecolor = '#8dd3c7'))
    ax13.set_ylabel("y, mm", color = 'black', bbox = dict(boxstyle = 'square', facecolor = '#8dd3c7'))
    
    plot_timeline_xy(rawdata["laser_3"], avgdata["laser_3"], ax11, ax12, ax13, "03TT41.CAM3",
                     1, 1, ref['laser_3'], ref_times_all, delta=4, starttime = starttime, endtime = endtime)
    
    
    """
    VLC 4
    """     
    
    ax21, ax22, ax23 = tri_subplot(gs01, fig, ' (avg.)')

    ax22.set_ylabel("x, mm", color = 'black', bbox = dict(boxstyle = 'square', facecolor = '#8dd3c7'))
    ax23.set_ylabel("y, mm", color = 'black', bbox = dict(boxstyle = 'square', facecolor = '#8dd3c7'))

    plot_timeline_xy(rawdata["laser_4"], avgdata["laser_4"], ax21, ax22, ax23, "04TT41.CAM4",
                     1, 1, ref['laser_4'], ref_times_all, delta=4, starttime = starttime, endtime = endtime)
    
    
    """
    VLC 5
    """  
        
    ax31, ax32, ax33 = tri_subplot(gs02, fig, ' (avg.)')

    ax32.set_ylabel("x, mm", color = 'black', bbox = dict(boxstyle = 'square', facecolor = '#8dd3c7'))
    ax33.set_ylabel("y, mm", color = 'black', bbox = dict(boxstyle = 'square', facecolor = '#8dd3c7'))
    
    plot_timeline_xy(rawdata["laser_5"], avgdata["laser_5"], ax31, ax32, ax33, "05TT41.CAM5",
                     1, 1, ref['laser_5'], ref_times_all, delta=4, starttime = starttime, endtime = endtime)
    
    
    """
    Energy Meters
    """  
    
    ax41 = fig.add_subplot(gs03[:, :9])
    ax42 = ax41.twinx()
    ax43 = ax41.twinx()
    ax44 = ax41.twinx()

    # Multiple axes formatting
    ax43.spines['right'].set_position(("axes", 1.05))
    ax44.spines['right'].set_position(("axes", 1.1))
    ax41.yaxis.label.set_color('w')
    ax44.yaxis.label.set_color('w')
    ax41.tick_params(axis='y', colors='#00FFFF')
    ax42.tick_params(axis='y', colors='#FFFF00')
    ax43.tick_params(axis='y', colors='#FF0000')
    ax44.tick_params(axis='y', colors='#00FF00')
    
    # Set labels
    ax41.set_xlabel("Time")
    ax41.set_ylabel("Energy, mJ")
    ax44.set_ylabel("Energy, mJ")

    # Set formatting of x axis as time
    ax41.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    ax42.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    ax43.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    ax44.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
  
    # Plot the raw data of each of the 4 emeters
    c1 = ax41.scatter(rawdata['EMETER_02']['time'][1:], rawdata['EMETER_02']['energy'][1:], s=15, c = '#00FFFF', label = 'Laser Room')
    c2 = ax42.scatter(rawdata['EMETER_03']['time'][1:], rawdata['EMETER_03']['energy'][1:], s=15, c = '#FFFF00', label = 'Compressor')
    c3 = ax43.scatter(rawdata['EMETER_04']['time'][1:], rawdata['EMETER_04']['energy'][1:], s=15, c = '#FF0000', label = 'Merging Point')
    c4 = ax44.scatter(rawdata['EMETER_05']['time'][1:], rawdata['EMETER_05']['energy'][1:], s=15, c = '#00FF00', label = 'Virtual Cathode (UV)')

    ax41.text(0.2,0.9, 'Energy Meters', weight="bold", fontsize=12, transform=ax41.transAxes, ) 
    
    ax41.set_xlim(starttime, endtime)
    ax41.legend(handles=[c1, c2, c3, c4], loc = 'upper left')

    if store:
        fig.savefig(savepath, bbox_inches = "tight")
    if closeplot:
        plt.close(fig)

def plot_timeline_average_bpm_protons(rawdata, avgdata, starttime, endtime, ref, ref_times_all, store=False, savepath=[], closeplot=False):
    """
    Plot the timeline average for proton BPMs
    """

    fig = plt.figure(figsize=(13,13),)        
    gs0 = fig.add_gridspec(ncols = 1, nrows = 4, hspace = 0.18, height_ratios = [2,2,2,2])

    # Sub layout for individual diagnostic
    gs00 = gs0[0].subgridspec(2, 5, hspace = 0, wspace = 0.45)
    gs01 = gs0[1].subgridspec(2, 5, hspace = 0, wspace = 0.45)
    gs02 = gs0[2].subgridspec(2, 5, hspace = 0, wspace = 0.45)
    gs03 = gs0[3].subgridspec(2, 5, hspace = 0, wspace = 0.45)

    """
    BPM Proton 1
    """  

    ax11, ax12, ax13 = tri_subplot(gs00, fig, ' (avg.)')
    plot_timeline_xy(rawdata["bpm_proton_1"], avgdata["bpm_proton_1"], ax11, ax12, ax13,
                     "TT41.BPM.412311", 1, 1, ref["bpm_proton_1"], ref_times_all, 
                     starttime = starttime, endtime = endtime, delta = 0.1)
    """
    BPM Proton 3
    """ 

    ax21, ax22, ax23 = tri_subplot(gs01, fig, ' (avg.)') 
    plot_timeline_xy(rawdata["bpm_proton_3"], avgdata["bpm_proton_3"], ax21, ax22, ax23, 
                     "TT41.BPM.412339", 1, 1, ref["bpm_proton_3"], ref_times_all,
                     starttime = starttime, endtime = endtime, delta = 0.1) 
    """
    BPM Proton 4
    """ 
    
    ax31, ax32, ax33 = tri_subplot(gs02, fig, ' (avg.)')
    plot_timeline_xy(rawdata["bpm_proton_4"], avgdata["bpm_proton_4"], ax31, ax32, ax33,
                     "TT41.BPM.412352", 1, 1,  ref["bpm_proton_4"], ref_times_all, 
                     starttime = starttime, endtime = endtime, delta = 0.1)
    """
    BPM Proton 5
    """ 

    ax41, ax42, ax43 = tri_subplot(gs03, fig, ' (avg.)')
    plot_timeline_xy(rawdata["bpm_proton_5"], avgdata["bpm_proton_5"], ax41, ax42, ax43, 
                     "TT41.BPM.412425", 1, 1, ref["bpm_proton_5"], ref_times_all,
                     starttime = starttime, endtime = endtime, delta = 0.5)


    if store:
        fig.savefig(savepath, bbox_inches = "tight")
    if closeplot:
        plt.close(fig)  
    

def plot_timeline_average_bpm_electrons(rawdata, avgdata, starttime, endtime, ref, ref_times_all, store=False, savepath=[], closeplot=False):

    """
    Plot the timeline average for electron BPMs
    """
    
    fig = plt.figure(figsize=(13,7),)
    
    gs0 = fig.add_gridspec(ncols = 1, nrows = 2, hspace = 0.18, height_ratios = [2,2])

    # Sub layout for individual diagnostic
    gs00 = gs0[0].subgridspec(2, 5, hspace = 0, wspace = 0.45)
    gs01 = gs0[1].subgridspec(2, 5, hspace = 0, wspace = 0.45)


    """
    BPM Electron 1
    """  
    ax11, ax12, ax13 = tri_subplot(gs00, fig, ' (avg.)')
    plot_timeline_xy(rawdata["bpm_electron_1"], avgdata["bpm_electron_1"], ax11, ax12, ax13, 
                     "TT41.BPM.412349", 1, 1, ref["bpm_electron_1"], ref_times_all, 
                     starttime = starttime, endtime = endtime, delta = 0.1)




    # ax12.set_ylim(-0.1,+0.1)
    # ax13.set_ylim(-0.1,+0.1)

    """
    BPM Electron 2
    """  

    ax21, ax22, ax23 = tri_subplot(gs01, fig, ' (avg.)')
    plot_timeline_xy(rawdata["bpm_electron_2"], avgdata["bpm_electron_2"], ax21, ax22, ax23,
                     "TT41.BPM.412351", 1, 1, ref["bpm_electron_2"], ref_times_all,
                     starttime = starttime, endtime = endtime, delta = 0.1)
    # ax12.set_ylim(-0.1,+0.1)
    # ax13.set_ylim(-0.1,+0.1)

    if store:
        fig.savefig(savepath, bbox_inches = "tight")
    if closeplot:
        plt.close(fig)

def plot_timeline_average_plasma(rawdata, avgdata, starttime, endtime, ref, ref_times_all, store=False, savepath=[], closeplot=False):
    """
    Plot the timeline average for plasma diagnostics
    """

    
    fig = plt.figure(figsize=(10,3),)
    fig.subplots_adjust(hspace=0.4, wspace=0.4)
    
    ax2 = plt.subplot2grid((2, 10), (0, 0), rowspan=2, colspan=10,)
    
    time_data = avgdata["plasma_light"]['time']
    # Plot the upstream plasma light
    plot_timeline_x(avgdata["plasma_light"]['upstream'], time_data, ax2,
                    starttime, endtime, color = 'turquoise', yax_label = "Integrated Counts",
                    leg_label = "Upstream")
    # Plot the donwstream plasma light
    plot_timeline_x(avgdata["plasma_light"]['downstream'], time_data, ax2, 
                     starttime, endtime, color = 'm', yax_label = "Integrated Counts", 
                    leg_label = "Downstream")

    ax2.legend(loc = 'best')
    fig.suptitle('Plasma Light Diagnostics', x=0.24, y = 0.95, weight="bold", fontsize=12,)


    if store:
        fig.savefig(savepath, bbox_inches = "tight")
    if closeplot:
        plt.close(fig)
        
        
# =============================================================================
# get BTVs        
# =============================================================================
        

def get_btv_50_analogue(file, file_timestamp):
    """
    Access and convert the data from TT41.BTV.412350 to a dictionary.
    """

    try:
        image = file["/AwakeEventData/TT41.BTV.412350/Image/"]
        nx = image["imagePositionSet1"][0].shape[0]
        ny = image["imagePositionSet2"][0].shape[0]
        dt_2d = image["imageSet"][0].reshape(ny, nx)
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data_2d' : dt_2d, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }


def get_btv_50_pxi(file, file_timestamp):
    """
    Access and convert the data from AWAKECAM09 to a dictionary.
    """

    try:
        image = file["/AwakeEventData/BOVWA.09TCC4.AWAKECAM09/ExtractionImage"]
        dt = image['imageRawData'][:]
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
        dt = _rebin(dt, [i // 4 for i in dt.shape])
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 
        
    return {'data_2d' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }


def get_btv_50_digi(file, file_timestamp):
    """
    Access and convert the data from TT41.BTV.412350.DigiCam to a dictionary.
    """

    try:
        image = file["/AwakeEventData/TT41.BTV.412350.DigiCam/ExtractionImage/"]
        dt = image["image2D"][:]
        dt_2d = _rebin(dt, [i // PIXELS['btv_50'][2] for i in dt.shape]) # Specify rebin factor here
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt_2d = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"    
    
    # Centroid extraction
    if (np.shape(dt_2d) != (1,0)):
        params =  def_centroid(dt_2d[100:200, 150:250])
        centr_params = params[0] 
        sigma_params = params[1]


        if np.prod(np.invert(np.isnan(centr_params))):
            x_pixel = PIXELS['btv_50'][0]*PIXELS['btv_50'][2]
            y_pixel = PIXELS['btv_50'][1]*PIXELS['btv_50'][2]  
            dt_1d = (centr_params[0] * x_pixel, centr_params[1] * y_pixel)  
            sig = (sigma_params[0] * x_pixel, sigma_params[1] * y_pixel)
        else:
            dt_1d = (math.nan, math.nan)    
            sig = (math.nan, math.nan)     
    else:
        dt_1d = (math.nan, math.nan)    
        sig = (math.nan, math.nan)
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 
    
 

    return {'data_2d' : dt_2d, 'data_1d' : {'x' : dt_1d[0], 'y' : dt_1d[1]},
           'sigma' : {'x' : sig[0], 'y' : sig[1]},
            'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }




def get_btv_53_analogue(file, file_timestamp):
    """
    Access and convert the data from TT41.BTV.412353 to a dictionary.
    """

    try:
        image = file["/AwakeEventData/TT41.BTV.412353/Image/"]
        nx = image["imagePositionSet1"][0].shape[0]
        ny = image["imagePositionSet2"][0].shape[0]
        dt = image["imageSet"][0].reshape(ny, nx)
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data_2d' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }
        

def get_btv_53_digi(file, file_timestamp):
    """
    Access and convert the data from TT41.BTV.412353.DigiCam to a dictionary.
    """

    try:
        image = file["/AwakeEventData/TT41.BTV.412353.DigiCam/ExtractionImage/"]
        dt = image["image2D"][:]
        dt_2d = _rebin(dt, [i // PIXELS['btv_53'][2] for i in dt.shape]) # Specify rebin factor here
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt_2d = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"    
    
    # Centroid extraction
    if (np.shape(dt_2d) != (1,0)):
        params =  def_centroid(dt_2d[100:200, 150:250])
        centr_params = params[0] 
        sigma_params = params[1]


        if np.prod(np.invert(np.isnan(centr_params))):
            x_pixel = PIXELS['btv_53'][0]*PIXELS['btv_53'][2]
            y_pixel = PIXELS['btv_53'][1]*PIXELS['btv_53'][2]  
            dt_1d = (centr_params[0] * x_pixel, centr_params[1] * y_pixel)  
            sig = (sigma_params[0] * x_pixel, sigma_params[1] * y_pixel)
        else:
            dt_1d = (math.nan, math.nan)    
            sig = (math.nan, math.nan)     
    else:
        dt_1d = (math.nan, math.nan)    
        sig = (math.nan, math.nan)
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data_2d' : dt_2d, 'data_1d' : {'x' : dt_1d[0], 'y' : dt_1d[1]},
           'sigma' : {'x' : sig[0], 'y' : sig[1]},
            'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }

def get_btv_26_analogue(file, file_timestamp):
    """
    Access and convert the data from BTV26 to a dictionary.
    """

    try:
        image = file["/AwakeEventData/TT41.BTV.412426/Image/"]
        nx = image["imagePositionSet1"][0].shape[0]
        ny = image["imagePositionSet2"][0].shape[0]
        dt = image["imageSet"][0].reshape(ny, nx)
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data_2d' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }
        


def get_btv_26_digi(file, file_timestamp):
    """
    Access and convert the data from BTV26 to a dictionary.
    """

    try:
        image = file["/AwakeEventData/TT41.BTV.412426.DigiCam/ExtractionImage/"]
        dt = image["image2D"][:]
        dt = _rebin(dt, [i // 2 for i in dt.shape]) # Specify rebin factor here
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data_2d' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }


def get_btv_54_pxi(file, file_timestamp):
    """
    Access and convert the data from BTV54 to a dictionary.
    """

    try:
        image = file["/AwakeEventData/BOVWA.11TCC4.AWAKECAM11/ExtractionImage/"]
        dt = image['imageRawData'][:]
        dt_2d = _rebin(dt, [i // PIXELS['btv_54'][2] for i in dt.shape])
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt_2d = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"
        
    # Centroid extraction
    if (np.shape(dt_2d) != (1,0)):
        params =  def_centroid(dt_2d)
        centr_params = params[0] 
        if np.prod(np.invert(np.isnan(centr_params))):
            x_pixel = PIXELS['btv_54'][0]*PIXELS['btv_54'][2]
            y_pixel = PIXELS['btv_54'][1]*PIXELS['btv_54'][2]  
            dt_1d = (centr_params[0] * x_pixel, centr_params[1] * y_pixel)            
        else:
            dt_1d = (math.nan, math.nan)
    else:
        dt_1d = (math.nan, math.nan)
          
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data_2d' : dt_2d, 'data_1d' : {'x' : dt_1d[0], 'y' : dt_1d[1]} ,
            'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }

def get_btv_54_digi(file, file_timestamp):
    """
    Access and convert the data from BTV54 to a dictionary.
    """

    try:
        image = file["/AwakeEventData/TT41.BTV.412354.DigiCam/ExtractionImage/"]
        dt = image["image2D"][:]
        dt_2d = _rebin(dt, [i // PIXELS['btv_54'][2] for i in dt.shape]) # Specify rebin factor here
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt_2d = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"   
        
    # Centroid extraction
    if (np.shape(dt_2d) != (1,0)):
        params =  def_centroid(dt_2d)
        centr_params = params[0] 
        if np.prod(np.invert(np.isnan(centr_params))):
            x_pixel = PIXELS['btv_54'][0]*PIXELS['btv_54'][2]
            y_pixel = PIXELS['btv_54'][1]*PIXELS['btv_54'][2]  
            dt_1d = (centr_params[0] * x_pixel, centr_params[1] * y_pixel)            
        else:
            dt_1d = (math.nan, math.nan)
    else:
        dt_1d = (math.nan, math.nan)    
             
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data_2d' : dt_2d, 'data_1d' : {'x' : dt_1d[0], 'y' : dt_1d[1]}, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }



def get_btv_is1_core_pxi(file, file_timestamp):
    """
    Access and convert the data from IS1 Core to a dictionary.
    """

    try:
        image = file["/AwakeEventData/BOVWA.07TCC4.AWAKECAM07/ExtractionImage/"]
        dt = image['imageRawData'][:]
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
        dt_2d = _rebin(dt, [i // PIXELS['is1_core'][2] for i in dt.shape])
    except :
        dt_2d = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"
    
    # Centroid extraction
    if (np.shape(dt_2d) != (1,0)):
        params =  def_centroid(dt_2d[100:200, 150:250])
        centr_params = params[0] 
        sigma_params = params[1]
        # print(f'IS1 core centr params: {centr_params}')

        if np.prod(np.invert(np.isnan(centr_params))):
            x_pixel = PIXELS['is1_core'][0]*PIXELS['is1_core'][2]
            y_pixel = PIXELS['is1_core'][1]*PIXELS['is1_core'][2]  
            dt_1d = (centr_params[0] * x_pixel, centr_params[1] * y_pixel)  
            sig = (sigma_params[0] * x_pixel, sigma_params[1] * y_pixel)
        else:
            dt_1d = (math.nan, math.nan)    
            sig = (math.nan, math.nan)     
    else:
        dt_1d = (math.nan, math.nan)    
        sig = (math.nan, math.nan)     
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 
    # print(f'IS1 Core dt2d: {dt_2d}')
    # print(f'IS1 Core dt1d: {dt_1d}')
    return {'data_2d' : dt_2d, 'data_1d' : {'x' : dt_1d[0], 'y' : dt_1d[1]},
            'sigma' : {'x' : sig[0], 'y' : sig[1]},
            'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }


def get_btv_is1_core_digi(file, file_timestamp):
    """
    Access and convert the data from IS1 Core to a dictionary.
    
    -TT41.BTV.412426.CORE.DigiCam 
    
    -TT41.BTV.412426.DigiCam
    """

    try:
        image = file["/AwakeEventData/TT41.BTV.412426.CORE.DigiCam/ExtractionImage/"]
        dt = image["image2D"][:]
        dt_2d = _rebin(dt, [i // PIXELS['is1_core'][2] for i in dt.shape]) # Specify rebin factor here
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt_2d = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"    
    
    # Centroid extraction
    if (np.shape(dt_2d) != (1,0)):
        params =  def_centroid(dt_2d[100:200, 150:250])
        centr_params = params[0] 
        sigma_params = params[1]


        if np.prod(np.invert(np.isnan(centr_params))):
            x_pixel = PIXELS['is1_core'][0]*PIXELS['is1_core'][2]
            y_pixel = PIXELS['is1_core'][1]*PIXELS['is1_core'][2]  
            dt_1d = (centr_params[0] * x_pixel, centr_params[1] * y_pixel)  
            sig = (sigma_params[0] * x_pixel, sigma_params[1] * y_pixel)
        else:
            dt_1d = (math.nan, math.nan)    
            sig = (math.nan, math.nan)     
    else:
        dt_1d = (math.nan, math.nan)    
        sig = (math.nan, math.nan)
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 
    
 

    return {'data_2d' : dt_2d, 'data_1d' : {'x' : dt_1d[0], 'y' : dt_1d[1]},
           'sigma' : {'x' : sig[0], 'y' : sig[1]},
            'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }


def get_btv_is1_halo_pxi(file, file_timestamp):
    """
    Access and convert the data from IS1 Halo to a dictionary.
    """
    
    try:
        image = file["/AwakeEventData/BOVWA.06TCC4.AWAKECAM06/ExtractionImage/"]
        dt = image['imageRawData'][:]
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
        dt = _rebin(dt, [i // 4 for i in dt.shape])
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data_2d' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }

def get_btv_is1_halo_digi(file, file_timestamp):
    """
    Access and convert the data from IS1 Halo to a dictionary.
    """

    try:
        image = file["/AwakeEventData/TT41.BTV.412426.HALO.DigiCam/ExtractionImage/"]
        dt = image["image2D"][:]
        dt = _rebin(dt, [i // PIXELS['is1_core'][2] for i in dt.shape]) # Specify rebin factor here
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data_2d' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }


def get_btv_is2_core_pxi(file, file_timestamp):
    """
    Access and convert the data from IS2 Core to a dictionary.
    """


    try:
        image = file["/AwakeEventData/BOVWA.02TCC4.AWAKECAM02/ExtractionImage/"]
        dt = image['imageRawData'][:]
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
        dt_2d = _rebin(dt, [i // PIXELS['is2_core'][2] for i in dt.shape])
    except :
        dt_2d = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"

    # Centroid extraction
    if (np.shape(dt_2d) != (1,0)):
        params =  def_centroid(dt_2d[100:200, 150:250])
        centr_params = params[0] 
        sigma_params = params[1]
       

        if np.prod(np.invert(np.isnan(centr_params))):
            x_pixel = PIXELS['is2_core'][0]*PIXELS['is2_core'][2]
            y_pixel = PIXELS['is2_core'][1]*PIXELS['is2_core'][2]  
            dt_1d = (centr_params[0] * x_pixel, centr_params[1] * y_pixel) 
            sig = (sigma_params[0] * x_pixel, sigma_params[1] * y_pixel) 
        else:
            dt_1d = (math.nan, math.nan)    
            sig = (math.nan, math.nan)     
    else:
        dt_1d = (math.nan, math.nan)
        sig = (math.nan, math.nan)
    
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 
   
    return {'data_2d' : dt_2d, 'data_1d' : {'x' : dt_1d[0], 'y' : dt_1d[1]},
            'sigma' : {'x' : sig[0], 'y' : sig[1]},
            'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }

def get_btv_is2_core_digi(file, file_timestamp):
    """
    Access and convert the data from IS2 Core to a dictionary.
    
    -TT41.BTV.412442.DigiCam
    
    """
    
    try:
        image = file["/AwakeEventData/TT41.BTV.412442.DigiCam/ExtractionImage/"]
        dt = image["image2D"][:]
        dt_2d = _rebin(dt, [i // PIXELS['is2_core'][2] for i in dt.shape]) # Specify rebin factor here
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except Exception as e:
        print('Exception for BTV.412442: ')
        print(e)
        dt_2d = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"    
      
    # Centroid extraction
    if (np.shape(dt_2d) != (1,0)):
        params =  def_centroid(dt_2d[100:200, 150:250])
        centr_params = params[0] 
       
        sigma_params = params[1]
        if np.prod(np.invert(np.isnan(centr_params))):
            x_pixel = PIXELS['is2_core'][0]*PIXELS['is2_core'][2]
            y_pixel = PIXELS['is2_core'][1]*PIXELS['is2_core'][2]  
            dt_1d = (centr_params[0] * x_pixel, centr_params[1] * y_pixel)  
            sig = (sigma_params[0] * x_pixel, sigma_params[1] * y_pixel) 
        else:
            dt_1d = (math.nan, math.nan)    
            sig = (math.nan, math.nan)     
    else:
        dt_1d = (math.nan, math.nan)    
        sig = (math.nan, math.nan)
    
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 
   
   
    return {'data_2d' : dt_2d, 'data_1d' : {'x' : dt_1d[0], 'y' : dt_1d[1]}, 
            'sigma' : {'x' : sig[0], 'y' : sig[1]},
            'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }

def get_btv_is2_halo_pxi(file, file_timestamp):
    """
    Access and convert the data from IS2 Halo to a dictionary.
    """


    try:
        image = file["/AwakeEventData/BOVWA.04TCC4.AWAKECAM04/ExtractionImage/"]
        dt = image['imageRawData'][:]
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
        dt = _rebin(dt, [i //PIXELS['is2_core'][2] for i in dt.shape])
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data_2d' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }

def get_btv_is2_halo_digi(file, file_timestamp):
    """
    Access and convert the data from IS2 Halo to a dictionary.
    """

    try:
        image = file["/AwakeEventData/TT41.BTV.412442.HALO.DigiCam/ExtractionImage/"]
        dt = image["image2D"][:]
        dt_2d = _rebin(dt, [i // PIXELS['is2_halo'][2] for i in dt.shape]) # Specify rebin factor here
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt_2d = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"    
    
    # Centroid extraction
    if (np.shape(dt_2d) != (1,0)):
        params =  def_centroid(dt_2d[100:200, 150:250])
        centr_params = params[0] 
        sigma_params = params[1]


        if np.prod(np.invert(np.isnan(centr_params))):
            x_pixel = PIXELS['is2_halo'][0]*PIXELS['is2_halo'][2]
            y_pixel = PIXELS['is2_halo'][1]*PIXELS['is2_halo'][2]  
            dt_1d = (centr_params[0] * x_pixel, centr_params[1] * y_pixel)  
            sig = (sigma_params[0] * x_pixel, sigma_params[1] * y_pixel)
        else:
            dt_1d = (math.nan, math.nan)    
            sig = (math.nan, math.nan)     
    else:
        dt_1d = (math.nan, math.nan)    
        sig = (math.nan, math.nan)
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data_2d' : dt_2d, 'data_1d' : {'x' : dt_1d[0], 'y' : dt_1d[1]},
           'sigma' : {'x' : sig[0], 'y' : sig[1]},
            'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }

def get_otr_angle_pxi(file, file_timestamp):
    """
    Access and convert the data from AWAKECAM01 (OTR Angle) to a dictionary
    """


    try:
        image = file["/AwakeEventData/BOVWA.01TCC4.AWAKECAM01/ExtractionImage/"]
        dt = image['imageRawData'][:]
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
        dt = _rebin(dt, [i // 4 for i in dt.shape])
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data_2d' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }



def get_exp_vol_1_pxi(file, file_timestamp):
    """
    Access and convert the data from AWAKECAM03 (Expansion Volume 1) to a dictionary.
    """
    try:
        image = file["/AwakeEventData/BOVWA.03TCC4.AWAKECAM03/ExtractionImage/"]
        dt = image['imageRawData'][:]
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
        dt = _rebin(dt, [i // 4 for i in dt.shape])
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data_2d' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }


def get_exp_vol_1_digi(file, file_timestamp):
    """
    Access and convert the data from TT41.BTV.412354.EXPVOL1.DigiCam (Expansion Volume 1) to a dictionary.
    """

    try:
        image = file["/AwakeEventData/TT41.BTV.412354.EXPVOL1.DigiCam/ExtractionImage/"]
        dt = image["image2D"][:]
        dt = _rebin(dt, [i // 4 for i in dt.shape]) # Specify rebin factor here
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data_2d' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }

# =============================================================================
# get streak cameras
# =============================================================================


def get_streak_1_analogue(file, file_timestamp):
    """
    Access and convert the data from XMPP-STREAK to a dictionary.
    """

    try:
        image = file["/AwakeEventData/XMPP-STREAK/StreakImage/"]
        dt = image["streakImageData"][:].reshape(512, 672)
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False  

    return {'data_2d' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }


def get_streak_2_analogue(file, file_timestamp):
    """
    Access and convert the data from TT41.BTV.412350.STREAK to a dictionary.
    """
    try:
        image = file["/AwakeEventData/TT41.BTV.412350.STREAK/StreakImage/"]
        dt = image["streakImageData"][:].reshape(512, 672)
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False  

    return {'data_2d' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }

def get_streak_3_analogue(file, file_timestamp):
    """
    Access and convert the data from YMPP-STREAK to a dictionary.
    """

    try:
        image = file["/AwakeEventData/YMPP-STREAK/StreakImage/"]
        dt = image["streakImageData"][:].reshape(512, 672)
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False  

    return {'data_2d' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }


def get_MPP_streak_pxi(file, file_timestamp):
    """
    Access and convert the data from AWAKECAM05 to a dictionary.
    """
    
    try:
        image = file["/AwakeEventData/BOVWA.05TCC4.AWAKECAM05/ExtractionImage/"]
        dt = image['imageRawData'][:]
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
        dt = _rebin(dt, [i // 4 for i in dt.shape])
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data_2d' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }



def get_MPP_streak_digi(file, file_timestamp):
    """
    Access and convert the data from TSG41.MPPSTREAK-REF.DigiCam to a dictionary.
    """

    try:
        image = file["/AwakeEventData/TSG41.MPPSTREAK-REF.DigiCam/ExtractionImage/"]
        dt = image["image2D"][:]
        dt = _rebin(dt, [i // 4 for i in dt.shape]) # Specify rebin factor here
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 
 
    return {'data_2d' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }

def get_BTV_streak_digi(file, file_timestamp):
    """
    Access and convert the data from TSG41.BTVSTREAK-REF.DigiCam to a dictionary.
    """

    try:
        image = file["/AwakeEventData/TSG41.BTVSTREAK-REF.DigiCam/ExtractionImage/"]
        dt = image["image2D"][:]
        dt = _rebin(dt, [i // 4 for i in dt.shape]) # Specify rebin factor here
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 
 
    return {'data_2d' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }



def get_BTV_streak_pxi(file, file_timestamp):
    """
    Access and convert the data from AWAKECAM08 to a dictionary.
    """

    try:
        image = file["/AwakeEventData/BOVWA.08TCC4.AWAKECAM08/ExtractionImage/"]
        dt = image['imageRawData'][:]
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
        dt = _rebin(dt, [i // 4 for i in dt.shape])
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data_2d' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }



#def get_BTV_streak_digi(file, file_timestamp):
#    """
#    Access and convert the data from TT41.BTVSTREAK-REF.DigiCam to a dictionary.
#    """
#
#    try:
#        image = file["/AwakeEventData/TT41.BTVSTREAK-REF.DigiCam/ExtractionImage/"]
#        dt = image["image2D"][:]
#        dt = _rebin(dt, [i // 4 for i in dt.shape]) # Specify rebin factor here
#        timestamp = image.attrs['acqStamp']
#        excep = image.attrs['exception']
#    except :
#        dt = [[]]
#        timestamp = "Reading Error"
#        excep = "Reading Error"        
#    try: 
#        etimestamp = file_timestamp
#        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
#    except:
#        timediff = False 
# 
#    return {'data_2d' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }


# =============================================================================
# get beam positon monitors
# =============================================================================

def get_bpm_electron_1(file, file_timestamp):

    try:
        data =  file["/AwakeEventData/TT41.BPM.412349/Acquisition/"]
        #print("Electron BPM Data: ", data["verPos"][:], data["spsExtraction"][()], data["spsExtractionIndex"][0])
        hor_dt = data["horPos"][:]
        ver_dt = data["verPos"][:]
        sps_extraction =data["spsExtraction"][()]
        hor_slice  = np.ma.masked_invalid(hor_dt)
        ver_slice = np.ma.masked_invalid(ver_dt)
        if sps_extraction == True:
            index = data["spsExtractionIndex"][0]
            hor_slice.mask[index] = True
            ver_slice.mask[index] = True       
            #print("Electron DAQ: index ",index,", hor_dt ",ver_dt," hor_slice ",ver_slice)
            
        avg_hor_dt = np.ma.mean(hor_slice)
        avg_ver_dt = np.ma.mean(ver_slice)
        #print("Electron average: ",avg_ver_dt)
        timestamp = data.attrs['acqStamp']
        excep = data.attrs['exception']

    except Exception as e:
        print(e)
        avg_hor_dt = []
        avg_ver_dt = []
        timestamp = "Reading Error"
        excep = "Reading Error"  
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False


    return {'data_1d' : {'x' : avg_hor_dt, 'y' : avg_ver_dt}, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }



def get_bpm_electron_2(file, file_timestamp):


    try:
        data =  file["/AwakeEventData/TT41.BPM.412351/Acquisition/"]
        hor_dt = data["horPos"][:]
        ver_dt = data["verPos"][:]
        sps_extraction =data["spsExtraction"][()]
        hor_slice  = np.ma.masked_invalid(hor_dt)
        ver_slice = np.ma.masked_invalid(ver_dt)
        if sps_extraction == True:
            index = data["spsExtractionIndex"][0]
            hor_slice.mask[index] = True
            ver_slice.mask[index] = True       
        avg_hor_dt = np.ma.mean(hor_slice)
        avg_ver_dt = np.ma.mean(ver_slice)
        timestamp = data.attrs['acqStamp']
        excep = data.attrs['exception']

    except:
        avg_hor_dt = []
        avg_ver_dt = []
        timestamp = "Reading Error"
        excep = "Reading Error"  
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False


    return {'data_1d' : {'x' : avg_hor_dt, 'y' : avg_ver_dt}, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }


def get_bpm_proton_all(file, file_timestamp):
    
    
    
    try:
        data =  file["/AwakeEventData/BPM.AWAKE.TT41/Acquisition/"]
        hor_dt = data["horPosition"][:]
        ver_dt = data["verPosition"][:]
        names = data["bpmNames"][:]
        timestamp = data.attrs['acqStamp']
        excep = data.attrs['exception']
    
        hor_1 = hor_dt[16] + gold_traject['proton_bpm_1']['x']
        ver_1 = ver_dt[16] + gold_traject['proton_bpm_1']['y']
        name1 = names[16]
        hor_2 = hor_dt[17] + gold_traject['proton_bpm_2']['x']
        ver_2 = ver_dt[17] + gold_traject['proton_bpm_2']['y']
        name2 = names[17]
        hor_3 = hor_dt[18] + gold_traject['proton_bpm_3']['x']
        ver_3 = ver_dt[18] + gold_traject['proton_bpm_3']['y']
        name3 = names[18]
        hor_4 = hor_dt[19] + gold_traject['proton_bpm_4']['x']
        ver_4 = ver_dt[19] + gold_traject['proton_bpm_4']['y']
        name4 = names[19]
        hor_5 = hor_dt[20] + gold_traject['proton_bpm_5']['x']
        ver_5 = ver_dt[20] + gold_traject['proton_bpm_5']['y']
        name5 = names[20]
        
        # Only add the timestamp if the data is not nan
        time_dict = {'time1':[], 'time2':[], 'time3':[], 'time4':[], 'time5':[]}
        for time, dt in zip(time_dict, [hor_1, hor_2, hor_3, hor_4, hor_5]):
            if dt != math.nan:
                time_dict[time] = timestamp
            if dt == math.nan:
                time_dict[time] = math.nan

    except:
        avg_hor_dt = []
        avg_ver_dt = []
        timestamp = "Reading Error"
        excep = "Reading Error"  
        
        hor_1 = []
        ver_1 = []
        name1 = ''
        hor_2 = []
        ver_2 = []
        name2 = ''
        hor_3 = []
        ver_3 = []
        name3 = ''
        hor_4 = []
        ver_4 = []
        name4 = ''
        hor_5 = []
        ver_5 = []
        name5 = ''
        
        data_dict = {}
    
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False
        
    data_dict = {
        '1':{'data_1d' : {'x' : hor_1, 'y' : ver_1}, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff, 'name' : name1 },
        '2':{'data_1d' : {'x' : hor_2, 'y' : ver_2}, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff, 'name' : name2 },
        '3':{'data_1d' : {'x' : hor_3, 'y' : ver_3}, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff, 'name' : name3 },
        '4':{'data_1d' : {'x' : hor_4, 'y' : ver_4}, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff, 'name' : name4 },
        '5':{'data_1d' : {'x' : hor_5, 'y' : ver_5}, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff, 'name' : name5 },
        }    
       
    return data_dict

  
# =============================================================================
# get laser energy meters
# =============================================================================


def get_emeter_2(file, file_timestamp):

    try:
        data = file["/AwakeEventData/EMETER02/Acq/"]
        dt = data["value"][:]
        timestamp = data.attrs['acqStamp']
        excep = data.attrs['exception']
    except:
        dt = []
        timestamp = "Reading Error"
        excep = "Reading Error" 
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False


    return {'energy' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff}
  
def get_emeter_3(file, file_timestamp):

    try:
        data = file["/AwakeEventData/EMETER03/Acq/"]
        dt = data["value"][:]
        timestamp = data.attrs['acqStamp']
        excep = data.attrs['exception']
    except:
        dt = []
        timestamp = "Reading Error"
        excep = "Reading Error" 
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False


    return {'energy' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff}
  
def get_emeter_4(file, file_timestamp):
        
    try:
        data = file["/AwakeEventData/EMETER04/Acq/"]
        dt = data["value"][:]
        timestamp = data.attrs['acqStamp']
        excep = data.attrs['exception']
    except:
        dt = []
        timestamp = "Reading Error"
        excep = "Reading Error" 
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False

    return {'energy' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff}
  
def get_emeter_5(file, file_timestamp):
     
        
    try:
        data = file["/AwakeEventData/EMETER05/Acq/"]
        dt = data["value"][:]
        timestamp = data.attrs['acqStamp']
        excep = data.attrs['exception']
    except:
        dt = []
        timestamp = "Reading Error"
        excep = "Reading Error" 
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False
    
    
    return {'energy' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff}


# =============================================================================
# get virtual line cameras
# =============================================================================

def get_laser_camera_1_pxi(file, file_timestamp):
    """
    Access and convert the data from BOVWA.01TCV4.CAM8 to a dictionary.
    """

    try:
        image = file["/AwakeEventData/BOVWA.01TCV4.CAM8/ExtractionImage/"]
        dt = image["imageRawData"][:]
        dt = _rebin(dt, [i // 4 for i in dt.shape])
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data_2d' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }

def get_laser_camera_2_pxi(file, file_timestamp):
    """
    Access and convert the data from BOVWA.02TSG40.CAM7 to a dictionary.
    """

   
    try:
        image = file["/AwakeEventData/BOVWA.02TSG40.CAM7/ExtractionImage/"]
        dt = image["imageRawData"][:]
        dt = _rebin(dt, [i // 4 for i in dt.shape])
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data_2d' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }


def get_laser_camera_3_pxi(file, file_timestamp):
    """
    Access and convert the data from BOVWA.03TT41.CAM3 to a dictionary.
    """

    try:
        image = file["/AwakeEventData/BOVWA.03TT41.CAM3/ExtractionImage/"]
        dt = image["imageRawData"][:]
        dt_2d = _rebin(dt, [i // PIXELS['vlc'][2] for i in dt.shape])
        max_hor_amp = image["horProjAmp"][0]  
        max_ver_amp = image["verProjAmp"][0]   
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']   
    except :
        dt_2d = [[]]
        max_hor_amp = 0
        max_ver_amp = 0   
        timestamp = "Reading Error"
        excep = "Reading Error"
            
    #print('Centroid 3:')
    # Centroid extraction
    if (np.shape(dt_2d) != (1,0)):
        params =  def_centroid(dt_2d)
        centr_params = params[0] 
        #print(centr_params)
        if np.prod(np.invert(np.isnan(centr_params))):
            x_pixel = PIXELS['vlc'][0]*PIXELS['vlc'][2]
            y_pixel = PIXELS['vlc'][1]*PIXELS['vlc'][2]  
            dt_1d = (centr_params[0] * x_pixel, centr_params[1] * y_pixel)            
            #print(dt_1d)
        else:
            dt_1d = (math.nan, math.nan)
        
    else:
        dt_1d = (math.nan, math.nan)
        

    #print(file_timestamp)
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data_2d' : dt_2d, 'data_1d' : {'x' : dt_1d[0], 'y' : dt_1d[1]} ,
            'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff, 
            'max_hor_amp' : max_hor_amp, 'max_ver_amp' : max_ver_amp}

def get_laser_camera_4_pxi(file, file_timestamp):
    """
    Access and convert the data from BOVWA.04TT41.CAM4 to a dictionary.
    """

    try:
        image = file["/AwakeEventData/BOVWA.04TT41.CAM4/ExtractionImage/"]
        dt = image["imageRawData"][:]
        dt_2d = _rebin(dt, [i // PIXELS['vlc'][2] for i in dt.shape])
        max_hor_amp = image["horProjAmp"][0]  
        max_ver_amp = image["verProjAmp"][0]   
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt_2d = [[]]
        max_hor_amp = 0
        max_ver_amp = 0   
        timestamp = "Reading Error"
        excep = "Reading Error"
    
    # Centroid extraction
    if (np.shape(dt_2d) != (1,0)):
        params =  def_centroid(dt_2d)
        centr_params = params[0] 
        if np.prod(np.invert(np.isnan(centr_params))):
            x_pixel = PIXELS['vlc'][0]*PIXELS['vlc'][2]
            y_pixel = PIXELS['vlc'][1]*PIXELS['vlc'][2]  
            dt_1d = (centr_params[0] * x_pixel, centr_params[1] * y_pixel)            
        else:
            dt_1d = (math.nan, math.nan)
        
    else:
        dt_1d = (math.nan, math.nan)    
    
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data_2d' : dt_2d, 'data_1d' : {'x' : dt_1d[0], 'y' : dt_1d[1]} ,
            'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff, 
            'max_hor_amp' : max_hor_amp, 'max_ver_amp' : max_ver_amp}

def get_laser_camera_5_pxi(file, file_timestamp):
    """
    Access and convert the data from BOVWA.05TT41.CAM5 to a dictionary.
    """


    try:
        image = file["/AwakeEventData/BOVWA.05TT41.CAM5/ExtractionImage/"]
        dt = image["imageRawData"][:]
        dt_2d = _rebin(dt, [i // PIXELS['vlc'][2] for i in dt.shape])
        max_hor_amp = image["horProjAmp"][0]  
        max_ver_amp = image["verProjAmp"][0]   
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt_2d = [[]]
        max_hor_amp = 0
        max_ver_amp = 0   
        timestamp = "Reading Error"
        excep = "Reading Error"
              
        
    # Centroid extraction
    if (np.shape(dt_2d) != (1,0)):
        params =  def_centroid(dt_2d)
        centr_params = params[0] 
        if np.prod(np.invert(np.isnan(centr_params))):
            x_pixel = PIXELS['vlc'][0]*PIXELS['vlc'][2]
            y_pixel = PIXELS['vlc'][1]*PIXELS['vlc'][2]  
            dt_1d = (centr_params[0] * x_pixel, centr_params[1] * y_pixel)            
        else:
            dt_1d = (math.nan, math.nan)
        
    else:
        dt_1d = (math.nan, math.nan)    
        
 
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data_2d' : dt_2d, 'data_1d' : {'x' : dt_1d[0], 'y' : dt_1d[1]} ,
            'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff, 
            'max_hor_amp' : max_hor_amp, 'max_ver_amp' : max_ver_amp}

def get_laser_camera_1_digi(file, file_timestamp):
    """
    Access and convert the data from BOVWA.01TCV4.CAM8 to a dictionary.
    """        
        
    try:
        image = file["/AwakeEventData/TCV4.UV_CATHODE.DigiCam/ExtractionImage/"]
        dt = image["image2D"][:]
        dt = _rebin(dt, [i // 4 for i in dt.shape])
        max_hor_amp = image["maxVal"][0]  
        max_ver_amp = image["maxVal"][0]   
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        max_hor_amp = 0
        max_ver_amp = 0   
        timestamp = "Reading Error"
        excep = "Reading Error"
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

        
    return {'data_2d' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff, 'max_hor_amp' : max_hor_amp, 'max_ver_amp' : max_ver_amp }



def get_laser_camera_2_digi(file, file_timestamp):
    """
    Access and convert the data from BOVWA.02TSG40.CAM7 to a dictionary.
    """

    try:
        image = file["/AwakeEventData/TSG40.IR_LASER.DigiCam/ExtractionImage/"]
        dt = image["image2D"][:]
        dt = _rebin(dt, [i // 4 for i in dt.shape])
        max_hor_amp = image["maxVal"][0]  
        max_ver_amp = image["maxVal"][0]   
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    #except :
    except Exception as e:
        print(e)
        dt = [[]]
        max_hor_amp = 0
        max_ver_amp = 0   
        timestamp = "Reading Error"
        excep = "Reading Error"
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data_2d' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff, 'max_hor_amp' : max_hor_amp, 'max_ver_amp' : max_ver_amp }

def get_laser_camera_3_digi(file, file_timestamp):
    """
    Access and convert the data from BOVWA.03TT41.CAM3 to a dictionary.
    """

    try:
        image = file["/AwakeEventData/TT41.VLC3.DigiCam/ExtractionImage/"]
        dt = image["image2D"][:]
        dt_2d = _rebin(dt, [i // PIXELS['vlc'][2] for i in dt.shape])
        max_hor_amp = image["maxVal"][0] 
        max_ver_amp = image["maxVal"][0]   
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt_2d = [[]]
        max_hor_amp = 0
        max_ver_amp = 0   
        timestamp = "Reading Error"
        excep = "Reading Error"

    # Centroid extraction
    if (np.shape(dt_2d) != (1,0)):
        params =  def_centroid(dt_2d)
        centr_params = params[0] 
        if np.prod(np.invert(np.isnan(centr_params))):
            x_pixel = PIXELS['vlc'][0]*PIXELS['vlc'][2]
            y_pixel = PIXELS['vlc'][1]*PIXELS['vlc'][2]  
            dt_1d = (centr_params[0] * x_pixel, centr_params[1] * y_pixel)            
        else:
            dt_1d = (math.nan, math.nan)
        
    else:
        dt_1d = (math.nan, math.nan)    
                

    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 
    
    return {'data_2d' : dt_2d, 'data_1d' : {'x' : dt_1d[0], 'y' : dt_1d[1]} ,
            'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff, 
            'max_hor_amp' : max_hor_amp, 'max_ver_amp' : max_ver_amp}

def get_laser_camera_4_digi(file, file_timestamp):
    """
    Access and convert the data from BOVWA.04TT41.CAM4 to a dictionary.
    """
        
    try:
        image = file["/AwakeEventData/TT41.VLC4.DigiCam/ExtractionImage/"]
        dt = image["image2D"][:]
        dt_2d = _rebin(dt, [i // PIXELS['vlc'][2] for i in dt.shape])
        max_hor_amp = image["maxVal"][0]
        max_ver_amp = image["maxVal"][0]
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt_2d = [[]]
        max_hor_amp = 0
        max_ver_amp = 0   
        timestamp = "Reading Error"
        excep = "Reading Error"
            
    # Centroid extraction
    if (np.shape(dt_2d) != (1,0)):
        params =  def_centroid(dt_2d)
        centr_params = params[0] 
        if np.prod(np.invert(np.isnan(centr_params))):
            x_pixel = PIXELS['vlc'][0]*PIXELS['vlc'][2]
            y_pixel = PIXELS['vlc'][1]*PIXELS['vlc'][2]  
            dt_1d = (centr_params[0] * x_pixel, centr_params[1] * y_pixel)            
        else:
            dt_1d = (math.nan, math.nan)
    else:
        dt_1d = (math.nan, math.nan)    
                        

    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data_2d' : dt_2d, 'data_1d' : {'x' : dt_1d[0], 'y' : dt_1d[1]} ,
            'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff, 
            'max_hor_amp' : max_hor_amp, 'max_ver_amp' : max_ver_amp}

def get_laser_camera_5_digi(file, file_timestamp):
    """
    Access and convert the data from BOVWA.05TT41.CAM5 to a dictionary.
    """

    try:
        image = file["/AwakeEventData/TT41.VLC5.DigiCam/ExtractionImage/"]
        dt = image["image2D"][:]
        dt_2d = _rebin(dt, [i // PIXELS['vlc'][2] for i in dt.shape])
        max_hor_amp = image["maxVal"][0]
        max_ver_amp = image["maxVal"][0]
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt_2d = [[]]
        max_hor_amp = 0
        max_ver_amp = 0   
        timestamp = "Reading Error"
        excep = "Reading Error"
        
    # Centroid extraction
    if (np.shape(dt_2d) != (1,0)):
        params =  def_centroid(dt_2d)
        centr_params = params[0] 
        if np.prod(np.invert(np.isnan(centr_params))):
            x_pixel = PIXELS['vlc'][0]*PIXELS['vlc'][2]
            y_pixel = PIXELS['vlc'][1]*PIXELS['vlc'][2]  
            dt_1d = (centr_params[0] * x_pixel, centr_params[1] * y_pixel)            
        else:
            dt_1d = (math.nan, math.nan)
        
    else:
        dt_1d = (math.nan, math.nan)    
                        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data_2d' : dt_2d, 'data_1d' : {'x' : dt_1d[0], 'y' : dt_1d[1]} ,
            'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff, 
            'max_hor_amp' : max_hor_amp, 'max_ver_amp' : max_ver_amp}

def get_spectro_camera_1_digi(file, file_timestamp):
    """
    Access and convert the data from BOVWA.05TT41.CAM5 to a dictionary.
    """

    try:
        image = file["/AwakeEventData/TCC4.SPECTRO1.DigiCam/ExtractionImage/"]
        dt = image["image2D"][:]
        dt = _rebin(dt, [i // 4 for i in dt.shape]) # Specify rebin factor here
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data_2d' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }


# =============================================================================
# get DMD cameras
# =============================================================================



def get_dmd_1_digi(file, file_timestamp):
    """
    Access and convert the data from TCC4.DMD1.DigiCam to a dictionary.
    
    """

    try:
        image = file["/AwakeEventData/TCC4.DMD1.DigiCam/ExtractionImage/"]
        dt =image["image2D"][:]
        dt = _rebin(dt, [i // 2 for i in dt.shape])
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data_2d' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff}


def get_dmd_1_pxi(file, file_timestamp):
    """
    Access and convert the data from AWAKECAM01 (DMD 1) to a dictionary.
    """
    try:
        image = file["/AwakeEventData/BOVWA.01TCC4.AWAKECAM01/ExtractionImage/"]
        dt =image["imageRawData"][:]
        dt = _rebin(dt, [i // 2 for i in dt.shape])
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data_2d' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff}


def get_dmd_2_digi(file, file_timestamp):
    """
    Access and convert the data from TCC4.DMD2.DigiCam to a dictionary.

    """

    try:
        image = file["/AwakeEventData/TCC4.DMD2.DigiCam/ExtractionImage/"]
        dt =image["image2D"][:]
        dt = _rebin(dt, [i // 2 for i in dt.shape])
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data_2d' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff}


def get_dmd_2_pxi(file, file_timestamp):
    """
    Access and convert the data from AWAKECAM12 (DMD 2) to a dictionary.
    """

    try:
        image = file["/AwakeEventData/BOVWA.12TCC4.AWAKECAM12/ExtractionImage/"]
        dt =image["imageRawData"][:]
        dt = _rebin(dt, [i // 2 for i in dt.shape])
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data_2d' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff}




# =============================================================================
# get plasma diagnostics
# =============================================================================
def get_spectrometer(file, file_timestamp):
    """
    Access and convert the data from XUCL-SPECTRO to a dictionary.
    """

    try:
        image = file["/AwakeEventData/XUCL-SPECTRO/ImageAcq/"]
        dt = image["imageData"][:].reshape(512, 2048)
        dt = _rebin(dt, [i // 2 for i in dt.shape])
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data_2d' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }




def get_plasma_light(file, file_timestamp):

    try:
        data = file["AwakeEventData/XMPP-SPECTRO/ImageAcq/"]
        dt = np.array(data["imageData"], dtype = float)
        up = np.sum(dt[350:550])-4*np.sum(dt[50:100])
        down =  np.sum(dt[150:350])-4*np.sum(dt[50:100])
        timestamp = data.attrs['acqStamp']
        excep = data.attrs['exception']

    except:
        dt = [[]]
        up = []
        down = []
        timestamp = "Reading Error"
        excep = "Reading Error"        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'image' : dt, 'point'  : {'upstream' : up, 'downstream' : down} , 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }
    

# =============================================================================
# get proton intensity
# =============================================================================


def get_population_bctfi(file, file_timestamp):
    
    try:
        data = file["/AwakeEventData/TT40.BCTFI.400344/CaptureAcquisition/"]
        dt = data["totalIntensity"][:]
        timestamp = data.attrs['acqStamp']
        excep = data.attrs['exception']

    except:
        dt = []
        timestamp = "Reading Error"
        excep = "Reading Error"
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'charge' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }


def get_population_bctf(file, file_timestamp):
    """
    Get bunch population.
    """

    try:
        population = file["/AwakeEventData/TT41.BCTF.412340/CaptureAcquisition/totalIntensity"][0]
    except:
        population = 0.0

    return population



# =============================================================================
# File properties
# =============================================================================

def get_timestamp(file):
    """
    Get event timestamp and convert it to time.
    """

    try:
        event_timestamp = round(file["/AwakeEventInfo/timestamp/"][...]/1E9, ndigits=5)
        event_time = time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(event_timestamp))
    except:
        event_timestamp = "Reading Error"
        event_time = "Reading Error"

    return (event_timestamp, event_time)

def get_event_number(file, file_timestamp):
    """
    Get event number.
    """

    try:
        event_number = file["/AwakeEventInfo/eventNumber/"][...]
    except:
        event_number = "Reading Error"

    return event_number


# =============================================================================
# Filename properties
# =============================================================================

def merge_fold_paths(path1, path2):
    """
    Append the month and day directories in path2 to path1 
    """

    tmp1 = path2.split('/')[-2] + '/' + path2.split('/')[-1]
    return os.path.join(path1, tmp1)

def find_lastest_subdirectory(path):
    """
    Find the most recently updated folder.
    """
    list_folders_1 = [i for i in os.listdir(path)  if os.path.isdir(os.path.join(path,i)) and (i[0] != '.') ]
    newest_folder = max([os.path.join(path,d) for d in list_folders_1], key=os.path.getmtime)

    #list_folders_2 = [i for i in os.listdir(newest_folder)  if os.path.isdir(os.path.join(newest_folder,i)) and (i[0] != '.') and (i != '16') ] #What was so special about 16
    list_folders_2 = [i for i in os.listdir(newest_folder)  if os.path.isdir(os.path.join(newest_folder,i)) and (i[0] != '.')  ]
    newest_folder_2 = max([os.path.join(newest_folder,d) for d in list_folders_2], key=os.path.getmtime)

    return newest_folder_2

def det_run_number(ename):
    return int(ename.split('_')[2]) 

# =============================================================================
# Misc.
# =============================================================================

def _rebin(a, shape): #'shape' is smaller version but same ratios
    sh = shape[0],a.shape[0]//shape[0],shape[1],a.shape[1]//shape[1]
    return a.reshape(sh).mean(-1).mean(1) #sh shape must be compatible with a shape

def nearest(items, pivot):
    return min(items, key=lambda x: abs(x - pivot))

def get_reference_time(path):
        
    with open(path, 'r') as file:
        
        date = file.readline()

        date = datetime.strptime(date[1:20], '%Y-%m-%d %H:%M:%S')
    
    return date


# =============================================================================
# Single detector delta from reference
# =============================================================================

def constr_deltas():
    
    dict = { 'proton' : {'detector_1' : {'x' : [], 'y' : [], 'time' : []}, 
                         'detector_2' : {'x' : [], 'y' : [], 'time' : []}
                         },
            'electron' : {'detector_1' : {'x' : [], 'y' : [], 'time' : []}, 
                          'detector_2' : {'x' : [], 'y' : [], 'time' : []}
                         },
            'laser' : {'detector_1' : {'x' : [], 'y' : [], 'time' : []}, 
                       'detector_2' : {'x' : [], 'y' : [], 'time' : []}
                      }
            }
    
    return copy.deepcopy(dict), copy.deepcopy(dict)


def get_deltas(tl_data, ev_data, ref, curr_delta, avg_delta):
    
    # cycle through each of the detectors in question: at this point, decide upon
    # the detectors which are being used 
    
    for spec, det_no, det_name in zip(['proton', 'proton', 'electron', 'electron', 'laser', 'laser'],
                                      ['detector_1', 'detector_2', 'detector_1', 'detector_2', 'detector_1', 'detector_2'],
                                      ['bpm_proton_1', 'bpm_proton_3', 'bpm_electron_1', 'bpm_electron_2', 'laser_3', 'laser_5']
                                      ):
        
        time = datetime.strptime(ev_data['time'],'%Y-%m-%d %H:%M:%S')
        
        # get the current deltas

      
        if [] in ref[spec][det_no].values(): # check that there is indeed a reference
          
            avg_delta[spec][det_no] = {'x' : math.nan, 'y' : math.nan, 'time' : math.nan }
            curr_delta[spec][det_no] = {'x' : math.nan, 'y' : math.nan, 'time' : math.nan }

        else:

            #print(det_name, ev_data[det_name]['data_1d'])
            delta = get_detector_delta(ev_data[det_name]['data_1d'], ref[spec][det_no], time)
            curr_delta[spec][det_no] = copy.deepcopy(delta)
        
            # get the average deltas
            if np.size(tl_data[det_name]['x']) > 10: # all working...
                avg_x = np.nanmean(tl_data[det_name]['x'][-10:]) 
                avg_y = np.nanmean(tl_data[det_name]['y'][-10:]) 
                avgd_data =  {'x' : avg_x , 'y' : avg_y}
                delta = get_detector_delta(avgd_data, ref[spec][det_no], time)
                avg_delta[spec][det_no] = copy.deepcopy(delta) # remember copy.deepcopy()

            else: 
                avg_delta[spec][det_no] = {'x' : math.nan, 'y' : math.nan, 'time' : math.nan }

    return curr_delta, avg_delta
    
    
def get_detector_delta(ev_data, reference, time):

    """
    Gets the average difference in position (delta) at a single detector,
    according to a reference. 

    ---
    Returns
    
    Dictionary {'x', 'y', 'time'}
    """

    reldata = {}
    

    reldata['x'] = ev_data['x'] - reference['x']
    reldata['y'] = ev_data['y'] - reference['y']
    reldata['time'] = time

    return reldata


# =============================================================================
# Two beam alignment stuff
# =============================================================================

def get_alignment(delta_beam1, delta_beam2, spec_beam1, spec_beam2, tl_align, tl_delta):
    
    try:  
        timeline_tmp_aligns = copy.deepcopy(tl_align)
        # Get proton and laser trajectories (offset and angle) at start and end of plasma cell
        fixed_pos_traject_beam1 = get_plasma_traject(delta_beam1['detector_1'], delta_beam1['detector_2'], spec_beam1)
        fixed_pos_traject_beam2 = get_plasma_traject(delta_beam2['detector_1'], delta_beam2['detector_2'], spec_beam2)

        # Get the difference between laser and proton trajetories at start and end of plasma cell
        diff_traject = get_traject_diff(fixed_pos_traject_beam1, fixed_pos_traject_beam2)
     
        for plasma_pos in ('upstream', 'downstream'):
            for diff in ('rel_pos', 'angle'):
                for dim in ('x', 'y'):
                    tl_tmp = copy.deepcopy(tl_align[plasma_pos][diff][dim])
                    dt_tmp = np.array(diff_traject[plasma_pos][dim][diff])
                    if np.isfinite(dt_tmp):
                        tmp = np.append(tl_tmp, dt_tmp)

                    tl_align[plasma_pos][diff].update({dim: tmp})
        
        tl_tmp = copy.deepcopy(tl_align['time'])
        dt_tmp = diff_traject['time']
        tmp = np.append(tl_tmp, dt_tmp)
        tl_align.update({'time': tmp })
       
        
    except:
        pass
        
        
        
def get_plasma_traject(detector1, detector2, beam_spec):

    """
    Get trajectory for a single beam species, at the start and end of the plasma
    cell. Only get trajectory if there is data present at both detectors. 

    """    
    
    traject = {'upstream': {'x' : {}, 'y' : {}},
               'downstream': {'x' : {}, 'y' : {}},
               'time' : []
               }
    
    
    for plasma_loc in ('upstream', 'downstream'):
        for dimension in ('x','y'):
                       
            upstream_offset = detector1[dimension]  
            upstream_dist =  DIMENSIONS_DICT[plasma_loc][beam_spec]['detector_1']
            downstream_offset = detector2[dimension]
            downstream_dist =  DIMENSIONS_DICT[plasma_loc][beam_spec]['detector_2']
            
            angle = -math.atan((upstream_offset - downstream_offset)/(downstream_dist-upstream_dist))
            offset =  upstream_offset + (upstream_offset*math.tan(-angle))
            
            traject[plasma_loc][dimension]['angle'] = angle*(10**6) # μrad
            traject[plasma_loc][dimension]['rel_pos'] = offset

    traject['time']= detector1['time']
    
    return traject

def get_traject_diff(beam_1, beam_2): 

    alignment = {'upstream': {'x' : {}, 'y' : {}},
                  'downstream': {'x' : {}, 'y' : {}},
                  'time' : []
                  }
    
    for plasma_loc in ('upstream', 'downstream'):
        for dimension in ('x','y'):
            beam_1_angle =  beam_1[plasma_loc][dimension]['angle']
            beam_2_angle =  beam_2[plasma_loc][dimension]['angle']
            beam_1_pos =  beam_1[plasma_loc][dimension]['rel_pos']
            beam_2_pos =  beam_2[plasma_loc][dimension]['rel_pos']
            
            alignment[plasma_loc][dimension]['angle'] = beam_1_angle - beam_2_angle
            alignment[plasma_loc][dimension]['rel_pos'] = beam_1_pos - beam_2_pos
    alignment['time'] = beam_1['time']

    return alignment

# =============================================================================
# Trajectory reference single beam
# =============================================================================


def get_beam_reference(tl_data, ref_time, ref_prev):
     
    def get_avg(data, time_start, ref_prev, no_events):

        ref_raw = {'x': [], 'y': []}

        # Maybe chnage so that the 20 or 100 shots are synchronous shots...
        try:
            indicesafter_1 = [i for i,v in enumerate(data['time'][1:]) if v > time_start]

            for dim in ('x', 'y'):
                if np.size(indicesafter_1) >= no_events: # redundant
                    ref_data = data[dim][1:][indicesafter_1[0:no_events]]
                #print('ref_data: ',ref_data)
                avg_data = np.nanmean(ref_data)
                #print('avg_data: ',avg_data)
                ref_raw.update({dim : avg_data})
        
        except: 
            ref_raw = {'x': [], 'y': []}
        #print('returning')
        return ref_raw

    for detector in list(ref_prev.keys()):

        #print('Getting average for detector ', detector, '. ref_prev.values() ', ref_prev[detector].values())
#        if [] in list(ref_prev[detector].values()): # This means only get a new ref is old is empty, while we should get it also if we have a new ref_time. For now, we could get it ONLY if we have a new ref time or if we are at the tenth event
        new_ref = get_avg(tl_data[detector], ref_time, ref_prev[detector], 3) 
        #print('Got it: ', new_ref)
        ref_prev.update({detector : new_ref})
  
    
  
# =============================================================================
# Hard-coded parameters
# =============================================================================


# Pixel sizes (in x and y) and 'rebin factor' for cameras used in timeline plots
PIXELS = {
    
    'vlc' : [0.0059, 0.0059, 4],
    'is1_core' : [0.0411, 0.0444, 2], 
    'is2_core' : [0.0404, 0.0396, 2], 
    'is2_halo' : [0.05, 0.05, 2], 
    'btv_50' : [0.0143, 0.0143, 2],  
    'btv_53' : [0.011, 0.011, 2],  
    'btv_54' : [0.039, 0.039, 2],  
    
    }
# Golden trajectory for the proton bpms
gold_traject = {'proton_bpm_1': {'x': 1.1734,'y': -0.14 },
                'proton_bpm_2': {'x': 2.7482,'y': 0.8772},
                'proton_bpm_3': {'x': 2.5409 ,'y': 0.9618},
                'proton_bpm_4': {'x': 2.0826,'y': 0.4946 },
                'proton_bpm_5': {'x': 2.4254,'y':0.2912 }
                }

# Dimensions of the experiment
dims = {'u': {'BTV_50' : 12, 
                             'BTV_53' : 12, 
                             'BTV_26' : 12,
                             'IS1_Core' :12300.5,
                             'IS2_Core' :20438.9,
                             'bpm_39' : -7979.5,
                             'bpm_11' : -22154.5, 
                             'laser_3' : 0,
                             'laser_5' : 9830,
                             'VLC_4' : 4945,
                             'bpm_proton_1': -22154.5, # BPM 412311
                             'bpm_proton_3':-7979.5,   # BPM 412339
                             'bpm_electron_1':-3323.5,  # BPM 412349
                             'bpm_electron_2':-2177,  # BPM 412351
                             },
                   
                   'd': {'BTV_50' : 12, 
                           'BTV_53' : 12, 
                           'BTV_26' : 12,
                           'IS1_Core' :2000.5,
                           'IS2_Core' :10138.9,
                           'bpm_39' : -18279.5,  
                           'bpm_11' : -32454.5,  
                           'laser_3' : -9830,
                           'VLC_4' : -4880,
                           'laser_5' : 0,
                           'bpm_proton_1': -32454.5, # BPM 412311
                           'bpm_proton_3': -18279.5,  # BPM 412339          
                           'bpm_electron_1':-13973.5, # BPM 412349
                           'bpm_electron_2':-12827, # BPM 412351

                           }
                   }

DIMENSIONS_DICT = {'upstream': { 'proton' : {'detector_1' : dims['u']['bpm_proton_1'],
                                             'detector_2' : dims['u']['bpm_proton_3'] },
                                 'electron' : {'detector_1' : dims['u']['bpm_electron_1'], 
                                               'detector_2' : dims['u']['bpm_electron_2']}, 
                                 'laser' : {'detector_1' : dims['u']['laser_3'] ,
                                            'detector_2' : dims['u']['laser_5'] }
                                 }, 
                   
                   'downstream': {'proton' : {'detector_1' : dims['d']['bpm_proton_1'],
                                              'detector_2' : dims['d']['bpm_proton_3'] },
                                  'electron' : {'detector_1' : dims['d']['bpm_electron_1'], 
                                                'detector_2' : dims['d']['bpm_electron_2']}, 
                                  'laser' : {'detector_1' : dims['d']['laser_3'] ,
                                             'detector_2' : dims['d']['laser_5']}
                                  }
                   }
# Warning sound
#SOUND_1= '/user/awakeop/felverson/quick_tools/246332__kwahmah-02__five-beeps.wav'
#SOUND_1 = '/user/awakeop/fernpannell/JRsound.wav' 
SOUND_1= '/user/awakeop/gzevi/sounds/central_c_piano_like.wav' 

def constr_warning_bool():

    return {'btv_50' :[],
            'btv_53': [], 
            'btv_54' :[], 
            'btv_26': [], 
            'laser_3' :[],
            #'laser_4' :[], 
            'laser_5':[], 
            'is1_core' : [], 
            'is1_halo' : [], 
            'is2_core' :[],
            'is2_halo':[],
            'dmd_1' :[], 
            'dmd_2':[],
            'exp_vol_1':[],
            'streak_1':[], 
            'streak_2': [], 
#            'streak_3': [], 
            'MPP_STREAK':[],
            'BTV_STREAK':[],
            #'spectro': [],
            }

def play_warning(event_data, warning_bool_new, warning_bool_prev,sound_bool):
  for detector in warning_bool_new.keys():
    if event_data[detector]['excep'] == True or event_data[detector]['timediff']>10:
        warning_bool_new[detector] = True
        sound_bool[detector] = True
        # Only sound the warning if the error is a new one
        #if warning_bool_prev[detector] == True:
        #    sound_bool[detector] = False
    else:
        warning_bool_new[detector] = False
        sound_bool[detector] = False
    warning_bool_prev = copy.deepcopy(warning_bool_new)
    print('warning_bool_new', warning_bool_new)
    print('sound_bool', sound_bool)
    if True in sound_bool.values():
        print('WARNING SOUND!!', sound_bool.values())
        os.system('aplay --device=hdmi --channels=3 ' + SOUND_1)
    return warning_bool_prev

