#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  8 09:53:31 2022

@author: francescaelverson
"""

import time, os, h5py
import matplotlib.pyplot as plt
import numpy as np
import shutil
from scipy.optimize import curve_fit
import sys
from scipy.ndimage import gaussian_filter
import copy
from datetime import datetime, timedelta
import matplotlib.dates as mdates
import matplotlib.gridspec as gridspec

# Get current date

COLLECT_DATA_TIME_NORM = (datetime.now()-timedelta(minutes = 1)).strftime('%Y-%m-%d %H:%M:%S')
print(COLLECT_DATA_TIME_NORM)
#COLLECT_DATA_TIME_NORM = "2022-06-08 12:05:00" # input date in format "YYYY-mm-dd HH:MM:SS" between quotation marks                                                                                                                           
COLLECT_DATA_TIME_EPOCH = 0000000000
if bool(COLLECT_DATA_TIME_NORM) == True:
    collect_data_time_object = datetime.strptime(COLLECT_DATA_TIME_NORM, '%Y-%m-%d %H:%M:%S')
    COLLECT_DATA_TIME_EPOCH = int(str(collect_data_time_object.timestamp())[0:10])


# Get golden trajectory for the proton bpms
gold_traject = {'proton_bpm_1': {'x': 1.1734,'y': -0.14 },
                'proton_bpm_2': {'x': 2.7482,'y': 0.8772},
                'proton_bpm_3': {'x': 2.5409 ,'y': 0.9618},
                'proton_bpm_4': {'x': 2.0826,'y': 0.4946 },
                'proton_bpm_5': {'x': 2.4254,'y':0.2912 }
                }

# Warning sound
#SOUND_1= '/user/awakeop/felverson/quick_tools/246332__kwahmah-02__five-beeps.wav' 
SOUND_1= '/user/awakeop/gzevi/sounds/central_c_piano_like.wav' 

# Directory path containing the h5 files; insert manually the relevant year
DATA_YEAR_PATH = '/user/awakeop/event_data/2022'

# Directory path where all historical graphics are stored; insert manually the relevant year
SAVE_PATH = "/user/awakeop/AwakeEventPreview/fran/events-preview/2022"

# File paths where the most recently updated preview graphics are stored
PREVIEW_PATHS = {'PLOT_FILE' : "/user/awakeop/AwakeEventPreview/fran/events-preview/img.pdf", # Mini diagnostics DQM (all detectors)
                 'MINI_IMGS_LASER_PATH' : "/user/awakeop/AwakeEventPreview/fran/events-preview/raw_imgs_laser.pdf", # Mini diagnostics DQM (laser-relevant detectors)
                 'TIMELINE_PROTONS_PATH' : "/user/awakeop/AwakeEventPreview/fran/events-preview/timeline-protons.pdf", # Proton BTV timeline (absolute trajectory),
                 'TIMELINE_LASER_PATH' : "/user/awakeop/AwakeEventPreview/fran/events-preview/timeline-laser.pdf", # Laser VLC timeline (absolute trajectory),
                 'TIMELINE_BPM_PROTONS_PATH' : "/user/awakeop/AwakeEventPreview/fran/events-preview/timeline-bpm-protons.pdf", # Proton BPM timeline (absolute trajectory),
                 'TIMELINE_BPM_ELECTRONS_PATH' : "/user/awakeop/AwakeEventPreview/fran/events-preview/timeline-bpm-electrons.pdf", # Electron BPM timeline (absolute trajectory),
                 'TIMELINE_PLASMA_PATH' : "/user/awakeop/AwakeEventPreview/fran/events-preview/timeline-plasma.pdf", # Plasma light timeline (absolute measurement),
                 'TIMELINE_AVERAGE_PROTONS_PATH' : "/user/awakeop/AwakeEventPreview/fran/events-preview/timeline-average-protons.pdf", #  Proton BTV timeline (average trajectory),
                 'TIMELINE_AVERAGE_LASER_PATH' : "/user/awakeop/AwakeEventPreview/fran/events-preview/timeline-average-laser.pdf", # Laser VLC timeline (average trajectory)
                 'TIMELINE_AVERAGE_BPM_PROTONS_PATH' : "/user/awakeop/AwakeEventPreview/fran/events-preview/timeline-average-bpm-protons.pdf", # Proton BPM timeline (average trajectory)
                 'TIMELINE_AVERAGE_BPM_ELECTRONS_PATH' : "/user/awakeop/AwakeEventPreview/fran/events-preview/timeline-average-bpm-electrons.pdf", # Electron BPM timeline (average trajectory)
                 'TIMELINE_AVERAGE_PLASMA_PATH' : "/user/awakeop/AwakeEventPreview/fran/events-preview/timeline-average-plasma.pdf", # Plasma light timeline (average measurement)
                 }

# Graphic font parameters
plt.style.use('dark_background')
SMALL_SIZE = 7
MEDIUM_SIZE = 9
BIGGER_SIZE = 10
plt.rc("font", size=SMALL_SIZE)
plt.rc("axes", titlesize=MEDIUM_SIZE) 
plt.rc("axes", labelsize=MEDIUM_SIZE) 
plt.rc("xtick", labelsize=SMALL_SIZE) 
plt.rc("ytick", labelsize=SMALL_SIZE) 
plt.rc("legend", fontsize=SMALL_SIZE) 
plt.rc("figure", titlesize=BIGGER_SIZE)
plt.style.use('dark_background')

start_time = [] 

file_timestamp = 0

def loop_events(update_interval = 5, ): 
    """
    The function plots a summary of individual events and the timeline plot from the most recently updated folder. The folder is updated every `update_interval=10` seconds, and new events are plotted if they appear. If the newest folder is created, it will be automatically considered for further updates.
    """  
    
    # Find the latest month and day
    newest_folder = find_lastest_subdirectory(DATA_YEAR_PATH) 
    
    # Get the most recent run number; include constraints on the h5 data (maybe include a try, except clause)
    run_number = max([det_run_number(i) for i in os.listdir(newest_folder)
                          if _isvalid(i)  and _good_data(i)]) 
    
    # Create empty save folder corresponding to the most recent day of data
    # (within a folder corresponding to the corresponding month i.e. SAVE_PATH/month/day/)
    save_folder = _merge_fold_paths(SAVE_PATH, newest_folder) 
    if not os.path.exists(save_folder): os.makedirs(save_folder)
    
    files_all = [] # Empty list to store all filenames in directory
    
    # Initialise dictionaries which will hold timeline data 
    timeline_data = constr_timeline_data()
    timeline_average = constr_timeline_data()
    proton_beam_size = constr_proton_beam_size() 
    proton_beam_size_average = constr_proton_beam_size() 
    
    counter = 0

    while True:
        tic = time.perf_counter() # Start performance counter
        # Find the most recent month, and day, of data collection
        newest_folder_now = find_lastest_subdirectory(DATA_YEAR_PATH)
        # Get the most recent run number
        try: 
            run_number_now = max([det_run_number(i) for i in os.listdir(newest_folder_now) 
                                     if _isvalid(i)  and  _good_data(i)])
        except: 
            run_number_now = False
            
        # Check if new HDF5 files correspond to a new run or a new day  
        # If so, redefine/update the save folders, latest day/run of data and reset timeline arrays and HDF5 files list 
        if (newest_folder_now != newest_folder) or (run_number_now != run_number):
            newest_folder = newest_folder_now 
            run_number = run_number_now 
            # Create new empty save folder if new HDF5 files correspond to a new day  
            save_folder = _merge_fold_paths(SAVE_PATH, newest_folder)
            if not os.path.exists(save_folder): os.makedirs(save_folder)
            files_all = [] # Empty list to store all filenames in directory
            # Initialise new dictionaries which will hold new timeline data 
            timeline_data = constr_timeline_data() 
            timeline_average = copy.deepcopy(timeline_data)
            proton_beam_size = constr_proton_beam_size() 
            proton_beam_size_average = copy.deepcopy(proton_beam_size)
            counter = 0

        # Get all filenames in directory
        files_all_new = [i for i in os.listdir(newest_folder)
                           if _isvalid(i)  and _good_data(i)] 
        # Get only newly added filenames in directory (sorted in time order)
        files_new = sorted(list(set(files_all_new).difference(set(files_all)))) 
        # Get paths for new files in directory
        files_new_path = [os.path.join(newest_folder, i) for i in files_new]

        # Print an update of the status of new HDF5 files
        print('New files detected ' + str(len(files_new_path)), flush=True) 
        t = time.localtime() 
        current_time = time.strftime("%H:%M:%S", t)
        print('Lastest update: ' + current_time,  flush=True)

        for path in files_new_path:
            event_data = get_event_data(path)  # Get the event data for new files
            if type(event_data) != bool: 
                print('HDF5 path is: ' + path)
                
                # Get timestamp of current file as 'end time'
                end_time = datetime.strptime(copy.deepcopy(event_data['time']), '%Y-%m-%d %H:%M:%S') 
                # 'Start time' corresponds to timestamp of first file of the current day,
                # or timestamp of first file of the current run, or 6 hours prior if data
                # was being collected then, (whichever is most recent)
                if path == files_new_path[0] and counter == 0:
                    start_time = datetime.strptime(copy.deepcopy(event_data['time']), '%Y-%m-%d %H:%M:%S')
                counter += 1
                if  end_time - start_time > timedelta(hours =6):
                    start_time = end_time - timedelta(hours =6) 
                  
                # Add event data to the timeline dictionaries    
                update_timeline(timeline_data, event_data)
                
                update_timeline_average(timeline_average, timeline_data, event_data)
                update_proton_beam_size(event_data, proton_beam_size)
                        
                update_proton_beam_size_average(proton_beam_size_average, event_data, proton_beam_size)
                
                # Create the mini diagnostics DQM graphic for the current event, and store with unique HDF5 reference
                save_figure_path =  os.path.join(save_folder, os.path.split(path)[-1][0:-3] + str("-preview.pdf"))
                plot_event_data(event_data, save_figure_path, closeplot=True, store=True) 
                
                if path == files_new_path[-1]: 
                    
                    # Play warning sound if there are any errors with the data
                    warning_bool = []
                    for detector in ('btv_50',
                                     'btv_53', 
                                     'btv_54', 
                                     'btv_26', 
                                     'laser_3',
                                     'laser_4', 
                                     'laser_5', 
                                     'is1_core', 
                                     'is1_halo', 
                                     'is2_core',
                                     'is2_halo',
                                     #'dmd_1', 
                                     'dmd_2',
                                     'exp_vol_1',
                                     'streak_1', 
                                     # 'streak_2', 
                                     'AWAKECAM_05',
                                     #'spectro'
                                     ):
                        if event_data[detector]['excep'] == True or event_data[detector]['timediff']>10:
                            warning_bool.append(True)
                        else:
                            warning_bool.append(False)
                    if True in warning_bool:
                        #os.system('aplay --device=hdmi --channels=3 ' + SOUND_1)
                        os.system('aplay ' + SOUND_1)

                    # Plot the event data and the timeline only for the last event                    
                    save_paths = {'save_preview_laser':[],
                                  'save_timeline_protons':[], 'save_timeline_laser':[],'save_timeline_protons_bpm':[],
                                  'save_timeline_electrons_bpm':[],'save_timeline_plasma':[],
                                  'save_average_timeline_protons':[], 'save_average_timeline_laser':[], 'save_average_timeline_protons_bpm':[],
                                  'save_average_timeline_electrons_bpm':[],'save_average_timeline_plasma':[], 
                                   }
                    for path in save_paths: 
                        if bool(COLLECT_DATA_TIME_EPOCH) == False:
                            save_paths[path] = os.path.join(save_folder, "Run-" + str(run_number_now) + '-'.join(path.split('_')[1:])+'.pdf')
                        if bool(COLLECT_DATA_TIME_EPOCH) == True:
                            save_paths[path] = os.path.join(save_folder, "Run-" + str(run_number_now)+'-'+ COLLECT_DATA_TIME_NORM[-8:] +'-' + '-'.join(path.split('_')[1:])+'.pdf')

                    # Create the mini diagnostics DQM graphic for the current event, and store with unique HDF5 reference in save folder
                    
                    # Create the timeline DQM graphics for the current event, and store with general run number reference in save folder 
                    plot_mini_imgs_laser(event_data, save_paths['save_preview_laser'], closeplot=True, store=True) 
                    print('About to save all timeline files')
                    plot_timeline_data_protons(timeline_data, proton_beam_size, closeplot=True, store=True, savepath=save_paths['save_timeline_protons'], starttime = start_time, endtime = end_time) 
                    plot_timeline_data_laser(timeline_data, closeplot=True, store=True, savepath=save_paths['save_timeline_laser'], starttime = start_time, endtime = end_time)                    
                    plot_timeline_bpm_protons(timeline_data, closeplot=True, store=True,  savepath=save_paths['save_timeline_protons_bpm'], starttime = start_time, endtime = end_time)                    
                    #plot_timeline_bpm_electrons(timeline_data, closeplot=True, store=True, savepath=save_paths['save_timeline_electrons_bpm'], starttime = start_time, endtime = end_time)                    
                    #plot_timeline_plasma(timeline_data, closeplot=True, store=True, savepath=save_timeline_path_plasma, starttime = start_time, endtime = end_time) 
                    #plot_timeline_average_protons(timeline_data, timeline_average, proton_beam_size_average, closeplot=True, store=True, savepath=save_paths['save_average_timeline_protons'], starttime = start_time, endtime = end_time)
                    #plot_timeline_average_laser(timeline_data, timeline_average, closeplot=True, store=True, savepath=save_paths['save_average_timeline_laser'], starttime = start_time, endtime = end_time)
                    
                    plot_timeline_average_bpm_protons(timeline_data, timeline_average, closeplot=True, store=True, savepath=save_paths['save_average_timeline_protons_bpm'], starttime = start_time, endtime = end_time)
                    plot_timeline_average_bpm_electrons(timeline_data, timeline_average, closeplot=True, store=True, savepath=save_paths['save_average_timeline_electrons_bpm'], starttime = start_time, endtime = end_time)
                    plot_timeline_average_plasma(timeline_data, timeline_average, closeplot=True, store=True, savepath=save_paths['save_average_timeline_plasma'], starttime = start_time, endtime = end_time)   
        
                    # Copy stored figures to most recent event preview folder
                    print('About to copy images to events-preview')
                    # Mini imgs DQM
                    shutil.copyfile(save_figure_path, PREVIEW_PATHS['PLOT_FILE'])
                    # Remainder of DQM pdfs
                    for paths in [
                            ('save_preview_laser', 'MINI_IMGS_LASER_PATH'),
                            ('save_timeline_protons','TIMELINE_PROTONS_PATH'), 
                            ('save_timeline_laser', 'TIMELINE_LASER_PATH'), 
                            ('save_timeline_protons_bpm', 'TIMELINE_BPM_PROTONS_PATH'),
                            # ('save_timeline_electrons_bpm', 'TIMELINE_BPM_ELECTRONS_PATH'), 
                            # ('save_average_timeline_protons', 'TIMELINE_AVERAGE_PROTONS_PATH'),
                            ('save_average_timeline_plasma', 'TIMELINE_AVERAGE_PLASMA_PATH'),
                            # ('save_average_timeline_laser', 'TIMELINE_AVERAGE_LASER_PATH'),
                            ('save_average_timeline_protons_bpm', 'TIMELINE_AVERAGE_BPM_PROTONS_PATH'), 
                            ('save_average_timeline_electrons_bpm', 'TIMELINE_AVERAGE_BPM_ELECTRONS_PATH')
                    ]:
                        shutil.copyfile(save_paths[paths[0]], PREVIEW_PATHS[paths[1]])
            
        files_all.extend(files_new) # Add on the new files to the previous file array
        toc = time.perf_counter() # End performance counter
        print(tic, toc, update_interval,  toc - tic , update_interval - toc + tic)
        if toc - tic < update_interval: # Search for new HDF5 files at most every 5 seconds
            time.sleep(update_interval - toc + tic)
    return True 

 
def _isvalid(ename):
    """
    Set restrictions on H5 file names
    """
    return bool((ename.split('_')[1] == 'Type0')* (ename[-2:] == 'h5'))# * (int(ename[:10])<1654683906))
def _good_data(ename):
    """
    Find H5 files which have a timestamp after the reference timestamp
    """
    return bool((int(ename[0:10])) >= COLLECT_DATA_TIME_EPOCH)

def _isrunid(ename, runid):
    return bool( (det_run_number(ename) == runid) )


def get_event_data(path):
    """
    Get data from all needed diagnostics. Can be extended with additional instances if needed.
    """
    global file_timestamp

    try:
        f = h5py.File(path,'r')
        file_timestamp = get_timestamp(f)[0]
        edata = {

            'timestamp' : get_timestamp(f)[0],
            'time' : get_timestamp(f)[1],
            'btv_50' : get_btv_50_pxi(f),
            'btv_53' : get_btv_53_analogue(f),
            'btv_54' : get_btv_54_pxi(f),
            'btv_26' : get_btv_26_analogue(f),
            'is1_core' : get_btv_is1_core_pxi(f),
            'is1_halo' : get_btv_is1_halo_pxi(f),
            'is2_core' : get_btv_is2_core_pxi(f),
            'is2_halo' : get_btv_is2_halo_pxi(f),
            'dmd_1' : get_dmd_1_pxi(f),
            'dmd_2' : get_dmd_2_pxi(f),
            # 'otr_angle': get_otr_angle_pxi(f),
            'exp_vol_1': get_exp_vol_1_pxi(f),
            'streak_1' : get_streak_1_analogue(f),
            'streak_2' : get_streak_2_analogue(f),
            'AWAKECAM_05' : get_MPP_streak_pxi(f),
            'laser_1' : get_laser_camera_1_pxi(f),
            'laser_2' : get_laser_camera_2_pxi(f),
            'laser_3' : get_laser_camera_3_pxi(f),
            'laser_4' : get_laser_camera_4_pxi(f),
            'laser_5' : get_laser_camera_5_pxi(f),
            'spectro' : get_spectrometer(f),
            'bpm_electron_1' : get_bpm_electron_1(f),
            'bpm_electron_2' : get_bpm_electron_2(f),
            'bpm_proton_1' : get_bpm_proton_1(f),
            'bpm_proton_2' : get_bpm_proton_2(f),
            'bpm_proton_3' : get_bpm_proton_3(f),
            'bpm_proton_4' : get_bpm_proton_4(f),
            'bpm_proton_5' : get_bpm_proton_5(f),
            'emeter_02' : get_emeter_2(f),
            'emeter_03' : get_emeter_3(f),
            'emeter_04' : get_emeter_4(f),
            'emeter_05' : get_emeter_5(f),
            'plasma_light' : get_plasma_light(f),
            'proton_charge' : get_proton_charge(f),
            'population' : get_bunch_population(f),
            'enumber' : get_event_number(f), 
        }
        
        f.close()
        
    except:

        edata = False

    return edata

def constr_timeline_data():
    """
    Dictionary constructor for the timeline data
    """
    
    return {
        'btv_50' : {'x': np.zeros(1), 'y': np.zeros(1), 'time' : np.zeros(1) },
        'btv_53' : {'x': np.zeros(1), 'y': np.zeros(1), 'time' : np.zeros(1) },
        'btv_54' : {'x': np.zeros(1), 'y': np.zeros(1),'time' : np.zeros(1), },
        'btv_26' : {'x': np.zeros(1), 'y': np.zeros(1), 'time' : np.zeros(1), },
        'laser_3' : {'x': np.zeros(1), 'y': np.zeros(1),'time' : np.zeros(1), },
        'laser_4' : {'x': np.zeros(1), 'y': np.zeros(1),'time' : np.zeros(1), },
        'laser_5' : {'x': np.zeros(1), 'y': np.zeros(1),'time' : np.zeros(1), },
        'is1_core' : {'x': np.zeros(1), 'y': np.zeros(1),'time' : np.zeros(1), },
        'is1_halo' : {'x': np.zeros(1), 'y': np.zeros(1),'time' : np.zeros(1), },
        'is2_core' : {'x': np.zeros(1), 'y': np.zeros(1),'time' : np.zeros(1), },
        'is2_halo' : {'x': np.zeros(1), 'y': np.zeros(1),'time' : np.zeros(1), },
        'bpm_electron_1': {'hor': np.zeros(1), 'ver': np.zeros(1), 'time' : np.zeros(1)},
        'bpm_electron_2': {'hor': np.zeros(1), 'ver': np.zeros(1), 'time' : np.zeros(1)},
        'bpm_proton_1': {'hor': np.zeros(1), 'ver': np.zeros(1), 'time' : np.zeros(1)},
        'bpm_proton_2': {'hor': np.zeros(1), 'ver': np.zeros(1), 'time' : np.zeros(1)},
        'bpm_proton_3': {'hor': np.zeros(1), 'ver': np.zeros(1), 'time' : np.zeros(1)},
        'bpm_proton_4': {'hor': np.zeros(1), 'ver': np.zeros(1), 'time' : np.zeros(1)},
        'bpm_proton_5': {'hor': np.zeros(1), 'ver': np.zeros(1), 'time' : np.zeros(1)},
        'AWAKECAM_05' : {'x': np.zeros(1), 'y': np.zeros(1),'time' : np.zeros(1), },
        #'otr_angle' : {'x': np.zeros(1), 'y': np.zeros(1),'time' : np.zeros(1), },
        'population' : {'pop': np.zeros(1), 'time': np.zeros(1), },
        'emeter_02' : {'energy' : np.zeros(1), 'time' : np.zeros(1), },
        'emeter_03' : {'energy' : np.zeros(1), 'time' : np.zeros(1), },
        'emeter_04' : {'energy' : np.zeros(1), 'time' : np.zeros(1), },
        'emeter_05' : {'energy' : np.zeros(1), 'time' : np.zeros(1), },
        'plasma_light' : {'upstream': np.zeros(1), 'downstream' : np.zeros(1), 'time' : np.zeros(1), },
        'proton_charge' :{'charge' : np.zeros(1), 'time' : np.zeros(1), }

    }

def update_timeline(timeline_data, eventdata):
    """
    Append the data from `eventdata` to `timeline_data`
    """
    # N.B: set restictions here on the data quality thresholds for each detector
    # i.e. proton population, ensuring that the shot is not late

    #cam_list = ['btv_50', 'btv_53', 'btv_54', 'btv_26', 'is1_core', 'is1_halo', 'is2_core', 'is2_halo']

    # timeline is1_core

    dttmp = eventdata['is1_core']['data']
    timeline_tmp = copy.deepcopy(timeline_data['is1_core'])
    if (np.shape(dttmp) != (1,0)) & (eventdata['is1_core']['timediff'] < 10) & (eventdata['population'] > 0.0) :
        centr_params = def_centroid(dttmp[100:200, 150:250]) 
        if np.prod(np.invert(np.isnan(centr_params))):
            tmp_1 = np.append(timeline_tmp['x'], np.array(centr_params[0]) )
            tmp_2 = np.append(timeline_tmp['y'], np.array(centr_params[1]) )
            timeadd = np.append(timeline_tmp['time'],datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S'))
            timeline_tmp.update({'x' : tmp_1})
            timeline_tmp.update({'y' : tmp_2})
            timeline_tmp.update({'time': timeadd})
        timeline_data.update({'is1_core' : timeline_tmp})
    
    # timeline is2_core

    dttmp = eventdata['is2_core']['data']
    timeline_tmp = copy.deepcopy(timeline_data['is2_core'])
    if (np.shape(dttmp) != (1,0)) & (eventdata['is2_core']['timediff'] < 10) & (eventdata['population'] > 0.0):
        centr_params = def_centroid(dttmp[100:200, 150:250])
        if np.prod(np.invert(np.isnan(centr_params))):
            tmp_1 = np.append(timeline_tmp['x'], np.array(centr_params[0]) )
            tmp_2 = np.append(timeline_tmp['y'], np.array(centr_params[1]) )
            timeadd = np.append(timeline_tmp['time'],datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S') )  
            timeline_tmp.update({'x' : tmp_1})
            timeline_tmp.update({'y' : tmp_2})
            timeline_tmp.update({'time': timeadd})
        timeline_data.update({'is2_core' : timeline_tmp})
    
    # timeline btv_54

    dttmp = eventdata['btv_54']['data']
    timeline_tmp = copy.deepcopy(timeline_data['btv_54'])
    if (np.shape(dttmp) != (1,0)) & (eventdata['btv_54']['timediff'] < 10) & (eventdata['population'] > 0.0):
        centr_params = def_centroid(dttmp)#dttmp[180:380, 200:400]) 
        if np.prod(np.invert(np.isnan(centr_params))):
            tmp_1 = np.append(timeline_tmp['x'], np.array(centr_params[0]) )
            tmp_2 = np.append(timeline_tmp['y'], np.array(centr_params[1]) )
            timeadd = np.append(timeline_tmp['time'],datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S') )
            timeline_tmp.update({'x' : tmp_1})
            timeline_tmp.update({'y' : tmp_2})
            timeline_tmp.update({'time': timeadd})
        timeline_data.update({'btv_54' : timeline_tmp})
    
    #timeline laser_3

    dttmp = eventdata['laser_3']['data']
    timeline_tmp = copy.deepcopy(timeline_data['laser_3'])
    if (np.shape(dttmp) != (1,0)) & (eventdata['laser_3']['timediff'] < 10)  :
        centr_params = def_centroid(dttmp)
        if np.prod(np.invert(np.isnan(centr_params))):
            tmp_1 = np.append(timeline_tmp['x'], np.array(centr_params[0]) )
            tmp_2 = np.append(timeline_tmp['y'], np.array(centr_params[1]) )
            timeadd = np.append(timeline_tmp['time'],datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S') )
            timeline_tmp.update({'x' : tmp_1})
            timeline_tmp.update({'y' : tmp_2})
            timeline_tmp.update({'time': timeadd})
        timeline_data.update({'laser_3' : timeline_tmp})

    #timeline laser_4
        
    dttmp = eventdata['laser_4']['data']
    timeline_tmp = copy.deepcopy(timeline_data['laser_4'])
    if (np.shape(dttmp) != (1,0)) & (eventdata['laser_4']['timediff'] < 10)  :
        centr_params = def_centroid(dttmp)
        if np.prod(np.invert(np.isnan(centr_params))):
            tmp_1 = np.append(timeline_tmp['x'], np.array(centr_params[0]) )
            tmp_2 = np.append(timeline_tmp['y'], np.array(centr_params[1]) )
            timeadd = np.append(timeline_tmp['time'],datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S'))
            timeline_tmp.update({'x' : tmp_1})
            timeline_tmp.update({'y' : tmp_2}) 
            timeline_tmp.update({'time': timeadd})
        timeline_data.update({'laser_4' : timeline_tmp})
    
    #timeline laser_5
     
    dttmp = eventdata['laser_5']['data']
    timeline_tmp = copy.deepcopy(timeline_data['laser_5'])
    if (np.shape(dttmp) != (1,0)) & (eventdata['laser_5']['timediff'] < 10)  :
        centr_params = def_centroid(dttmp)
        if np.prod(np.invert(np.isnan(centr_params))):
            tmp_1 = np.append(timeline_tmp['x'], np.array(centr_params[0]) )
            tmp_2 = np.append(timeline_tmp['y'], np.array(centr_params[1]) )
            timeadd = np.append(timeline_tmp['time'], datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S') )
            timeline_tmp.update({'x' : tmp_1})
            timeline_tmp.update({'y' : tmp_2})
            timeline_tmp.update({'time': timeadd})
        timeline_data.update({'laser_5' : timeline_tmp})

    # timeline bpm proton 1

    dttmp = eventdata['bpm_proton_1']['data']
    if np.isfinite(dttmp['hor']):
        timeline_tmp = copy.deepcopy(timeline_data['bpm_proton_1'])
        tmp_1 = np.append(timeline_tmp['hor'],np.array(dttmp['hor']))
        tmp_2 = np.append(timeline_tmp['ver'],np.array(dttmp['ver']))
        timeadd = np.append(timeline_tmp['time'], datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S'))
        timeline_tmp.update({'hor' : tmp_1})
        timeline_tmp.update({'ver' : tmp_2})
        timeline_tmp.update({'time': timeadd})
        timeline_data.update({'bpm_proton_1' : timeline_tmp})
 

    # timeline bpm proton 2

    dttmp = eventdata['bpm_proton_2']['data']
    timeline_tmp = copy.deepcopy(timeline_data['bpm_proton_2'])
    if np.isfinite(dttmp['hor']):
        timeline_tmp = copy.deepcopy(timeline_data['bpm_proton_2'])
        tmp_1 = np.append(timeline_tmp['hor'],np.array(dttmp['hor']))
        tmp_2 = np.append(timeline_tmp['ver'],np.array(dttmp['ver']))
        timeadd = np.append(timeline_tmp['time'], datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S'))
        timeline_tmp.update({'hor' : tmp_1})
        timeline_tmp.update({'ver' : tmp_2})
        timeline_tmp.update({'time': timeadd})
        timeline_data.update({'bpm_proton_2' : timeline_tmp})
   

    # timeline bpm proton 3

    dttmp = eventdata['bpm_proton_3']['data']
    timeline_tmp = copy.deepcopy(timeline_data['bpm_proton_3'])
    if np.isfinite(dttmp['hor']):
        tmp_1 = np.append(timeline_tmp['hor'],np.array(dttmp['hor']))
        tmp_2 = np.append(timeline_tmp['ver'],np.array(dttmp['ver']))
        timeadd = np.append(timeline_tmp['time'], datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S'))
        timeline_tmp.update({'hor' : tmp_1})
        timeline_tmp.update({'ver' : tmp_2})
        timeline_tmp.update({'time': timeadd})
        timeline_data.update({'bpm_proton_3' : timeline_tmp})
  

    # timeline bpm proton 4

    dttmp = eventdata['bpm_proton_4']['data']
    timeline_tmp = copy.deepcopy(timeline_data['bpm_proton_4'])
    if np.isfinite(dttmp['hor']):
        tmp_1 = np.append(timeline_tmp['hor'],np.array(dttmp['hor']))
        tmp_2 = np.append(timeline_tmp['ver'],np.array(dttmp['ver']))
        timeadd = np.append(timeline_tmp['time'], datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S'))
        timeline_tmp.update({'hor' : tmp_1})
        timeline_tmp.update({'ver' : tmp_2})
        timeline_tmp.update({'time': timeadd})
        timeline_data.update({'bpm_proton_4' : timeline_tmp})


    # timeline bpm proton 5

    dttmp = eventdata['bpm_proton_5']['data']
    timeline_tmp = copy.deepcopy(timeline_data['bpm_proton_5'])
    if np.isfinite(dttmp['hor']):
        tmp_1 = np.append(timeline_tmp['hor'],np.array(dttmp['hor']))
        tmp_2 = np.append(timeline_tmp['ver'],np.array(dttmp['ver']))
        timeadd = np.append(timeline_tmp['time'], datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S'))
        timeline_tmp.update({'hor' : tmp_1})
        timeline_tmp.update({'ver' : tmp_2})
        timeline_tmp.update({'time': timeadd})
        timeline_data.update({'bpm_proton_5' : timeline_tmp})
        
        
    # timeline bpm electron 1

    dttmp = eventdata['bpm_electron_1']['data']
    timeline_tmp = copy.deepcopy(timeline_data['bpm_electron_1'])
    tmp_1 = np.append(timeline_tmp['hor'],np.array(dttmp['hor']))
    tmp_2 = np.append(timeline_tmp['ver'],np.array(dttmp['ver']))
    timeadd = np.append(timeline_tmp['time'], datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S'))
    timeline_tmp.update({'hor' : tmp_1})
    timeline_tmp.update({'ver' : tmp_2})
    timeline_tmp.update({'time': timeadd})
    timeline_data.update({'bpm_electron_1' : timeline_tmp})

    # timeline bpm electron 2

    dttmp = eventdata['bpm_electron_2']['data']
    timeline_tmp = copy.deepcopy(timeline_data['bpm_electron_2'])
    tmp_1 = np.append(timeline_tmp['hor'],np.array(dttmp['hor']))
    tmp_2 = np.append(timeline_tmp['ver'],np.array(dttmp['ver']))
    timeadd = np.append(timeline_tmp['time'], datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S'))
    timeline_tmp.update({'hor' : tmp_1})
    timeline_tmp.update({'ver' : tmp_2})
    timeline_tmp.update({'time': timeadd})
    timeline_data.update({'bpm_electron_2' : timeline_tmp})


    # timeline emeter 2

    dttmp = eventdata['emeter_02']['energy']
    timeline_tmp = copy.deepcopy(timeline_data['emeter_02'])
    if np.isfinite(dttmp):
        tmp = np.append(timeline_tmp['energy'],np.array(dttmp))
        timeadd = np.append(timeline_tmp['time'], datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S'))
        timeline_tmp.update({'energy' : tmp})
        timeline_tmp.update({'time': timeadd})
        timeline_data.update({'emeter_02' : timeline_tmp})

    # timeline emeter 3

    dttmp = eventdata['emeter_03']['energy']
    timeline_tmp = copy.deepcopy(timeline_data['emeter_03'])
    if np.isfinite(dttmp):
        tmp = np.append(timeline_tmp['energy'],np.array(dttmp))
        timeadd = np.append(timeline_tmp['time'], datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S'))
        timeline_tmp.update({'energy' : tmp})
        timeline_tmp.update({'time': timeadd})
        timeline_data.update({'emeter_03' : timeline_tmp})

    # timeline emeter 4

    dttmp = eventdata['emeter_04']['energy']
    timeline_tmp = copy.deepcopy(timeline_data['emeter_04'])
    if np.isfinite(dttmp):
        tmp = np.append(timeline_tmp['energy'],np.array(dttmp))
        timeadd = np.append(timeline_tmp['time'], datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S'))
        timeline_tmp.update({'energy' : tmp})
        timeline_tmp.update({'time': timeadd})
        timeline_data.update({'emeter_04' : timeline_tmp})

    # timeline emeter 5

    dttmp = eventdata['emeter_05']['energy']
    timeline_tmp = copy.deepcopy(timeline_data['emeter_05'])
    if np.isfinite(dttmp):
        tmp = np.append(timeline_tmp['energy'],np.array(dttmp))
        timeadd = np.append(timeline_tmp['time'], datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S'))
        timeline_tmp.update({'energy' : tmp})
        timeline_tmp.update({'time': timeadd})
        timeline_data.update({'emeter_05' : timeline_tmp})
    
    # timeline plasma light

    dttmp = eventdata['plasma_light']['point']
    timeline_tmp = copy.deepcopy(timeline_data['plasma_light'])
   
    tmp_1 = np.append(timeline_tmp['upstream'],np.array(dttmp['upstream']))
    tmp_2 = np.append(timeline_tmp['downstream'],np.array(dttmp['downstream']))
    timeadd = np.append(timeline_tmp['time'], datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S'))
    timeline_tmp.update({'upstream' : tmp_1})
    timeline_tmp.update({'downstream' : tmp_2})
    timeline_tmp.update({'time': timeadd})
    timeline_data.update({'plasma_light' : timeline_tmp})

    # timeline Awake Cam 05

    dttmp = eventdata['AWAKECAM_05']['data']
    timeline_tmp = copy.deepcopy(timeline_data['AWAKECAM_05'])
    if (np.shape(dttmp) != (1,0)) & (eventdata['AWAKECAM_05']['timediff'] < 10) & (eventdata['population'] > 0.0):
        centr_params = def_centroid(dttmp)
        if np.prod(np.invert(np.isnan(centr_params))):
            tmp_1 = np.append(timeline_tmp['x'], np.array(centr_params[0]) )
            tmp_2 = np.append(timeline_tmp['y'], np.array(centr_params[1]) )
            timeadd = np.append(timeline_tmp['time'],datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S') )
            timeline_tmp.update({'x' : tmp_1})
            timeline_tmp.update({'y' : tmp_2})
            timeline_tmp.update({'time': timeadd})
        timeline_data.update({'AWAKECAM_05' : timeline_tmp})

    # timeline proton beam intensity
    
    timeline_tmp = copy.deepcopy(timeline_data)['population']
    if type(dttmp) is not bool:
        tmp = np.append(timeline_tmp['pop'], np.array(eventdata['population']))
        timeadd = np.append(timeline_tmp['time'], datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S'))
    else:
        tmp = np.append(timeline_tmp['pop'], np.array(np.nan))
        timeadd = np.append(timeline_tmp['time'], np.array(np.nan))
    timeline_data['population'].update({'pop' : tmp})
    timeline_data['population'].update({'time': timeadd})

    dttmp = eventdata['proton_charge']['charge']
    timeline_tmp = copy.deepcopy(timeline_data['proton_charge'])
    if np.size(dttmp) > 0:
        if np.isfinite(dttmp):
            tmp = np.append(timeline_tmp['charge'], np.array(dttmp))
            timeadd = np.append(timeline_tmp['time'],datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S') )
            timeline_tmp.update({'charge' : tmp})
            timeline_tmp.update({'time': timeadd})
            timeline_data.update({'proton_charge' : timeline_tmp})



def update_timeline_average(timeline_average, timeline_data, eventdata):    
    
    # N.B: timeline_data is zeroed at the start

    sample_size = 20 
    for detector, data in timeline_data.items():
    
        detector_name = detector # redundant 

        if detector == 'population': 
            timeline_average['population'] = timeline_data['population']    

        elif detector.startswith('bpm'):

            total_number_events = data['hor'].size

            # Only update if there is a new entry on the raw data timeline array
            if timeline_data[detector_name]['time'][-1] != timeline_average[detector_name]['time'][-1]:

                end_index = total_number_events # Index of most recent event
                start_index = end_index - sample_size # Index of event used as the start of the average
                
                if total_number_events < 21: # Wait until we have 20 non-zeroed data points before averaging - copy the raw data for now
                    timeline_average[detector_name] = copy.deepcopy(timeline_data[detector_name])
                
                if total_number_events > 20: # Once we have 20 non-zeroed data points - start averaging
                    timeline_tmp = copy.deepcopy(timeline_data[detector_name])
                    timeline_avg = copy.deepcopy(timeline_average[detector_name])
                    timeline_hor = timeline_tmp['hor']
                    timeline_ver = timeline_tmp['ver']
                    sample_data_hor = timeline_hor[start_index:end_index] # Slice out 20 'x' events to average over
                    sample_data_ver = timeline_ver[start_index:end_index] # Slice out 20 'y' events to average over
                    average_hor = np.mean(sample_data_hor) # Average the slice to form a new data point 
                    average_ver = np.mean(sample_data_ver) # Average the slice to form a new data point
                    tmp_1 = np.append(timeline_avg['hor'], average_hor)
                    tmp_2 = np.append(timeline_avg['ver'], average_ver) 
                    timeline_tmp.update({'hor' : tmp_1})
                    timeline_tmp.update({'ver' : tmp_2})
                    timeline_average.update({detector_name : timeline_tmp})

        elif detector.startswith('emeter') or  detector == 'proton_charge':

            if detector.startswith('emeter'):
                data_1D = 'energy'
            if detector == 'proton_charge':
                data_1D = 'charge'
    
            total_number_events = data[data_1D].size

            if timeline_data[detector_name]['time'][-1] != timeline_average[detector_name]['time'][-1]:

                end_index = total_number_events # Index of most recent event
                start_index = end_index - sample_size # Index of event used as the start of the average 
                
                if total_number_events < 21:# Wait until we have 20 non-zeroed data points before averaging - copy the raw data for now  
                    timeline_average[detector_name] = copy.deepcopy(timeline_data[detector_name])
                
                if total_number_events > 20: # Once we have 20 non-zeroed data points - start averaging 
                    timeline_tmp = copy.deepcopy(timeline_data[detector_name]) 
                    timeline_avg = copy.deepcopy(timeline_average[detector_name])
                    timeline_energy = timeline_tmp[data_1D]
                    sample_data_energy = timeline_energy[start_index:end_index] # Slice out 20 events to average over
                    average_energy = np.mean(sample_data_energy) # Average the slice to form a new data point 
                    tmp = np.append(timeline_avg[data_1D], average_energy) 
                    timeline_tmp.update({data_1D : tmp})
                    timeline_average.update({detector_name : timeline_tmp})

        else:
            try: 
                if detector == 'plasma_light':
                    x = 'upstream'
                    y = 'downstream'
                # BTVs, IS, Laser
                else:
                    x = 'x'
                    y = 'y'
        
                total_number_events = data[x].size
    
                if timeline_data[detector_name]['time'][-1] != timeline_average[detector_name]['time'][-1]:
    
                    end_index = total_number_events
                    start_index = end_index - sample_size
                    
                    if total_number_events < 21:
                        timeline_average[detector_name] = copy.deepcopy(timeline_data[detector_name])
                    
                    if total_number_events > 20:
                        timeline_tmp = copy.deepcopy(timeline_data[detector_name])
                        timeline_avg = copy.deepcopy(timeline_average[detector_name])
                        timeline_x = timeline_tmp[x]
                        timeline_y = timeline_tmp[y]
                        sample_data_x = timeline_x[start_index:end_index] 
                        sample_data_y = timeline_y[start_index:end_index] 
                        average_x = np.mean(sample_data_x)
                        average_y = np.mean(sample_data_y) 
                        tmp_1 = np.append(timeline_avg[x], average_x)
                        tmp_2 = np.append(timeline_avg[y], average_y) 
                        timeline_tmp.update({x : tmp_1})
                        timeline_tmp.update({y : tmp_2})
                        timeline_average.update({detector_name : timeline_tmp})
            except:
                pass



def Gauss(x, a, x0, sigma, p):
    return a * np.exp(-(x - x0)**2 / (2 * sigma**2)) + p

def fit_gaussian(xdata, ydata): # xdata are 1D coordinates, ydata is rough gaussian

    mean = np.nansum(xdata * ydata) / np.nansum(ydata) # average coord using weighted coords
    sigma = np.sqrt(np.nansum(ydata * (xdata - mean)**2) / np.nansum(ydata))

    try:
        popt, pcov = curve_fit(Gauss, xdata, ydata, p0=[np.nanmax(ydata), mean, sigma, 0.2])
        
        if (popt[1] > len(ydata)) or (popt[1] <= 0) or (np.abs(popt[2]) > 0.5 * len(ydata)):
            popt = [np.nan, np.nan, np.nan, np.nan]
            
    except :
        popt = [np.nan, np.nan, np.nan, np.nan]

    return popt

def def_centroid(data):
    """
    Fit centroid to the data from IS1 Core. Customize certain data range for a better fit if needed.
    """

    xpr = np.nansum(data, 0) #sum of data points vertically (y direction) stored in horizontal array for sum of each column
    xpr = xpr / np.nanmax(xpr) # normalising to most populated column to form 1D gaussian expressing vertical density of points
    xpr = xpr
    best_parm_x = fit_gaussian(np.arange(len(xpr)), xpr)[1] 

    ypr = np.nansum(data, 1) #sum of data points horizontally (x direction) stored in horizontal array for sum of each row
    ypr = ypr / np.nanmax(ypr) # normalising to most populated row to form 1D gaussian expressing horizontal density of points
    ypr = ypr
    best_parm_y = fit_gaussian(np.arange(len(ypr)), ypr)[1]

    return (best_parm_x, best_parm_y)



def constr_proton_beam_size():
    
    return {
        'is1_core' : {'x': np.zeros(1), 'y' : np.zeros(1), 'time' : np.zeros(1)},
        'is2_core' : {'x': np.zeros(1), 'y' : np.zeros(1), 'time' : np.zeros(1)}
    }

def update_proton_beam_size(eventdata, proton_beam_size):
    """
    
    """

    for thing in eventdata.items():

        detector_name = thing[0]

        if detector_name.startswith('is') and detector_name[-4:] == 'core':

                if (np.shape(eventdata[detector_name]['data']) != (1,0)) & (eventdata[detector_name]['timediff'] < 10) & (eventdata['population'] > 0.0) :
                    if detector_name == 'is1_core':
                        centr_params = def_centroid(eventdata[detector_name]['data'][100:200, 150:250]) 
                    if detector_name == 'is2_core':
                        centr_params = def_centroid(eventdata[detector_name]['data'][100:200, 150:250])#what happens if the if statement does not apply - what is centr_params then?
                    if np.prod(np.invert(np.isnan(centr_params))): #returns true if equal to one (all central params isnan equal to false)

                        is_data= copy.deepcopy(eventdata[detector_name]['data'])
                        listsizes = copy.deepcopy(proton_beam_size[detector_name])
                        
                        xpr = np.nansum(is_data, 0) #sum of data points vertically (y direction) stored in horizontal array for sum of each column
                        xpr = xpr / np.nanmax(xpr)  # normalising
                        meanx = np.nansum(np.arange(len(xpr)) * xpr ) / np.nansum(xpr) #find  std deviation in x direction
                        sigmax = np.sqrt(np.nansum(xpr * (np.arange(len(xpr)) - meanx)**2) / np.nansum(xpr))
                
                        ypr = np.nansum(is_data, 1) #sum of data points vertically (y direction) stored in horizontal array for sum of each column
                        ypr = ypr / np.nanmax(ypr)  #normalising
                        meany = np.nansum(np.arange(len(ypr)) * ypr) / np.nansum(ypr) #find std deviation in y direction
                        sigmay = np.sqrt(np.nansum(ypr * (np.arange(len(ypr)) - meany)**2) / np.nansum(ypr))

                        listsizesx = np.append(proton_beam_size[detector_name]['x'],sigmax) #np array
                        listsizesy = np.append(proton_beam_size[detector_name]['y'],sigmay) #np array
                        listsizes.update({'x' : listsizesx})
                        listsizes.update({'y' : listsizesy})
                        timeadd = np.append(proton_beam_size[detector_name]['time'],datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S') )
                        listsizes.update({'time': timeadd})

                        proton_beam_size.update({detector_name : listsizes})

def update_proton_beam_size_average(proton_beam_size_average, eventdata, proton_beam_size):

    sample_size = 20

    for thing in eventdata.items():

        detector_name = thing[0]

        if detector_name.startswith('is') and detector_name[-4:] == 'core':

            if proton_beam_size[detector_name]['time'][-1] != proton_beam_size_average[detector_name]['time'][-1]:
        
                total_number_events = proton_beam_size[detector_name]['x'].size
                end_index = total_number_events # last event stored in timeline data array
                start_index = end_index - sample_size # first event used for average stored in timeline data array
                
                if total_number_events < 21: #wait until we have 20 data points before averaging
                    proton_beam_size_average[detector_name] = copy.deepcopy(proton_beam_size[detector_name])
                
                if total_number_events > 20: # use averaged value instead once we have 20 data points
                    timeline_tmp = copy.deepcopy(proton_beam_size[detector_name])
                    timeline_avg = copy.deepcopy(proton_beam_size_average[detector_name])
                    timeline_x = timeline_tmp['x'] # resolve timeline data into x component
                    timeline_y = timeline_tmp['y'] # resolve timeline data into y component
                    sample_data_x = timeline_x[start_index:end_index] # slice out 20 'x' events to average over
                    sample_data_y = timeline_y[start_index:end_index] # slice out 20 'y' events to average over
                    average_x = np.median(sample_data_x) # average the slice to form a new data point 
                    average_y = np.median(sample_data_y) 
                    tmp_1 = np.append(timeline_avg['x'], average_x) # append average value to timeline_average
                    tmp_2 = np.append(timeline_avg['y'], average_y) 

                    #update timeline
                    timeline_tmp.update({'x' : tmp_1})
                    timeline_tmp.update({'y' : tmp_2})
                    proton_beam_size_average.update({detector_name : timeline_tmp})


def plot_timeline_xy(leftdata, rightdata, ax1, ax2, ax3, camname, dx, dy, starttime, endtime, delta=10):
    
    if "BPM" in camname:
        x = 'hor'
        y = 'ver'

    else: 
        x = 'x'
        y = 'y'
    print(leftdata)
    print(rightdata)
    # Convert the timeline data (e.g. pixels) to mm 
    leftxdata = leftdata[x]*dx
    leftydata = leftdata[y]*dy
    rightxdata = rightdata[x]*dx
    rightydata = rightdata[y]*dy 
    # Format the axis to transform datetime objects correctly
    ax2.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    ax3.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
  
    xhist = np.histogram(leftxdata[-20:], bins=200)
    yhist = np.histogram(leftydata[-20:], bins=200)

    # Get mean values
    xmean = xhist[1][np.argmax(xhist[0])]
    ymean = yhist[1][np.argmax(yhist[0])]

    # Plot the last 20 events only on the 2D plot
    ax1.scatter(leftxdata[-20:], leftydata[-20:], alpha=0.4, s=15,)
    # Plot all events on the 1D plot
    ax2.scatter(rightdata['time'][1:], rightxdata[1:], s=15,)
    ax3.scatter(rightdata['time'][1:], rightydata[1:], s= 15)
    # Plot reference line corresponding to 20th event

    if np.size(leftxdata)>20 and np.size(leftydata)>20: 
        ax1.axhline(y = rightydata[20])
        ax1.axvline(x = rightxdata[20])
        ax2.axhline(y = rightxdata[20])
        ax3.axhline(y = rightydata[20])
        #  Get mean values
        xmean = rightxdata[20]
        ymean = rightydata[20]

        # if endtime-start_time >  :
        #     _5minsago_ = endtime - timedelta(minutes=5)
        #     for item in leftdata['time']:
        #         _5minsindex = leftdata.index(nearest(item, _5minsago_ ))
        #     _10minsago_= endtime - timedelta(minutes=10)
        #     for item in leftdata['time']:
        #         _10minsleftdata.index(nearest(item, _10minsago_))
        #     _15minsago_=endtime - timedelta(minutes=15)
        #     for item in leftdata['time']:
        #         leftdata.index(nearest(item, _15minsago_))
        #     _20minsago_=endtime - timedelta(minutes=20)
        #     for item in leftdata['time']:
        #         leftdata.index(nearest(i, _15minsago_))
        # # ax1.axhline(y = rightdata)
        # #      '-5 minutes'
        # # '-10 minutes'
        # # '-15 minutes'
        # # '-20 minutes'
        
    # Plot histogram on 2D plot
    ax111 = ax1.twinx() 
    ax112 = ax1.twiny()
    ax111.set_yticks([])
    ax112.set_xticks([])
    hist111 = ax111.hist(leftxdata[-20:], bins = np.linspace(xmean-delta*dx*2.5, xmean+delta*dx*2.5, 20), histtype='step',)
    hist222 = ax112.hist(leftydata[-20:], bins = np.linspace(ymean-delta*dy*2.5, ymean+delta*dy*2.5, 20), histtype='step', orientation='horizontal', )
    ax111.set_ylim(0, 4*max(hist111[0]))
    ax112.set_xlim(0, 4*max(hist222[0]))

    # Set limits for axes, position limits
    ax1.set_xlim(xmean-delta*dx*2.5, xmean+delta*dx*2.5)
    ax1.set_ylim(ymean-delta*dy*2.5, ymean+delta*dy*2.5)
    ax2.set_ylim(xmean-delta*dx, xmean+delta*dx)
    ax3.set_ylim(ymean-delta*dy, ymean+delta*dy)
    ax2.set_xlim(starttime, endtime)
    ax3.set_xlim(starttime, endtime)


#    text = ax1.text(0.5,0.9,camname, weight="bold", fontsize=12, transform=ax1.transAxes, )
    # Add jitter information
    camname2 = camname+', jitter: '+'{0:.2f}'.format(np.std(leftxdata[-20:]))+', '+'{0:.2f}'.format(np.std(leftydata[-20:]))
    text = ax1.text(0.03,0.9,camname2, weight="bold", fontsize=12, transform=ax1.transAxes, ) 

def plot_timeline_x(data, ax1, camname, dx, starttime, endtime, delta=1000000, _colour_ = 'turquoise', _label_ = None, offset = 0.0):
    
    if "emeter" in camname:
        x = 'energy'
        ax1.text(0.5,0.9, 'emeter', weight="bold", fontsize=12, transform=ax1.transAxes, )  

    if "PLASMA" in camname:
        if "Up" in _label_:
            x = 'upstream'
        if "Down" in _label_:
            x = 'downstream'
              
    xdata = data[x]*dx


    ax1.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    ax1.scatter(data['time'][1:], xdata[1:], s=15, c = _colour_, label = _label_, )
    ax1.set_xlim(starttime, endtime)

    # ax1.set_ylim(ymean-delta*dy, ymean+delta*dy)
   

def _plot_figure_with_proj(ax, data):

    try:

        nx = data.shape[1]
        ny = data.shape[0]

        xpr = np.nansum(data, 0) ; xpr = xpr / np.nanmax(xpr)
        ypr = np.nansum(data, 1) ; ypr = ypr / np.nanmax(ypr)

        xrange = list(np.asarray(range(0, nx)) + 0.5)
        yrange =  list(np.asarray(range(0, ny)) + 0.5)
        
        max_amp = np.max(gaussian_filter(data, sigma=3))

        ax.pcolormesh(range(0,nx+1), range(0,ny+1), data, vmin=0, vmax=max_amp, rasterized=True, cmap="inferno")
        
        ax.text(0.21,0.78,"max. amp: " + str(round(max_amp, ndigits=2)), transform=ax.transAxes,  )

        ax2 = ax.twinx()
        ax3 = ax.twiny()
        ax2.fill_between(xrange, list(xpr),  step='mid', color="white", alpha=0.5, lw=0.0, zorder = 2)
        ax2.set_ylim(0, 4.5)
        ax3.fill_betweenx(yrange, list(ypr), step='mid', color="white", alpha=0.5,  lw=0.0,  zorder = 2)
        ax3.set_xlim(0, 4.5)
        ax2.set_yticks([])
        ax3.set_xticks([])

        ax.set_xlim(xrange[0], xrange[-1])
        ax.set_ylim(yrange[0], yrange[-1])
        ax3.set_ylim(yrange[0], yrange[-1])
        ax2.set_xlim(xrange[0], xrange[-1])

    except:
        ax.set_title("Error")



def plot_event_data(data, savepath, store=True, closeplot=True):
    """
    Plot the data from one event.
    """

    x_text = 0.21
    y_text_2 = 0.92
    y_text_1 = 0.85
    y_text_3 = 0.78

    fig, ax = plt.subplots(4,4, figsize=(18,10),)
    fig.subplots_adjust(hspace=0.23, wspace=0.23)

    if type(data) is not bool:

        # Plot BTV50 data
        _plot_figure_with_proj(ax[0,0], data['btv_50']['data'])
        ax[0,0].set_title("BTV50", weight="bold",)
        ax[0,0].text(x_text,y_text_2,data['btv_50']['timestamp'], transform=ax[0,0].transAxes,  )
        ax[0,0].text(x_text,y_text_1,"dt: " + str(data['btv_50']['timediff']) + " [s]", transform=ax[0,0].transAxes,  )

        
        if data['btv_50']['timediff'] > 10 :
            ax[0,0].text(0.15,0.5,"Outdated Image!", transform=ax[0,0].transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['btv_50']['excep']:
            ax[0,0].text(0.15,0.3,"Except Error", transform=ax[0,0].transAxes, color="red", weight="bold", fontsize=12,)

        # Plot BTV53 data
        _plot_figure_with_proj(ax[0,1], data['btv_53']['data'])
        ax[0,1].set_title("BTV53", weight="bold",)
        ax[0,1].text(x_text,y_text_2,data['btv_53']['timestamp'], transform=ax[0,1].transAxes,  )
        ax[0,1].text(x_text,y_text_1,"dt: " + str(data['btv_53']['timediff']) + " [s]", transform=ax[0,1].transAxes,  )

        if data['btv_53']['timediff'] > 10 :
            ax[0,1].text(0.15,0.5,"Outdated Image!", transform=ax[0,1].transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['btv_53']['excep']:
            ax[0,1].text(0.15,0.3,"Except Error", transform=ax[0,1].transAxes, color="red", weight="bold", fontsize=12,)   

        #Plot BTV54 data
        _plot_figure_with_proj(ax[0,2], data['btv_54']['data'])
        ax[0,2].set_title("BTV54", weight="bold",)
        ax[0,2].text(x_text,y_text_2,data['btv_54']['timestamp'], transform=ax[0,2].transAxes,  )
        ax[0,2].text(x_text,y_text_1,"dt: " + str(data['btv_54']['timediff']) + " [s]", transform=ax[0,2].transAxes,  )

        if data['btv_54']['timediff'] > 10 :
            ax[0,2].text(0.15,0.5,"Outdated Image!", transform=ax[0,2].transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['btv_54']['excep']:
            ax[0,2].text(0.15,0.3,"Except Error", transform=ax[0,2].transAxes, color="red", weight="bold", fontsize=12,)   

        # Plot ExpVol01 data

        _plot_figure_with_proj(ax[0,3], data['exp_vol_1']['data'])
        ax[0,3].set_title("ExpVol01", weight="bold",)
        ax[0,3].text(x_text,y_text_2,data['exp_vol_1']['timestamp'], transform=ax[0,3].transAxes,  )
        ax[0,3].text(x_text,y_text_1,"dt: " + str(data['exp_vol_1']['timediff']) + " [s]", transform=ax[0,3].transAxes,  )

        if data['exp_vol_1']['timediff'] > 10 :
            ax[0,3].text(0.15,0.5,"Outdated Image!", transform=ax[0,3].transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['exp_vol_1']['excep']:
             ax[0,3].text(0.15,0.3,"Except Error", transform=ax[0,3].transAxes, color="red", weight="bold", fontsize=12,)   
        
        # Plot BTV26 data
        _plot_figure_with_proj(ax[1,0], data['btv_26']['data'])
        ax[1,0].set_title("BTV26", weight="bold",)
        ax[1,0].text(x_text,y_text_2,data['btv_26']['timestamp'], transform=ax[1,0].transAxes,  )
        ax[1,0].text(x_text,y_text_1,"dt: " + str(data['btv_26']['timediff']) + " [s]", transform=ax[1,0].transAxes,  )

        if data['btv_26']['timediff'] > 10 :
            ax[1,0].text(0.15,0.5,"Outdated Image!", transform=ax[1,0].transAxes, color="red", weight="bold", fontsize=12,)
        
        if data['btv_26']['excep']:
            ax[1,0].text(0.15,0.3,"Except Error", transform=ax[1,0].transAxes, color="red", weight="bold", fontsize=12,)

        # Plot IS1-Core data
        _plot_figure_with_proj(ax[1,1], data['is1_core']['data'])
        ax[1,1].set_title("IS1-Core", weight="bold",)
        ax[1,1].text(x_text,y_text_2,data['is1_core']['timestamp'], transform=ax[1,1].transAxes,  )
        ax[1,1].text(x_text,y_text_1,"dt: " + str(data['is1_core']['timediff']) + " [s]", transform=ax[1,1].transAxes,  )

        if data['is1_core']['timediff'] > 10  :
            ax[1,1].text(0.15,0.5,"Outdated Image!", transform=ax[1,1].transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['is1_core']['excep']:
            ax[1,1].text(0.15,0.3,"Except Error", transform=ax[1,1].transAxes, color="red", weight="bold", fontsize=12,)


        # Plot IS1-Halo data
        _plot_figure_with_proj(ax[1,2], data['is1_halo']['data'])
        ax[1,2].set_title("IS1-Halo", weight="bold",)
        ax[1,2].text(x_text,y_text_2,data['is1_halo']['timestamp'], transform=ax[1,2].transAxes,  )
        ax[1,2].text(x_text,y_text_1,"dt: " + str(data['is1_halo']['timediff']) + " [s]", transform=ax[1,2].transAxes,  )

        if data['is1_halo']['timediff'] > 10  :
            ax[1,2].text(0.15,0.5,"Outdated Image!", transform=ax[1,2].transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['is1_halo']['excep']:     
            ax[1,2].text(0.15,0.3,"Except Error", transform=ax[1,2].transAxes, color="red", weight="bold", fontsize= 12,)

        # PLot DMD 1
        _plot_figure_with_proj(ax[1,3], data['dmd_1']['data'])
        ax[1,3].set_title("DMD1", weight="bold",)
        ax[1,3].text(x_text,y_text_2,data['dmd_1']['timestamp'], transform=ax[1,3].transAxes,  )
        ax[1,3].text(x_text,y_text_1,"dt: " + str(data['dmd_1']['timediff']) + " [s]", transform=ax[1,3].transAxes,  )

        if data['dmd_1']['timediff'] > 10 :
            ax[1,3].text(0.15,0.5,"Outdated Image!", transform=ax[1,3].transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['dmd_1']['excep']:
            ax[1,3].text(0.15,0.3,"Except Error", transform=ax[1,3].transAxes, color="red", weight="bold", fontsize= 12, )

        # Plot DMD 2
        _plot_figure_with_proj(ax[2,0], data['dmd_2']['data'])
        ax[2,0].set_title("DMD2", weight="bold",)
        ax[2,0].text(x_text,y_text_2,data['dmd_2']['timestamp'], transform=ax[2,0].transAxes,  )
        ax[2,0].text(x_text,y_text_1,"dt: " + str(data['dmd_2']['timediff']) + " [s]", transform=ax[2,0].transAxes,  )

        if data['dmd_2']['timediff'] > 10 :
            ax[2,0].text(0.15,0.5,"Outdated Image!", transform=ax[2,0].transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['dmd_2']['excep']:
            ax[2,0].text(0.15,0.3,"Except Error", transform=ax[2,0].transAxes, color="red", weight="bold", fontsize=12,)   

        # Plot AWAKECAM 05
        _plot_figure_with_proj(ax[2,1], data['AWAKECAM_05']['data'])
        ax[2,1].set_title("AWAKECAM_05", weight="bold",)
        ax[2,1].text(x_text,y_text_2,data['AWAKECAM_05']['timestamp'], transform=ax[2,1].transAxes,  )
        ax[2,1].text(x_text,y_text_1,"dt: " + str(data['AWAKECAM_05']['timediff']) + " [s]", transform=ax[2,1].transAxes,  )

        if data['AWAKECAM_05']['timediff'] > 10 :
            ax[2,1].text(0.15,0.5,"Outdated Image!", transform=ax[2,1].transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['AWAKECAM_05']['excep']:
            ax[2,1].text(0.15,0.3,"Except Error", transform=ax[2,1].transAxes, color="red", weight="bold", fontsize=12,)   
        

        # Plot IS2-Core data
        _plot_figure_with_proj(ax[2,2], data['is2_core']['data'])
        ax[2,2].set_title("IS2-Core", weight="bold",)
        ax[2,2].text(x_text,y_text_2,data['is2_core']['timestamp'], transform=ax[2,2].transAxes, )
        ax[2,2].text(x_text,y_text_1,"dt: " + str(data['is2_core']['timediff']) + " [s]", transform=ax[2,2].transAxes,  )

        if data['is2_core']['timediff'] > 10  :
            ax[2,2].text(0.15,0.5,"Outdated Image!", transform=ax[2,2].transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['is2_core']['excep']:
            ax[2,2].text(0.15,0.3,"Except Error", transform=ax[2,2].transAxes, color="red", weight="bold", fontsize=12,)

        # Plot IS2-Halo data
        _plot_figure_with_proj(ax[2,3], data['is2_halo']['data'])
        ax[2,3].set_title("IS2-Halo", weight="bold",)
        ax[2,3].text(x_text,y_text_2,data['is2_halo']['timestamp'], transform=ax[2,3].transAxes,  )
        ax[2,3].text(x_text,y_text_1,"dt: " + str(data['is2_halo']['timediff']) + " [s]", transform=ax[2,3].transAxes,  )

        if data['is2_halo']['timediff'] > 10 :
            ax[2,3].text(0.15,0.5,"Outdated Image!", transform=ax[2,3].transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['is2_halo']['excep']:
            ax[2,3].text(0.15,0.3,"Except Error", transform=ax[2,3].transAxes, color="red", weight="bold", fontsize=12,)


        # Plot Streak TT41 data
        _plot_figure_with_proj(ax[3,0], data['streak_2']['data'])
        ax[3,0].set_title("Upstream Streak",  weight="bold",)
        ax[3,0].text(x_text,y_text_2,data['streak_2']['timestamp'], transform=ax[3,0].transAxes,  )
        ax[3,0].text(x_text,y_text_1,"dt: " + str(data['streak_2']['timediff']) + " [s]", transform=ax[3,0].transAxes,  )

        if data['streak_2']['timediff'] > 10 :
            ax[3,0].text(0.15,0.5,"Outdated Image!", transform=ax[3,0].transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['streak_2']['excep']:
            ax[3,0].text(0.15,0.3,"Except Error", transform=ax[3,0].transAxes, color="red", weight="bold", fontsize=12,)

        # Plot Streak XMPP data    
        _plot_figure_with_proj(ax[3,1], data['streak_1']['data'])
        ax[3,1].set_title("Downstream Streak", weight="bold",)
        ax[3,1].text(x_text,y_text_2,data['streak_1']['timestamp'], transform=ax[3,1].transAxes,  )
        ax[3,1].text(x_text,y_text_1,"dt: " + str(data['streak_1']['timediff']) + " [s]", transform=ax[3,1].transAxes,  )

        if data['streak_1']['timediff'] > 10 :
            ax[3,1].text(0.15,0.5,"Outdated Image!", transform=ax[3,1].transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['streak_1']['excep']:
            ax[3,1].text(0.15,0.3,"Except Error", transform=ax[3,1].transAxes, color="red", weight="bold", fontsize=12,)

                   
        # Plot XUCL-SPECTRO
        _plot_figure_with_proj(ax[3,2], data['spectro']['data'])
        ax[3,2].set_title("e- Spectrometer", weight="bold",)
        ax[3,2].text(x_text,y_text_2,data['spectro']['timestamp'], transform=ax[3,2].transAxes,  )
        ax[3,2].text(x_text,y_text_1,"dt: " + str(data['spectro']['timediff']) + " [s]", transform=ax[3,2].transAxes,  )

        if data['spectro']['timediff'] > 10 :
            ax[3,2].text(0.15,0.5,"Outdated Image!", transform=ax[3,2].transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['spectro']['excep']:
            ax[3,2].text(0.15,0.3,"Except Error", transform=ax[3,2].transAxes, color="red", weight="bold", fontsize=12,)

        # Plot plasma light
        
        ax[3,3].set_title("Plasma Light Diagnostics", weight="bold",)
        
        try:
            ax[3,3].text(x_text,y_text_2,data['plasma_light']['timestamp'], transform=ax[3,3].transAxes,  )
            ax[3,3].text(x_text,y_text_1,"dt: " + str(data['plasma_light']['timediff']) + " [s]", transform=ax[3,3].transAxes,  )
            ax[3,3].set_ylabel('counts')      


            ax[3,3].bar(np.linspace(0, np.size(data['plasma_light']['image']), num = np.size(data['plasma_light']['image']) ), data['plasma_light']['image'],1)



            if data['plasma_light']['timediff'] > 10 :
                ax[3,3].text(0.15,0.5,"Outdated Image!", transform=ax[3,3].transAxes, color="red", weight="bold", fontsize=12,)

            if data['plasma_light']['excep']:
                ax[3,3].text(0.15,0.3,"Except Error", transform=ax[3,3].transAxes, color="red", weight="bold", fontsize=12,)
              
        except:
            pass
        fig.suptitle("Event #: " + str(data['enumber']) + ". Timestamp: " + str(data['timestamp']) + ". Population: " + str(data['population']) + ". Time: " + str(data['time']), y=0.93, fontsize=12, weight="bold",)

    else:
        fig.suptitle("Error reading HDF5 file", y=0.93)

    if store: 
        print('saving to ', savepath)
        fig.savefig(savepath, bbox_inches = "tight")
    if closeplot:
        plt.close(fig)
        
        
def plot_mini_imgs_laser(data, savepath, closeplot=True, store=True):
    
    
    # Turn this all into a loop
    
    fig = plt.figure(figsize=(10,8))
    gs = gridspec.GridSpec(2, 3, figure=fig,  hspace = 0.2,  wspace = 0.2)
    ax1 = plt.subplot(gs[0, 1]) # UV cathode
    ax2 = plt.subplot(gs[0, 2]) # IR laser room
    ax3 = plt.subplot(gs[-1, 0]) # VLC 3
    ax4 = plt.subplot(gs[-1, 1]) # VLC 4
    ax5 = plt.subplot(gs[-1, 2]) # VLC 5
    
    x_text = 0.21
    y_text_2 = 0.92
    y_text_1 = 0.85
    y_text_3 = 0.78
    
    if type(data) is not bool:
    
        # Plot Laser 1 data
        _plot_figure_with_proj(ax1, data['laser_1']['data'])
        ax1.set_title("UV Cathode", weight="bold",)
        ax1.text(x_text,y_text_2,data['laser_1']['timestamp'], transform=ax1.transAxes,  )
        ax1.text(x_text,y_text_1,"dt: " + str(data['laser_1']['timediff']) + " [s]", transform=ax1.transAxes,  )
    
        if data['laser_1']['timediff'] > 10 :
            ax1.text(0.15,0.5,"Outdated Image!", transform=ax1.transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['laser_1']['excep']:
            ax1.text(0.15,0.3,"Except Error", transform=ax1.transAxes, color="red", weight="bold", fontsize=12,)
    
        # Plot laser 2
        _plot_figure_with_proj(ax2, data['laser_2']['data'])
        ax2.set_title("IR Laser Room", weight="bold",)
        ax2.text(x_text,y_text_2,data['laser_2']['timestamp'], transform=ax2.transAxes,  )
        ax2.text(x_text,y_text_1,"dt: " + str(data['laser_2']['timediff']) + " [s]", transform=ax2.transAxes,  )
    
        if data['laser_2']['timediff'] > 10 :
            ax2.text(0.15,0.5,"Outdated Image!", transform=ax2.transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['laser_2']['excep']:
            ax2.text(0.15,0.3,"Except Error", transform=ax2.transAxes, color="red", weight="bold", fontsize=12,)
    
        # Plot laser 3
        _plot_figure_with_proj(ax3, data['laser_3']['data'])
        ax3.set_title("Virtual Line 3", weight="bold",)
        ax3.text(x_text,y_text_2,data['laser_3']['timestamp'], transform=ax3.transAxes,  )
        ax3.text(x_text,y_text_1,"dt: " + str(data['laser_3']['timediff']) + " [s]", transform=ax3.transAxes,  )
    
        if data['laser_3']['timediff'] > 10 :
            ax3.text(0.15,0.5,"Outdated Image!", transform=ax3.transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['laser_3']['excep']:
            ax3.text(0.15,0.3,"Except Error", transform=ax3.transAxes, color="red", weight="bold", fontsize=12,)
    
        if (data['laser_3']['max_hor_amp']< 500 and (data['laser_4']['max_hor_amp']>1000 or data['laser_5']['max_hor_amp']>1000)) or (data['laser_3']['max_ver_amp']< 500 and (data['laser_4']['max_ver_amp']>1000 or data['laser_5']['max_ver_amp']>1000)):
            ax3.text(0.15,0.5,"Wrong Shot!", transform=ax3.transAxes, color = "red", weight = "bold", fontsize=12,)
    
        # Plot laser 4
        _plot_figure_with_proj(ax4, data['laser_4']['data'])
        ax4.set_title("Virtual Line 4",  weight="bold",)
        ax4.text(x_text,y_text_2,data['laser_4']['timestamp'], transform=ax4.transAxes,  )
        ax4.text(x_text,y_text_1,"dt: " + str(data['laser_4']['timediff']) + " [s]", transform=ax4.transAxes,  )
    
        if data['laser_4']['timediff'] > 10 :
            ax4.text(0.15,0.3,"Outdated Image!", transform=ax4.transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['laser_4']['excep']:
            ax4.text(0.15,0.3,"Except Error", transform=ax4.transAxes, color="red", weight="bold", fontsize=12,)
       
        if (data['laser_4']['max_hor_amp']< 500 and (data['laser_3']['max_hor_amp']>1000 or data['laser_5']['max_hor_amp']>1000)) or (data['laser_4']['max_ver_amp']< 500 and (data['laser_3']['max_ver_amp']>1000 or data['laser_5']['max_ver_amp']>1000)):
            ax4.text(0.15,0.5,"Wrong Shot!", transform=ax4.transAxes, color = "red", weight = "bold", fontsize=12,)
        
    
        # Plot laser 5
        _plot_figure_with_proj(ax5, data['laser_5']['data'])
        ax5.set_title("Virtual Line 5", weight="bold",)
        ax5.text(x_text,y_text_2,data['laser_5']['timestamp'], transform=ax5.transAxes,  )
        ax5.text(x_text,y_text_1,"dt: " + str(data['laser_5']['timediff']) + " [s]", transform=ax5.transAxes,  )
    
        if data['laser_5']['timediff'] > 10 :
            ax5.text(0.15,0.5,"Outdated Image!", transform=ax5.transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['laser_5']['excep']:
            ax5.text(0.15,0.3,"Except Error", transform=ax5.transAxes, color="red", weight="bold", fontsize=12,)
        
        if (data['laser_5']['max_hor_amp']< 500 and (data['laser_3']['max_hor_amp']>1000 or data['laser_4']['max_hor_amp']>1000)) or (data['laser_5']['max_ver_amp']< 500 and (data['laser_3']['max_ver_amp']>1000 or data['laser_4']['max_ver_amp']>1000)):
            ax5.text(0.15,0.5,"Wrong Shot!", transform=ax5.transAxes, color = "red", weight = "bold", fontsize=12,) 
    
        
        
    else:
        fig.suptitle("Error reading HDF5 file", y=0.93)

    if store: 
        fig.savefig(savepath, bbox_inches = "tight")
    if closeplot:
        plt.close(fig)





def plot_timeline_data_protons(data, sizedata, starttime, endtime, store=False, savepath=[], closeplot=False):
    """
    Plot the timeline data for BTVs.
    """

    #is1_core
    
    fig = plt.figure(figsize=(13,13),)
    
    fig.subplots_adjust(hspace=0.4, wspace=0.4)
    
    ax11 = plt.subplot2grid((7, 5), (0, 0), rowspan=2, colspan=2,  )
    ax12 = plt.subplot2grid((7, 5), (0, 2), rowspan=1, colspan=3,  )
    ax13 = plt.subplot2grid((7, 5), (1, 2), rowspan=1, colspan=3,  )

    ax11.set_xlabel("x, mm")                        # leftmost xy plot
    ax11.set_ylabel("y, mm")
                          
    ax12.set_xlabel("Time")                  # rightmost topmost 
    ax13.set_xlabel("Time")                  # rightmost bottommost

    ax12.set_ylabel("x, mm")
    ax13.set_ylabel("y, mm")
    
    # # Hide the ticks for the 1D x plot
    # ax12.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=False, right=False, left=False, labelleft=False)
 
    # Plot beam size    
    xdata = sizedata['is1_core']['x']
    ax12_2 = ax12.twinx()  # instantiate a second axes that shares the same x-axis
    ax12_2.set_ylabel('Beam Size', color='#feffb3')
    ax12_2.scatter(sizedata['is1_core']['time'][1:], xdata[1:], color='#feffb3', marker = 'x', s=12,)
    ax12_2.tick_params(axis='y', labelcolor='#feffb3') 
    ydata = sizedata['is1_core']['y']
    ax13_2 = ax13.twinx()
    ax13_2.set_ylabel('Beam Size', color='#feffb3') 
    ax13_2.scatter(sizedata['is1_core']['time'][1:], ydata[1:], color='#feffb3', marker = 'x', s=12,)
    ax13_2.tick_params(axis='y', labelcolor='#feffb3')
       
    # Plot beam position
    plot_timeline_xy(data["is1_core"], data["is1_core"], ax11, ax12, ax13, "IS1 Core", 0.0411, 0.0444, starttime = starttime, endtime = endtime) # how to choose dx and dy

    #is2_core
    
    ax21 = plt.subplot2grid((7, 5), (2, 0), rowspan=2, colspan=2,  )
    ax22 = plt.subplot2grid((7, 5), (2, 2), rowspan=1, colspan=3,  )
    ax23 = plt.subplot2grid((7, 5), (3, 2), rowspan=1, colspan=3,  )
    
    ax21.set_xlabel("x, mm")
    ax21.set_ylabel("y, mm")

    ax22.set_xlabel("Time")
    ax23.set_xlabel("Time")

    ax22.set_ylabel("x, mm")
    ax23.set_ylabel("y, mm")
    
    # # Hide the ticks for the 1D x plot
    # ax22.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=False, right=False, left=False, labelleft=False)

    # Plot beam size    
    xdata = sizedata['is2_core']['x']
    ax22_2 = ax22.twinx()  # instantiate a second axes that shares the same x-axis
    ax22_2.set_ylabel('Beam Size', color='#feffb3')
    ax22_2.scatter(sizedata['is2_core']['time'][1:], xdata[1:], color='#feffb3', marker = 'x', s=12,)
    ax22_2.tick_params(axis='y', labelcolor='#feffb3')
    ydata = sizedata['is2_core']['y']
    ax23_2 = ax23.twinx()
    ax23_2.set_ylabel('Beam Size', color='#feffb3') 
    ax23_2.scatter(sizedata['is2_core']['time'][1:], ydata[1:], color='#feffb3', marker = 'x', s=12,)
    ax23_2.tick_params(axis='y', labelcolor='#feffb3')
    
    # Plot beam position
    plot_timeline_xy(data["is2_core"], data["is2_core"], ax21, ax22, ax23, "IS2 Core", 0.0404, 0.0396, starttime = starttime, endtime = endtime)

    # Awakecam 05
    
    ax31 = plt.subplot2grid((7, 5), (4, 0), rowspan=2, colspan=2,  )
    ax32 = plt.subplot2grid((7, 5), (4, 2), rowspan=1, colspan=3,  )
    ax33 = plt.subplot2grid((7, 5), (5, 2), rowspan=1, colspan=3,  )

    ax31.set_xlabel("x, mm")
    ax31.set_ylabel("y, mm")
    ax32.set_xlabel("Time")
    ax33.set_xlabel("Time")

    ax32.set_ylabel("x, mm")
    ax33.set_ylabel("y, mm")
   
    # # Hide the ticks for the 1D x plot
    # ax32.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=False, right=False, left=False, labelleft= pLot beam position
    plot_timeline_xy(data["AWAKECAM_05"], data["AWAKECAM_05"], ax31, ax32, ax33, "AWAKECAM_05", 0.114, 0.1250, starttime = starttime, endtime = endtime)
    
    # Population

    ax41 = plt.subplot2grid((7, 5), (6, 2), rowspan=1, colspan=3,  )
    
    data_pop = [x for x in data['population']['pop'] if (np.isnan(x) == False) and x > 0.0]
    indices = [i for i, n in enumerate(data['population']['pop']) if (np.isnan(n) == False) and n > 0.0]
    data_time = [data['population']['time'][i] for i in indices]
   
    ax41.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    ax41.scatter(data_time[1:], data_pop[1:], s=15,)
    ax41.set_xlim(starttime, endtime)

    # Proton charge

    ax411 = ax41.twinx()
    ax411.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    ax411.scatter(data['proton_charge']['time'][1:], data['proton_charge']['charge'][1:], s = 15, c = '#feffb3')
    ax411.set_xlim(starttime, endtime)

    ax41.set_ylabel("Population")
    ax41.set_xlabel("Time")

    ax411.set_ylabel("Charge (C)", color = '#feffb3')
    ax411.tick_params(axis='y', labelcolor='#feffb3')
    
    if store:
        fig.savefig(savepath, bbox_inches = "tight")
    if closeplot:
        plt.close(fig)
        
def plot_timeline_data_laser(data, starttime, endtime, store=False, savepath=[], closeplot=False):
    """
    Plot the timeline data for the laser cameras.
    """

    fig = plt.figure(figsize=(13,15),)
    
    fig.subplots_adjust(hspace=0.4, wspace=0.4)

    # timeline laser 3

    ax11 = plt.subplot2grid((9, 5), (0, 0), rowspan=2, colspan=2,  )
    ax12 = plt.subplot2grid((9, 5), (0, 2), rowspan=1, colspan=3,  )
    ax13 = plt.subplot2grid((9, 5), (1, 2), rowspan=1, colspan=3,  )

    ax11.set_xlabel("x, mm")
    ax11.set_ylabel("y, mm")

    ax12.set_xlabel("Time")
    ax13.set_xlabel("Time")

    ax12.set_ylabel("x, mm")
    ax13.set_ylabel("y, mm")
    
    # Multiply pixel size by the amount of rebinning done when taking the image from the file
    plot_timeline_xy(data["laser_3"], data["laser_3"], ax11, ax12, ax13, "03TT41.CAM3", 0.0059*4, 0.0059*4, delta=40, starttime = starttime, endtime = endtime)
    
    # timeline laser 4

    ax21 = plt.subplot2grid((9,5), (2, 0), rowspan=2, colspan=2,  )
    ax22 = plt.subplot2grid((9,5), (2, 2), rowspan=1, colspan=3,  )
    ax23 = plt.subplot2grid((9,5), (3, 2), rowspan=1, colspan=3,  )

    ax21.set_xlabel("x, mm")
    ax21.set_ylabel("y, mm")

    ax22.set_xlabel("Time")
    ax23.set_xlabel("Time")

    ax22.set_ylabel("x, mm")
    ax23.set_ylabel("y, mm")

    
    plot_timeline_xy(data["laser_4"], data["laser_4"], ax21, ax22, ax23, "04TT41.CAM4", 0.0059*4, 0.0059*4, delta=40, starttime = starttime, endtime = endtime)

    # timeline laser 5
    
    ax31 = plt.subplot2grid((9,5), (4, 0), rowspan=2, colspan=2,  )
    ax32 = plt.subplot2grid((9,5), (4, 2), rowspan=1, colspan=3,  )
    ax33 = plt.subplot2grid((9,5), (5, 2), rowspan=1, colspan=3,  )

    ax31.set_xlabel("x, mm")
    ax31.set_ylabel("y, mm")

    ax32.set_xlabel("Time")
    ax33.set_xlabel("Time")

    ax32.set_ylabel("x, mm")
    ax33.set_ylabel("y, mm")
    
    plot_timeline_xy(data["laser_5"], data["laser_5"], ax31, ax32, ax33, "05TT41.CAM5", 0.0059*4, 0.0059*4, delta=40, starttime = starttime, endtime = endtime)
    
    # emeters
    
    ax41 = plt.subplot2grid((9,5), (6, 0), rowspan=2, colspan=5,  ) 
    ax41.set_xlabel("Time")
    ax41.set_ylabel("energy, mJ")

    # ax42.scatter(data['emeter_02']['time'][1:], data['emeter_02']['energy'][1:]*0.0059, s=15, c = 'r', label = 'emeter_02', )
    # ax42.scatter(data['emeter_03']['time'][1:], data['emeter_03']['energy'][1:]*0.0059, s=15, c = 'g', label = 'emeter_03', )
    # ax42.scatter(data['emeter_04']['time'][1:], data['emeter_04']['energy'][1:]*0.0059, s=15, c = 'm', label = 'emeter_04', )
    ax41.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    ax41.scatter(data['emeter_02']['time'][1:], data['emeter_02']['energy'][1:], s=15, c = 'b', label = 'emeter_02')
    ax41.scatter(data['emeter_03']['time'][1:], data['emeter_03']['energy'][1:], s=15, c = 'r', label = 'emeter_03')
    ax41.scatter(data['emeter_04']['time'][1:], data['emeter_04']['energy'][1:], s=15, c = 'g', label = 'emeter_04')
    ax41.scatter(data['emeter_05']['time'][1:], data['emeter_05']['energy'][1:], s=15, c = 'm', label = 'emeter_05')

    ax41.text(0.2,0.9, 'emeter', weight="bold", fontsize=12, transform=ax41.transAxes, ) 
    
    ax41.set_xlim(starttime, endtime)
    ax41.legend()
   
    if store:
        fig.savefig(savepath, bbox_inches = "tight")
    if closeplot:
        plt.close(fig)

def plot_timeline_bpm_protons(data, starttime, endtime, store = False, savepath=[], closeplot = False):

    """
    Plot the timeline data for proton BPMs
    """

    fig = plt.figure(figsize=(13,13),)
    
    fig.subplots_adjust(hspace=0.4, wspace=0.4)


    # bpm proton 1
    
    ax11 = plt.subplot2grid((8, 5), (0, 0), rowspan=2, colspan=2,  )
    ax12 = plt.subplot2grid((8, 5), (0, 2), rowspan=1, colspan=3,  )
    ax13 = plt.subplot2grid((8, 5), (1, 2), rowspan=1, colspan=3,  )

    ax11.set_xlabel("x, mm")
    ax11.set_ylabel("y, mm")

    ax12.set_xlabel("Time")
    ax13.set_xlabel("Time")

    ax12.set_ylabel("x, mm")
    ax13.set_ylabel("y, mm")
    
    plot_timeline_xy(data["bpm_proton_1"], data["bpm_proton_1"], ax11, ax12, ax13, "TT41.BPM.412311", 1, 1, starttime = starttime, endtime = endtime, delta =0.5) 

    # bpm proton 3

    ax21 = plt.subplot2grid((8, 5), (2, 0), rowspan=2, colspan=2,  )
    ax22 = plt.subplot2grid((8, 5), (2, 2), rowspan=1, colspan=3,  )
    ax23 = plt.subplot2grid((8, 5), (3, 2), rowspan=1, colspan=3,  )

    ax21.set_xlabel("x, mm")
    ax21.set_ylabel("y, mm")

    ax22.set_xlabel("Time")
    ax23.set_xlabel("Time")

    ax22.set_ylabel("x, mm")
    ax23.set_ylabel("y, mm")
    
    plot_timeline_xy(data["bpm_proton_3"], data["bpm_proton_3"], ax21, ax22, ax23, "TT41.BPM.412339", 1, 1, starttime = starttime, endtime = endtime, delta =0.5)   

    # bpm proton 4

    ax31 = plt.subplot2grid((8, 5), (4, 0), rowspan=2, colspan=2,  )
    ax32 = plt.subplot2grid((8, 5), (4, 2), rowspan=1, colspan=3,  )
    ax33 = plt.subplot2grid((8, 5), (5, 2), rowspan=1, colspan=3,  )

    ax31.set_xlabel("x, mm")
    ax31.set_ylabel("y, mm")

    ax32.set_xlabel("Time")
    ax33.set_xlabel("Time")

    ax32.set_ylabel("x, mm")
    ax33.set_ylabel("y, mm")
    
    plot_timeline_xy(data["bpm_proton_4"], data["bpm_proton_4"], ax31, ax32, ax33, "TT41.BPM.412352", 1, 1, starttime = starttime, endtime = endtime, delta=0.5)

    # bpm proton 5

    ax41 = plt.subplot2grid((8, 5), (6, 0), rowspan=2, colspan=2,  )
    ax42 = plt.subplot2grid((8, 5), (6, 2), rowspan=1, colspan=3,  )
    ax43 = plt.subplot2grid((8, 5), (7, 2), rowspan=1, colspan=3,  )

    ax41.set_xlabel("x, mm")
    ax41.set_ylabel("y, mm")

    ax42.set_xlabel("Time")
    ax43.set_xlabel("Time")

    ax42.set_ylabel("x, mm")
    ax43.set_ylabel("y, mm")
    
    plot_timeline_xy(data["bpm_proton_5"], data["bpm_proton_5"], ax41, ax42, ax43, "TT41.BPM.412425", 1, 1, starttime = starttime, endtime = endtime, delta =0.5)

    if store:
        fig.savefig(savepath, bbox_inches = "tight")
    if closeplot:
        plt.close(fig)

def plot_timeline_bpm_electrons(data, starttime, endtime, store = False, savepath=[], closeplot = False):

    """
    Plot the timeline data for electron BPMs
    """

    fig = plt.figure(figsize=(13,13),)
    
    fig.subplots_adjust(hspace=0.4, wspace=0.4)

    # bpm electron 1
    
    ax11 = plt.subplot2grid((4, 5), (0, 0), rowspan=2, colspan=2,  )
    ax12 = plt.subplot2grid((4, 5), (0, 2), rowspan=1, colspan=3,  )
    ax13 = plt.subplot2grid((4, 5), (1, 2), rowspan=1, colspan=3,  )

    ax11.set_xlabel("x, mm")
    ax11.set_ylabel("y, mm")

    ax12.set_xlabel("Time")
    ax13.set_xlabel("Time")

    ax12.set_ylabel("x, mm")
    ax13.set_ylabel("y, mm")
    
    plot_timeline_xy(data["bpm_electron_1"],  data["bpm_electron_1"], ax11, ax12, ax13, "TT41.BPM.412349", 1, 1, starttime = starttime, endtime = endtime, delta = 0.02)

    ax12.set_ylim(-3,3)
    ax13.set_ylim(-3,3)

    # bpm electron 2

    ax21 = plt.subplot2grid((4, 5), (2, 0), rowspan=2, colspan=2,  )
    ax22 = plt.subplot2grid((4, 5), (2, 2), rowspan=1, colspan=3,  )
    ax23 = plt.subplot2grid((4, 5), (3, 2), rowspan=1, colspan=3,  )

    ax21.set_xlabel("x, mm")
    ax21.set_ylabel("y, mm")

    ax22.set_xlabel("Time")
    ax23.set_xlabel("Time")

    ax22.set_ylabel("x, mm")
    ax23.set_ylabel("y, mm")
    
    plot_timeline_xy(data["bpm_electron_2"], data["bpm_electron_2"], ax21, ax22, ax23, "TT41.BPM.412351", 1, 1, starttime = starttime, endtime = endtime, delta = 0.02) 

    ax22.set_ylim(-3,+3)
    ax23.set_ylim(-3,+3)

    if store:
        fig.savefig(savepath, bbox_inches = "tight")
    if closeplot:
        plt.close(fig)

def plot_timeline_plasma(data, starttime, endtime, store = False, savepath=[], closeplot = False):


    """
    Plot the timeline average for plasma diagnostics
    """

    
    fig = plt.figure(figsize=(10,3),)
    fig.subplots_adjust(hspace=0.4, wspace=0.4)
    
    ax2 = plt.subplot2grid((2, 10), (0, 0), rowspan=2, colspan=10,)
    
    
    ax2.set_xlabel("Time")
    ax2.set_ylabel("Integrated Counts")
    
    plot_timeline_x(data["plasma_light"], ax2, "PLASMA DIAGNOSTICS", 1, starttime, endtime, _colour_ = 'turquoise', _label_ = "Upstream")
    plot_timeline_x(data["plasma_light"], ax2, "PLASMA DIAGNOSTICS", 1, starttime, endtime, _colour_ = 'm', _label_ = "Downstream")
    fig.legend()
    fig.suptitle('PLASMA DIAGNOSTICS', x=0.70, y = 0.95, weight="bold", fontsize=12,)

    if store:
        fig.savefig(savepath, bbox_inches = "tight")
    if closeplot:
        plt.close(fig)



def plot_timeline_average_protons(rawdata, avgdata, sizedata, starttime, endtime, store=False, savepath=[], closeplot=False):
    """
    Plot the timeline average data for BTVs.
    """

    #is1 core

    fig = plt.figure(figsize=(13,13),)
    
    fig.subplots_adjust(hspace=0.4, wspace=0.4)
    
    ax11 = plt.subplot2grid((7, 5), (0, 0), rowspan=2, colspan=2,  )
    ax12 = plt.subplot2grid((7, 5), (0, 2), rowspan=1, colspan=3,  )
    ax13 = plt.subplot2grid((7, 5), (1, 2), rowspan=1, colspan=3,  )

    ax11.set_xlabel("x, mm ")
    ax11.set_ylabel("y, mm ")

    ax12.set_xlabel("Time")
    ax13.set_xlabel("Time")

    ax12.set_ylabel("x, mm")
    ax13.set_ylabel("y, mm")

    xdata = sizedata['is1_core']['x']
    ax12_2 = ax12.twinx()  
    ax12_2.set_ylabel('Beam Size', color='#feffb3')
    ax12_2.scatter(sizedata['is1_core']['time'][1:], xdata[1:], color='#feffb3', marker = 'x', s=12,)
    ax12_2.tick_params(axis='y', labelcolor='#feffb3')
    
    ydata = sizedata['is1_core']['y']
    ax13_2 = ax13.twinx()
    ax13_2.set_ylabel('Beam Size', color='#feffb3') 
    ax13_2.scatter(sizedata['is1_core']['time'][1:], ydata[1:], color='#feffb3', marker='x', s=12,)
    ax13_2.tick_params(axis='y', labelcolor='#feffb3')


    plot_timeline_xy(rawdata["is1_core"],avgdata["is1_core"], ax11, ax12, ax13, "IS1 Core", 0.0411, 0.0444, starttime = starttime, endtime = endtime)
  
  
    #is2 core

    ax21 = plt.subplot2grid((7, 5), (2, 0), rowspan=2, colspan=2,  )
    ax22 = plt.subplot2grid((7, 5), (2, 2), rowspan=1, colspan=3,  )
    ax23 = plt.subplot2grid((7, 5), (3, 2), rowspan=1, colspan=3,  )

    ax21.set_xlabel("x, mm ")
    ax21.set_ylabel("y, mm ")

    ax22.set_xlabel("Time")
    ax23.set_xlabel("Time")

    ax22.set_ylabel("x, mm ")
    ax23.set_ylabel("y, mm ")

    # beam size is2 core

    xdata = sizedata['is2_core']['x']
    ax22_2 = ax22.twinx()  # instantiate a second axes that shares the same x-axis
    ax22_2.set_ylabel('Beam Size', color='#feffb3')
    ax22_2.scatter(sizedata['is2_core']['time'][1:], xdata[1:], color='#feffb3', s=12, marker = 'x')
    ax22_2.tick_params(axis='y', labelcolor='#feffb3')
    
    ydata = sizedata['is2_core']['y']
    ax23_2 = ax23.twinx()
    ax23_2.set_ylabel('Beam Size', color='#feffb3') 
    ax23_2.scatter(sizedata['is2_core']['time'][1:], ydata[1:], color='#feffb3', marker = 'x', s=12)
    ax23_2.tick_params(axis='y', labelcolor='#feffb3')
       
    plot_timeline_xy(rawdata["is2_core"],avgdata["is2_core"], ax21, ax22, ax23, "IS2 Core", 0.0404, 0.0396, starttime = starttime, endtime = endtime)
   
    #AWAKECAM_05

    ax31 = plt.subplot2grid((7, 5), (4, 0), rowspan=2, colspan=2,  )
    ax32 = plt.subplot2grid((7, 5), (4, 2), rowspan=1, colspan=3,  )
    ax33 = plt.subplot2grid((7, 5), (5, 2), rowspan=1, colspan=3,  )

    ax31.set_xlabel("x, mm ")
    ax31.set_ylabel("y, mm ")

    ax32.set_xlabel("Time")
    ax33.set_xlabel("Time")

    ax32.set_ylabel("x, mm")
    ax33.set_ylabel("y, mm")
    
    plot_timeline_xy(rawdata["AWAKECAM_05"],avgdata["AWAKECAM_05"], ax31, ax32, ax33, "AWAKECAM_05", 0.114, 0.1250, starttime = starttime, endtime = endtime)
    
   
    #population


    ax41 = plt.subplot2grid((7, 5), (6, 2), rowspan=1, colspan=3,  )
    
    data_pop = [x for x in avgdata['population']['pop'] if (np.isnan(x) == False) and x > 0.0]
    indices = [i for i, n in enumerate(avgdata['population']['pop']) if (np.isnan(n) == False) and n > 0.0]
    data_time = [avgdata['population']['time'][i] for i in indices]
   
    ax41.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    ax41.scatter(data_time[1:], data_pop[1:], s=15,)
    ax41.set_xlim(starttime, endtime)

    ax41.set_ylabel("Population")
    ax41.set_xlabel("Time")

    #proton charge

    ax411 = ax41.twinx()
    ax411.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    ax411.scatter(avgdata['proton_charge']['time'][1:], avgdata['proton_charge']['charge'][1:], s = 15, c = '#feffb3')
    ax411.set_xlim(starttime, endtime)

    ax411.set_ylabel("Charge", color = '#feffb3')
    ax411.tick_params(axis='y', labelcolor='#feffb3')

    
    if store:
        fig.savefig(savepath, bbox_inches = "tight")
    if closeplot:
        plt.close(fig)
        
def plot_timeline_average_laser(rawdata, avgdata, starttime, endtime, store=False, savepath=[], closeplot=False):
   
      
    """
    Plot the timeline average for the laser cameras.
    """

    fig = plt.figure(figsize=(13,15),)    
    fig.subplots_adjust(hspace=0.5, wspace=0.5)

    # timeline laser 3

    ax11 = plt.subplot2grid((18, 10), (0, 0), rowspan=4, colspan=4,  )
    ax12 = plt.subplot2grid((18, 10), (0, 4), rowspan=2, colspan=6,  )
    ax13 = plt.subplot2grid((18, 10), (2, 4), rowspan=2, colspan=6,  )

    ax11.set_xlabel("x, mm")
    ax11.set_ylabel("y, mm")

    ax12.set_xlabel("Time")
    ax13.set_xlabel("Time")

    ax12.set_ylabel("x, mm")
    ax13.set_ylabel("y, mm")
    
    plot_timeline_xy(rawdata["laser_3"], avgdata["laser_3"], ax11, ax12, ax13, "03TT41.CAM3", 0.0059, 0.0059, delta=40, starttime = starttime, endtime = endtime)
    
    # timeline laser 4

    ax21 = plt.subplot2grid((18,10), (4, 0), rowspan=4, colspan=4,  )
    ax22 = plt.subplot2grid((18,10), (4, 4), rowspan=2, colspan=6,  )
    ax23 = plt.subplot2grid((18,10), (6, 4), rowspan=2, colspan=6,  )

    ax21.set_xlabel("x, mm")
    ax21.set_ylabel("y, mm")

    ax22.set_xlabel("Time")
    ax23.set_xlabel("Time")

    ax22.set_ylabel("x, mm")
    ax23.set_ylabel("y, mm")

    
    plot_timeline_xy(rawdata["laser_4"], avgdata["laser_4"], ax21, ax22, ax23, "04TT41.CAM4", 0.0059, 0.0059, delta=40, starttime = starttime, endtime = endtime)

    # timeline laser 5
    
    ax31 = plt.subplot2grid((18,10), (8, 0), rowspan=4, colspan=4,  )
    ax32 = plt.subplot2grid((18,10), (8, 4), rowspan=2, colspan=6,  )
    ax33 = plt.subplot2grid((18,10), (10, 4), rowspan=2, colspan=6,  )

    ax31.set_xlabel("x, mm")
    ax31.set_ylabel("y, mm")

    ax32.set_xlabel("Time")
    ax33.set_xlabel("Time")

    ax32.set_ylabel("x, mm")
    ax33.set_ylabel("y, mm")
    
    plot_timeline_xy(rawdata["laser_5"], avgdata["laser_5"], ax31, ax32, ax33, "05TT41.CAM5", 0.0059, 0.0059, delta=40, starttime = starttime, endtime = endtime)
    
    # emeters
    
    # gs = gridspec.GridSpec(9,5)
    # ax41 = plt.subplot(gs[6:7, 0:])
    ax41 = plt.subplot2grid((18, 10), (12, 0), rowspan=4, colspan = 9,  ) 
    ax42 = ax41.twinx()
    ax43 = ax41.twinx()
    ax44 = ax41.twinx()

    ax43.spines['right'].set_position(("axes", 1.05))
    ax44.spines['right'].set_position(("axes", 1.1))

    # ax41.set_ylim()
    # ax42.set_ylim()
    # ax43.set_ylim()
    # ax44.set_ylim()

    ax41.set_xlabel("Time")
    ax41.set_ylabel("energy, mJ")
    ax44.set_ylabel("energy, mJ")

    # ax42.scatter(data['emeter_02']['time'][1:], data['emeter_02']['energy'][1:]*0.0059, s=15, c = 'r', label = 'emeter_02', )
    # ax42.scatter(data['emeter_03']['time'][1:], data['emeter_03']['energy'][1:]*0.0059, s=15, c = 'g', label = 'emeter_03', )
    # ax42.scatter(data['emeter_04']['time'][1:], data['emeter_04']['energy'][1:]*0.0059, s=15, c = 'm', label = 'emeter_04', )
    ax41.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    ax42.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    ax43.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    ax44.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))

    c1 = ax41.scatter(rawdata['emeter_02']['time'][1:], rawdata['emeter_02']['energy'][1:], s=15, c = '#00FFFF', label = 'emeter_02')
    c2 = ax42.scatter(rawdata['emeter_03']['time'][1:], rawdata['emeter_03']['energy'][1:], s=15, c = '#FFFF00', label = 'emeter_03')
    c3 = ax43.scatter(rawdata['emeter_04']['time'][1:], rawdata['emeter_04']['energy'][1:], s=15, c = '#FF0000', label = 'emeter_04')
    c4 = ax44.scatter(rawdata['emeter_05']['time'][1:], rawdata['emeter_05']['energy'][1:], s=15, c = '#00FF00', label = 'emeter_05')


    ax41.yaxis.label.set_color('w')
    ax44.yaxis.label.set_color('w')

    ax41.tick_params(axis='y', colors='#00FFFF')
    ax42.tick_params(axis='y', colors='#FFFF00')
    ax43.tick_params(axis='y', colors='#FF0000')
    ax44.tick_params(axis='y', colors='#00FF00')



    ax41.text(0.2,0.9, 'EMETER', weight="bold", fontsize=12, transform=ax41.transAxes, ) 
    
    ax41.set_xlim(starttime, endtime)
    ax41.legend(handles=[c1, c2, c3, c4], loc = 'upper left')

    if store:
        fig.savefig(savepath, bbox_inches = "tight")
    if closeplot:
        plt.close(fig)

def plot_timeline_average_bpm_protons(rawdata, avgdata, starttime, endtime, store=False, savepath=[], closeplot=False):
    """
    Plot the timeline average for proton BPMs
    """

    fig = plt.figure(figsize=(13,13),)
    
    fig.subplots_adjust(hspace=0.4, wspace=0.4)


    # bpm proton 1
    
    ax11 = plt.subplot2grid((8, 5), (0, 0), rowspan=2, colspan=2,  )
    ax12 = plt.subplot2grid((8, 5), (0, 2), rowspan=1, colspan=3,  )
    ax13 = plt.subplot2grid((8, 5), (1, 2), rowspan=1, colspan=3,  )

    ax11.set_xlabel("x, mm")
    ax11.set_ylabel("y, mm")

    ax12.set_xlabel("Time")
    ax13.set_xlabel("Time")

    ax12.set_ylabel("x, mm (average)")
    ax13.set_ylabel("y, mm (average)")
    
    plot_timeline_xy(rawdata["bpm_proton_1"], avgdata["bpm_proton_1"], ax11, ax12, ax13, "TT41.BPM.412311", 1, 1, starttime = starttime, endtime = endtime, delta = 0.5)
    
    # bpm proton 3

    ax21 = plt.subplot2grid((8, 5), (2, 0), rowspan=2, colspan=2,  )
    ax22 = plt.subplot2grid((8, 5), (2, 2), rowspan=1, colspan=3,  )
    ax23 = plt.subplot2grid((8, 5), (3, 2), rowspan=1, colspan=3,  )

    ax21.set_xlabel("x, mm")
    ax21.set_ylabel("y, mm")

    ax22.set_xlabel("Time")
    ax23.set_xlabel("Time")

    ax22.set_ylabel("x, mm (average)")
    ax23.set_ylabel("y, mm (average)")
    
    plot_timeline_xy(rawdata["bpm_proton_3"], avgdata["bpm_proton_3"], ax21, ax22, ax23, "TT41.BPM.412339", 1, 1, starttime = starttime, endtime = endtime, delta = 0.5) 
    

    # bpm proton 4

    ax31 = plt.subplot2grid((8, 5), (4, 0), rowspan=2, colspan=2,  )
    ax32 = plt.subplot2grid((8, 5), (4, 2), rowspan=1, colspan=3,  )
    ax33 = plt.subplot2grid((8, 5), (5, 2), rowspan=1, colspan=3,  )

    ax31.set_xlabel("x, mm")
    ax31.set_ylabel("y, mm")

    ax32.set_xlabel("Time")
    ax33.set_xlabel("Time")

    ax32.set_ylabel("x, mm (average)")
    ax33.set_ylabel("y, mm (average)")
    
    plot_timeline_xy(rawdata["bpm_proton_4"], avgdata["bpm_proton_4"], ax31, ax32, ax33, "TT41.BPM.412352", 1, 1, starttime = starttime, endtime = endtime, delta = 0.5)


    # bpm proton 5

    ax41 = plt.subplot2grid((8, 5), (6, 0), rowspan=2, colspan=2,  )
    ax42 = plt.subplot2grid((8, 5), (6, 2), rowspan=1, colspan=3,  )
    ax43 = plt.subplot2grid((8, 5), (7, 2), rowspan=1, colspan=3,  )

    ax41.set_xlabel("x, mm")
    ax41.set_ylabel("y, mm")

    ax42.set_xlabel("Time")
    ax43.set_xlabel("Time")

    ax42.set_ylabel("x, mm (average)")
    ax43.set_ylabel("y, mm (average)")
    
    plot_timeline_xy(rawdata["bpm_proton_5"], avgdata["bpm_proton_5"], ax41, ax42, ax43, "TT41.BPM.412425", 1, 1, starttime = starttime, endtime = endtime, delta = 0.5)


   
    if store:
        fig.savefig(savepath, bbox_inches = "tight")
    if closeplot:
        plt.close(fig)

def plot_timeline_average_bpm_electrons(rawdata, avgdata, starttime, endtime, store=False, savepath=[], closeplot=False):

    """
    Plot the timeline average for electron BPMs
    """

    fig = plt.figure(figsize=(13,13),)
    
    fig.subplots_adjust(hspace=0.4, wspace=0.4)

    # bpm electron 1
    
    ax11 = plt.subplot2grid((4, 5), (0, 0), rowspan=2, colspan=2,  )
    ax12 = plt.subplot2grid((4, 5), (0, 2), rowspan=1, colspan=3,  )
    ax13 = plt.subplot2grid((4, 5), (1, 2), rowspan=1, colspan=3,  )

    ax11.set_xlabel("x, mm")
    ax11.set_ylabel("y, mm")

    ax12.set_xlabel("Time")
    ax13.set_xlabel("Time")

    ax12.set_ylabel("x, mm (average)")
    ax13.set_ylabel("y, mm (average)")
    
    plot_timeline_xy(rawdata["bpm_electron_1"], avgdata["bpm_electron_1"], ax11, ax12, ax13, "TT41.BPM.412349", 1, 1, starttime = starttime, endtime = endtime, delta = 0.02)

    ax12.set_ylim(-0.1,+0.1)
    ax13.set_ylim(-0.1,+0.1)

    # bpm electron 2

    ax21 = plt.subplot2grid((4, 5), (2, 0), rowspan=2, colspan=2,  )
    ax22 = plt.subplot2grid((4, 5), (2, 2), rowspan=1, colspan=3,  )
    ax23 = plt.subplot2grid((4, 5), (3, 2), rowspan=1, colspan=3,  )

    ax21.set_xlabel("x, mm (average)")
    ax21.set_ylabel("y, mm (average)")

    ax22.set_xlabel("Time")
    ax23.set_xlabel("Time")

    ax22.set_ylabel("x, mm (average)")
    ax23.set_ylabel("y, mm (average)")
    
    plot_timeline_xy( rawdata["bpm_electron_2"], avgdata["bpm_electron_2"], ax21, ax22, ax23, "TT41.BPM.412351", 1, 1, starttime = starttime, endtime = endtime, delta = 0.02)
    
    ax12.set_ylim(-0.1,+0.1)
    ax13.set_ylim(-0.1,+0.1)

    if store:
        fig.savefig(savepath, bbox_inches = "tight")
    if closeplot:
        plt.close(fig)

def plot_timeline_average_plasma(rawdata, avgdata, starttime, endtime, store=False, savepath=[], closeplot=False):
    """
    Plot the timeline average for plasma diagnostics
    """

    
    fig = plt.figure(figsize=(10,3),)
    fig.subplots_adjust(hspace=0.4, wspace=0.4)
    
    ax2 = plt.subplot2grid((2, 10), (0, 0), rowspan=2, colspan=10,)
    
    
    ax2.set_xlabel("Time")
    ax2.set_ylabel("Integrated Counts")
    
    plot_timeline_x(avgdata["plasma_light"], ax2, "PLASMA DIAGNOSTICS", 1, starttime, endtime, _colour_ = 'turquoise', _label_ = "Upstream")
    plot_timeline_x(avgdata["plasma_light"], ax2, "PLASMA DIAGNOSTICS", 1, starttime, endtime, _colour_ = 'm', _label_ = "Downstream")
    fig.legend()
    fig.suptitle('PLASMA DIAGNOSTICS', x=0.70, y = 0.95, weight="bold", fontsize=12,)

    if store:
        fig.savefig(savepath, bbox_inches = "tight")
    if closeplot:
        plt.close(fig)
        
        
# =============================================================================
# BTVs        
# =============================================================================
        

def get_btv_50_analogue(file):
    """
    Access and convert the data from TT41.BTV.412350 to a dictionary.
    """

    try:
        nx = file["/AwakeEventData/TT41.BTV.412350/Image/imagePositionSet1"][0].shape[0]
        ny = file["/AwakeEventData/TT41.BTV.412350/Image/imagePositionSet2"][0].shape[0]
        dt = file["/AwakeEventData/TT41.BTV.412350/Image/imageSet"][0].reshape(ny, nx)
    except :
        dt = [[]]

    try:
        timestamp = file["/AwakeEventData/TT41.BTV.412350/Image/"].attrs['acqStamp']
    except :
        timestamp = "Reading Error"

    try:
        excep = file["/AwakeEventData/TT41.BTV.412350/Image/"].attrs['exception']
    except :
        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }


def get_btv_50_pxi(file):
    """
    Access and convert the data from AWAKECAM09 to a dictionary.
    """

    try:
        image = file["/AwakeEventData/BOVWA.09TCC4.AWAKECAM09/ExtractionImage"]
        dt = image['imageRawData'][:]
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
        dt = _rebin(dt, [i // 4 for i in dt.shape])
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"        
    try: 
        print('Timestamp is: ',file_timestamp)
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }


def get_btv_50_digi(file):
    """
    Access and convert the data from TT41.BTV.412350.DigiCam to a dictionary.
    """

    try: 
        dt = file["/AwakeEventData/TT41.BTV.412350.DigiCam/ExtractionImage/image2D"][:]
        dt = _rebin(dt, [i // 4 for i in dt.shape])
    except :
        dt = [[]]

    try:
        timestamp = file["/AwakeEventData/TT41.BTV.412350.DigiCam/ExtractionImage/"].attrs['acqStamp']
    except :
        timestamp = "Reading Error"

    try:
        excep = file["/AwakeEventData/TT41.BTV.412350.DigiCam/ExtractionImage/"].attrs['exception']
    except :
        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 
        

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }





def get_btv_53_analogue(file):
    """
    Access and convert the data from TT41.BTV.412353 to a dictionary.
    """

    try:
        nx = file["/AwakeEventData/TT41.BTV.412353/Image/imagePositionSet1"][0].shape[0]
        ny = file["/AwakeEventData/TT41.BTV.412353/Image/imagePositionSet2"][0].shape[0]
        dt = file["/AwakeEventData/TT41.BTV.412353/Image/imageSet"][0].reshape(ny, nx)
    except :
        dt = [[]]

    try:
        timestamp = file["/AwakeEventData/TT41.BTV.412353/Image/"].attrs['acqStamp']
    except :
        timestamp = "Reading Error"

    try:
        excep = file["/AwakeEventData/TT41.BTV.412353/Image/"].attrs['exception']
    except :
        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }        



        

def get_btv_53_digi(file):
    """
    Access and convert the data from TT41.BTV.412353.DigiCam to a dictionary.
    """

    try: 
        dt = file["/AwakeEventData/TT41.BTV.412353.DigiCam/ExtractionImage/image2D"][:]
        dt = _rebin(dt, [i // 2 for i in dt.shape])
        
    except :
        dt = [[]]

    try:
        timestamp = file["/AwakeEventData/TT41.BTV.412353.DigiCam/ExtractionImage/"].attrs['acqStamp']
    except :
        timestamp = "Reading Error"

    try:
        excep = file["/AwakeEventData/TT41.BTV.412353.DigiCam/ExtractionImage/"].attrs['exception']
    except :
        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 
        

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }



def get_btv_26_analogue(file):
    """
    Access and convert the data from BTV26 to a dictionary.
    """

    try:
        nx = file["/AwakeEventData/TT41.BTV.412426/Image/imagePositionSet1"][0].shape[0]
        ny = file["/AwakeEventData/TT41.BTV.412426/Image/imagePositionSet2"][0].shape[0]
        dt = file["/AwakeEventData/TT41.BTV.412426/Image/imageSet"][0].reshape(ny, nx)
    except :
        dt = [[]]

    try:
        timestamp = file["/AwakeEventData/TT41.BTV.412426/Image/"].attrs['acqStamp']
    except :
        timestamp = "Reading Error"

    try:
        excep = file["/AwakeEventData/TT41.BTV.412426/Image/"].attrs['exception']
    except :
        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep , 'timediff' : timediff}


def get_btv_26_digi(file):
    """
    Access and convert the data from BTV53 to a dictionary.
    """

    try: 
        dt = file["/AwakeEventData/TT41.BTV.412426.DigiCam/ExtractionImage/image2D"][:]
        dt = _rebin(dt, [i // 2 for i in dt.shape])

    except :
        dt = [[]]

    try:
        timestamp = file["/AwakeEventData/TT41.BTV.412426.DigiCam/ExtractionImage/"].attrs['acqStamp']
    except :
        timestamp = "Reading Error"

    try:
        excep = file["/AwakeEventData/TT41.BTV.412426.DigiCam/ExtractionImage/"].attrs['exception']
    except :
        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 
        

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }




def get_btv_54_pxi(file):
    """
    Access and convert the data from BTV54 to a dictionary.
    """

    try:
        image = file["/AwakeEventData/BOVWA.11TCC4.AWAKECAM11/ExtractionImage/"]
        dt = image['imageRawData'][:]
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
        dt = _rebin(dt, [i // 4 for i in dt.shape])
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }

def get_btv_54_digi(file):
    """
    Access and convert the data from BTV54 to a dictionary.
    """

    try:
        dt = file["AwakeEventData/TT41.BTV.412354.DigiCam/ExtractionImage/image2D"][:]
        dt = _rebin(dt, [i // 4 for i in dt.shape])
    except :
        dt = [[]]

    try:
        timestamp = file["AwakeEventData/TT41.BTV.412354.DigiCam/ExtractionImage/"].attrs['acqStamp']
    except :
        timestamp = "Reading Error"

    try:
        excep = file["AwakeEventData/TT41.BTV.412354.DigiCam/ExtractionImage/"].attrs['exception']
    except :
        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }






def get_btv_is1_core_pxi(file):
    """
    Access and convert the data from IS1 Core to a dictionary.
    """

    try:
        image = file["/AwakeEventData/BOVWA.07TCC4.AWAKECAM07/ExtractionImage/"]
        dt = image['imageRawData'][:]
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
        dt = _rebin(dt, [i // 4 for i in dt.shape])
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"
    
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }

def get_btv_is1_core_digi(file):
    """
    Access and convert the data from IS1 Core to a dictionary.
    
    -TT41.BTV.412426.CORE.DigiCam 
    
    -TT41.BTV.412426.DigiCam
    """

    try:
        dt = file["/AwakeEventData/TT41.BTV.412426.CORE.DigiCam/ExtractionImage/image2D"][:]
        dt = _rebin(dt, [i // 4 for i in dt.shape])
    except :
        dt = [[]]

    try:
        timestamp = file["/AwakeEventData/TT41.BTV.412426.CORE.DigiCam/ExtractionImage/"].attrs['acqStamp']
    except :
        timestamp = "Reading Error"

    try:
        excep = file["/AwakeEventData/TT41.BTV.412426.CORE.DigiCam/ExtractionImage/"].attrs['exception']
    except :
        excep = "Reading Error"
    
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }





def get_btv_is1_halo_pxi(file):
    """
    Access and convert the data from IS1 Halo to a dictionary.
    """


    try:
        image = file["/AwakeEventData/BOVWA.06TCC4.AWAKECAM06/ExtractionImage/"]
        dt = image['imageRawData'][:]
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
        dt = _rebin(dt, [i // 4 for i in dt.shape])
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"

#    try:
#        dt = file["/AwakeEventData/BOVWA.06TCC4.AWAKECAM06/ExtractionImage/imageRawData"][:]
#        dt = _rebin(dt, [i // 4 for i in dt.shape])
#    except :
#        dt = [[]]
#
#    try:
#        timestamp = file["/AwakeEventData/BOVWA.06TCC4.AWAKECAM06/ExtractionImage/"].attrs['acqStamp']
#    except :
#        timestamp = "Reading Error"
#
#    try:
#        excep = file["/AwakeEventData/BOVWA.06TCC4.AWAKECAM06/ExtractionImage/"].attrs['exception']
#    except :
#        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }

def get_btv_is1_halo_digi(file):
    """
    Access and convert the data from IS1 Halo to a dictionary.
    """

    try:
        dt = file["/AwakeEventData/TT41.BTV.412426.HALO.DigiCam/ExtractionImage/image2D"][:]
        dt = _rebin(dt, [i // 4 for i in dt.shape])
    except :
        dt = [[]]

    try:
        timestamp = file["/AwakeEventData/TT41.BTV.412426.HALO.DigiCam/ExtractionImage/"].attrs['acqStamp']
    except :
        timestamp = "Reading Error"

    try:
        excep = file["/AwakeEventData/TT41.BTV.412426.HALO.DigiCam/ExtractionImage/"].attrs['exception']
    except :
        excep = "Reading Error"
    
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }

def get_btv_is2_core_pxi(file):
    """
    Access and convert the data from IS2 Core to a dictionary.
    """


    try:
        image = file["/AwakeEventData/BOVWA.02TCC4.AWAKECAM02/ExtractionImage/"]
        dt = image['imageRawData'][:]
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
        dt = _rebin(dt, [i // 4 for i in dt.shape])
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"

#    try:
#        dt = file["/AwakeEventData/BOVWA.02TCC4.AWAKECAM02/ExtractionImage/imageRawData"][:]
#        dt = _rebin(dt, [i // 4 for i in dt.shape])
#    except :
#        dt = [[]]
#
#    try:
#        timestamp = file["/AwakeEventData/BOVWA.02TCC4.AWAKECAM02/ExtractionImage/"].attrs['acqStamp']
#    except :
#        timestamp = "Reading Error"
#
#    try:
#        excep = file["/AwakeEventData/BOVWA.02TCC4.AWAKECAM02/ExtractionImage/"].attrs['exception']
#    except :
#        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }

def get_btv_is2_core_digi(file):
    """
    Access and convert the data from IS2 Core to a dictionary.
    
    -TT41.BTV.412442.DigiCam
    
    """

    try:
        dt = file["/AwakeEventData/TT41.BTV.412442.DigiCam/ExtractionImage/image2D"][:]
        dt = _rebin(dt, [i // 4 for i in dt.shape])
    except :
        dt = [[]]

    try:
        timestamp = file["/AwakeEventData/TT41.BTV.412442.DigiCam/ExtractionImage/"].attrs['acqStamp']
    except :
        timestamp = "Reading Error"

    try:
        excep = file["/AwakeEventData/TT41.BTV.412442.DigiCam/ExtractionImage/"].attrs['exception']
    except :
        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }




def get_btv_is2_halo_pxi(file):
    """
    Access and convert the data from IS2 Halo to a dictionary.
    """


    try:
        image = file["/AwakeEventData/BOVWA.04TCC4.AWAKECAM04/ExtractionImage/"]
        dt = image['imageRawData'][:]
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
        dt = _rebin(dt, [i // 4 for i in dt.shape])
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"

#    try:
#        dt = file["/AwakeEventData/BOVWA.04TCC4.AWAKECAM04/ExtractionImage/imageRawData"][:]
#        dt = _rebin(dt, [i // 4 for i in dt.shape])
#    except :
#        dt = [[]]
#
#    try:
#        timestamp = file["/AwakeEventData/BOVWA.04TCC4.AWAKECAM04/ExtractionImage/"].attrs['acqStamp']
#    except :
#        timestamp = "Reading Error"
#
#    try:
#        excep = file["/AwakeEventData/BOVWA.04TCC4.AWAKECAM04/ExtractionImage/"].attrs['exception']
#    except :
#        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }

def get_btv_is2_halo_digi(file):
    """
    Access and convert the data from IS2 Halo to a dictionary.
    """
    
    try:
        dt = file["/AwakeEventData/TT41.BTV.412442.HALO.DigiCam/ExtractionImage/image2D"][:]
        dt = _rebin(dt, [i // 4 for i in dt.shape])
    except :
        dt = [[]]

    try:
        timestamp = file["/AwakeEventData/TT41.BTV.412442.HALO.DigiCam/ExtractionImage/"].attrs['acqStamp']
    except :
        timestamp = "Reading Error"

    try:
        excep = file["/AwakeEventData/TT41.BTV.412442.HALO.DigiCam/ExtractionImage/"].attrs['exception']
    except :
        excep = "Reading Error"
    
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }


def get_otr_angle_pxi(file):
    """
    Access and convert the data from AWAKECAM01 (OTR Angle) to a dictionary
    """


    try:
        image = file["/AwakeEventData/BOVWA.01TCC4.AWAKECAM01/ExtractionImage/"]
        dt = image['imageRawData'][:]
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
        dt = _rebin(dt, [i // 4 for i in dt.shape])
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"

#    try:
#        dt = file["/AwakeEventData/BOVWA.01TCC4.AWAKECAM01/ExtractionImage/imageRawData"][:]
#        dt = _rebin(dt, [i // 4 for i in dt.shape])
#    except :
#        dt = [[]]
#
#    try:
#        timestamp = file["/AwakeEventData/BOVWA.01TCC4.AWAKECAM01/ExtractionImage/"].attrs['acqStamp']
#    except :
#        timestamp = "Reading Error"
#
#    try:
#        excep = file["/AwakeEventData/BOVWA.01TCC4.AWAKECAM01/ExtractionImage/"].attrs['exception']
#    except :
#        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }



def get_exp_vol_1_pxi(file):
    """
    Access and convert the data from AWAKECAM03 (Expansion Volume 1) to a dictionary.
    """


    try:
        image = file["/AwakeEventData/BOVWA.03TCC4.AWAKECAM03/ExtractionImage/"]
        dt = image['imageRawData'][:]
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
        dt = _rebin(dt, [i // 4 for i in dt.shape])
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"

#    try:
#        dt = file["/AwakeEventData/BOVWA.03TCC4.AWAKECAM03/ExtractionImage/imageRawData"][:]
#        dt = _rebin(dt, [i // 4 for i in dt.shape])
#    except :
#        dt = [[]]
#
#    try:
#        timestamp = file["/AwakeEventData/BOVWA.03TCC4.AWAKECAM03/ExtractionImage/"].attrs['acqStamp']
#    except :
#        timestamp = "Reading Error"
#
#    try:
#        excep = file["/AwakeEventData/BOVWA.03TCC4.AWAKECAM03/ExtractionImage/"].attrs['exception']
#    except :
#        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }


def get_exp_vol_1_digi(file):
    """
    Access and convert the data from TT41.BTV.412354.EXPVOL1.DigiCam (Expansion Volume 1) to a dictionary.
    """

    try:
        dt = file["AwakeEventData/TT41.BTV.412354.EXPVOL1.DigiCam/ExtractionImage/image2D"][:]
        dt = _rebin(dt, [i // 4 for i in dt.shape])
    except :
        dt = [[]]

    try:
        timestamp = file["/AwakeEventData/TT41.BTV.412354.EXPVOL1.DigiCam/ExtractionImage/"].attrs['acqStamp']
    except :
        timestamp = "Reading Error"

    try:
        excep = file["/AwakeEventData/TT41.BTV.412354.EXPVOL1.DigiCam/ExtractionImage/"].attrs['exception']
    except :
        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff}


# =============================================================================
# Streak cameras
# =============================================================================


def get_streak_1_analogue(file):
    """
    Access and convert the data from XMPP-STREAK to a dictionary.
    """

    try:
        image = file["/AwakeEventData/XMPP-STREAK/StreakImage"]
        dt = image["streakImageData"][:].reshape(512, 672)
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False  

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }




def get_streak_2_analogue(file):
    """
    Access and convert the data from TT41.BTV.412350.STREAK to a dictionary.
    """

    try:
        dt = file["/AwakeEventData/TT41.BTV.412350.STREAK/StreakImage/streakImageData"][:].reshape(512, 672)
    except :
        dt = [[]]

    try:
        timestamp = file["/AwakeEventData/TT41.BTV.412350.STREAK/StreakImage/"].attrs['acqStamp']
    except :
        timestamp = "Reading Error"

    try:
        excep = file["/AwakeEventData/TT41.BTV.412350.STREAK/StreakImage/"].attrs['exception']
    except :
        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }



def get_MPP_streak_pxi(file):
    """
    Access and convert the data from AWAKECAM05 to a dictionary.
    """
    try:
        dt = file["/AwakeEventData/BOVWA.05TCC4.AWAKECAM05/ExtractionImage/imageRawData"][:]
        dt = _rebin(dt, [i // 4 for i in dt.shape])
    except :
        dt = [[]]

    try:
        timestamp = file["/AwakeEventData/BOVWA.05TCC4.AWAKECAM05/ExtractionImage/"].attrs['acqStamp']
    except :
        timestamp = "Reading Error"

    try:
        excep = file["/AwakeEventData/BOVWA.05TCC4.AWAKECAM05/ExtractionImage/"].attrs['exception']
    except :
        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }

def get_MPP_streak_digi(file):
    """
    Access and convert the data from TSG41.MPPSTREAK-REF.DigiCam to a dictionary.
    """
    try:
        dt = file["/AwakeEventData/TSG41.MPPSTREAK-REF.DigiCam/ExtractionImage/image2D"][:]
        dt = _rebin(dt, [i // 4 for i in dt.shape])
    except :
        dt = [[]]

    try:
        timestamp = file["/AwakeEventData/TSG41.MPPSTREAK-REF.DigiCam/ExtractionImage/"].attrs['acqStamp']
    except :
        timestamp = "Reading Error"

    try:
        excep = file["/AwakeEventData/TSG41.MPPSTREAK-REF.DigiCam/ExtractionImage/"].attrs['exception']
    except :
        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }

def get_BTV_streak_pxi(file):
    """
    Access and convert the data from WAKECAM08 to a dictionary.
    """
    try:
        dt = file["/AwakeEventData/BOVWA.08TCC4.AWAKECAM08/ExtractionImage/imageRawData"][:]
        dt = _rebin(dt, [i // 4 for i in dt.shape])
    except :
        dt = [[]]

    try:
        timestamp = file["/AwakeEventData/BOVWA.08TCC4.AWAKECAM08/ExtractionImage/"].attrs['acqStamp']
    except :
        timestamp = "Reading Error"

    try:
        excep = file["/AwakeEventData/BOVWA.08TCC4.AWAKECAM08/ExtractionImage/"].attrs['exception']
    except :
        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }

def get_BTV_streak_digi(file):
    """
    Access and convert the data from TT41.BTVSTREAK-REF.DigiCam to a dictionary.
    """
    try:
        dt = file["/AwakeEventData/TT41.BTVSTREAK-REF.DigiCam/ExtractionImage/image2D"][:]
        dt = _rebin(dt, [i // 4 for i in dt.shape])
    except :
        dt = [[]]

    try:
        timestamp = file["/AwakeEventData/TT41.BTVSTREAK-REF.DigiCam/ExtractionImage/"].attrs['acqStamp']
    except :
        timestamp = "Reading Error"

    try:
        excep = file["/AwakeEventData/TT41.BTVSTREAK-REF.DigiCam/ExtractionImage/"].attrs['exception']
    except :
        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }

# =============================================================================
# Beam positon monitors
# =============================================================================

def get_bpm_electron_1(file):

    try:
        hor_dt = file["/AwakeEventData/TT41.BPM.412349/Acquisition/horPos"][:]
        ver_dt = file["/AwakeEventData/TT41.BPM.412349/Acquisition/verPos"][:]
        sps_extraction = ["/AwakeEventData/TT41.BPM.412349/Acquisition/spsExtraction"]
        hor_slice  = np.ma.masked_invalid(hor_dt)
        ver_slice = np.ma.masked_invalid(ver_dt)
        if sps_extraction == True:
            index = file["/AwakeEventData/TT41.BPM.412349/Acquisition/spsExtractionIndex"]
            hor_slice.mask[index] = True
            ver_slice.mask[index] = True       
        avg_hor_dt = np.ma.mean(hor_slice)
        avg_ver_dt = np.ma.mean(ver_slice)
    except:
        avg_hor_dt = []
        avg_ver_dt = []
    try:
        timestamp = file["/AwakeEventData/TT41.BPM.412349/Acquisition/"].attrs['acqStamp']
    except :
        timestamp = "Reading Error"

    try:
        excep = file["/AwakeEventData/TT41.BPM.412349/Acquisition/"].attrs['exception']
    except :
        excep = "Reading Error"

    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False


    return {'data' : {'hor' : avg_hor_dt, 'ver' : avg_ver_dt}, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }



def get_bpm_electron_2(file):

    try:
        hor_dt = file["/AwakeEventData/TT41.BPM.412351/Acquisition/horPos"][:]
        ver_dt = file["/AwakeEventData/TT41.BPM.412351/Acquisition/verPos"][:]
        sps_extraction = ["/AwakeEventData/TT41.BPM.412351/Acquisition/spsExtraction"]
        hor_slice  = np.ma.masked_invalid(hor_dt)
        ver_slice = np.ma.masked_invalid(ver_dt)
        if sps_extraction == True:
            index = file["/AwakeEventData/TT41.BPM.412351/Acquisition/spsExtractionIndex"]
            hor_slice.mask[index] = True
            ver_slice.mask[index] = True   
        avg_hor_dt = np.ma.mean(hor_slice)
        avg_ver_dt = np.ma.mean(ver_slice)
    except:
        avg_hor_dt = []
        avg_ver_dt = []

    try:
        timestamp = file["/AwakeEventData/TT41.BPM.412351/Acquisition/"].attrs['acqStamp']
    except :
        timestamp = "Reading Error"

    try:
        excep = file["/AwakeEventData/TT41.BPM.412351/Acquisition/"].attrs['exception']
    except :
        excep = "Reading Error"

    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False


    return {'data' : {'hor' : avg_hor_dt, 'ver' : avg_ver_dt}, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }

def get_bpm_proton_1(file):
    
    try:
        hor_dt = file["/AwakeEventData/BPM.AWAKE.TT41/Acquisition/horPosition"][16] + gold_traject['proton_bpm_1']['x']
        ver_dt = file["/AwakeEventData/BPM.AWAKE.TT41/Acquisition/verPosition"][16] + gold_traject['proton_bpm_1']['y']
        name = file["/AwakeEventData/BPM.AWAKE.TT41/Acquisition/bpmNames"][16]
    except:
        hor_dt = []
        ver_dt = []
        name = ""

    try:
        timestamp = file["/AwakeEventData/BPM.AWAKE.TT41/Acquisition/"].attrs['acqStamp']
    except :
        timestamp = "Reading Error"

    try:
        excep = file["/AwakeEventData/BPM.AWAKE.TT41/Acquisition/"].attrs['exception']
    except :
        excep = "Reading Error"

    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False


    return {'data' : {'hor' : hor_dt, 'ver' : ver_dt}, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff, 'name' : name }

def get_bpm_proton_2(file):

    try:
        hor_dt = file["/AwakeEventData/BPM.AWAKE.TT41/Acquisition/horPosition"][17] + gold_traject['proton_bpm_2']['x']
        ver_dt = file["/AwakeEventData/BPM.AWAKE.TT41/Acquisition/verPosition"][17] + gold_traject['proton_bpm_2']['y']
        name = file["/AwakeEventData/BPM.AWAKE.TT41/Acquisition/bpmNames"][17]
    except:
        hor_dt = []
        ver_dt = []
        name = ""
        
    try:
        timestamp = file["/AwakeEventData/BPM.AWAKE.TT41/Acquisition/"].attrs['acqStamp']
    except :
        timestamp = "Reading Error"

    try:
        excep = file["/AwakeEventData/BPM.AWAKE.TT41/Acquisition/"].attrs['exception']
    except :
        excep = "Reading Error"

    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False


    return {'data' : {'hor' : hor_dt, 'ver' : ver_dt}, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff, 'name' : name }

def get_bpm_proton_3(file):

    try:
        hor_dt = file["/AwakeEventData/BPM.AWAKE.TT41/Acquisition/horPosition"][18] + gold_traject['proton_bpm_3']['x']
        ver_dt = file["/AwakeEventData/BPM.AWAKE.TT41/Acquisition/verPosition"][18] + gold_traject['proton_bpm_3']['y']
        name = file["/AwakeEventData/BPM.AWAKE.TT41/Acquisition/bpmNames"][18]
    except:
        hor_dt = []
        ver_dt = []
        name = ""
    
    try:
        timestamp = file["/AwakeEventData/BPM.AWAKE.TT41/Acquisition/"].attrs['acqStamp']
    except :
        timestamp = "Reading Error"

    try:
        excep = file["/AwakeEventData/BPM.AWAKE.TT41/Acquisition/"].attrs['exception']
    except :
        excep = "Reading Error"

    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False


    return {'data' : {'hor' : hor_dt, 'ver' : ver_dt}, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff, 'name' : name }

def get_bpm_proton_4(file):

    
    try:
        hor_dt = file["/AwakeEventData/BPM.AWAKE.TT41/Acquisition/horPosition"][19]  + gold_traject['proton_bpm_4']['x']
        ver_dt = file["/AwakeEventData/BPM.AWAKE.TT41/Acquisition/verPosition"][19]  + gold_traject['proton_bpm_4']['y']
        name = file["/AwakeEventData/BPM.AWAKE.TT41/Acquisition/bpmNames"][19]
    except:
        hor_dt = []
        ver_dt = []
        name = ""       

    try:
        timestamp = file["/AwakeEventData/BPM.AWAKE.TT41/Acquisition/"].attrs['acqStamp']
    except :
        timestamp = "Reading Error"

    try:
        excep = file["/AwakeEventData/BPM.AWAKE.TT41/Acquisition/"].attrs['exception']
    except :
        excep = "Reading Error"

    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False


    return {'data' : {'hor' : hor_dt, 'ver' : ver_dt}, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff, 'name' : name }

def get_bpm_proton_5(file):

   
    try:
        hor_dt = file["/AwakeEventData/BPM.AWAKE.TT41/Acquisition/horPosition"][20]  + gold_traject['proton_bpm_5']['x']
        ver_dt = file["/AwakeEventData/BPM.AWAKE.TT41/Acquisition/verPosition"][20]  + gold_traject['proton_bpm_5']['y']
        name = file["/AwakeEventData/BPM.AWAKE.TT41/Acquisition/bpmNames"][20]
    except:
        hor_dt = []
        ver_dt = []
        name = ""         

    try:
        timestamp = file["/AwakeEventData/BPM.AWAKE.TT41/Acquisition/"].attrs['acqStamp']
    except :
        timestamp = "Reading Error"

    try:
        excep = file["/AwakeEventData/BPM.AWAKE.TT41/Acquisition/"].attrs['exception']
    except :
        excep = "Reading Error"

    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False


    return {'data' : {'hor' : hor_dt, 'ver' : ver_dt}, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff, 'name' : name }



# =============================================================================
# Laser energy meters
# =============================================================================


def get_emeter_2(file):

    try:
        dt = file["/AwakeEventData/EMETER02/Acq/value"][:]
    except:
        dt = []

    try:
        timestamp = file["/AwakeEventData/EMETER02/Acq/"].attrs['acqStamp']
    except :
        timestamp = "Reading Error"

    try:
        excep = file["/AwakeEventData/EMETER02/Acq/"].attrs['exception']
    except :
        excep = "Reading Error"

    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False


    return {'energy' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff}
  
def get_emeter_3(file):

    try:
        dt = file["/AwakeEventData/EMETER03/Acq/value"][:]
    except:
        dt = []

    try:
        timestamp = file["/AwakeEventData/EMETER03/Acq/"].attrs['acqStamp']
    except :
        timestamp = "Reading Error"

    try:
        excep = file["/AwakeEventData/EMETER03/Acq/"].attrs['exception']
    except :
        excep = "Reading Error"

    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False


    return {'energy' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff}
  
def get_emeter_4(file):

    try:
        dt = file["/AwakeEventData/EMETER04/Acq/value"][:]
    except:
        dt = []

    try:
        timestamp = file["/AwakeEventData/EMETER04/Acq/"].attrs['acqStamp']
    except :
        timestamp = "Reading Error"

    try:
        excep = file["/AwakeEventData/EMETER04/Acq/"].attrs['exception']
    except :
        excep = "Reading Error"

    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False


    return {'energy' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff}
  
def get_emeter_5(file):

    try:
        dt = file["/AwakeEventData/EMETER05/Acq/value"][:]
    except:
        dt = []

    try:
        timestamp = file["/AwakeEventData/EMETER05/Acq/"].attrs['acqStamp']
    except :
        timestamp = "Reading Error"

    try:
        excep = file["/AwakeEventData/EMETER05/Acq/"].attrs['exception']
    except :
        excep = "Reading Error"

    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False


    return {'energy' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff}


# =============================================================================
# Virtual line cameras
# =============================================================================

def get_laser_camera_1_pxi(file):
    """
    Access and convert the data from BOVWA.01TCV4.CAM8 to a dictionary.
    """

    try:
        dt = file["/AwakeEventData/BOVWA.01TCV4.CAM8/ExtractionImage/imageRawData"][:]
        dt = _rebin(dt, [i // 4 for i in dt.shape])
    except :
        dt = [[]]

    try:
        timestamp = file["/AwakeEventData/BOVWA.01TCV4.CAM8/ExtractionImage/"].attrs['acqStamp']
    except :
        timestamp = "Reading Error"

    try:
        excep = file["/AwakeEventData/BOVWA.01TCV4.CAM8/ExtractionImage/"].attrs['exception']
    except :
        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }

def get_laser_camera_2_pxi(file):
    """
    Access and convert the data from BOVWA.02TSG40.CAM7 to a dictionary.
    """

    try:
        dt = file["/AwakeEventData/BOVWA.02TSG40.CAM7/ExtractionImage/imageRawData"][:]
        dt = _rebin(dt, [i // 4 for i in dt.shape])
    except :
        dt = [[]]

    try:
        timestamp = file["/AwakeEventData/BOVWA.02TSG40.CAM7/ExtractionImage/"].attrs['acqStamp']
    except :
        timestamp = "Reading Error"

    try:
        excep = file["/AwakeEventData/BOVWA.02TSG40.CAM7/ExtractionImage/"].attrs['exception']
    except :
        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }

def get_laser_camera_3_pxi(file):
    """
    Access and convert the data from BOVWA.03TT41.CAM3 to a dictionary.
    """

    try:
        dt = file["/AwakeEventData/BOVWA.03TT41.CAM3/ExtractionImage/imageRawData"][:]
        dt = _rebin(dt, [i // 4 for i in dt.shape]) #integer division (making more pixellated by 4 times)
    except :
        dt = [[]]

    try:
        max_hor_amp = file["/AwakeEventData/BOVWA.03TT41.CAM3/ExtractionImage/horProjAmp"][0]   
    except :
        max_hor_amp = 0
    
    try:
        max_ver_amp = file["/AwakeEventData/BOVWA.03TT41.CAM3/ExtractionImage/verProjAmp"][0]  
    except :
        max_ver_amp = 0   
    
    try:
        timestamp = file["/AwakeEventData/BOVWA.03TT41.CAM3/ExtractionImage"].attrs['acqStamp']
    except :
        timestamp = "Reading Error"

    try:
        excep = file["/AwakeEventData/BOVWA.03TT41.CAM3/ExtractionImage"].attrs['exception']
    except :
        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff, 'max_hor_amp' : max_hor_amp, 'max_ver_amp' : max_ver_amp}

def get_laser_camera_4_pxi(file):
    """
    Access and convert the data from BOVWA.04TT41.CAM4 to a dictionary.
    """

    try:
        dt = file["/AwakeEventData/BOVWA.04TT41.CAM4/ExtractionImage/imageRawData"][:]
        dt = _rebin(dt, [i // 4 for i in dt.shape])
    except :
        dt = [[]]

    try:
        max_hor_amp = file["/AwakeEventData/BOVWA.04TT41.CAM4/ExtractionImage/horProjAmp"][0]
    except :
        max_hor_amp = 0
    
    try:
        max_ver_amp = file["/AwakeEventData/BOVWA.04TT41.CAM4/ExtractionImage/verProjAmp"][0]  
    except :
        max_ver_amp = 0
    
    try:
        timestamp = file["/AwakeEventData/BOVWA.04TT41.CAM4/ExtractionImage/"].attrs['acqStamp']
    except :
        timestamp = "Reading Error"

    try:
        excep = file["/AwakeEventData/BOVWA.04TT41.CAM4/ExtractionImage/"].attrs['exception']
    except :
        excep = "Reading Error"
    
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 


    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff, 'max_hor_amp' : max_hor_amp, 'max_ver_amp' : max_ver_amp }

def get_laser_camera_5_pxi(file):
    """
    Access and convert the data from BOVWA.05TT41.CAM5 to a dictionary.
    """

    try:
        dt = file["/AwakeEventData/BOVWA.05TT41.CAM5/ExtractionImage/imageRawData"][:]
        dt = _rebin(dt, [i // 4 for i in dt.shape])
    except :
        dt = [[]]

    try:
        max_hor_amp = file["/AwakeEventData/BOVWA.05TT41.CAM5/ExtractionImage/horProjAmp"][0]
    except :
        max_hor_amp = 0
    
    try:
        max_ver_amp = file["/AwakeEventData/BOVWA.05TT41.CAM5/ExtractionImage/verProjAmp"][0] 
    except :
        max_ver_amp = 0
    
    try:
        timestamp = file["/AwakeEventData/BOVWA.05TT41.CAM5/ExtractionImage/"].attrs['acqStamp']
    except :
        timestamp = "Reading Error"

    try:
        excep = file["/AwakeEventData/BOVWA.05TT41.CAM5/ExtractionImage/"].attrs['exception']
    except :
        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff, 'max_hor_amp' : max_hor_amp, 'max_ver_amp' : max_ver_amp }




def get_laser_camera_1_digi(file):
    """
    Access and convert the data from BOVWA.01TCV4.CAM8 to a dictionary.
    """

    try:
        dt = file["/AwakeEventData/TT41.VLC1.DigiCam/ExtractionImage/image2D"][:]
        dt = _rebin(dt, [i // 4 for i in dt.shape])
    except :
        dt = [[]]

    try:
        timestamp = file["/AwakeEventData/TT41.VLC1.DigiCam/ExtractionImage/"].attrs['acqStamp']
    except :
        timestamp = "Reading Error"
        
    try:
        max_hor_amp = file["/AwakeEventData/TT41.VLC1.DigiCam/ExtractionImage/horProjAmp"][0]   
    except :
        max_hor_amp = 0
    
    try:
        max_ver_amp = file["/AwakeEventData/TT41.VLC1.DigiCam/ExtractionImage/verProjAmp"][0]  
    except :
        max_ver_amp = 0   
            

    try:
        excep = file["/AwakeEventData/TT41.VLC1.DigiCam/ExtractionImage/"].attrs['exception']
    except :
        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff, 'max_hor_amp' : max_hor_amp, 'max_ver_amp' : max_ver_amp }

def get_laser_camera_2_digi(file):
    """
    Access and convert the data from BOVWA.02TSG40.CAM7 to a dictionary.
    """
    try:
        dt = file["/AwakeEventData/TT41.VLC2.DigiCam/ExtractionImage/image2D"][:]
        dt = _rebin(dt, [i // 4 for i in dt.shape])
    except :
        dt = [[]]

    try:
        timestamp = file["/AwakeEventData/TT41.VLC2.DigiCam/ExtractionImage/"].attrs['acqStamp']
    except :
        timestamp = "Reading Error"
    try:
        max_hor_amp = file["/AwakeEventData/TT41.VLC2.DigiCam/ExtractionImage/horProjAmp"][0]   
    except :
        max_hor_amp = 0
    
    try:
        max_ver_amp = file["/AwakeEventData/TT41.VLC2.DigiCam/ExtractionImage/verProjAmp"][0]  
    except :
        max_ver_amp = 0           

    try:
        excep = file["/AwakeEventData/TT41.VLC2.DigiCam/ExtractionImage/"].attrs['exception']
    except :
        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff, 'max_hor_amp' : max_hor_amp, 'max_ver_amp' : max_ver_amp }

def get_laser_camera_3_digi(file):
    """
    Access and convert the data from BOVWA.03TT41.CAM3 to a dictionary.
    """

    try:
        dt = file["/AwakeEventData/TT41.VLC3.DigiCam/ExtractionImage/image2D"][:]
        dt = _rebin(dt, [i // 4 for i in dt.shape])
    except :
        dt = [[]]

    try:
        timestamp = file["/AwakeEventData/TT41.VLC3.DigiCam/ExtractionImage/"].attrs['acqStamp']
    except :
        timestamp = "Reading Error"
    try:
        max_hor_amp = file["/AwakeEventData/TT41.VLC3.DigiCam/ExtractionImage/horProjAmp"][0]   
    except :
        max_hor_amp = 0
    
    try:
        max_ver_amp = file["/AwakeEventData/TT41.VLC3.DigiCam/ExtractionImage/verProjAmp"][0]  
    except :
        max_ver_amp = 0   
    try:
        excep = file["/AwakeEventData/TT41.VLC3.DigiCam/ExtractionImage/"].attrs['exception']
    except :
        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff, 'max_hor_amp' : max_hor_amp, 'max_ver_amp' : max_ver_amp }


def get_laser_camera_4_digi(file):
    """
    Access and convert the data from BOVWA.04TT41.CAM4 to a dictionary.
    """

    try:
        dt = file["/AwakeEventData/TT41.VLC4.DigiCam/ExtractionImage/image2D"][:]
        dt = _rebin(dt, [i // 4 for i in dt.shape])
    except :
        dt = [[]]

    try:
        timestamp = file["/AwakeEventData/TT41.VLC4.DigiCam/ExtractionImage/"].attrs['acqStamp']
    except :
        timestamp = "Reading Error"
    try:
        max_hor_amp = file["/AwakeEventData/TT41.VLC4.DigiCam/ExtractionImage/horProjAmp"][0]   
    except :
        max_hor_amp = 0
    
    try:
        max_ver_amp = file["/AwakeEventData/TT41.VLC4.DigiCam/ExtractionImage/verProjAmp"][0]  
    except :
        max_ver_amp = 0   
    try:
        excep = file["/AwakeEventData/TT41.VLC4.DigiCam/ExtractionImage/"].attrs['exception']
    except :
        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff, 'max_hor_amp' : max_hor_amp, 'max_ver_amp' : max_ver_amp }


def get_laser_camera_5_digi(file):
    """
    Access and convert the data from BOVWA.05TT41.CAM5 to a dictionary.
    """

    try:
        dt = file["/AwakeEventData/TT41.VLC5.DigiCam/ExtractionImage/image2D"][:]
        dt = _rebin(dt, [i // 4 for i in dt.shape])
    except :
        dt = [[]]

    try:
        timestamp = file["/AwakeEventData/TT41.VLC5.DigiCam/ExtractionImage/"].attrs['acqStamp']
    except :
        timestamp = "Reading Error"
    try:
        max_hor_amp = file["/AwakeEventData/TT41.VLC5.DigiCam/ExtractionImage/horProjAmp"][0]   
    except :
        max_hor_amp = 0
    
    try:
        max_ver_amp = file["/AwakeEventData/TT41.VLC5.DigiCam/ExtractionImage/verProjAmp"][0]  
    except :
        max_ver_amp = 0   
    try:
        excep = file["/AwakeEventData/TT41.VLC5.DigiCam/ExtractionImage/"].attrs['exception']
    except :
        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 


    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff, 'max_hor_amp' : max_hor_amp, 'max_ver_amp' : max_ver_amp }



# =============================================================================
# DMD cameras
# =============================================================================



def get_dmd_1_digi(file):
    """
    Access and convert the data from TCC4.DMD1.DigiCam to a dictionary.
    
    """

    try:
        dt = file["/AwakeEventData/TCC4.DMD1.DigiCam/ExtractionImage/image2D"][:]
        dt = _rebin(dt, [i // 2 for i in dt.shape])
    except :
        dt = [[]]

    try:
        timestamp = file["/AwakeEventData/TCC4.DMD1.DigiCam/ExtractionImage/"].attrs['acqStamp']
    except :
        timestamp = "Reading Error"

    try:
            excep = file["/AwakeEventData/TCC4.DMD1.DigiCam/ExtractionImage/"].attrs['exception']
    except :
        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff}


def get_dmd_1_pxi(file):
    """
    Access and convert the data from AWAKECAM01 (DMD 1) to a dictionary.
    """

    try:
        dt = file["/AwakeEventData/BOVWA.01TCC4.AWAKECAM01/ExtractionImage/imageRawData"][:]
        dt = _rebin(dt, [i // 2 for i in dt.shape])
    except :
        dt = [[]]

    try:
        timestamp = file["/AwakeEventData/BOVWA.01TCC4.AWAKECAM01/ExtractionImage/"].attrs['acqStamp']
    except :
        timestamp = "Reading Error"

    try:
        excep = file["/AwakeEventData/BOVWA.01TCC4.AWAKECAM01/ExtractionImage/"].attrs['exception']
    except :
        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }



def get_dmd_2_digi(file):
    """
    Access and convert the data from TCC4.DMD2.DigiCam to a dictionary.

    """

    try:
        dt = file["/AwakeEventData/TCC4.DMD2.DigiCam/ExtractionImage/image2D"][:]
        dt = _rebin(dt, [i // 2 for i in dt.shape])
    except :
        dt = [[]]

    try:
        timestamp = file["/AwakeEventData/TCC4.DMD2.DigiCam/ExtractionImage/"].attrs['acqStamp']
    except :
        timestamp = "Reading Error"

    try:
            excep = file["/AwakeEventData/TCC4.DMD2.DigiCam/ExtractionImage/"].attrs['exception']
    except :
        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }



def get_dmd_2_pxi(file):
    """
    Access and convert the data from AWAKECAM12 (DMD 2) to a dictionary.
    """

    try:
        dt = file["/AwakeEventData/BOVWA.12TCC4.AWAKECAM12/ExtractionImage/imageRawData"][:]
        dt = _rebin(dt, [i // 2 for i in dt.shape])
    except :
        dt = [[]]

    try:
        timestamp = file["/AwakeEventData/BOVWA.12TCC4.AWAKECAM12/ExtractionImage/"].attrs['acqStamp']
    except :
        timestamp = "Reading Error"

    try:
        excep = file["/AwakeEventData/BOVWA.12TCC4.AWAKECAM12/ExtractionImage/"].attrs['exception']
    except :
        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }

# =============================================================================
# Plasma diagnostics
# =============================================================================
def get_spectrometer(file):
    """
    Access and convert the data from XUCL-SPECTRO to a dictionary.
    """

    try:
        dt = file["/AwakeEventData/XUCL-SPECTRO/ImageAcq/imageData"][:].reshape(512, 2048)
        dt = _rebin(dt, [i // 2 for i in dt.shape])
    except :
        dt = [[]]

    try:
        timestamp = file["/AwakeEventData/XUCL-SPECTRO/ImageAcq/"].attrs['acqStamp']
    except :
        timestamp = "Reading Error"

    try:
        excep = file["/AwakeEventData/XUCL-SPECTRO/ImageAcq/"].attrs['exception']
    except :
        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }




def get_plasma_light(file):

    try:
        dt = file['AwakeEventData/XMPP-SPECTRO/ImageAcq/imageData'][:]
        image = np.array(file['AwakeEventData/XMPP-SPECTRO/ImageAcq/imageData'], dtype=float)
        up = np.sum(image[350:550])-4*np.sum(image[50:100])
        down =  np.sum(image[150:350])-4*np.sum(image[50:100])

    except:
        dt = []
        up = "Reading Error"
        down = "Reading Error"

    try:
        timestamp = file["AwakeEventData/XMPP-SPECTRO/ImageAcq/"].attrs['acqStamp']
    except :
        timestamp = "Reading Error"

    try:
        excep = file["AwakeEventData/XMPP-SPECTRO/ImageAcq/"].attrs['exception']
    except :
        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'image' : dt, 'point'  : {'upstream' : up, 'downstream' : down} , 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }
    

# =============================================================================
# Proton intensity
# =============================================================================


def get_proton_charge(file):
    
    try:
        dt = file["/AwakeEventData/TT40.BCTFI.400344/CaptureAcquisition/totalIntensity"][:]
    except:
        dt = []

    try:
        timestamp = file['/AwakeEventData/TT40.BCTFI.400344/CaptureAcquisition/'].attrs['acqStamp']
    except :
        timestamp = "Reading Error"

    try:
        excep = file["/AwakeEventData/TT40.BCTFI.400344/CaptureAcquisition/"].attrs['exception']
    except :
        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'charge' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }


def get_bunch_population(file):
    """
    Get bunch population.
    """

    try:
        population = file["/AwakeEventData/TT41.BCTF.412340/CaptureAcquisition/totalIntensity"][0]
    except:
        population = 0.0

    return population



def get_timestamp(file):
    """
    Get event timestamp and convert it to time.
    """

    try:
        event_timestamp = round(file["/AwakeEventInfo/timestamp/"][...]/1E9, ndigits=5)
        event_time = time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(event_timestamp))
    except:
        event_timestamp = "Reading Error"
        event_time = "Reading Error"

    return (event_timestamp, event_time)

def get_event_number(file):
    """
    Get event number.
    """

    try:
        event_number = file["/AwakeEventInfo/eventNumber/"][...]
    except:
        event_number = "Reading Error"

    return event_number




def _merge_fold_paths(path1, path2):
    """
    Append the month and day directories in path2 to path1 
    """

    tmp1 = path2.split('/')[-2] + '/' + path2.split('/')[-1]
    return os.path.join(path1, tmp1)

def find_lastest_subdirectory(path):
    """
    Find the most recently updated folder.
    """
    list_folders_1 = [i for i in os.listdir(path)  if os.path.isdir(os.path.join(path,i)) and (i[0] != '.') ]
    newest_folder = max([os.path.join(path,d) for d in list_folders_1], key=os.path.getmtime)

    list_folders_2 = [i for i in os.listdir(newest_folder)  if os.path.isdir(os.path.join(newest_folder,i)) and (i[0] != '.') ]
    newest_folder_2 = max([os.path.join(newest_folder,d) for d in list_folders_2], key=os.path.getmtime)

    return newest_folder_2

def det_run_number(ename):
    return int(ename.split('_')[2]) 
def _rebin(a, shape): #'shape' is smaller version but same ratios
    sh = shape[0],a.shape[0]//shape[0],shape[1],a.shape[1]//shape[1]
    return a.reshape(sh).mean(-1).mean(1) #sh shape must be compatible with a shape

def nearest(items, pivot):
    return min(items, key=lambda x: abs(x - pivot))


loop_events(update_interval = 1)
