#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  8 09:53:31 2022

@author: vasylhafyc, francescaelverson, gzevi

"""

import sys
sys.path.insert(0,'/user/awakeop/AwakeEventPreview/src/')
from functions import *


# Get current date
COLLECT_DATA_TIME_NORM = (datetime.now()-timedelta(seconds = 30)).strftime('%Y-%m-%d %H:%M:%S')
#COLLECT_DATA_TIME_NORM = "2022-06-08 12:05:00" # input date in format "YYYY-mm-dd HH:MM:SS" between quotation marks                                                                                                                           
COLLECT_DATA_TIME_EPOCH = 0000000000
# no
if bool(COLLECT_DATA_TIME_NORM) == True:
    collect_data_time_object = datetime.strptime(COLLECT_DATA_TIME_NORM, '%Y-%m-%d %H:%M:%S')
    COLLECT_DATA_TIME_EPOCH = int(str(collect_data_time_object.timestamp())[0:10])

# Warning sound#
#SOUND_1= '/user/awakeop/felverson/quick_tools/246332__kwahmah-02__five-beeps.wav' 
#SOUND_1= '/user/awakeop/fernpannell/JRsound32bitstereo.wav' 
#SOUND_1 = '/user/awakeop/felverson/quick_tools/JRsound.wav'
SOUND_1= '/user/awakeop/gzevi/sounds/central_c_piano_like.wav' 



# Directory path containing the h5 files; insert manually the relevant year
DATA_YEAR_PATH = '/user/awakeop/event_data/2023'

# Directory path where all historical graphics are stored; insert manually the relevant year
SAVE_PATH =  "/user/awakeop/AwakeEventPreview/events-preview/2023"

# Graphic font parameters
plt.style.use('dark_background')
SMALL_SIZE = 7
MEDIUM_SIZE = 9
BIGGER_SIZE = 10
plt.rc("font", size=SMALL_SIZE)
plt.rc("axes", titlesize=MEDIUM_SIZE) 
plt.rc("axes", labelsize=MEDIUM_SIZE) 
plt.rc("xtick", labelsize=SMALL_SIZE) 
plt.rc("ytick", labelsize=SMALL_SIZE) 
plt.rc("legend", fontsize=SMALL_SIZE) 
plt.rc("figure", titlesize=BIGGER_SIZE)
plt.style.use('dark_background')

start_time = [] 

file_timestamp = 0


# File paths where the most recently updated preview graphics are stored
PREVIEW_PATHS = { 'MINI_IMGS_PATH' : "/user/awakeop/AwakeEventPreview/events-preview/img.pdf", # Mini diagnostics DQM (all detectors)
                  'MINI_IMGS_LASER_PATH' : "/user/awakeop/AwakeEventPreview/events-preview/raw_imgs_laser.pdf", # Mini diagnostics DQM (laser-relevant detectors)
                  'PROTON_OPTICS_PATH' : "/user/awakeop/AwakeEventPreview/events-preview/img_proton_optics.pdf", # Proton optics
                  'TIMELINE_PROTONS_PATH' : "/user/awakeop/AwakeEventPreview/events-preview/timeline-protons.pdf", # Proton BTV timeline (absolute trajectory),
                  'TIMELINE_LASER_PATH' : "/user/awakeop/AwakeEventPreview/events-preview/timeline-laser.pdf", # Laser VLC timeline (absolute trajectory),
                  'TIMELINE_BPM_PROTONS_PATH' : "/user/awakeop/AwakeEventPreview/events-preview/timeline-bpm-protons.pdf", # Proton BPM timeline (absolute trajectory),
                  'TIMELINE_BPM_ELECTRONS_PATH' : "/user/awakeop/AwakeEventPreview/events-preview/timeline-bpm-electrons.pdf", # Electron BPM timeline (absolute trajectory),
                  'TIMELINE_PLASMA_PATH' : "/user/awakeop/AwakeEventPreview/events-preview/timeline-plasma.pdf", # Plasma light timeline (absolute measurement),
                  'TIMELINE_AVERAGE_PROTONS_PATH' : "/user/awakeop/AwakeEventPreview/events-preview/timeline-average-protons.pdf", #  Proton BTV timeline (average trajectory),
                  'TIMELINE_AVERAGE_LASER_PATH' : "/user/awakeop/AwakeEventPreview/events-preview/timeline-average-laser.pdf", # Laser VLC timeline (average trajectory)
                  'TIMELINE_AVERAGE_BPM_PROTONS_PATH' : "/user/awakeop/AwakeEventPreview/events-preview/timeline-average-bpm-protons.pdf", # Proton BPM timeline (average trajectory)
                  'TIMELINE_AVERAGE_BPM_ELECTRONS_PATH' : "/user/awakeop/AwakeEventPreview/events-preview/timeline-average-bpm-electrons.pdf", # Electron BPM timeline (average trajectory)
                  #'TIMELINE_AVERAGE_PLASMA_PATH' : "/user/awakeop/AwakeEventPreview/events-preview/timeline-average-plasma.pdf", # Plasma light timeline (average measurement)
                  'TIMELINE_ALIGNMENT_PATH' :  "/user/awakeop/AwakeEventPreview/events-preview/timeline-alignment.pdf", # Electron BPM timeline (average trajectory)
                  }


def loop_events(update_interval = 5, ): 
    """
    The function plots a summary of individual events and the timeline plot from the most recently updated folder. The folder is updated every `update_interval=10` seconds, and new events are plotted if they appear. If the newest folder is created, it will be automatically considered for further updates.
    """  
    
    # Find the latest month and day
    newest_folder_old = find_lastest_subdirectory(DATA_YEAR_PATH) 
    
    # Get the most recent run number; include constraints on the h5 data 
    try:
        run_number_old = max([det_run_number(i) for i in os.listdir(newest_folder_old)
                          if _isvalid(i)  and _recentdata(i)]) 
    except Exception as e:
        print (e)
        return
    # Create empty save folder corresponding to the most recent day of data
    # (within a folder corresponding to the corresponding month i.e. SAVE_PATH/month/day/)
    save_folder = merge_fold_paths(SAVE_PATH, newest_folder_old) 
    if not os.path.exists(save_folder): os.makedirs(save_folder)
    
    files_all_old = [] # Empty list to store all filenames in directory
    ref_time = []
    ref_time_old = []
    ref_times_all = []
    ref = constr_ref()

    # Initialise dictionaries which will hold timeline data 
    timeline_data = constr_timeline_data()
    timeline_average = constr_timeline_data()
    alignment_tl = constr_alignment_tl()
    
    counter = 0
    
    # Initialise dictionaries which will hold the delta position data for two 
    # detectors per beam species
    curr_delta, avg_delta = constr_deltas()
    
    # Warnings
    warning_bool_new = constr_warning_bool()
    sound_bool = copy.deepcopy(warning_bool_new)
    warning_bool_prev = copy.deepcopy(warning_bool_new)

    while True:
        tic = time.perf_counter() # Start performance counter
       
        # Find the most recent month, and day, of data collection
        newest_folder = find_lastest_subdirectory(DATA_YEAR_PATH)
        # Get the most recent run number
        try: 
            run_number = max([det_run_number(i) for i in os.listdir(newest_folder) 
                                     if _isvalid(i)  and  _recentdata(i)])
        except: 
            run_number = False
        
        # Check if new HDF5 files correspond to a new run or a new day  
        # If so, redefine/update the save folders, latest day/run of data and
        # reset timeline arrays and HDF5 files list 
        if (newest_folder != newest_folder_old) or (run_number != run_number_old):
            newest_folder_old = copy.deepcopy(newest_folder)
            run_number_old = copy.deepcopy(run_number)
            # Create new empty save folder if new HDF5 files correspond to a new day  
            save_folder = merge_fold_paths(SAVE_PATH, newest_folder)
            if not os.path.exists(save_folder): os.makedirs(save_folder)
            # Initialise new dictionaries which will hold new timeline data 
            timeline_data = constr_timeline_data() 
            timeline_average = copy.deepcopy(timeline_data)
            alignment_tl = constr_alignment_tl()
            counter = 0
            ref_time = []
            ref_time_old = []
            files_all_old = []
            ref = constr_ref()


        # Get all filenames in directory
        files_all_new = [i for i in os.listdir(newest_folder)
                           if _isvalid(i)  and _recentdata(i) and _isrunid(i, run_number)] 
        # Get only newly added filenames in directory (sorted in time order)
        files_new = sorted(list(set(files_all_new).difference(set(files_all_old)))) 
       
        
        
        # Get paths for new files in directory
        files_new_path = [os.path.join(newest_folder, i) for i in files_new]

        # Print an update of the status of new HDF5 files
        print('New files detected ' + str(len(files_new_path)), flush=True) 
        t = time.localtime() 
        current_time = time.strftime("%H:%M:%S", t)
        print('Lastest update: ' + current_time,  flush=True)

        for event in files_new_path:
            event_data = get_event_data(event) # Get the event data for new files
            if type(event_data) != bool: 
                print('HDF5 event is: ' + event)
                
                # Get timestamp of current file as 'end time'
                end_time = datetime.strptime(copy.deepcopy(event_data['time']), '%Y-%m-%d %H:%M:%S') 
                #print('laser_5: ', event_data['laser_5'])
                # 'Start time' corresponds to timestamp of first file of the current day,
                # or timestamp of first file of the current run, or 6 hours prior if data
                # was being collected then, (whichever is most recent)
                if counter == 0 :
                    start_time = datetime.strptime(copy.deepcopy(event_data['time']), '%Y-%m-%d %H:%M:%S')
                counter += 1
                if  end_time - start_time > timedelta(hours =6):
                    start_time = end_time - timedelta(hours =6) 
                  
                # Add event data to the timeline dictionaries    
                update_timeline(timeline_data, event_data)
                update_timeline_average(timeline_average, timeline_data, event_data)
               
                
                # Assign two detectors to characterise each beam species
                proton_data = (timeline_data['bpm_proton_1'], timeline_data['bpm_proton_3'])
                electron_data = (timeline_data['bpm_electron_1'], timeline_data['bpm_electron_2'])
                laser_data = (timeline_data['laser_3'], timeline_data['laser_5'])
                
                # Get the time info for the reference beam from the txt file                
                try:
                    ref_time = get_reference_time('/user/awakeop/AwakeEventPreview/src/src_ref_time.txt') 
                except: print('REFERENCE TIME HAS WRONG FORMAT!!!')
                if ref_time != ref_time_old:
                    ref_times_all.append(ref_time)
                
                    
                # # Get the beam reference trajectories for all detectors used in itmeline plots
            
                if ref_time != ref_time_old or {'x': [], 'y': []} in ref.values():
                    get_beam_reference(timeline_data, ref_time, ref)
                ref_alignment = update_ref_alignment(ref)
                ref_time_old = copy.deepcopy(ref_time)
                
                
                # Get position deltas from reference trajectory    
                delta_curr, delta_avg = get_deltas(timeline_data, event_data, ref_alignment, curr_delta, avg_delta)


                # Get the alignment
                update_alignment_tl(event_data, delta_curr, alignment_tl)

                # Create the mini diagnostics DQM graphic for the current event, and store with unique HDF5 reference
                save_figure_path =  os.path.join(save_folder, os.path.split(event)[-1][0:-3] + str("-preview.pdf"))
                plot_event_data(event_data, save_figure_path, closeplot=True, store=True) 


                # if event == files_new_path[-1]: 
                if event == files_new_path[-1]:
                    
                    # Play warning sound if there are any errors with the data
                    # warning_bool_prev = play_warning(event_data, warning_bool_new, warning_bool_prev, sound_bool)
                  
                    for detector in warning_bool_new.keys():
                        if event_data[detector]['excep'] or event_data[detector]['timediff']>10:
                            previous_sound_bool  = copy.deepcopy(warning_bool_new)
                            warning_bool_new[detector] = True
                            # Most annoying: sound every time there is an issue
                            sound_bool[detector] = True
                            
                            # Less annoying: only sound the warning if the error is a new one
                            # if warning_bool_prev[detector] == True:
                            #     sound_bool[detector] = False
                            
                            # Pretty annoying: only sound the warning if also previous event was bad
                            if not warning_bool_prev[detector]:
                                print('Switch off warning for ',detector)
                                sound_bool[detector] = False

                        else:
                            warning_bool_new[detector] = False
                            sound_bool[detector] = False

                    warning_bool_prev = copy.deepcopy(warning_bool_new)
                    #print('warning_bool_new', warning_bool_new)
                    print('sound_bool', sound_bool)
                    if True in sound_bool.values():
                        listOfWarnings = {key: value for key, value in sound_bool.items() if value }
                        print('========== WARNING SOUND!! ==========')
                        print('Errors from:',  listOfWarnings)
                        print('=====================================')
                        os.system('aplay --device=hdmi --channels=3 ' + SOUND_1)
                        
                    # Plot the event data and the timeline only for the last event 
                    # Initialise list of paths for the timeline figures
                    save_paths = {'save_preview_laser':[],'save_proton_optics':[],
                                  'save_timeline_protons':[], 'save_timeline_laser':[],'save_timeline_protons_bpm':[],
                                  'save_timeline_electrons_bpm':[],'save_timeline_plasma':[],
                                  'save_average_timeline_protons':[], 'save_average_timeline_laser':[], 'save_average_timeline_protons_bpm':[],
                                  'save_average_timeline_electrons_bpm':[],'save_average_timeline_plasma':[], 'save_alignment':[],
                                   }
                    for path in save_paths: 
                        if bool(COLLECT_DATA_TIME_EPOCH) == False:
                           # name the file based upon the key in 'save_paths' dictionary 
                            save_paths[path] = os.path.join(save_folder, "Run-" + str(run_number) + '-'.join(path.split('_')[1:])+'.pdf')
                        if bool(COLLECT_DATA_TIME_EPOCH) == True:
                            save_paths[path] = os.path.join(save_folder, "Run-" + str(run_number)+'-'+ COLLECT_DATA_TIME_NORM[-8:] +'-' + '-'.join(path.split('_')[1:])+'.pdf')
                    
                    # Create the mini diagnostics DQM graphic for the current event, and store with unique HDF5 reference in save folder
                    
                    # Create the timeline DQM graphics for the current event, and store with general run number reference in save folder 
                    plot_mini_imgs_laser(event_data, save_paths['save_preview_laser'], closeplot=True, store=True) 
                    plot_proton_optics(event_data, save_paths['save_proton_optics'], closeplot=True, store=True) 
                    print('About to save all timeline files')
                    plot_timeline_data_protons(timeline_data, closeplot=True, store=True, savepath=save_paths['save_timeline_protons'], starttime = start_time, endtime = end_time, ref = ref, ref_times_all = ref_times_all) 
                    plot_timeline_data_laser(timeline_data, closeplot=True, store=True, savepath=save_paths['save_timeline_laser'], starttime = start_time, endtime = end_time, ref = ref, ref_times_all = ref_times_all)                    
                    plot_timeline_bpm_protons(timeline_data, closeplot=True, store=True,  savepath=save_paths['save_timeline_protons_bpm'], starttime = start_time, endtime = end_time, ref = ref, ref_times_all = ref_times_all)                    
                    #plot_timeline_bpm_electrons(timeline_data, closeplot=True, store=True, savepath=save_paths['save_timeline_electrons_bpm'], starttime = start_time, endtime = end_time)                    
                    plot_timeline_plasma(timeline_data, closeplot=True, store=True, savepath=save_paths['save_timeline_plasma'], starttime = start_time, endtime = end_time, ref = ref, ref_times_all = ref_times_all) 
                    #plot_timeline_average_protons(timeline_data, timeline_average, proton_beam_size_average, closeplot=True, store=True, savepath=save_paths['save_average_timeline_protons'], starttime = start_time, endtime = end_time)
                    #plot_timeline_average_laser(timeline_data, timeline_average, closeplot=True, store=True, savepath=save_paths['save_average_timeline_laser'], starttime = start_time, endtime = end_time)
                    plot_timeline_average_bpm_protons(timeline_data, timeline_average, closeplot=True, store=True, savepath=save_paths['save_average_timeline_protons_bpm'], starttime = start_time, endtime = end_time, ref = ref, ref_times_all = ref_times_all)
                    plot_timeline_average_bpm_electrons(timeline_data, timeline_average, closeplot=True, store=True, savepath=save_paths['save_average_timeline_electrons_bpm'], starttime = start_time, endtime = end_time, ref = ref, ref_times_all = ref_times_all)
                    plot_timeline_average_plasma(timeline_data, timeline_average, closeplot=True, store=True, savepath=save_paths['save_average_timeline_plasma'], starttime = start_time, endtime = end_time, ref = ref, ref_times_all = ref_times_all)   
                    plot_alignment(alignment_tl, delta_curr, delta_avg, closeplot=True, store=True,  savepath=save_paths['save_alignment'], starttime = start_time, endtime = end_time, reftime = ref_time, ref_times_all = ref_times_all)                    

                    # Copy stored figures to most recent event preview folder
                    print('About to copy images to events-preview')
                    # Mini imgs DQM
                    shutil.copyfile(save_figure_path, PREVIEW_PATHS['MINI_IMGS_PATH'])
                    # Remainder of DQM pdfs
                    
                    for paths in [
                            ('save_preview_laser', 'MINI_IMGS_LASER_PATH'),
                            ('save_proton_optics', 'PROTON_OPTICS_PATH'),
                            ('save_timeline_protons','TIMELINE_PROTONS_PATH'), 
                            ('save_timeline_laser', 'TIMELINE_LASER_PATH'), 
                            ('save_timeline_protons_bpm', 'TIMELINE_BPM_PROTONS_PATH'),
                            # ('save_timeline_electrons_bpm', 'TIMELINE_BPM_ELECTRONS_PATH'), 
                            # ('save_average_timeline_protons', 'TIMELINE_AVERAGE_PROTONS_PATH'),
                            # ('save_average_timeline_plasma', 'TIMELINE_AVERAGE_PLASMA_PATH'),
                            # ('save_average_timeline_laser', 'TIMELINE_AVERAGE_LASER_PATH'),
                            ('save_average_timeline_protons_bpm', 'TIMELINE_AVERAGE_BPM_PROTONS_PATH'), 
                            ('save_average_timeline_electrons_bpm', 'TIMELINE_AVERAGE_BPM_ELECTRONS_PATH'),
                            ('save_alignment', 'TIMELINE_ALIGNMENT_PATH')

                    ]:
                        
                        
                        shutil.copyfile(save_paths[paths[0]], PREVIEW_PATHS[paths[1]])
            
        files_all_old.extend(files_new) # Add on the new files to the previous file array
        toc = time.perf_counter() # End performance counter
        print(tic, toc, update_interval,  toc - tic , update_interval - toc + tic)
        if toc - tic < update_interval: # Search for new HDF5 files at most every 5 seconds
            time.sleep(update_interval - toc + tic)
    return True 

 
def _isvalid(ename):
    """
    Set restrictions on H5 file names
    """
#    return bool((ename.split('_')[1] == 'Type0')* (ename[-2:] == 'h5'))# * (int(ename[:10])<1654683906))
    # Only look every NMOD events
    nmod = 2
    if ename[-2:] == 'h5':
        #print(ename)
        #print('Checking modulo: ', ename.split('_')[3][:-3], np.mod(int(ename.split('_')[3][:-3]),5))
        return bool((ename.split('_')[1] == 'Type0')* (ename[-2:] == 'h5') * (np.mod(int(ename.split('_')[3][:-3]),int(nmod))==0))
    else: return False
def _recentdata(ename):
    """
    Find H5 files which have a timestamp after the reference timestamp
    """
    return bool((int(ename[0:10])) >= COLLECT_DATA_TIME_EPOCH)

def _isrunid(ename, runid):
    return bool( (det_run_number(ename) == runid) )


def get_event_data(path):
    """
    Get data from all needed diagnostics. Can be extended with additional instances if needed.
    """
    global file_timestamp

    try:
        f = h5py.File(path,'r')
        file_timestamp = get_timestamp(f)[0]
        print('setting file_timestamp to: ', file_timestamp)
        edata = {

            'timestamp' : get_timestamp(f)[0],
            'time' : get_timestamp(f)[1],
            'btv_50' : get_btv_50_digi(f, file_timestamp),
            'btv_53' : get_btv_53_digi(f, file_timestamp),
            'btv_54' : get_btv_54_digi(f, file_timestamp),
            'btv_26' : get_btv_26_digi(f, file_timestamp),
            'is1_core' : get_btv_is1_core_digi(f, file_timestamp),
            'is1_halo' : get_btv_is1_halo_digi(f, file_timestamp),
            'is2_core' : get_btv_is2_core_digi(f, file_timestamp),
            'is2_halo' : get_btv_is2_halo_digi(f, file_timestamp),
            'dmd_1' : get_dmd_1_digi(f, file_timestamp),
            'dmd_2' : get_dmd_2_digi(f, file_timestamp),
            # 'otr_angle': get_otr_angle_pxi(f, file_timestamp),
            'exp_vol_1': get_exp_vol_1_digi(f, file_timestamp),
            'streak_1' : get_streak_1_analogue(f, file_timestamp),
            'streak_2' : get_streak_2_analogue(f, file_timestamp),
            'streak_3' : get_streak_3_analogue(f, file_timestamp),
            'MPP_STREAK' : get_MPP_streak_digi(f, file_timestamp),
            'BTV_STREAK' : get_BTV_streak_digi(f, file_timestamp),
            'laser_1' : get_laser_camera_1_digi(f, file_timestamp),
            'laser_2' : get_laser_camera_2_digi(f, file_timestamp),
            'laser_3' : get_laser_camera_3_digi(f, file_timestamp),
            'laser_4' : get_laser_camera_4_digi(f, file_timestamp),
            'laser_5' : get_laser_camera_5_digi(f, file_timestamp),
            'spectro_1' : get_spectro_camera_1_digi(f, file_timestamp),
            'spectro' : get_spectrometer(f, file_timestamp),
            'bpm_electron_1' : get_bpm_electron_1(f, file_timestamp),
            'bpm_electron_2' : get_bpm_electron_2(f, file_timestamp),
            'bpm_proton_all': get_bpm_proton_all(f, file_timestamp), 
            'emeter_02' : get_emeter_2(f, file_timestamp),
            'emeter_03' : get_emeter_3(f, file_timestamp),
            'emeter_04' : get_emeter_4(f, file_timestamp),
            'emeter_05' : get_emeter_5(f, file_timestamp),
            'plasma_light' : get_plasma_light(f, file_timestamp),
            'proton_pop_bctfi' : get_population_bctfi(f, file_timestamp),
            'proton_pop_bctf' : get_population_bctf(f, file_timestamp),
            'enumber' : get_event_number(f, file_timestamp), 
        }
        
        edata['bpm_proton_1'] = edata['bpm_proton_all']['1']
        edata['bpm_proton_2'] = edata['bpm_proton_all']['2']
        edata['bpm_proton_3'] = edata['bpm_proton_all']['3']
        edata['bpm_proton_4'] = edata['bpm_proton_all']['4']
        edata['bpm_proton_5'] = edata['bpm_proton_all']['5']

        f.close()
        
    except Exception as e:
        print(e)
        edata = False
        

    return edata

#loop_events(update_interval = 1)
def await_run():
    await_run_start = 10

    while True:
        
        loop_events(update_interval = 1)
        time.sleep(await_run_start)
        print('Awaiting new run ...') 
    return True

# Start running the code
await_run()
