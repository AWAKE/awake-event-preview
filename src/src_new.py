#plasma_light!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  8 09:53:31 2022

@author: francescaelverson
"""

import time, os, h5py
import matplotlib.pyplot as plt
import numpy as np
import shutil
from scipy.optimize import curve_fit
import sys
from scipy.ndimage import gaussian_filter
import copy
from datetime import datetime, timedelta
import matplotlib.dates as mdates
import matplotlib.gridspec as gridspec
import math


# Get current date
COLLECT_DATA_TIME_NORM = (datetime.now()-timedelta(minutes = 1)).strftime('%Y-%m-%d %H:%M:%S')
print(COLLECT_DATA_TIME_NORM)
#COLLECT_DATA_TIME_NORM = "2022-06-08 12:05:00" # input date in format "YYYY-mm-dd HH:MM:SS" between quotation marks                                                                                                                           
COLLECT_DATA_TIME_EPOCH = 0000000000
if bool(COLLECT_DATA_TIME_NORM) == True:
    collect_data_time_object = datetime.strptime(COLLECT_DATA_TIME_NORM, '%Y-%m-%d %H:%M:%S')
    COLLECT_DATA_TIME_EPOCH = int(str(collect_data_time_object.timestamp())[0:10])


# Get golden trajectory for the proton bpms
gold_traject = {'proton_bpm_1': {'x': 1.1734,'y': -0.14 },
                'proton_bpm_2': {'x': 2.7482,'y': 0.8772},
                'proton_bpm_3': {'x': 2.5409 ,'y': 0.9618},
                'proton_bpm_4': {'x': 2.0826,'y': 0.4946 },
                'proton_bpm_5': {'x': 2.4254,'y':0.2912 }
                }

# Warning sound
SOUND_1= '/user/awakeop/felverson/quick_tools/246332__kwahmah-02__five-beeps.wav' 

# Directory path containing the h5 files; insert manually the relevant year
DATA_YEAR_PATH = '/user/awakeop/event_data/2022'

# Directory path where all historical graphics are stored; insert manually the relevant year
SAVE_PATH = "/user/awakeop/AwakeEventPreview/fran/events-preview/2022"

# File paths where the most recently updated preview graphics are stored
PREVIEW_PATHS = { 'MINI_IMGS_PATH' : "/user/awakeop/AwakeEventPreview/fran/events-preview/img.pdf", # Mini diagnostics DQM (all detectors)
                  'MINI_IMGS_LASER_PATH' : "/user/awakeop/AwakeEventPreview/fran/events-preview/raw_imgs_laser.pdf", # Mini diagnostics DQM (laser-relevant detectors)
                  'TIMELINE_PROTONS_PATH' : "/user/awakeop/AwakeEventPreview/fran/events-preview/timeline-protons.pdf", # Proton BTV timeline (absolute trajectory),
                  'TIMELINE_LASER_PATH' : "/user/awakeop/AwakeEventPreview/fran/events-preview/timeline-laser.pdf", # Laser VLC timeline (absolute trajectory),
                  'TIMELINE_BPM_PROTONS_PATH' : "/user/awakeop/AwakeEventPreview/fran/events-preview/timeline-bpm-protons.pdf", # Proton BPM timeline (absolute trajectory),
                  'TIMELINE_BPM_ELECTRONS_PATH' : "/user/awakeop/AwakeEventPreview/fran/events-preview/timeline-bpm-electrons.pdf", # Electron BPM timeline (absolute trajectory),
                  'TIMELINE_PLASMA_PATH' : "/user/awakeop/AwakeEventPreview/fran/events-preview/timeline-plasma.pdf", # Plasma light timeline (absolute measurement),
                  'TIMELINE_AVERAGE_PROTONS_PATH' : "/user/awakeop/AwakeEventPreview/fran/events-preview/timeline-average-protons.pdf", #  Proton BTV timeline (average trajectory),
                  'TIMELINE_AVERAGE_LASER_PATH' : "/user/awakeop/AwakeEventPreview/fran/events-preview/timeline-average-laser.pdf", # Laser VLC timeline (average trajectory)
                  'TIMELINE_AVERAGE_BPM_PROTONS_PATH' : "/user/awakeop/AwakeEventPreview/fran/events-preview/timeline-average-bpm-protons.pdf", # Proton BPM timeline (average trajectory)
                  'TIMELINE_AVERAGE_BPM_ELECTRONS_PATH' : "/user/awakeop/AwakeEventPreview/fran/events-preview/timeline-average-bpm-electrons.pdf", # Electron BPM timeline (average trajectory)
                  #'TIMELINE_AVERAGE_PLASMA_PATH' : "/user/awakeop/AwakeEventPreview/fran/events-preview/timeline-average-plasma.pdf", # Plasma light timeline (average measurement)
                  }

# Graphic font parameters
plt.style.use('dark_background')
SMALL_SIZE = 7
MEDIUM_SIZE = 9
BIGGER_SIZE = 10
plt.rc("font", size=SMALL_SIZE)
plt.rc("axes", titlesize=MEDIUM_SIZE) 
plt.rc("axes", labelsize=MEDIUM_SIZE) 
plt.rc("xtick", labelsize=SMALL_SIZE) 
plt.rc("ytick", labelsize=SMALL_SIZE) 
plt.rc("legend", fontsize=SMALL_SIZE) 
plt.rc("figure", titlesize=BIGGER_SIZE)
plt.style.use('dark_background')

start_time = [] 

file_timestamp = 0

# Pixel sizes (in x and y) and 'rebin factor' for cameras used in timeline plots
PIXELS = {
    
    'vlc' : [0.0059, 0.0059, 4],
    'is1_core' : [0.0411, 0.0444, 2], 
    'is2_core' : [0.0404, 0.0396, 2], 
    'btv_54' : [0.114, 0.1250, 2],  
    
    }




def loop_events(update_interval = 5, ): 
    """
    The function plots a summary of individual events and the timeline plot from the most recently updated folder. The folder is updated every `update_interval=10` seconds, and new events are plotted if they appear. If the newest folder is created, it will be automatically considered for further updates.
    """  
    
    # Find the latest month and day
    newest_folder = find_lastest_subdirectory(DATA_YEAR_PATH) 
    


    # Get the most recent run number; include constraints on the h5 data (maybe include a try, except clause)
    try:
        run_number = max([det_run_number(i) for i in os.listdir(newest_folder)
                          if _isvalid(i)  and _recentdata(i)]) 
    except:
        return
    # Create empty save folder corresponding to the most recent day of data
    # (within a folder corresponding to the corresponding month i.e. SAVE_PATH/month/day/)
    save_folder = _merge_fold_paths(SAVE_PATH, newest_folder) 
    if not os.path.exists(save_folder): os.makedirs(save_folder)
    
    files_all = [] # Empty list to store all filenames in directory
    
    # Initialise dictionaries which will hold timeline data 
    timeline_data = constr_timeline_data()
    timeline_average = constr_timeline_data()
    
    counter = 0
    
    # Warnings
    warning_bool_new = { 
        'btv_50' :[],
        'btv_53': [], 
        'btv_54' :[], 
        'btv_26': [], 
        'laser_3' :[],
        'laser_4' :[], 
        'laser_5':[], 
        'is1_core' : [], 
        'is1_halo' : [], 
        'is2_core' :[],
        'is2_halo':[],
        'dmd_1' :[], 
        #'dmd_2':[],
        'exp_vol_1':[],
        'streak_1':[], 
        'streak_2': [], 
        'AWAKECAM_05':[],
        #'spectro': [],
        }
    
    sound_bool = copy.deepcopy(warning_bool_new)
    warning_bool_prev = copy.deepcopy(warning_bool_new)

    while True:
        tic = time.perf_counter() # Start performance counter
        # Find the most recent month, and day, of data collection
        newest_folder_now = find_lastest_subdirectory(DATA_YEAR_PATH)

        # Get the most recent run number
        try: 
            run_number_now = max([det_run_number(i) for i in os.listdir(newest_folder_now) 
                                     if _isvalid(i)  and  _recentdata(i)])
        except: 
            run_number_now = False
            
        # Check if new HDF5 files correspond to a new run or a new day  
        # If so, redefine/update the save folders, latest day/run of data and
        # reset timeline arrays and HDF5 files list 
        if (newest_folder_now != newest_folder) or (run_number_now != run_number):
            newest_folder = newest_folder_now 
            run_number = run_number_now 
            # Create new empty save folder if new HDF5 files correspond to a new day  
            save_folder = _merge_fold_paths(SAVE_PATH, newest_folder)
            if not os.path.exists(save_folder): os.makedirs(save_folder)

            # Initialise new dictionaries which will hold new timeline data 
            timeline_data = constr_timeline_data() 
            timeline_average = copy.deepcopy(timeline_data)
            counter = 0

        # Get all filenames in directory
        files_all_new = [i for i in os.listdir(newest_folder)
                           if _isvalid(i)  and _recentdata(i)] 
        # Get only newly added filenames in directory (sorted in time order)
        files_new = sorted(list(set(files_all_new).difference(set(files_all)))) 
        # Get paths for new files in directory
        files_new_path = [os.path.join(newest_folder, i) for i in files_new]


        # Print an update of the status of new HDF5 files
        print('New files detected ' + str(len(files_new_path)), flush=True) 
        t = time.localtime() 
        current_time = time.strftime("%H:%M:%S", t)
        print('Lastest update: ' + current_time,  flush=True)

        for path in files_new_path:
            event_data = get_event_data(path)  # Get the event data for new files
            print(event_data['plasma_light'])
        

            if type(event_data) != bool: 
                print('HDF5 path is: ' + path)
                
                # Get timestamp of current file as 'end time'
                end_time = datetime.strptime(copy.deepcopy(event_data['time']), '%Y-%m-%d %H:%M:%S') 
                
                # 'Start time' corresponds to timestamp of first file of the current day,
                # or timestamp of first file of the current run, or 6 hours prior if data
                # was being collected then, (whichever is most recent)
                if path == files_new_path[0] and counter == 0:
                    start_time = datetime.strptime(copy.deepcopy(event_data['time']), '%Y-%m-%d %H:%M:%S')
                counter += 1
                if  end_time - start_time > timedelta(hours =6):
                    start_time = end_time - timedelta(hours =6) 
                  
                # Add event data to the timeline dictionaries    
                update_timeline(timeline_data, event_data)
                update_timeline_average(timeline_average, timeline_data, event_data)
                print('timeline updated')


                # Create the mini diagnostics DQM graphic for the current event, and store with unique HDF5 reference
                save_figure_path =  os.path.join(save_folder, os.path.split(path)[-1][0:-3] + str("-preview.pdf"))
                print('about to plot event data')
                plot_event_data(event_data, save_figure_path, closeplot=True, store=True) 
                print('event data plotted')


                if path == files_new_path[-1]: 
                    
                    # Play warning sound if there are any errors with the data

                    for detector in warning_bool_new.keys():
                        if event_data[detector]['excep'] == True or event_data[detector]['timediff']>10:
                            warning_bool_new[detector] = True
                            sound_bool[detector] = True
                            # Only sound the warning if the error is a new one
                            #if warning_bool_prev[detector] == True:
                            #    sound_bool[detector] = False
                        else:
                            warning_bool_new[detector] = False
                            sound_bool[detector] = False
                    
                    print('warning_bool_new', warning_bool_new)
                    print('sound_bool', sound_bool)
                    warning_bool_prev = copy.deepcopy(warning_bool_new)
                    if True in sound_bool.values():
                        print('WARNING SOUND!!', sound_bool.values())
                        os.system('aplay --device=hdmi --channels=3 ' + SOUND_1)


                    # Plot the event data and the timeline only for the last event                    
                    save_paths = {'save_preview_laser':[],
                                  'save_timeline_protons':[], 'save_timeline_laser':[],'save_timeline_protons_bpm':[],
                                  'save_timeline_electrons_bpm':[],'save_timeline_plasma':[],
                                  'save_average_timeline_protons':[], 'save_average_timeline_laser':[], 'save_average_timeline_protons_bpm':[],
                                  'save_average_timeline_electrons_bpm':[],'save_average_timeline_plasma':[], 
                                   }
                    for path in save_paths: 
                        if bool(COLLECT_DATA_TIME_EPOCH) == False:
                            save_paths[path] = os.path.join(save_folder, "Run-" + str(run_number_now) + '-'.join(path.split('_')[1:])+'.pdf')
                        if bool(COLLECT_DATA_TIME_EPOCH) == True:
                            save_paths[path] = os.path.join(save_folder, "Run-" + str(run_number_now)+'-'+ COLLECT_DATA_TIME_NORM[-8:] +'-' + '-'.join(path.split('_')[1:])+'.pdf')

                    # Create the mini diagnostics DQM graphic for the current event, and store with unique HDF5 reference in save folder
                    
                    # Create the timeline DQM graphics for the current event, and store with general run number reference in save folder 
                    plot_mini_imgs_laser(event_data, save_paths['save_preview_laser'], closeplot=True, store=True) 
                    print('About to save all timeline files')
                    plot_timeline_data_protons(timeline_data, closeplot=True, store=True, savepath=save_paths['save_timeline_protons'], starttime = start_time, endtime = end_time) 
                    plot_timeline_data_laser(timeline_data, closeplot=True, store=True, savepath=save_paths['save_timeline_laser'], starttime = start_time, endtime = end_time)                    
                    plot_timeline_bpm_protons(timeline_data, closeplot=True, store=True,  savepath=save_paths['save_timeline_protons_bpm'], starttime = start_time, endtime = end_time)                    
                    #plot_timeline_bpm_electrons(timeline_data, closeplot=True, store=True, savepath=save_paths['save_timeline_electrons_bpm'], starttime = start_time, endtime = end_time)                    
                    plot_timeline_plasma(timeline_data, closeplot=True, store=True, savepath=save_paths['save_timeline_plasma'], starttime = start_time, endtime = end_time) 
                    #plot_timeline_average_protons(timeline_data, timeline_average, proton_beam_size_average, closeplot=True, store=True, savepath=save_paths['save_average_timeline_protons'], starttime = start_time, endtime = end_time)
                    #plot_timeline_average_laser(timeline_data, timeline_average, closeplot=True, store=True, savepath=save_paths['save_average_timeline_laser'], starttime = start_time, endtime = end_time)
                    
                    plot_timeline_average_bpm_protons(timeline_data, timeline_average, closeplot=True, store=True, savepath=save_paths['save_average_timeline_protons_bpm'], starttime = start_time, endtime = end_time)
                    plot_timeline_average_bpm_electrons(timeline_data, timeline_average, closeplot=True, store=True, savepath=save_paths['save_average_timeline_electrons_bpm'], starttime = start_time, endtime = end_time)
                    plot_timeline_average_plasma(timeline_data, timeline_average, closeplot=True, store=True, savepath=save_paths['save_average_timeline_plasma'], starttime = start_time, endtime = end_time)   
        
                    # Copy stored figures to most recent event preview folder
                    print('About to copy images to events-preview')
                    # Mini imgs DQM
                    shutil.copyfile(save_figure_path, PREVIEW_PATHS['MINI_IMGS_PATH'])
                    # Remainder of DQM pdfs
                    for paths in [
                            ('save_preview_laser', 'MINI_IMGS_LASER_PATH'),
                            ('save_timeline_protons','TIMELINE_PROTONS_PATH'), 
                            ('save_timeline_laser', 'TIMELINE_LASER_PATH'), 
                            ('save_timeline_protons_bpm', 'TIMELINE_BPM_PROTONS_PATH'),
                            # ('save_timeline_electrons_bpm', 'TIMELINE_BPM_ELECTRONS_PATH'), 
                            # ('save_average_timeline_protons', 'TIMELINE_AVERAGE_PROTONS_PATH'),
                            #('save_average_timeline_plasma', 'TIMELINE_AVERAGE_PLASMA_PATH'),
                            # ('save_average_timeline_laser', 'TIMELINE_AVERAGE_LASER_PATH'),
                            ('save_average_timeline_protons_bpm', 'TIMELINE_AVERAGE_BPM_PROTONS_PATH'), 
                            ('save_average_timeline_electrons_bpm', 'TIMELINE_AVERAGE_BPM_ELECTRONS_PATH')
                    ]:
                        shutil.copyfile(save_paths[paths[0]], PREVIEW_PATHS[paths[1]])
            
        files_all.extend(files_new) # Add on the new files to the previous file array
        toc = time.perf_counter() # End performance counter
        print(tic, toc, update_interval,  toc - tic , update_interval - toc + tic)
        if toc - tic < update_interval: # Search for new HDF5 files at most every 5 seconds
            time.sleep(update_interval - toc + tic)
    return True 

 
def _isvalid(ename):
    """
    Set restrictions on H5 file names
    """
    return bool((ename.split('_')[1] == 'Type0')* (ename[-2:] == 'h5'))# * (int(ename[:10])<1654683906))
def _recentdata(ename):
    """
    Find H5 files which have a timestamp after the reference timestamp
    """
    return bool((int(ename[0:10])) >= COLLECT_DATA_TIME_EPOCH)

def _isrunid(ename, runid):
    return bool( (det_run_number(ename) == runid) )


def get_event_data(path):
    """
    Get data from all needed diagnostics. Can be extended with additional instances if needed.
    """
    global file_timestamp

    try:
        f = h5py.File(path,'r')
        file_timestamp = get_timestamp(f)[0]
        edata = {

            'timestamp' : get_timestamp(f)[0],
            'time' : get_timestamp(f)[1],
            'btv_50' : get_btv_50_digi(f),
            'btv_53' : get_btv_53_analogue(f),
            'btv_54' : get_btv_54_digi(f),
            'btv_26' : get_btv_26_analogue(f),
            'is1_core' : get_btv_is1_core_digi(f),
            'is1_halo' : get_btv_is1_halo_digi(f),
            'is2_core' : get_btv_is2_core_digi(f),
            'is2_halo' : get_btv_is2_halo_digi(f),
            'dmd_1' : get_dmd_1_digi(f),
            'dmd_2' : get_dmd_2_digi(f),
            # 'otr_angle': get_otr_angle_pxi(f),
            'exp_vol_1': get_exp_vol_1_digi(f),
            'streak_1' : get_streak_1_analogue(f),
            'streak_2' : get_streak_2_analogue(f),
            'AWAKECAM_05' : get_MPP_streak_digi(f),
            'laser_1' : get_laser_camera_1_pxi(f),
            'laser_2' : get_laser_camera_2_pxi(f),
            'laser_3' : get_laser_camera_3_pxi(f),
            'laser_4' : get_laser_camera_4_pxi(f),
            'laser_5' : get_laser_camera_5_pxi(f),
            'spectro' : get_spectrometer(f),
            'bpm_electron_1' : get_bpm_electron_1(f),
            'bpm_electron_2' : get_bpm_electron_2(f),
            'bpm_proton_all': get_bpm_proton_all(f), 
            'emeter_02' : get_emeter_2(f),
            'emeter_03' : get_emeter_3(f),
            'emeter_04' : get_emeter_4(f),
            'emeter_05' : get_emeter_5(f),
            'plasma_light' : get_plasma_light(f),
            'proton_pop_bctfi' : get_population_bctfi(f),
            'proton_pop_bctf' : get_population_bctf(f),
            'enumber' : get_event_number(f), 
        }
        
        edata['bpm_proton_1'] = edata['bpm_proton_all']['1']
        edata['bpm_proton_2'] = edata['bpm_proton_all']['2']
        edata['bpm_proton_3'] = edata['bpm_proton_all']['3']
        edata['bpm_proton_4'] = edata['bpm_proton_all']['4']
        edata['bpm_proton_5'] = edata['bpm_proton_all']['5']

        
        f.close()
        
    except:

        edata = False

    return edata

def constr_timeline_data():
    """
    Dictionary constructor for the timeline data
    """
    
    return {
        'btv_50' : {'x': np.zeros(1), 'y': np.zeros(1), 'time' : np.zeros(1) },
        'btv_53' : {'x': np.zeros(1), 'y': np.zeros(1), 'time' : np.zeros(1) },
        'btv_54' : {'x': np.zeros(1), 'y': np.zeros(1),'time' : np.zeros(1), },
        'btv_26' : {'x': np.zeros(1), 'y': np.zeros(1), 'time' : np.zeros(1), },
        'laser_3' : {'x': np.zeros(1), 'y': np.zeros(1),'time' : np.zeros(1), },
        'laser_4' : {'x': np.zeros(1), 'y': np.zeros(1),'time' : np.zeros(1), },
        'laser_5' : {'x': np.zeros(1), 'y': np.zeros(1),'time' : np.zeros(1), },
        'is1_core' : {'x': np.zeros(1), 'y': np.zeros(1), 'sigma_x': np.zeros(1), 'sigma_y': np.zeros(1), 'time' : np.zeros(1), },
        'is1_halo' : {'x': np.zeros(1), 'y': np.zeros(1), 'time' : np.zeros(1), },
        'is2_core' : {'x': np.zeros(1), 'y': np.zeros(1), 'sigma_x': np.zeros(1), 'sigma_y': np.zeros(1), 'time' : np.zeros(1), },
        'is2_halo' : {'x': np.zeros(1), 'y': np.zeros(1),'time' : np.zeros(1), },
        'bpm_electron_1': {'hor': np.zeros(1), 'ver': np.zeros(1), 'time' : np.zeros(1)},
        'bpm_electron_2': {'hor': np.zeros(1), 'ver': np.zeros(1), 'time' : np.zeros(1)},
        'bpm_proton_1': {'hor': np.zeros(1), 'ver': np.zeros(1), 'time' : np.zeros(1)},
        'bpm_proton_2': {'hor': np.zeros(1), 'ver': np.zeros(1), 'time' : np.zeros(1)},
        'bpm_proton_3': {'hor': np.zeros(1), 'ver': np.zeros(1), 'time' : np.zeros(1)},
        'bpm_proton_4': {'hor': np.zeros(1), 'ver': np.zeros(1), 'time' : np.zeros(1)},
        'bpm_proton_5': {'hor': np.zeros(1), 'ver': np.zeros(1), 'time' : np.zeros(1)},
        'AWAKECAM_05' : {'x': np.zeros(1), 'y': np.zeros(1),'time' : np.zeros(1), },
        #'otr_angle' : {'x': np.zeros(1), 'y': np.zeros(1),'time' : np.zeros(1), },
        'proton_pop_bctf' : {'pop': np.zeros(1), 'time': np.zeros(1), },
        'emeter_02' : {'energy' : np.zeros(1), 'time' : np.zeros(1), },
        'emeter_03' : {'energy' : np.zeros(1), 'time' : np.zeros(1), },
        'emeter_04' : {'energy' : np.zeros(1), 'time' : np.zeros(1), },
        'emeter_05' : {'energy' : np.zeros(1), 'time' : np.zeros(1), },
        'plasma_light' : {'upstream': np.zeros(1), 'downstream' : np.zeros(1), 'time' : np.zeros(1), },
        'proton_pop_bctfi' :{'charge' : np.zeros(1), 'time' : np.zeros(1), }

    }

def update_timeline(timeline_data, eventdata):
    """
    Append the data from `eventdata` to `timeline_data`
    """
    # N.B: set restictions here on the data quality thresholds for each detector
    # i.e. proton population, ensuring that the shot is not late

    # timeline is1_core

    dttmp = eventdata['is1_core']['data']
    timeline_tmp = copy.deepcopy(timeline_data['is1_core'])
    if (np.shape(dttmp) != (1,0)) & (eventdata['is1_core']['timediff'] < 10) & (eventdata['proton_pop_bctf'] > 0.0) :
        params =  def_centroid(dttmp[100:200, 150:250])
        centr_params = params[0] 
        sigma_params = params[1]
        if np.prod(np.invert(np.isnan(centr_params))):
            x_pixel = PIXELS['is1_core'][0]*PIXELS['is1_core'][2]
            y_pixel = PIXELS['is1_core'][1]*PIXELS['is1_core'][2]
            tmp_1 = np.append(timeline_tmp['x'], np.array(centr_params[0] * x_pixel) )
            tmp_2 = np.append(timeline_tmp['y'], np.array(centr_params[1] * y_pixel) )
            tmp_3 = np.append(timeline_tmp['sigma_x'], np.array(sigma_params[0]) )
            tmp_4 = np.append(timeline_tmp['sigma_y'], np.array(sigma_params[1]) )
            timeadd = np.append(timeline_tmp['time'],datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S'))
            timeline_tmp.update({'x' : tmp_1})
            timeline_tmp.update({'y' : tmp_2})
            timeline_tmp.update({'sigma_x' : tmp_3})
            timeline_tmp.update({'sigma_y' : tmp_4})
            timeline_tmp.update({'time': timeadd})
        timeline_data.update({'is1_core' : timeline_tmp})
    
    # timeline is2_core

    dttmp = eventdata['is2_core']['data']
    timeline_tmp = copy.deepcopy(timeline_data['is2_core'])
    if (np.shape(dttmp) != (1,0)) & (eventdata['is2_core']['timediff'] < 10) & (eventdata['proton_pop_bctf'] > 0.0):
        params =  def_centroid(dttmp[100:200, 150:250])
        centr_params = params[0] 
        sigma_params = params[1]
        if np.prod(np.invert(np.isnan(centr_params))):
            x_pixel = PIXELS['is2_core'][0]*PIXELS['is2_core'][2]
            y_pixel = PIXELS['is2_core'][1]*PIXELS['is2_core'][2]
            tmp_1 = np.append(timeline_tmp['x'], np.array(centr_params[0] * x_pixel) )
            tmp_2 = np.append(timeline_tmp['y'], np.array(centr_params[1] * y_pixel) )
            tmp_3 = np.append(timeline_tmp['sigma_x'], np.array(sigma_params[0]) )
            tmp_4 = np.append(timeline_tmp['sigma_y'], np.array(sigma_params[1]) )
            timeadd = np.append(timeline_tmp['time'],datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S') )  
            timeline_tmp.update({'x' : tmp_1})
            timeline_tmp.update({'y' : tmp_2})
            timeline_tmp.update({'sigma_x' : tmp_3})
            timeline_tmp.update({'sigma_y' : tmp_4})
            timeline_tmp.update({'time': timeadd})
        timeline_data.update({'is2_core' : timeline_tmp})
    
    # timeline btv_54

    dttmp = eventdata['btv_54']['data']
    timeline_tmp = copy.deepcopy(timeline_data['btv_54'])
    if (np.shape(dttmp) != (1,0)) & (eventdata['btv_54']['timediff'] < 10) & (eventdata['proton_pop_bctf'] > 0.0):
        centr_params = def_centroid(dttmp)[0]#dttmp[180:380, 200:400]) 
        if np.prod(np.invert(np.isnan(centr_params))):
            x_pixel = PIXELS['btv_54'][0]*PIXELS['btv_54'][2]
            y_pixel = PIXELS['btv_54'][1]*PIXELS['btv_54'][2]
            tmp_1 = np.append(timeline_tmp['x'], np.array(centr_params[0] * x_pixel) )
            tmp_2 = np.append(timeline_tmp['y'], np.array(centr_params[1] * y_pixel) )
            timeadd = np.append(timeline_tmp['time'],datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S') )
            timeline_tmp.update({'x' : tmp_1})
            timeline_tmp.update({'y' : tmp_2})
            timeline_tmp.update({'time': timeadd})
        timeline_data.update({'btv_54' : timeline_tmp})
    
    
    # timeline Awake Cam 05

    dttmp = eventdata['AWAKECAM_05']['data']
    timeline_tmp = copy.deepcopy(timeline_data['AWAKECAM_05'])
    if (np.shape(dttmp) != (1,0)) & (eventdata['AWAKECAM_05']['timediff'] < 10) & (eventdata['proton_pop_bctf'] > 0.0):
        centr_params = def_centroid(dttmp)[0]
        if np.prod(np.invert(np.isnan(centr_params))):
            tmp_1 = np.append(timeline_tmp['x'], np.array(centr_params[0]) )
            tmp_2 = np.append(timeline_tmp['y'], np.array(centr_params[1]) )
            timeadd = np.append(timeline_tmp['time'],datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S') )
            timeline_tmp.update({'x' : tmp_1})
            timeline_tmp.update({'y' : tmp_2})
            timeline_tmp.update({'time': timeadd})
        timeline_data.update({'AWAKECAM_05' : timeline_tmp})
    
    
    #timeline laser_3

    dttmp = eventdata['laser_3']['data']
    timeline_tmp = copy.deepcopy(timeline_data['laser_3'])
    if (np.shape(dttmp) != (1,0)) & (eventdata['laser_3']['timediff'] < 10)  :
        centr_params = def_centroid(dttmp)[0]
        if np.prod(np.invert(np.isnan(centr_params))):
            x_pixel = PIXELS['vlc'][0]*PIXELS['vlc'][2]
            y_pixel = PIXELS['vlc'][1]*PIXELS['vlc'][2]
            tmp_1 = np.append(timeline_tmp['x'], np.array(centr_params[0] * x_pixel) )
            tmp_2 = np.append(timeline_tmp['y'], np.array(centr_params[1] * y_pixel) )
            timeadd = np.append(timeline_tmp['time'],datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S') )
            timeline_tmp.update({'x' : tmp_1})
            timeline_tmp.update({'y' : tmp_2})
            timeline_tmp.update({'time': timeadd})
        timeline_data.update({'laser_3' : timeline_tmp})

    #timeline laser_4
        
    dttmp = eventdata['laser_4']['data']
    timeline_tmp = copy.deepcopy(timeline_data['laser_4'])
    if (np.shape(dttmp) != (1,0)) & (eventdata['laser_4']['timediff'] < 10)  :
        centr_params = def_centroid(dttmp)[0]
        if np.prod(np.invert(np.isnan(centr_params))):
            x_pixel = PIXELS['vlc'][0]*PIXELS['vlc'][2]
            y_pixel = PIXELS['vlc'][1]*PIXELS['vlc'][2]
            tmp_1 = np.append(timeline_tmp['x'], np.array(centr_params[0] * x_pixel) )
            tmp_2 = np.append(timeline_tmp['y'], np.array(centr_params[1] * y_pixel) )
            timeadd = np.append(timeline_tmp['time'],datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S'))
            timeline_tmp.update({'x' : tmp_1})
            timeline_tmp.update({'y' : tmp_2}) 
            timeline_tmp.update({'time': timeadd})
        timeline_data.update({'laser_4' : timeline_tmp})
    
    #timeline laser_5
     
    dttmp = eventdata['laser_5']['data']
    timeline_tmp = copy.deepcopy(timeline_data['laser_5'])
    if (np.shape(dttmp) != (1,0)) & (eventdata['laser_5']['timediff'] < 10)  :
        centr_params = def_centroid(dttmp)[0]
        if np.prod(np.invert(np.isnan(centr_params))):
            x_pixel = PIXELS['vlc'][0]*PIXELS['vlc'][2]
            y_pixel = PIXELS['vlc'][1]*PIXELS['vlc'][2]
            tmp_1 = np.append(timeline_tmp['x'], np.array(centr_params[0] * x_pixel) )
            tmp_2 = np.append(timeline_tmp['y'], np.array(centr_params[1] * y_pixel) )
            timeadd = np.append(timeline_tmp['time'], datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S') )
            timeline_tmp.update({'x' : tmp_1})
            timeline_tmp.update({'y' : tmp_2})
            timeline_tmp.update({'time': timeadd})
        timeline_data.update({'laser_5' : timeline_tmp})

    # timeline bpm proton 1

    dttmp = eventdata['bpm_proton_1']['data']
    if np.isfinite(dttmp['hor']):
        timeline_tmp = copy.deepcopy(timeline_data['bpm_proton_1'])
        tmp_1 = np.append(timeline_tmp['hor'],np.array(dttmp['hor']))
        tmp_2 = np.append(timeline_tmp['ver'],np.array(dttmp['ver']))
        timeadd = np.append(timeline_tmp['time'], datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S'))
        timeline_tmp.update({'hor' : tmp_1})
        timeline_tmp.update({'ver' : tmp_2})
        timeline_tmp.update({'time': timeadd})
        timeline_data.update({'bpm_proton_1' : timeline_tmp})
 

    # timeline bpm proton 2

    dttmp = eventdata['bpm_proton_2']['data']
    timeline_tmp = copy.deepcopy(timeline_data['bpm_proton_2'])
    if np.isfinite(dttmp['hor']):
        timeline_tmp = copy.deepcopy(timeline_data['bpm_proton_2'])
        tmp_1 = np.append(timeline_tmp['hor'],np.array(dttmp['hor']))
        tmp_2 = np.append(timeline_tmp['ver'],np.array(dttmp['ver']))
        timeadd = np.append(timeline_tmp['time'], datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S'))
        timeline_tmp.update({'hor' : tmp_1})
        timeline_tmp.update({'ver' : tmp_2})
        timeline_tmp.update({'time': timeadd})
        timeline_data.update({'bpm_proton_2' : timeline_tmp})
   

    # timeline bpm proton 3

    dttmp = eventdata['bpm_proton_3']['data']
    timeline_tmp = copy.deepcopy(timeline_data['bpm_proton_3'])
    if np.isfinite(dttmp['hor']):
        tmp_1 = np.append(timeline_tmp['hor'],np.array(dttmp['hor']))
        tmp_2 = np.append(timeline_tmp['ver'],np.array(dttmp['ver']))
        timeadd = np.append(timeline_tmp['time'], datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S'))
        timeline_tmp.update({'hor' : tmp_1})
        timeline_tmp.update({'ver' : tmp_2})
        timeline_tmp.update({'time': timeadd})
        timeline_data.update({'bpm_proton_3' : timeline_tmp})
  

    # timeline bpm proton 4

    dttmp = eventdata['bpm_proton_4']['data']
    timeline_tmp = copy.deepcopy(timeline_data['bpm_proton_4'])
    if np.isfinite(dttmp['hor']):
        tmp_1 = np.append(timeline_tmp['hor'],np.array(dttmp['hor']))
        tmp_2 = np.append(timeline_tmp['ver'],np.array(dttmp['ver']))
        timeadd = np.append(timeline_tmp['time'], datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S'))
        timeline_tmp.update({'hor' : tmp_1})
        timeline_tmp.update({'ver' : tmp_2})
        timeline_tmp.update({'time': timeadd})
        timeline_data.update({'bpm_proton_4' : timeline_tmp})


    # timeline bpm proton 5

    dttmp = eventdata['bpm_proton_5']['data']
    timeline_tmp = copy.deepcopy(timeline_data['bpm_proton_5'])
    if np.isfinite(dttmp['hor']):
        tmp_1 = np.append(timeline_tmp['hor'],np.array(dttmp['hor']))
        tmp_2 = np.append(timeline_tmp['ver'],np.array(dttmp['ver']))
        timeadd = np.append(timeline_tmp['time'], datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S'))
        timeline_tmp.update({'hor' : tmp_1})
        timeline_tmp.update({'ver' : tmp_2})
        timeline_tmp.update({'time': timeadd})
        timeline_data.update({'bpm_proton_5' : timeline_tmp})
        
        
    # timeline bpm electron 1

    dttmp = eventdata['bpm_electron_1']['data']
    timeline_tmp = copy.deepcopy(timeline_data['bpm_electron_1'])
    if np.isfinite(dttmp['hor']):
        tmp_1 = np.append(timeline_tmp['hor'],np.array(dttmp['hor']))
        tmp_2 = np.append(timeline_tmp['ver'],np.array(dttmp['ver']))
        timeadd = np.append(timeline_tmp['time'], datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S'))
        timeline_tmp.update({'hor' : tmp_1})
        timeline_tmp.update({'ver' : tmp_2})
        timeline_tmp.update({'time': timeadd})
        timeline_data.update({'bpm_electron_1' : timeline_tmp})

    # timeline bpm electron 2

    dttmp = eventdata['bpm_electron_2']['data']
    timeline_tmp = copy.deepcopy(timeline_data['bpm_electron_2'])


    if np.isfinite(dttmp['hor']):
        tmp_1 = np.append(timeline_tmp['hor'],np.array(dttmp['hor']))
        tmp_2 = np.append(timeline_tmp['ver'],np.array(dttmp['ver']))
        timeadd = np.append(timeline_tmp['time'], datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S'))
        timeline_tmp.update({'hor' : tmp_1})
        timeline_tmp.update({'ver' : tmp_2})
        timeline_tmp.update({'time': timeadd})
        timeline_data.update({'bpm_electron_2' : timeline_tmp})


    # timeline emeter 2

    dttmp = eventdata['emeter_02']['energy']
    timeline_tmp = copy.deepcopy(timeline_data['emeter_02'])
    if np.isfinite(dttmp):
        tmp = np.append(timeline_tmp['energy'],np.array(dttmp))
        timeadd = np.append(timeline_tmp['time'], datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S'))
        timeline_tmp.update({'energy' : tmp})
        timeline_tmp.update({'time': timeadd})
        timeline_data.update({'emeter_02' : timeline_tmp})

    # timeline emeter 3

    dttmp = eventdata['emeter_03']['energy']
    timeline_tmp = copy.deepcopy(timeline_data['emeter_03'])
    if np.isfinite(dttmp):
        tmp = np.append(timeline_tmp['energy'],np.array(dttmp))
        timeadd = np.append(timeline_tmp['time'], datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S'))
        timeline_tmp.update({'energy' : tmp})
        timeline_tmp.update({'time': timeadd})
        timeline_data.update({'emeter_03' : timeline_tmp})

    # timeline emeter 4

    dttmp = eventdata['emeter_04']['energy']
    timeline_tmp = copy.deepcopy(timeline_data['emeter_04'])
    if np.isfinite(dttmp):
        tmp = np.append(timeline_tmp['energy'],np.array(dttmp))
        timeadd = np.append(timeline_tmp['time'], datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S'))
        timeline_tmp.update({'energy' : tmp})
        timeline_tmp.update({'time': timeadd})
        timeline_data.update({'emeter_04' : timeline_tmp})

    # timeline emeter 5

    dttmp = eventdata['emeter_05']['energy']
    timeline_tmp = copy.deepcopy(timeline_data['emeter_05'])
    if np.isfinite(dttmp):
        tmp = np.append(timeline_tmp['energy'],np.array(dttmp))
        timeadd = np.append(timeline_tmp['time'], datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S'))
        timeline_tmp.update({'energy' : tmp})
        timeline_tmp.update({'time': timeadd})
        timeline_data.update({'emeter_05' : timeline_tmp})
    
    # timeline plasma light

    dttmp = eventdata['plasma_light']['point']
    timeline_tmp = copy.deepcopy(timeline_data['plasma_light'])
    if np.isfinite(dttmp['upstream']) and np.isfinite(dttmp['downstream']):
        tmp_1 = np.append(timeline_tmp['upstream'],np.array(dttmp['upstream']))
        tmp_2 = np.append(timeline_tmp['downstream'],np.array(dttmp['downstream']))
        timeadd = np.append(timeline_tmp['time'], datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S'))
        timeline_tmp.update({'upstream' : tmp_1})
        timeline_tmp.update({'downstream' : tmp_2})
        timeline_tmp.update({'time': timeadd})
        timeline_data.update({'plasma_light' : timeline_tmp})


    # timeline proton beam intensity: BCTF
    
    timeline_tmp = copy.deepcopy(timeline_data)['proton_pop_bctf']
    if type(dttmp) is not bool:
        tmp = np.append(timeline_tmp['pop'], np.array(eventdata['proton_pop_bctf']))
        timeadd = np.append(timeline_tmp['time'], datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S'))
    else:
        tmp = np.append(timeline_tmp['pop'], np.array(np.nan))
        timeadd = np.append(timeline_tmp['time'], np.array(np.nan))
    timeline_data['proton_pop_bctf'].update({'pop' : tmp})
    timeline_data['proton_pop_bctf'].update({'time': timeadd})
  
    # timeline proton beam intensity: BCTFI
    dttmp = eventdata['proton_pop_bctfi']['charge']
    timeline_tmp = copy.deepcopy(timeline_data['proton_pop_bctfi'])
    if np.size(dttmp) > 0:
        if np.isfinite(dttmp):
            tmp = np.append(timeline_tmp['charge'], np.array(dttmp))
            timeadd = np.append(timeline_tmp['time'],datetime.strptime(eventdata['time'],'%Y-%m-%d %H:%M:%S') )
            timeline_tmp.update({'charge' : tmp})
            timeline_tmp.update({'time': timeadd})
            timeline_data.update({'proton_pop_bctfi' : timeline_tmp})



def update_timeline_average(timeline_average, timeline_data, eventdata):    
    
    # N.B: timeline_data is zeroed at the start
    
    def average_array(data, avg_data, start_index, end_index):
        sample_data = data[start_index:end_index] # slice out latest 'sample_size' number of events 
        average = np.median(sample_data) # use mean... alternatively use median
        new_data = np.append(avg_data, average)    
        return new_data

    sample_size = 20 
    
    for detector, data in timeline_data.items():
        # Do not average the proton_pop_bctf
        if detector == 'proton_pop_bctf': 
            timeline_average['proton_pop_bctf'] = timeline_data['proton_pop_bctf'] 
            continue
        
        # Streamline all the naming conventions
        if detector.startswith('bpm'): x = 'hor'; y = 'ver'
        elif detector.startswith('emeter'): data_1D = 'energy'
        elif detector == 'proton_pop_bctfi':  data_1D = 'charge'
        elif detector == 'plasma_light': x = 'upstream'; y = 'downstream'
        else: x = 'x'; y = 'y'
            

        # One dimensional data
        if detector.startswith('emeter') or detector == 'proton_pop_bctfi':
            total_number_events = data[data_1D].size
            # Only update if there is a new entry on the raw data timeline array
            if timeline_data[detector]['time'][-1] != timeline_average[detector]['time'][-1]:
    
                end_index = total_number_events # Index of most recent event
                start_index = end_index - sample_size # Index of event used as the start of the average 
                
                if total_number_events < 21:# Wait until we have 20 non-zeroed data points before averaging - copy the raw data for now  
                    timeline_average[detector] = copy.deepcopy(timeline_data[detector])
                
                if total_number_events > 20: # Once we have 20 non-zeroed data points - start averaging 
                    timeline_tmp = copy.deepcopy(timeline_data[detector]) 
                    timeline_avg = copy.deepcopy(timeline_average[detector])
                    tmp = average_array(timeline_tmp[data_1D], timeline_avg[data_1D], start_index, end_index)
                    timeline_tmp.update({data_1D : tmp})
                    timeline_average.update({detector : timeline_tmp})
            
        # Two dimensional data
        else:  
            total_number_events = data[x].size
            if timeline_data[detector]['time'][-1] != timeline_average[detector]['time'][-1]:

                end_index = total_number_events # Index of most recent event
                start_index = end_index - sample_size # Index of event used as the start of the average
                
                if total_number_events < 21: # Wait until we have 20 non-zeroed data points before averaging - copy the raw data for now
                    timeline_average[detector] = copy.deepcopy(timeline_data[detector])
                
                if total_number_events > 20: # Once we have 20 non-zeroed data points - start averaging
                    timeline_tmp = copy.deepcopy(timeline_data[detector])
                    timeline_avg = copy.deepcopy(timeline_average[detector])
                    tmp_1 = average_array(timeline_tmp[x], timeline_avg[x], start_index, end_index)
                    tmp_2 = average_array(timeline_tmp[y], timeline_avg[y], start_index, end_index)
                    timeline_tmp.update({x : tmp_1})
                    timeline_tmp.update({y : tmp_2})
                    # Update average sigma for proton beam at IS1 Core and IS2 Core
                    if detector == 'is1_core' or detector == 'is2_core':
                        tmp_3 = average_array(timeline_tmp['sigma_x'],timeline_avg['sigma_x'], start_index, end_index)
                        tmp_4 = average_array(timeline_tmp['sigma_y'],timeline_avg['sigma_y'], start_index, end_index)                    
                        timeline_tmp.update({'sigma_x':tmp_3})
                        timeline_tmp.update({'sigma_y':tmp_4})
                    timeline_average.update({detector : timeline_tmp})


def Gauss(x, a, x0, sigma, p):
    return a * np.exp(-(x - x0)**2 / (2 * sigma**2)) + p

def fit_gaussian(xdata, ydata): # xdata are 1D coordinates, ydata is rough gaussian

    mean = np.nansum(xdata * ydata) / np.nansum(ydata) # average coord using weighted coords
    sigma = np.sqrt(np.nansum(ydata * (xdata - mean)**2) / np.nansum(ydata))

    try:
        popt, pcov = curve_fit(Gauss, xdata, ydata, p0=[np.nanmax(ydata), mean, sigma, 0.2])
        
        if (popt[1] > len(ydata)) or (popt[1] <= 0) or (np.abs(popt[2]) > 0.5 * len(ydata)):
            popt = [np.nan, np.nan, np.nan, np.nan]
            
    except :
        popt = [np.nan, np.nan, np.nan, np.nan] 
        sigma = np.nan

    return (popt, sigma)

def def_centroid(data):
    """
    Fit centroid to the data from IS1 Core. Customize certain data range for a better fit if needed.
    Returns x and y coordinates and σ. 
    """

    xpr = np.nansum(data, 0) #sum of data points vertically (y direction) stored in horizontal array for sum of each column
    xpr = xpr / np.nanmax(xpr) # normalising to most populated column to form 1D gaussian expressing vertical density of points
    xpr = xpr
    params = fit_gaussian(np.arange(len(xpr)), xpr)
    best_parm_x = params[0][1]
    best_sigma_x = params[1]

    ypr = np.nansum(data, 1) #sum of data points horizontally (x direction) stored in horizontal array for sum of each row
    ypr = ypr / np.nanmax(ypr) # normalising to most populated row to form 1D gaussian expressing horizontal density of points
    ypr = ypr
    params =fit_gaussian(np.arange(len(ypr)), ypr)
    best_parm_y = params[0][1]
    best_sigma_y =  params[1]

    return ((best_parm_x, best_parm_y), (best_sigma_x, best_sigma_y))


def plot_timeline_xy(leftdata, rightdata, ax1, ax2, ax3, camname, dx, dy, starttime, endtime, delta=10):
    
    """
    Plots timeline for two-dimensional data

    """


    if "BPM" in camname:
        x = 'hor'
        y = 'ver'

    else: 
        x = 'x'
        y = 'y'
   
    
    # Convert the timeline data (e.g. pixels) to mm 
    leftxdata = leftdata[x]*dx
    leftydata = leftdata[y]*dy
    rightxdata = rightdata[x]*dx
    rightydata = rightdata[y]*dy 
   
    # Format the axis to transform datetime objects correctly
    ax2.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    ax3.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    ax2.set_xlim(starttime, endtime)
    ax3.set_xlim(starttime, endtime)
    
    # Plot the last 20 events only on the 2D plot
    if np.size(leftxdata) != 1: # ignore zeroed data 
    
        xhist = np.histogram(leftxdata[1:][-20:], bins=200)
        yhist = np.histogram(leftydata[1:][-20:], bins=200)

        # Get mean values
        xmean = xhist[1][np.argmax(xhist[0])]
        ymean = yhist[1][np.argmax(yhist[0])]
        
        ax1.scatter(leftxdata[1:][-20:], leftydata[1:][-20:], alpha=0.4, s=15,)
        
        # Plot histogram on 2D plot
        ax111 = ax1.twinx() 
        ax112 = ax1.twiny()
        ax111.set_yticks([])
        ax112.set_xticks([])
        hist111 = ax111.hist(leftxdata[-20:], bins = np.linspace(xmean-delta*dx*2.5, xmean+delta*dx*2.5, 20), histtype='step',)
        hist222 = ax112.hist(leftydata[-20:], bins = np.linspace(ymean-delta*dy*2.5, ymean+delta*dy*2.5, 20), histtype='step', orientation='horizontal', )
        ax111.set_ylim(0, 4*max(hist111[0]))
        ax112.set_xlim(0, 4*max(hist222[0]))
        
        # Set limits for axes, position limits
        ax1.set_xlim(xmean-delta*dx*2.5, xmean+delta*dx*2.5)
        ax1.set_ylim(ymean-delta*dy*2.5, ymean+delta*dy*2.5)
        ax2.set_ylim(xmean-delta*dx, xmean+delta*dx)
        ax3.set_ylim(ymean-delta*dy, ymean+delta*dy)
    
    # Plot all events on the 1D plot
    ax2.scatter(rightdata['time'][1:], rightxdata[1:], s=15,)
    ax3.scatter(rightdata['time'][1:], rightydata[1:], s= 15)
   
    # Plot reference line corresponding to 20th event

    if np.size(leftxdata)>20 and np.size(leftydata)>20: 
        ax1.axhline(y = rightydata[20])
        ax1.axvline(x = rightxdata[20])
        ax2.axhline(y = rightxdata[20])
        ax3.axhline(y = rightydata[20])
        #  Get mean values
        xmean = rightxdata[20]
        ymean = rightydata[20]
        
        ax2.set_ylim(xmean-delta*dx, xmean+delta*dx)
        ax3.set_ylim(ymean-delta*dy, ymean+delta*dy)

    # Add jitter information                                                                                                                                                                                                                  
    camname2 = camname+', jitter: '+'{0:.2f}'.format(np.std(leftxdata[-20:]))+', '+'{0:.2f}'.format(np.std(leftydata[-20:]))
    text = ax1.text(0.03,0.9,camname2, weight="bold", fontsize=12, transform=ax1.transAxes, )

def plot_timeline_x(data, ax1, camname, dx, starttime, endtime, delta=1000000, _colour_ = 'turquoise', _label_ = None, offset = 0.0):
    
    """
    Plots timeline for one-dimensional data
    ---
    Arguments

    dx: scaling factor if required, delta: specifies the axes limits

    """


    if "emeter" in camname:
        x = 'energy'
        ax1.text(0.5,0.9, 'emeter', weight="bold", fontsize=12, transform=ax1.transAxes, )  

    if "PLASMA" in camname:
        if "Up" in _label_:
            x = 'upstream'
        if "Down" in _label_:
            x = 'downstream'
              
    xdata = data[x]*dx
    ax1.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    ax1.scatter(data['time'][1:], xdata[1:], s=15, c = _colour_, label = _label_, )
    ax1.set_xlim(starttime, endtime)

    # ax1.set_ylim(ymean-delta*dy, ymean+delta*dy)
   

def _plot_figure_with_proj(ax, data):

    try:

        nx = data.shape[1]
        ny = data.shape[0]

        xpr = np.nansum(data, 0) ; xpr = xpr / np.nanmax(xpr)
        ypr = np.nansum(data, 1) ; ypr = ypr / np.nanmax(ypr)

        xrange = list(np.asarray(range(0, nx)) + 0.5)
        yrange =  list(np.asarray(range(0, ny)) + 0.5)
        
        max_amp = np.max(gaussian_filter(data, sigma=3))

        ax.pcolormesh(range(0,nx+1), range(0,ny+1), data, vmin=0, vmax=max_amp, rasterized=True, cmap="inferno")
        
        ax.text(0.21,0.78,"max. amp: " + str(round(max_amp, ndigits=2)), transform=ax.transAxes,  )

        ax2 = ax.twinx()
        ax3 = ax.twiny()
        ax2.fill_between(xrange, list(xpr),  step='mid', color="white", alpha=0.5, lw=0.0, zorder = 2)
        ax2.set_ylim(0, 4.5)
        ax3.fill_betweenx(yrange, list(ypr), step='mid', color="white", alpha=0.5,  lw=0.0,  zorder = 2)
        ax3.set_xlim(0, 4.5)
        ax2.set_yticks([])
        ax3.set_xticks([])

        ax.set_xlim(xrange[0], xrange[-1])
        ax.set_ylim(yrange[0], yrange[-1])
        ax3.set_ylim(yrange[0], yrange[-1])
        ax2.set_xlim(xrange[0], xrange[-1])

    except:
        ax.set_title("Error")

# =============================================================================
# Plot mini images of diagnostics
# =============================================================================

def plot_event_data(data, savepath, store=True, closeplot=True):
    """
    Plot the data from one event.
    """

    x_text = 0.21
    y_text_2 = 0.92
    y_text_1 = 0.85
    y_text_3 = 0.78

    fig, ax = plt.subplots(4,4, figsize=(19,10),)
    fig.subplots_adjust(hspace=0.23, wspace=0.23)

    if type(data) is not bool:

        # Plot BTV50 data
        _plot_figure_with_proj(ax[0,0], data['btv_50']['data'])
        ax[0,0].set_title("BTV50", weight="bold",)
        ax[0,0].text(x_text,y_text_2,data['btv_50']['timestamp'], transform=ax[0,0].transAxes,  )
        ax[0,0].text(x_text,y_text_1,"dt: " + str(data['btv_50']['timediff']) + " [s]", transform=ax[0,0].transAxes,  )

        if data['btv_50']['timediff'] > 10 :
            ax[0,0].text(0.15,0.5,"Outdated Image!", transform=ax[0,0].transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['btv_50']['excep']:
            ax[0,0].text(0.15,0.3,"Except Error", transform=ax[0,0].transAxes, color="red", weight="bold", fontsize=12,)

        # Plot BTV53 data
        _plot_figure_with_proj(ax[0,1], data['btv_53']['data'])
        ax[0,1].set_title("BTV53", weight="bold",)
        ax[0,1].text(x_text,y_text_2,data['btv_53']['timestamp'], transform=ax[0,1].transAxes,  )
        ax[0,1].text(x_text,y_text_1,"dt: " + str(data['btv_53']['timediff']) + " [s]", transform=ax[0,1].transAxes,  )

        if data['btv_53']['timediff'] > 10 :
            ax[0,1].text(0.15,0.5,"Outdated Image!", transform=ax[0,1].transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['btv_53']['excep']:
            ax[0,1].text(0.15,0.3,"Except Error", transform=ax[0,1].transAxes, color="red", weight="bold", fontsize=12,)   

        #Plot BTV54 data
        _plot_figure_with_proj(ax[0,2], data['btv_54']['data'])
        ax[0,2].set_title("BTV54", weight="bold",)
        ax[0,2].text(x_text,y_text_2,data['btv_54']['timestamp'], transform=ax[0,2].transAxes,  )
        ax[0,2].text(x_text,y_text_1,"dt: " + str(data['btv_54']['timediff']) + " [s]", transform=ax[0,2].transAxes,  )

        if data['btv_54']['timediff'] > 10 :
            ax[0,2].text(0.15,0.5,"Outdated Image!", transform=ax[0,2].transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['btv_54']['excep']:
            ax[0,2].text(0.15,0.3,"Except Error", transform=ax[0,2].transAxes, color="red", weight="bold", fontsize=12,)   

        # Plot ExpVol01 data

        _plot_figure_with_proj(ax[0,3], data['exp_vol_1']['data'])
        ax[0,3].set_title("ExpVol01", weight="bold",)
        ax[0,3].text(x_text,y_text_2,data['exp_vol_1']['timestamp'], transform=ax[0,3].transAxes,  )
        ax[0,3].text(x_text,y_text_1,"dt: " + str(data['exp_vol_1']['timediff']) + " [s]", transform=ax[0,3].transAxes,  )

        if data['exp_vol_1']['timediff'] > 10 :
            ax[0,3].text(0.15,0.5,"Outdated Image!", transform=ax[0,3].transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['exp_vol_1']['excep']:
             ax[0,3].text(0.15,0.3,"Except Error", transform=ax[0,3].transAxes, color="red", weight="bold", fontsize=12,)   
        
        # Plot BTV26 data
        _plot_figure_with_proj(ax[1,0], data['btv_26']['data'])
        ax[1,0].set_title("BTV26", weight="bold",)
        ax[1,0].text(x_text,y_text_2,data['btv_26']['timestamp'], transform=ax[1,0].transAxes,  )
        ax[1,0].text(x_text,y_text_1,"dt: " + str(data['btv_26']['timediff']) + " [s]", transform=ax[1,0].transAxes,  )

        if data['btv_26']['timediff'] > 10 :
            ax[1,0].text(0.15,0.5,"Outdated Image!", transform=ax[1,0].transAxes, color="red", weight="bold", fontsize=12,)
        
        if data['btv_26']['excep']:
            ax[1,0].text(0.15,0.3,"Except Error", transform=ax[1,0].transAxes, color="red", weight="bold", fontsize=12,)

        # Plot IS1-Core data
        _plot_figure_with_proj(ax[1,1], data['is1_core']['data'])
        ax[1,1].set_title("IS1-Core", weight="bold",)
        ax[1,1].text(x_text,y_text_2,data['is1_core']['timestamp'], transform=ax[1,1].transAxes,  )
        ax[1,1].text(x_text,y_text_1,"dt: " + str(data['is1_core']['timediff']) + " [s]", transform=ax[1,1].transAxes,  )

        if data['is1_core']['timediff'] > 10  :
            ax[1,1].text(0.15,0.5,"Outdated Image!", transform=ax[1,1].transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['is1_core']['excep']:
            ax[1,1].text(0.15,0.3,"Except Error", transform=ax[1,1].transAxes, color="red", weight="bold", fontsize=12,)


        # Plot IS1-Halo data
        _plot_figure_with_proj(ax[1,2], data['is1_halo']['data'])
        ax[1,2].set_title("IS1-Halo", weight="bold",)
        ax[1,2].text(x_text,y_text_2,data['is1_halo']['timestamp'], transform=ax[1,2].transAxes,  )
        ax[1,2].text(x_text,y_text_1,"dt: " + str(data['is1_halo']['timediff']) + " [s]", transform=ax[1,2].transAxes,  )

        if data['is1_halo']['timediff'] > 10  :
            ax[1,2].text(0.15,0.5,"Outdated Image!", transform=ax[1,2].transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['is1_halo']['excep']:     
            ax[1,2].text(0.15,0.3,"Except Error", transform=ax[1,2].transAxes, color="red", weight="bold", fontsize= 12,)

        # PLot DMD 1
        _plot_figure_with_proj(ax[1,3], data['dmd_1']['data'])
        ax[1,3].set_title("DMD1", weight="bold",)
        ax[1,3].text(x_text,y_text_2,data['dmd_1']['timestamp'], transform=ax[1,3].transAxes,  )
        ax[1,3].text(x_text,y_text_1,"dt: " + str(data['dmd_1']['timediff']) + " [s]", transform=ax[1,3].transAxes,  )

        if data['dmd_1']['timediff'] > 10 :
            ax[1,3].text(0.15,0.5,"Outdated Image!", transform=ax[1,3].transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['dmd_1']['excep']:
            ax[1,3].text(0.15,0.3,"Except Error", transform=ax[1,3].transAxes, color="red", weight="bold", fontsize= 12, )

        # Plot DMD 2
        _plot_figure_with_proj(ax[2,0], data['dmd_2']['data'])
        ax[2,0].set_title("DMD2", weight="bold",)
        ax[2,0].text(x_text,y_text_2,data['dmd_2']['timestamp'], transform=ax[2,0].transAxes,  )
        ax[2,0].text(x_text,y_text_1,"dt: " + str(data['dmd_2']['timediff']) + " [s]", transform=ax[2,0].transAxes,  )

        if data['dmd_2']['timediff'] > 10 :
            ax[2,0].text(0.15,0.5,"Outdated Image!", transform=ax[2,0].transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['dmd_2']['excep']:
            ax[2,0].text(0.15,0.3,"Except Error", transform=ax[2,0].transAxes, color="red", weight="bold", fontsize=12,)   

        # Plot AWAKECAM 05
        _plot_figure_with_proj(ax[2,1], data['AWAKECAM_05']['data'])
        ax[2,1].set_title("AWAKECAM_05", weight="bold",)
        ax[2,1].text(x_text,y_text_2,data['AWAKECAM_05']['timestamp'], transform=ax[2,1].transAxes,  )
        ax[2,1].text(x_text,y_text_1,"dt: " + str(data['AWAKECAM_05']['timediff']) + " [s]", transform=ax[2,1].transAxes,  )

        if data['AWAKECAM_05']['timediff'] > 10 :
            ax[2,1].text(0.15,0.5,"Outdated Image!", transform=ax[2,1].transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['AWAKECAM_05']['excep']:
            ax[2,1].text(0.15,0.3,"Except Error", transform=ax[2,1].transAxes, color="red", weight="bold", fontsize=12,)   
        

        # Plot IS2-Core data
        _plot_figure_with_proj(ax[2,2], data['is2_core']['data'])
        ax[2,2].set_title("IS2-Core", weight="bold",)
        ax[2,2].text(x_text,y_text_2,data['is2_core']['timestamp'], transform=ax[2,2].transAxes, )
        ax[2,2].text(x_text,y_text_1,"dt: " + str(data['is2_core']['timediff']) + " [s]", transform=ax[2,2].transAxes,  )

        if data['is2_core']['timediff'] > 10  :
            ax[2,2].text(0.15,0.5,"Outdated Image!", transform=ax[2,2].transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['is2_core']['excep']:
            ax[2,2].text(0.15,0.3,"Except Error", transform=ax[2,2].transAxes, color="red", weight="bold", fontsize=12,)

        # Plot IS2-Halo data
        _plot_figure_with_proj(ax[2,3], data['is2_halo']['data'])
        ax[2,3].set_title("IS2-Halo", weight="bold",)
        ax[2,3].text(x_text,y_text_2,data['is2_halo']['timestamp'], transform=ax[2,3].transAxes,  )
        ax[2,3].text(x_text,y_text_1,"dt: " + str(data['is2_halo']['timediff']) + " [s]", transform=ax[2,3].transAxes,  )

        if data['is2_halo']['timediff'] > 10 :
            ax[2,3].text(0.15,0.5,"Outdated Image!", transform=ax[2,3].transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['is2_halo']['excep']:
            ax[2,3].text(0.15,0.3,"Except Error", transform=ax[2,3].transAxes, color="red", weight="bold", fontsize=12,)


        # Plot Streak TT41 data
        _plot_figure_with_proj(ax[3,0], data['streak_2']['data'])
        ax[3,0].set_title("Upstream Streak",  weight="bold",)
        ax[3,0].text(x_text,y_text_2,data['streak_2']['timestamp'], transform=ax[3,0].transAxes,  )
        ax[3,0].text(x_text,y_text_1,"dt: " + str(data['streak_2']['timediff']) + " [s]", transform=ax[3,0].transAxes,  )

        if data['streak_2']['timediff'] > 10 :
            ax[3,0].text(0.15,0.5,"Outdated Image!", transform=ax[3,0].transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['streak_2']['excep']:
            ax[3,0].text(0.15,0.3,"Except Error", transform=ax[3,0].transAxes, color="red", weight="bold", fontsize=12,)

        # Plot Streak XMPP data    
        _plot_figure_with_proj(ax[3,1], data['streak_1']['data'])
        ax[3,1].set_title("Downstream Streak", weight="bold",)
        ax[3,1].text(x_text,y_text_2,data['streak_1']['timestamp'], transform=ax[3,1].transAxes,  )
        ax[3,1].text(x_text,y_text_1,"dt: " + str(data['streak_1']['timediff']) + " [s]", transform=ax[3,1].transAxes,  )

        if data['streak_1']['timediff'] > 10 :
            ax[3,1].text(0.15,0.5,"Outdated Image!", transform=ax[3,1].transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['streak_1']['excep']:
            ax[3,1].text(0.15,0.3,"Except Error", transform=ax[3,1].transAxes, color="red", weight="bold", fontsize=12,)

                   
        # Plot XUCL-SPECTRO
        _plot_figure_with_proj(ax[3,2], data['spectro']['data'])
        ax[3,2].set_title("e- Spectrometer", weight="bold",)
        ax[3,2].text(x_text,y_text_2,data['spectro']['timestamp'], transform=ax[3,2].transAxes,  )
        ax[3,2].text(x_text,y_text_1,"dt: " + str(data['spectro']['timediff']) + " [s]", transform=ax[3,2].transAxes,  )

        if data['spectro']['timediff'] > 10 :
            ax[3,2].text(0.15,0.5,"Outdated Image!", transform=ax[3,2].transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['spectro']['excep']:
            ax[3,2].text(0.15,0.3,"Except Error", transform=ax[3,2].transAxes, color="red", weight="bold", fontsize=12,)

        # Plot plasma light
        
        ax[3,3].set_title("Plasma Light Diagnostics", weight="bold",)
        

        try:
            ax[3,3].text(x_text,y_text_2,data['plasma_light']['timestamp'], transform=ax[3,3].transAxes,  )
            ax[3,3].text(x_text,y_text_1,"dt: " + str(data['plasma_light']['timediff']) + " [s]", transform=ax[3,3].transAxes,  )
            ax[3,3].set_ylabel('counts')      
            # ax[3,3].bar(np.linspace(0, np.size(data['plasma_light']['image']), num = np.size(data['plasma_light']['image']) ), data['plasma_light']['image'],1)
            ax[3,3].hist( data['plasma_light']['image'], bins = 150) 
            if data['plasma_light']['timediff'] > 10:
                ax[3,3].text(0.15,0.5,"Outdated Image!", transform=ax[3,3].transAxes, color="red", weight="bold", fontsize=12,)

            if data['plasma_light']['excep']:
                ax[3,3].text(0.15,0.3,"Except Error", transform=ax[3,3].transAxes, color="red", weight="bold", fontsize=12,)
              
        except:
            pass
        fig.suptitle("Event #: " + str(data['enumber']) + ". Timestamp: " + str(data['timestamp']) + ". Population: " + str(data['proton_pop_bctf']) + ". Time: " + str(data['time']), y=0.93, fontsize=12, weight="bold",)

    else:
        fig.suptitle("Error reading HDF5 file", y=0.93)

    if store: 
        print('saving to ', savepath)
        fig.savefig(savepath, bbox_inches = "tight")
    if closeplot:
        plt.close(fig)
        
        
def plot_mini_imgs_laser(data, savepath, closeplot=True, store=True):
    
    
    # Turn this all into a loop
    
    fig = plt.figure(figsize=(19,4))
    gs = gridspec.GridSpec(1, 5, figure=fig,  hspace = 0.5,  wspace = 0.2)
    
    ax1 = plt.subplot(gs[0, 0]) # UV cathode
    ax2 = plt.subplot(gs[0, 1]) # IR laser room
    ax3 = plt.subplot(gs[0, 2]) # VLC 3
    ax4 = plt.subplot(gs[0, 3]) # VLC 4
    ax5 = plt.subplot(gs[0, 4]) # VLC 5
    
    x_text = 0.21
    y_text_2 = 0.92
    y_text_1 = 0.85
    y_text_3 = 0.78
    
    if type(data) is not bool:
    
        # Plot Laser 1 data
        _plot_figure_with_proj(ax1, data['laser_1']['data'])
        ax1.set_title("UV Cathode", weight="bold",)
        ax1.text(x_text,y_text_2,data['laser_1']['timestamp'], transform=ax1.transAxes,  )
        ax1.text(x_text,y_text_1,"dt: " + str(data['laser_1']['timediff']) + " [s]", transform=ax1.transAxes,  )
    
        if data['laser_1']['timediff'] > 10 :
            ax1.text(0.15,0.5,"Outdated Image!", transform=ax1.transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['laser_1']['excep']:
            ax1.text(0.15,0.3,"Except Error", transform=ax1.transAxes, color="red", weight="bold", fontsize=12,)
    
        # Plot laser 2
        _plot_figure_with_proj(ax2, data['laser_2']['data'])
        ax2.set_title("IR Laser Room", weight="bold",)
        ax2.text(x_text,y_text_2,data['laser_2']['timestamp'], transform=ax2.transAxes,  )
        ax2.text(x_text,y_text_1,"dt: " + str(data['laser_2']['timediff']) + " [s]", transform=ax2.transAxes,  )
    
        if data['laser_2']['timediff'] > 10 :
            ax2.text(0.15,0.5,"Outdated Image!", transform=ax2.transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['laser_2']['excep']:
            ax2.text(0.15,0.3,"Except Error", transform=ax2.transAxes, color="red", weight="bold", fontsize=12,)
    
        # Plot laser 3
        _plot_figure_with_proj(ax3, data['laser_3']['data'])
        ax3.set_title("Virtual Line 3", weight="bold",)
        ax3.text(x_text,y_text_2,data['laser_3']['timestamp'], transform=ax3.transAxes,  )
        ax3.text(x_text,y_text_1,"dt: " + str(data['laser_3']['timediff']) + " [s]", transform=ax3.transAxes,  )
    
        if data['laser_3']['timediff'] > 10 :
            ax3.text(0.15,0.5,"Outdated Image!", transform=ax3.transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['laser_3']['excep']:
            ax3.text(0.15,0.3,"Except Error", transform=ax3.transAxes, color="red", weight="bold", fontsize=12,)
    
        if (data['laser_3']['max_hor_amp']< 500 and (data['laser_4']['max_hor_amp']>1000 or data['laser_5']['max_hor_amp']>1000)) or (data['laser_3']['max_ver_amp']< 500 and (data['laser_4']['max_ver_amp']>1000 or data['laser_5']['max_ver_amp']>1000)):
            ax3.text(0.15,0.5,"Wrong Shot!", transform=ax3.transAxes, color = "red", weight = "bold", fontsize=12,)
    
        # Plot laser 4
        _plot_figure_with_proj(ax4, data['laser_4']['data'])
        ax4.set_title("Virtual Line 4",  weight="bold",)
        ax4.text(x_text,y_text_2,data['laser_4']['timestamp'], transform=ax4.transAxes,  )
        ax4.text(x_text,y_text_1,"dt: " + str(data['laser_4']['timediff']) + " [s]", transform=ax4.transAxes,  )
    
        if data['laser_4']['timediff'] > 10 :
            ax4.text(0.15,0.3,"Outdated Image!", transform=ax4.transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['laser_4']['excep']:
            ax4.text(0.15,0.3,"Except Error", transform=ax4.transAxes, color="red", weight="bold", fontsize=12,)
       
        if (data['laser_4']['max_hor_amp']< 500 and (data['laser_3']['max_hor_amp']>1000 or data['laser_5']['max_hor_amp']>1000)) or (data['laser_4']['max_ver_amp']< 500 and (data['laser_3']['max_ver_amp']>1000 or data['laser_5']['max_ver_amp']>1000)):
            ax4.text(0.15,0.5,"Wrong Shot!", transform=ax4.transAxes, color = "red", weight = "bold", fontsize=12,)
        
    
        # Plot laser 5
        _plot_figure_with_proj(ax5, data['laser_5']['data'])
        ax5.set_title("Virtual Line 5", weight="bold",)
        ax5.text(x_text,y_text_2,data['laser_5']['timestamp'], transform=ax5.transAxes,  )
        ax5.text(x_text,y_text_1,"dt: " + str(data['laser_5']['timediff']) + " [s]", transform=ax5.transAxes,  )
    
        if data['laser_5']['timediff'] > 10 :
            ax5.text(0.15,0.5,"Outdated Image!", transform=ax5.transAxes, color="red", weight="bold", fontsize=12,)
            
        if data['laser_5']['excep']:
            ax5.text(0.15,0.3,"Except Error", transform=ax5.transAxes, color="red", weight="bold", fontsize=12,)
        
        if (data['laser_5']['max_hor_amp']< 500 and (data['laser_3']['max_hor_amp']>1000 or data['laser_4']['max_hor_amp']>1000)) or (data['laser_5']['max_ver_amp']< 500 and (data['laser_3']['max_ver_amp']>1000 or data['laser_4']['max_ver_amp']>1000)):
            ax5.text(0.15,0.5,"Wrong Shot!", transform=ax5.transAxes, color = "red", weight = "bold", fontsize=12,) 
       
        fig.suptitle("Event #: " + str(data['enumber']) + ". Timestamp: " + str(data['timestamp']) + ". Population: " + str(data['proton_pop_bctf']) + ". Time: " + str(data['time']), y=1.02, fontsize=15, weight="bold",)

    else:
        fig.suptitle("Error reading HDF5 file", y=1.02)

    if store: 
        fig.savefig(savepath, bbox_inches = "tight")
    if closeplot:
        plt.close(fig)


def tri_subplot(gs, fig, string):
    ax1 = fig.add_subplot(gs[:, :2])
    ax2 = fig.add_subplot(gs[0, 2:])
    ax3 = fig.add_subplot(gs[1, 2:])                
    ax1.set_xlabel("x, mm" )  # the 2D plot always shows the raw data              
    ax1.set_ylabel("y, mm" )                 
    ax2.set_xlabel("Time")   
    ax2.set_ylabel("x, mm" + string)          
    ax3.set_xlabel("Time")                  
    ax3.set_ylabel("y, mm" + string)
    ax2.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=False, right=False, left=False, labelleft=False)

    return ax1, ax2, ax3


# =============================================================================
# Plot timelines (raw data)
# =============================================================================

def plot_timeline_data_protons(data, starttime, endtime, store=False, savepath=[], closeplot=False):
   
    """
    Plot the timeline data for proton BTVs.
    """

    fig = plt.figure(figsize=(13,13),) 

    gs0 = fig.add_gridspec(ncols = 1, nrows = 5, hspace = 0.25, height_ratios = [2,2,2,0.001,1.3])
    
    # Sub layout for individual diagnostic
    gs00 = gs0[0].subgridspec(2, 5, hspace = 0, wspace = 0.45)
    gs01 = gs0[1].subgridspec(2, 5, hspace = 0, wspace = 0.45)
    gs02 = gs0[2].subgridspec(2, 5, hspace = 0, wspace = 0.45)
    # gs04 = gs0[3].subgridspec(1, 5, hspace = 0, wspace = 0.4)
    gs03 = gs0[4].subgridspec(1, 5, hspace = 0, wspace = 0.4)
    
    """
    IS1 Core
    """  
    
    ax11, ax12, ax13 = tri_subplot(gs00, fig, '')
    
    # Plot beam size    
    xdata = data['is1_core']['sigma_x']
    ax12_2 = ax12.twinx()  # instantiate a second axes that shares the same x-axis
    ax12_2.set_ylabel('Beam Size', color='#feffb3')
    ax12_2.scatter(data['is1_core']['time'][1:], xdata[1:], color='#feffb3', marker = 'x', s=12,)
    ax12_2.tick_params(axis='y', labelcolor='#feffb3') 
    ydata = data['is1_core']['sigma_y']
    ax13_2 = ax13.twinx()
    ax13_2.set_ylabel('Beam Size', color='#feffb3') 
    ax13_2.scatter(data['is1_core']['time'][1:], ydata[1:], color='#feffb3', marker = 'x', s=12,)
    ax13_2.tick_params(axis='y', labelcolor='#feffb3')   
    
    # Plot beam position
    plot_timeline_xy(data["is1_core"], data["is1_core"], ax11, ax12, ax13, "IS1 Core", 1, 1, delta = 0.4, starttime = starttime, endtime = endtime) # how to choose dx and dy

    """
    IS2 Core
    """  
    
    ax21, ax22, ax23 = tri_subplot(gs01, fig, '')

    # Plot beam size        
    xdata = data['is2_core']['sigma_x']
    ax22_2 = ax22.twinx()  # instantiate a second axes that shares the same x-axis
    ax22_2.set_ylabel('Beam Size', color='#feffb3')
    ax22_2.scatter(data['is2_core']['time'][1:], xdata[1:], color='#feffb3', marker = 'x', s=12,)
    ax22_2.tick_params(axis='y', labelcolor='#feffb3')
    ydata = data['is2_core']['sigma_y']
    ax23_2 = ax23.twinx()
    ax23_2.set_ylabel('Beam Size', color='#feffb3') 
    ax23_2.scatter(data['is2_core']['time'][1:], ydata[1:], color='#feffb3', marker = 'x', s=12,)
    ax23_2.tick_params(axis='y', labelcolor='#feffb3')
    
    # Plot beam position
    plot_timeline_xy(data["is2_core"], data["is2_core"], ax21, ax22, ax23, "IS2 Core", 1, 1, delta = 0.4, starttime = starttime, endtime = endtime)

    """
    BTV 54
    """  
    
    
    ax31, ax32, ax33 = tri_subplot(gs02, fig, '')

    plot_timeline_xy(data["btv_54"], data["btv_54"], ax31, ax32, ax33, "BTV 54", 1, 1, delta = 0.4, starttime = starttime, endtime = endtime)
    
    
    """
    Proton beam intensity
    """  
    

    ax41 = fig.add_subplot(gs03[:, :])
    ax41.set_title('Proton Bunch Population', fontsize = 14, weight = 'bold', loc = 'left')

    # BCTF
    data_pop = [x for x in data['proton_pop_bctf']['pop'] if (np.isnan(x) == False) and x > 0.0]
    indices = [i for i, n in enumerate(data['proton_pop_bctf']['pop']) if (np.isnan(n) == False) and n > 0.0]
    data_time = [data['proton_pop_bctf']['time'][i] for i in indices]
   
    ax41.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    ax41.scatter(data_time[1:], data_pop[1:], s=15,)
    ax41.set_xlim(starttime, endtime)
    ax41.set_ylabel("TT41.BCTF.412340")
    ax41.set_xlabel("Time")
    
    # BCTFI
    ax41_1 = ax41.twinx()
    ax41_1.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    ax41_1.scatter(data['proton_pop_bctfi']['time'][1:], data['proton_pop_bctfi']['charge'][1:], s = 15, c = '#feffb3')
    ax41_1.set_xlim(starttime, endtime)
    ax41_1.set_ylabel("TT40.BCTFI.400344", color = '#feffb3')
    ax41_1.tick_params(axis='y', labelcolor='#feffb3')
        
    if store:
        fig.savefig(savepath, bbox_inches = "tight")
    if closeplot:
        plt.close(fig)   

        
def plot_timeline_data_laser(data, starttime, endtime, store=False, savepath=[], closeplot=False):
    """
    Plot the timeline data for the laser cameras.
    """

    fig = plt.figure(figsize=(13,15),)

    gs0 = fig.add_gridspec(ncols = 1, nrows = 4, hspace = 0.18, height_ratios = [1,1,1,1])

    # Sub layout for individual diagnostic
    gs00 = gs0[0].subgridspec(2, 5, hspace = 0, wspace = 0.45)
    gs01 = gs0[1].subgridspec(2, 5, hspace = 0, wspace = 0.45)
    gs02 = gs0[2].subgridspec(2, 5, hspace = 0, wspace = 0.45)
    gs03 = gs0[3].subgridspec(1, 10, hspace = 0, wspace = 0.4)   
    
    """
    VLC 3
    """  
    
    ax11, ax12, ax13 = tri_subplot(gs00, fig, '')

    ax12.set_ylabel("x, mm", color = 'black', bbox = dict(boxstyle = 'square', facecolor = '#8dd3c7'))
    ax13.set_ylabel("y, mm", color = 'black', bbox = dict(boxstyle = 'square', facecolor = '#8dd3c7'))
    
    plot_timeline_xy(data["laser_3"], data["laser_3"], ax11, ax12, ax13, "03TT41.CAM3", 1, 1, delta=0.6, starttime = starttime, endtime = endtime)

    """
    VLC 4
    """     
    
    ax21, ax22, ax23 = tri_subplot(gs01, fig, '')

    ax22.set_ylabel("x, mm", color = 'black', bbox = dict(boxstyle = 'square', facecolor = '#8dd3c7'))
    ax23.set_ylabel("y, mm", color = 'black', bbox = dict(boxstyle = 'square', facecolor = '#8dd3c7'))

    plot_timeline_xy(data["laser_4"], data["laser_4"], ax21, ax22, ax23, "04TT41.CAM4", 1, 1, delta=0.6, starttime = starttime, endtime = endtime)

    """
    VLC 5
    """  
        
    ax31, ax32, ax33 = tri_subplot(gs02, fig, '')

    ax32.set_ylabel("x, mm", color = 'black', bbox = dict(boxstyle = 'square', facecolor = '#8dd3c7'))
    ax33.set_ylabel("y, mm", color = 'black', bbox = dict(boxstyle = 'square', facecolor = '#8dd3c7'))
    
    plot_timeline_xy(data["laser_5"], data["laser_5"], ax31, ax32, ax33, "05TT41.CAM5", 1, 1, delta=0.6, starttime = starttime, endtime = endtime)
    
    """
    Energy Meters
    """  
    
    ax41 = fig.add_subplot(gs03[:, :9])
    ax42 = ax41.twinx()
    ax43 = ax41.twinx()
    ax44 = ax41.twinx()

    # Multiple axes formatting
    ax43.spines['right'].set_position(("axes", 1.05))
    ax44.spines['right'].set_position(("axes", 1.1))
    ax41.yaxis.label.set_color('w')
    ax44.yaxis.label.set_color('w')
    ax41.tick_params(axis='y', colors='#00FFFF')
    ax42.tick_params(axis='y', colors='#FFFF00')
    ax43.tick_params(axis='y', colors='#FF0000')
    ax44.tick_params(axis='y', colors='#00FF00')
    
    # Set labels
    ax41.set_xlabel("Time")
    ax41.set_ylabel("Energy, mJ")
    ax44.set_ylabel("Energy, mJ")

    # Set formatting of x axis as time
    ax41.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    ax42.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    ax43.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    ax44.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
  
    # Plot the data or each of the 4 emeters
    c1 = ax41.scatter(data['emeter_02']['time'][1:], data['emeter_02']['energy'][1:], s=15, c = '#00FFFF', label = 'Laser Room')
    c2 = ax42.scatter(data['emeter_03']['time'][1:], data['emeter_03']['energy'][1:], s=15, c = '#FFFF00', label = 'Compressor')
    c3 = ax43.scatter(data['emeter_04']['time'][1:], data['emeter_04']['energy'][1:], s=15, c = '#FF0000', label = 'Merging Point')
    c4 = ax44.scatter(data['emeter_05']['time'][1:], data['emeter_05']['energy'][1:], s=15, c = '#00FF00', label = 'Virtual Cathode (UV)')

    ax41.text(0.2,0.9, 'Energy Meters', weight="bold", fontsize=12, transform=ax41.transAxes, ) 
    
    ax41.set_xlim(starttime, endtime)
    ax41.legend(handles=[c1, c2, c3, c4], loc = 'upper left')

    if store:
        fig.savefig(savepath, bbox_inches = "tight")
    if closeplot:
        plt.close(fig)

 
def plot_timeline_bpm_protons(data, starttime, endtime, store = False, savepath=[], closeplot = False):

    """
    Plot the timeline data for proton BPMs
    """

    fig = plt.figure(figsize=(13,13),)
    
    gs0 = fig.add_gridspec(ncols = 1, nrows = 4, hspace = 0.18, height_ratios = [2,2,2,2])

    # Sub layout for individual diagnostic
    gs00 = gs0[0].subgridspec(2, 5, hspace = 0, wspace = 0.45)
    gs01 = gs0[1].subgridspec(2, 5, hspace = 0, wspace = 0.45)
    gs02 = gs0[2].subgridspec(2, 5, hspace = 0, wspace = 0.45)
    gs03 = gs0[3].subgridspec(2, 5, hspace = 0, wspace = 0.45)

    """
    BPM Proton 1
    """  

    ax11, ax12, ax13 = tri_subplot(gs00, fig, '')

    plot_timeline_xy(data["bpm_proton_1"], data["bpm_proton_1"], ax11, ax12, ax13, "TT41.BPM.412311", 1, 1, starttime = starttime, endtime = endtime, delta = 0.5)
    
    """
    BPM Proton 3
    """ 

    ax21, ax22, ax23 = tri_subplot(gs01, fig, '')
    
    plot_timeline_xy(data["bpm_proton_3"], data["bpm_proton_3"], ax21, ax22, ax23, "TT41.BPM.412339", 1, 1, starttime = starttime, endtime = endtime, delta = 0.5) 
    
    """
    BPM Proton 4
    """ 
    
    ax31, ax32, ax33 = tri_subplot(gs02, fig, '')
    
    plot_timeline_xy(data["bpm_proton_4"], data["bpm_proton_4"], ax31, ax32, ax33, "TT41.BPM.412352", 1, 1, starttime = starttime, endtime = endtime, delta = 0.5)

    """
    BPM Proton 5
    """ 

    ax41, ax42, ax43 = tri_subplot(gs03, fig, '')
    
    plot_timeline_xy(data["bpm_proton_5"], data["bpm_proton_5"], ax41, ax42, ax43, "TT41.BPM.412425", 1, 1, starttime = starttime, endtime = endtime, delta = 0.5)

   
    if store:
        fig.savefig(savepath, bbox_inches = "tight")
    if closeplot:
        plt.close(fig)    

def plot_timeline_bpm_electrons(data, starttime, endtime, store = False, savepath=[], closeplot = False):

    """
    Plot the timeline data for electron BPMs
    """
    fig = plt.figure(figsize=(13,7),)
    
    gs0 = fig.add_gridspec(ncols = 1, nrows = 2, hspace = 0.18, height_ratios = [2,2])

    # Sub layout for individual diagnostic
    gs00 = gs0[0].subgridspec(2, 5, hspace = 0, wspace = 0.45)
    gs01 = gs0[1].subgridspec(2, 5, hspace = 0, wspace = 0.45)


    """
    BPM Electron 1
    """  
    ax11, ax12, ax13 = tri_subplot(gs00, fig, '')

    plot_timeline_xy(data["bpm_electron_1"], data["bpm_electron_1"], ax11, ax12, ax13, "TT41.BPM.412349", 1, 1, starttime = starttime, endtime = endtime, delta = 0.2)

    # ax12.set_ylim(-0.1,+0.1)
    # ax13.set_ylim(-0.1,+0.1)

    """
    BPM Electron 2
    """  

    ax21, ax22, ax23 = tri_subplot(gs01, fig, '')

    plot_timeline_xy(data["bpm_electron_2"], data["bpm_electron_2"], ax21, ax22, ax23, "TT41.BPM.412351", 1, 1, starttime = starttime, endtime = endtime, delta = 0.2)
    
    # ax12.set_ylim(-0.1,+0.1)
    # ax13.set_ylim(-0.1,+0.1)

    if store:
        fig.savefig(savepath, bbox_inches = "tight")
    if closeplot:
        plt.close(fig)

def plot_timeline_plasma(data, starttime, endtime, store = False, savepath=[], closeplot = False):


    """
    Plot the timeline average for plasma diagnostics
    """

    
    fig = plt.figure(figsize=(10,3),)
    fig.subplots_adjust(hspace=0.4, wspace=0.4)
    
    ax2 = plt.subplot2grid((2, 10), (0, 0), rowspan=2, colspan=10,)
    
    
    ax2.set_xlabel("Time")
    ax2.set_ylabel("Integrated Counts")
    
    
    plot_timeline_x(data["plasma_light"], ax2, "PLASMA DIAGNOSTICS", 1, starttime, endtime, _colour_ = 'turquoise', _label_ = "Upstream")
    plot_timeline_x(data["plasma_light"], ax2, "PLASMA DIAGNOSTICS", 1, starttime, endtime, _colour_ = 'm', _label_ = "Downstream")
    ax2.legend(loc = 'best')
    fig.suptitle('Plasma Diagnostics', x=0.24, y = 0.95, weight="bold", fontsize=12,)

    if store:
        fig.savefig(savepath, bbox_inches = "tight")
    if closeplot:
        plt.close(fig)

        
# =============================================================================
# Plot timelines (averaged data)        
# =============================================================================
        

def plot_timeline_average_protons(rawdata, avgdata, starttime, endtime, store=False, savepath=[], closeplot=False):
    """
    Plot the timeline average data for BTVs.
    """   
        
    fig = plt.figure(figsize=(13,13),) 

    gs0 = fig.add_gridspec(ncols = 1, nrows = 5, hspace = 0.25, height_ratios = [2,2,2,0.001,1.3])
    
    # Sub layout for individual diagnostic
    gs00 = gs0[0].subgridspec(2, 5, hspace = 0, wspace = 0.45)
    gs01 = gs0[1].subgridspec(2, 5, hspace = 0, wspace = 0.45)
    gs02 = gs0[2].subgridspec(2, 5, hspace = 0, wspace = 0.45)
    # gs04 = gs0[3].subgridspec(1, 5, hspace = 0, wspace = 0.4)
    gs03 = gs0[4].subgridspec(1, 5, hspace = 0, wspace = 0.4)
    
    """
    IS1 Core
    """  
    
    ax11, ax12, ax13 = tri_subplot(gs00, fig, ' (avg.)')
    
    # Plot beam size (averaged data)  
    xdata = avgdata['is1_core']['sigma_x']
    ax12_2 = ax12.twinx()  # instantiate a second axes that shares the same x-axis
    ax12_2.set_ylabel('Beam Size', color='#feffb3')
    ax12_2.scatter(avgdata['is1_core']['time'][1:], xdata[1:], color='#feffb3', marker = 'x', s=12,)
    ax12_2.tick_params(axis='y', labelcolor='#feffb3') 
    ydata = avgdata['is1_core']['sigma_y']
    ax13_2 = ax13.twinx()
    ax13_2.set_ylabel('Beam Size', color='#feffb3') 
    ax13_2.scatter(avgdata['is1_core']['time'][1:], ydata[1:], color='#feffb3', marker = 'x', s=12,)
    ax13_2.tick_params(axis='y', labelcolor='#feffb3')   
    
    # Plot beam position (x-y plot shows raw data, 1D plots show averaged data)
    plot_timeline_xy(rawdata["is1_core"], avgdata["is1_core"], ax11, ax12, ax13, "IS1 Core", 1, 1, delta = 0.4, starttime = starttime, endtime = endtime) # how to choose dx and dy

    """
    IS2 Core
    """  
    
    ax21, ax22, ax23 = tri_subplot(gs01, fig, ' (avg.)')

    # Plot beam size (averaged data)   
    xdata = avgdata['is2_core']['sigma_x']
    ax22_2 = ax22.twinx()  # instantiate a second axes that shares the same x-axis
    ax22_2.set_ylabel('Beam Size', color='#feffb3')
    ax22_2.scatter(avgdata['is2_core']['time'][1:], xdata[1:], color='#feffb3', marker = 'x', s=12,)
    ax22_2.tick_params(axis='y', labelcolor='#feffb3')
    ydata = avgdata['is2_core']['sigma_y']
    ax23_2 = ax23.twinx()
    ax23_2.set_ylabel('Beam Size', color='#feffb3') 
    ax23_2.scatter(avgdata['is2_core']['time'][1:], ydata[1:], color='#feffb3', marker = 'x', s=12,)
    ax23_2.tick_params(axis='y', labelcolor='#feffb3')
    
    # Plot beam position (x-y plot shows raw data, 1D plots show averaged data)
    plot_timeline_xy(rawdata["is2_core"], avgdata["is2_core"], ax21, ax22, ax23, "IS2 Core", 1, 1, delta = 0.4, starttime = starttime, endtime = endtime)

    """
    BTV 54
    """  
    
    ax31, ax32, ax33 = tri_subplot(gs02, fig, ' (avg.)')

    plot_timeline_xy(rawdata["btv_54"], avgdata["btv_54"], ax31, ax32, ax33, "BTV 54", 1, 1, delta = 0.4, starttime = starttime, endtime = endtime)
    
    """
    Proton beam intensity
    """  
    
    ax41 = fig.add_subplot(gs03[:, :])
    ax41.set_title('Proton Bunch Population', fontsize = 14, weight = 'bold', loc = 'left')

    # BCTF
    data_pop = [x for x in avgdata['proton_pop_bctf']['pop'] if (np.isnan(x) == False) and x > 0.0]
    indices = [i for i, n in enumerate(avgdata['proton_pop_bctf']['pop']) if (np.isnan(n) == False) and n > 0.0]
    data_time = [avgdata['proton_pop_bctf']['time'][i] for i in indices]
   
    ax41.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    ax41.scatter(data_time[1:], data_pop[1:], s=15,)
    ax41.set_xlim(starttime, endtime)
    ax41.set_ylabel("TT41.BCTF.412340")
    ax41.set_xlabel("Time")
    
    # BCTFI
    ax41_1 = ax41.twinx()
    ax41_1.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    ax41_1.scatter(avgdata['proton_pop_bctfi']['time'][1:], avgdata['proton_pop_bctfi']['charge'][1:], s = 15, c = '#feffb3')
    ax41_1.set_xlim(starttime, endtime)
    ax41_1.set_ylabel("TT40.BCTFI.400344", color = '#feffb3')
    ax41_1.tick_params(axis='y', labelcolor='#feffb3')
    
    
    
    ax41.set_title('Proton bunch no. charges')
    if store:
        fig.savefig(savepath, bbox_inches = "tight")
    if closeplot:
        plt.close(fig)        

def plot_timeline_average_laser(rawdata, avgdata, starttime, endtime, store=False, savepath=[], closeplot=False):
   
      
    """
    Plot the timeline average for the laser cameras.
    """      
        
    fig = plt.figure(figsize=(13,15),)

    gs0 = fig.add_gridspec(ncols = 1, nrows = 4, hspace = 0.18, height_ratios = [1,1,1,1])

    # Sub layout for individual diagnostic
    gs00 = gs0[0].subgridspec(2, 5, hspace = 0, wspace = 0.45)
    gs01 = gs0[1].subgridspec(2, 5, hspace = 0, wspace = 0.45)
    gs02 = gs0[2].subgridspec(2, 5, hspace = 0, wspace = 0.45)
    gs03 = gs0[3].subgridspec(1, 10, hspace = 0, wspace = 0.4)   
    
    """
    VLC 3
    """  
    
    ax11, ax12, ax13 = tri_subplot(gs00, fig, ' (avg.)')

    ax12.set_ylabel("x, mm", color = 'black', bbox = dict(boxstyle = 'square', facecolor = '#8dd3c7'))
    ax13.set_ylabel("y, mm", color = 'black', bbox = dict(boxstyle = 'square', facecolor = '#8dd3c7'))
    
    plot_timeline_xy(rawdata["laser_3"], avgdata["laser_3"], ax11, ax12, ax13, "03TT41.CAM3", 1, 1, delta=0.2, starttime = starttime, endtime = endtime, type = 'laser')

    """
    VLC 4
    """     
    
    ax21, ax22, ax23 = tri_subplot(gs01, fig, ' (avg.)')

    ax22.set_ylabel("x, mm", color = 'black', bbox = dict(boxstyle = 'square', facecolor = '#8dd3c7'))
    ax23.set_ylabel("y, mm", color = 'black', bbox = dict(boxstyle = 'square', facecolor = '#8dd3c7'))

    plot_timeline_xy(rawdata["laser_4"], avgdata["laser_4"], ax21, ax22, ax23, "04TT41.CAM4", 1, 1, delta=0.2, starttime = starttime, endtime = endtime, type = 'laser')

    """
    VLC 5
    """  
        
    ax31, ax32, ax33 = tri_subplot(gs02, fig, ' (avg.)')

    ax32.set_ylabel("x, mm", color = 'black', bbox = dict(boxstyle = 'square', facecolor = '#8dd3c7'))
    ax33.set_ylabel("y, mm", color = 'black', bbox = dict(boxstyle = 'square', facecolor = '#8dd3c7'))
    
    plot_timeline_xy(rawdata["laser_5"], avgdata["laser_5"], ax31, ax32, ax33, "05TT41.CAM5", 1, 1, delta=0.2, starttime = starttime, endtime = endtime, type = 'laser')
    
    """
    Energy Meters
    """  
    
    ax41 = fig.add_subplot(gs03[:, :9])
    ax42 = ax41.twinx()
    ax43 = ax41.twinx()
    ax44 = ax41.twinx()

    # Multiple axes formatting
    ax43.spines['right'].set_position(("axes", 1.05))
    ax44.spines['right'].set_position(("axes", 1.1))
    ax41.yaxis.label.set_color('w')
    ax44.yaxis.label.set_color('w')
    ax41.tick_params(axis='y', colors='#00FFFF')
    ax42.tick_params(axis='y', colors='#FFFF00')
    ax43.tick_params(axis='y', colors='#FF0000')
    ax44.tick_params(axis='y', colors='#00FF00')
    
    # Set labels
    ax41.set_xlabel("Time")
    ax41.set_ylabel("Energy, mJ")
    ax44.set_ylabel("Energy, mJ")

    # Set formatting of x axis as time
    ax41.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    ax42.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    ax43.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    ax44.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
  
    # Plot the raw data of each of the 4 emeters
    c1 = ax41.scatter(rawdata['EMETER_02']['time'][1:], rawdata['EMETER_02']['energy'][1:], s=15, c = '#00FFFF', label = 'Laser Room')
    c2 = ax42.scatter(rawdata['EMETER_03']['time'][1:], rawdata['EMETER_03']['energy'][1:], s=15, c = '#FFFF00', label = 'Compressor')
    c3 = ax43.scatter(rawdata['EMETER_04']['time'][1:], rawdata['EMETER_04']['energy'][1:], s=15, c = '#FF0000', label = 'Merging Point')
    c4 = ax44.scatter(rawdata['EMETER_05']['time'][1:], rawdata['EMETER_05']['energy'][1:], s=15, c = '#00FF00', label = 'Virtual Cathode (UV)')

    ax41.text(0.2,0.9, 'Energy Meters', weight="bold", fontsize=12, transform=ax41.transAxes, ) 
    
    ax41.set_xlim(starttime, endtime)
    ax41.legend(handles=[c1, c2, c3, c4], loc = 'upper left')

    if store:
        fig.savefig(savepath, bbox_inches = "tight")
    if closeplot:
        plt.close(fig)

def plot_timeline_average_bpm_protons(rawdata, avgdata, starttime, endtime, store=False, savepath=[], closeplot=False):
    """
    Plot the timeline average for proton BPMs
    """

    fig = plt.figure(figsize=(13,13),)        
    gs0 = fig.add_gridspec(ncols = 1, nrows = 4, hspace = 0.18, height_ratios = [2,2,2,2])

    # Sub layout for individual diagnostic
    gs00 = gs0[0].subgridspec(2, 5, hspace = 0, wspace = 0.45)
    gs01 = gs0[1].subgridspec(2, 5, hspace = 0, wspace = 0.45)
    gs02 = gs0[2].subgridspec(2, 5, hspace = 0, wspace = 0.45)
    gs03 = gs0[3].subgridspec(2, 5, hspace = 0, wspace = 0.45)

    """
    BPM Proton 1
    """  

    ax11, ax12, ax13 = tri_subplot(gs00, fig, ' (avg.)')

    plot_timeline_xy(rawdata["bpm_proton_1"], avgdata["bpm_proton_1"], ax11, ax12, ax13, "TT41.BPM.412311", 1, 1, starttime = starttime, endtime = endtime, delta = 0.5)
    
    """
    BPM Proton 3
    """ 

    ax21, ax22, ax23 = tri_subplot(gs01, fig, ' (avg.)')
    
    plot_timeline_xy(rawdata["bpm_proton_3"], avgdata["bpm_proton_3"], ax21, ax22, ax23, "TT41.BPM.412339", 1, 1, starttime = starttime, endtime = endtime, delta = 0.5) 
    
    """
    BPM Proton 4
    """ 
    
    ax31, ax32, ax33 = tri_subplot(gs02, fig, ' (avg.)')
    
    plot_timeline_xy(rawdata["bpm_proton_4"], avgdata["bpm_proton_4"], ax31, ax32, ax33, "TT41.BPM.412352", 1, 1, starttime = starttime, endtime = endtime, delta = 0.5)

    """
    BPM Proton 5
    """ 

    ax41, ax42, ax43 = tri_subplot(gs03, fig, ' (avg.)')
    
    plot_timeline_xy(rawdata["bpm_proton_5"], avgdata["bpm_proton_5"], ax41, ax42, ax43, "TT41.BPM.412425", 1, 1, starttime = starttime, endtime = endtime, delta = 0.5)

   
    if store:
        fig.savefig(savepath, bbox_inches = "tight")
    if closeplot:
        plt.close(fig)  
    

def plot_timeline_average_bpm_electrons(rawdata, avgdata, starttime, endtime, store=False, savepath=[], closeplot=False):

    """
    Plot the timeline average for electron BPMs
    """
    
    fig = plt.figure(figsize=(13,7),)
    
    gs0 = fig.add_gridspec(ncols = 1, nrows = 2, hspace = 0.18, height_ratios = [2,2])

    # Sub layout for individual diagnostic
    gs00 = gs0[0].subgridspec(2, 5, hspace = 0, wspace = 0.45)
    gs01 = gs0[1].subgridspec(2, 5, hspace = 0, wspace = 0.45)


    """
    BPM Electron 1
    """  
    ax11, ax12, ax13 = tri_subplot(gs00, fig, ' (avg.)')

    plot_timeline_xy(rawdata["bpm_electron_1"], avgdata["bpm_electron_1"], ax11, ax12, ax13, "TT41.BPM.412349", 1, 1, starttime = starttime, endtime = endtime, delta = 0.2)

    # ax12.set_ylim(-0.1,+0.1)
    # ax13.set_ylim(-0.1,+0.1)

    """
    BPM Electron 2
    """  

    ax21, ax22, ax23 = tri_subplot(gs01, fig, ' (avg.)')

    plot_timeline_xy(rawdata["bpm_electron_2"], avgdata["bpm_electron_2"], ax21, ax22, ax23, "TT41.BPM.412351", 1, 1, starttime = starttime, endtime = endtime, delta = 0.2)
    
    # ax12.set_ylim(-0.1,+0.1)
    # ax13.set_ylim(-0.1,+0.1)

    if store:
        fig.savefig(savepath, bbox_inches = "tight")
    if closeplot:
        plt.close(fig)

def plot_timeline_average_plasma(rawdata, avgdata, starttime, endtime, store=False, savepath=[], closeplot=False):
    """
    Plot the timeline average for plasma diagnostics
    """

    
    fig = plt.figure(figsize=(10,3),)
    fig.subplots_adjust(hspace=0.4, wspace=0.4)
    
    ax2 = plt.subplot2grid((2, 10), (0, 0), rowspan=2, colspan=10,)
    
    
    ax2.set_xlabel("Time")
    ax2.set_ylabel("Integrated Counts")

    # Plot the upstream plasma light
    plot_timeline_x(avgdata["plasma_light"], ax2, "PLASMA DIAGNOSTICS", 1, starttime, endtime, _colour_ = 'turquoise', _label_ = "Upstream")
    # Plot the donwstream plasma light
    plot_timeline_x(avgdata["plasma_light"], ax2, "PLASMA DIAGNOSTICS", 1, starttime, endtime, _colour_ = 'm', _label_ = "Downstream")

    ax2.legend(loc = 'best')
    fig.suptitle('Plasma Diagnostics', x=0.24, y = 0.95, weight="bold", fontsize=12,)

    if store:
        fig.savefig(savepath, bbox_inches = "tight")
    if closeplot:
        plt.close(fig)
        
        
# =============================================================================
# get BTVs        
# =============================================================================
        

def get_btv_50_analogue(file):
    """
    Access and convert the data from TT41.BTV.412350 to a dictionary.
    """

    try:
        image = file["/AwakeEventData/TT41.BTV.412350/Image/"]
        nx = image["imagePositionSet1"][0].shape[0]
        ny = image["imagePositionSet2"][0].shape[0]
        dt = image["imageSet"][0].reshape(ny, nx)
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }


def get_btv_50_pxi(file):
    """
    Access and convert the data from AWAKECAM09 to a dictionary.
    """

    try:
        image = file["/AwakeEventData/BOVWA.09TCC4.AWAKECAM09/ExtractionImage"]
        dt = image['imageRawData'][:]
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
        dt = _rebin(dt, [i // 4 for i in dt.shape])
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 
        
    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }


def get_btv_50_digi(file):
    """
    Access and convert the data from TT41.BTV.412350.DigiCam to a dictionary.
    """
       
        
    try:
        image = file["/AwakeEventData/TT41.BTV.412350.DigiCam/ExtractionImage/"]
        dt = image["image2D"][:]
        dt = _rebin(dt, [i // 4 for i in dt.shape])
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }


def get_btv_53_analogue(file):
    """
    Access and convert the data from TT41.BTV.412353 to a dictionary.
    """

    try:
        image = file["/AwakeEventData/TT41.BTV.412353/Image/"]
        nx = image["imagePositionSet1"][0].shape[0]
        ny = image["imagePositionSet2"][0].shape[0]
        dt = image["imageSet"][0].reshape(ny, nx)
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }
        

def get_btv_53_digi(file):
    """
    Access and convert the data from TT41.BTV.412353.DigiCam to a dictionary.
    """

    try:
        image = file["/AwakeEventData/TT41.BTV.412353.DigiCam/ExtractionImage/"]
        dt = image["image2D"][:]
        dt = _rebin(dt, [i // 2 for i in dt.shape]) # Specify rebin factor here
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }


def get_btv_26_analogue(file):
    """
    Access and convert the data from BTV26 to a dictionary.
    """

    try:
        image = file["/AwakeEventData/TT41.BTV.412426/Image/"]
        nx = image["imagePositionSet1"][0].shape[0]
        ny = image["imagePositionSet2"][0].shape[0]
        dt = image["imageSet"][0].reshape(ny, nx)
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }
        


def get_btv_26_digi(file):
    """
    Access and convert the data from BTV26 to a dictionary.
    """

    try:
        image = file["/AwakeEventData/TT41.BTV.412426.DigiCam/ExtractionImage/"]
        dt = image["image2D"][:]
        dt = _rebin(dt, [i // 2 for i in dt.shape]) # Specify rebin factor here
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }


def get_btv_54_pxi(file):
    """
    Access and convert the data from BTV54 to a dictionary.
    """

    try:
        image = file["/AwakeEventData/BOVWA.11TCC4.AWAKECAM11/ExtractionImage/"]
        dt = image['imageRawData'][:]
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
        dt = _rebin(dt, [i // PIXELS['btv_54'][2] for i in dt.shape])
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }

def get_btv_54_digi(file):
    """
    Access and convert the data from BTV54 to a dictionary.
    """

    try:
        image = file["/AwakeEventData/TT41.BTV.412354.DigiCam/ExtractionImage/"]
        dt = image["image2D"][:]
        dt = _rebin(dt, [i // PIXELS['btv_54'][2] for i in dt.shape]) # Specify rebin factor here
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }



def get_btv_is1_core_pxi(file):
    """
    Access and convert the data from IS1 Core to a dictionary.
    """

    try:
        image = file["/AwakeEventData/BOVWA.07TCC4.AWAKECAM07/ExtractionImage/"]
        dt = image['imageRawData'][:]
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
        dt = _rebin(dt, [i // PIXELS['is1_core'][2] for i in dt.shape])
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"
    
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }

def get_btv_is1_core_digi(file):
    """
    Access and convert the data from IS1 Core to a dictionary.
    
    -TT41.BTV.412426.CORE.DigiCam 
    
    -TT41.BTV.412426.DigiCam
    """

    try:
        image = file["/AwakeEventData/TT41.BTV.412426.CORE.DigiCam/ExtractionImage/"]
        dt = image["image2D"][:]
        dt = _rebin(dt, [i // PIXELS['is1_core'][2] for i in dt.shape]) # Specify rebin factor here
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }


def get_btv_is1_halo_pxi(file):
    """
    Access and convert the data from IS1 Halo to a dictionary.
    """
    
    try:
        image = file["/AwakeEventData/BOVWA.06TCC4.AWAKECAM06/ExtractionImage/"]
        dt = image['imageRawData'][:]
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
        dt = _rebin(dt, [i // 4 for i in dt.shape])
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }

def get_btv_is1_halo_digi(file):
    """
    Access and convert the data from IS1 Halo to a dictionary.
    """

    try:
        image = file["/AwakeEventData/TT41.BTV.412426.HALO.DigiCam/ExtractionImage/"]
        dt = image["image2D"][:]
        dt = _rebin(dt, [i // PIXELS['is1_core'][2] for i in dt.shape]) # Specify rebin factor here
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }


def get_btv_is2_core_pxi(file):
    """
    Access and convert the data from IS2 Core to a dictionary.
    """


    try:
        image = file["/AwakeEventData/BOVWA.02TCC4.AWAKECAM02/ExtractionImage/"]
        dt = image['imageRawData'][:]
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
        dt = _rebin(dt, [i // PIXELS['is2_core'][2] for i in dt.shape])
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }

def get_btv_is2_core_digi(file):
    """
    Access and convert the data from IS2 Core to a dictionary.
    
    -TT41.BTV.412442.DigiCam
    
    """
    
    try:
        image = file["/AwakeEventData/TT41.BTV.412442.DigiCam/ExtractionImage/"]
        dt = image["image2D"][:]
        dt = _rebin(dt, [i // PIXELS['is2_core'][2] for i in dt.shape]) # Specify rebin factor here
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }



def get_btv_is2_halo_pxi(file):
    """
    Access and convert the data from IS2 Halo to a dictionary.
    """


    try:
        image = file["/AwakeEventData/BOVWA.04TCC4.AWAKECAM04/ExtractionImage/"]
        dt = image['imageRawData'][:]
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
        dt = _rebin(dt, [i //PIXELS['is2_core'][2] for i in dt.shape])
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }

def get_btv_is2_halo_digi(file):
    """
    Access and convert the data from IS2 Halo to a dictionary.
    """
    
    try:
        image = file["/AwakeEventData/TT41.BTV.412442.HALO.DigiCam/ExtractionImage/"]
        dt = image["image2D"][:]
        dt = _rebin(dt, [i // 4 for i in dt.shape]) # Specify rebin factor here
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }


def get_otr_angle_pxi(file):
    """
    Access and convert the data from AWAKECAM01 (OTR Angle) to a dictionary
    """


    try:
        image = file["/AwakeEventData/BOVWA.01TCC4.AWAKECAM01/ExtractionImage/"]
        dt = image['imageRawData'][:]
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
        dt = _rebin(dt, [i // 4 for i in dt.shape])
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }



def get_exp_vol_1_pxi(file):
    """
    Access and convert the data from AWAKECAM03 (Expansion Volume 1) to a dictionary.
    """
    try:
        image = file["/AwakeEventData/BOVWA.03TCC4.AWAKECAM03/ExtractionImage/"]
        dt = image['imageRawData'][:]
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
        dt = _rebin(dt, [i // 4 for i in dt.shape])
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }


def get_exp_vol_1_digi(file):
    """
    Access and convert the data from TT41.BTV.412354.EXPVOL1.DigiCam (Expansion Volume 1) to a dictionary.
    """

    try:
        image = file["/AwakeEventData/TT41.BTV.412354.EXPVOL1.DigiCam/ExtractionImage/"]
        dt = image["image2D"][:]
        dt = _rebin(dt, [i // 4 for i in dt.shape]) # Specify rebin factor here
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }

# =============================================================================
# get streak cameras
# =============================================================================


def get_streak_1_analogue(file):
    """
    Access and convert the data from XMPP-STREAK to a dictionary.
    """

    try:
        image = file["/AwakeEventData/XMPP-STREAK/StreakImage/"]
        dt = image["streakImageData"][:].reshape(512, 672)
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False  

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }


def get_streak_2_analogue(file):
    """
    Access and convert the data from TT41.BTV.412350.STREAK to a dictionary.
    """
    try:
        image = file["/AwakeEventData/TT41.BTV.412350.STREAK/StreakImage/"]
        dt = image["streakImageData"][:].reshape(508, 672)
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False  

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }


def get_MPP_streak_pxi(file):
    """
    Access and convert the data from AWAKECAM05 to a dictionary.
    """
    
    try:
        image = file["/AwakeEventData/BOVWA.05TCC4.AWAKECAM05/ExtractionImage/"]
        dt = image['imageRawData'][:]
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
        dt = _rebin(dt, [i // 4 for i in dt.shape])
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }



def get_MPP_streak_digi(file):
    """
    Access and convert the data from TSG41.MPPSTREAK-REF.DigiCam to a dictionary.
    """

    try:
        image = file["/AwakeEventData/TSG41.MPPSTREAK-REF.DigiCam/ExtractionImage/"]
        dt = image["image2D"][:]
        dt = _rebin(dt, [i // 4 for i in dt.shape]) # Specify rebin factor here
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 
 
    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }



def get_BTV_streak_pxi(file):
    """
    Access and convert the data from WAKECAM08 to a dictionary.
    """

    try:
        image = file["/AwakeEventData/BOVWA.08TCC4.AWAKECAM08/ExtractionImage/"]
        dt = image['imageRawData'][:]
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
        dt = _rebin(dt, [i // 4 for i in dt.shape])
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }



def get_BTV_streak_digi(file):
    """
    Access and convert the data from TT41.BTVSTREAK-REF.DigiCam to a dictionary.
    """

    try:
        image = file["/AwakeEventData/TT41.BTVSTREAK-REF.DigiCam/ExtractionImage/"]
        dt = image["image2D"][:]
        dt = _rebin(dt, [i // 4 for i in dt.shape]) # Specify rebin factor here
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 
 
    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }


# =============================================================================
# get beam positon monitors
# =============================================================================

def get_bpm_electron_1(file):

    try:
        data =  file["/AwakeEventData/TT41.BPM.412349/Acquisition/"]
        hor_dt = data["horPos"][:]
        ver_dt = data["verPos"][:]
        sps_extraction =data["spsExtraction"]
        hor_slice  = np.ma.masked_invalid(hor_dt)
        ver_slice = np.ma.masked_invalid(ver_dt)
        if sps_extraction == True:
            index = data["spsExtractionIndex"]
            hor_slice.mask[index] = True
            ver_slice.mask[index] = True       
        avg_hor_dt = np.ma.mean(hor_slice)
        avg_ver_dt = np.ma.mean(ver_slice)
        timestamp = data.attrs['acqStamp']
        excep = data.attrs['exception']

    except:
        avg_hor_dt = []
        avg_ver_dt = []
        timestamp = "Reading Error"
        excep = "Reading Error"  
        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False


    return {'data' : {'hor' : avg_hor_dt, 'ver' : avg_ver_dt}, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }



def get_bpm_electron_2(file):


    try:
        data =  file["/AwakeEventData/TT41.BPM.412351/Acquisition/"]
        hor_dt = data["horPos"][:]
        ver_dt = data["verPos"][:]
        sps_extraction =data["spsExtraction"]
        hor_slice  = np.ma.masked_invalid(hor_dt)
        ver_slice = np.ma.masked_invalid(ver_dt)
        if sps_extraction == True:
            index = data["spsExtractionIndex"]
            hor_slice.mask[index] = True
            ver_slice.mask[index] = True       
        avg_hor_dt = np.ma.mean(hor_slice)
        avg_ver_dt = np.ma.mean(ver_slice)
        timestamp = data.attrs['acqStamp']
        excep = data.attrs['exception']

    except:
        avg_hor_dt = []
        avg_ver_dt = []
        timestamp = "Reading Error"
        excep = "Reading Error"  
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False


    return {'data' : {'hor' : avg_hor_dt, 'ver' : avg_ver_dt}, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }


def get_bpm_proton_all(file):
    
    
    
    try:
        data =  file["/AwakeEventData/BPM.AWAKE.TT41/Acquisition/"]
        hor_dt = data["horPosition"][:]
        ver_dt = data["verPosition"][:]
        names = data["bpmNames"][:]
        timestamp = data.attrs['acqStamp']
        excep = data.attrs['exception']
    
        hor_1 = hor_dt[16] + gold_traject['proton_bpm_1']['x']
        ver_1 = ver_dt[16] + gold_traject['proton_bpm_1']['y']
        name1 = names[16]
        hor_2 = hor_dt[17] + gold_traject['proton_bpm_2']['x']
        ver_2 = ver_dt[17] + gold_traject['proton_bpm_2']['y']
        name2 = names[17]
        hor_3 = hor_dt[18] + gold_traject['proton_bpm_3']['x']
        ver_3 = ver_dt[18] + gold_traject['proton_bpm_3']['y']
        name3 = names[18]
        hor_4 = hor_dt[19] + gold_traject['proton_bpm_4']['x']
        ver_4 = ver_dt[19] + gold_traject['proton_bpm_4']['y']
        name4 = names[19]
        hor_5 = hor_dt[20] + gold_traject['proton_bpm_5']['x']
        ver_5 = ver_dt[20] + gold_traject['proton_bpm_5']['y']
        name5 = names[20]
        
        # Only add the timestamp if the data is not nan
        time_dict = {'time1':[], 'time2':[], 'time3':[], 'time4':[], 'time5':[]}
        for time, dt in zip(time_dict, [hor_1, hor_2, hor_3, hor_4, hor_5]):
            if dt != math.nan:
                time_dict[time] = timestamp
            if dt == math.nan:
                time_dict[time] = math.nan

    except:
        avg_hor_dt = []
        avg_ver_dt = []
        timestamp = "Reading Error"
        excep = "Reading Error"  
        
        hor_1 = []
        ver_1 = []
        name1 = ''
        hor_2 = []
        ver_2 = []
        name2 = ''
        hor_3 = []
        ver_3 = []
        name3 = ''
        hor_4 = []
        ver_4 = []
        name4 = ''
        hor_5 = []
        ver_5 = []
        name5 = ''
        
        data_dict = {}
    
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False
        
    data_dict = {
        '1':{'data' : {'hor' : hor_1, 'ver' : ver_1}, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff, 'name' : name1 },
        '2':{'data' : {'hor' : hor_2, 'ver' : ver_2}, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff, 'name' : name2 },
        '3':{'data' : {'hor' : hor_3, 'ver' : ver_3}, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff, 'name' : name3 },
        '4':{'data' : {'hor' : hor_4, 'ver' : ver_4}, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff, 'name' : name4 },
        '5':{'data' : {'hor' : hor_5, 'ver' : ver_5}, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff, 'name' : name5 },
        }    
    
   
    return data_dict

  
# =============================================================================
# get laser energy meters
# =============================================================================


def get_emeter_2(file):

    try:
        data = file["/AwakeEventData/EMETER02/Acq/"]
        dt = data["value"][:]
        timestamp = data.attrs['acqStamp']
        excep = data.attrs['exception']
    except:
        dt = []
        timestamp = "Reading Error"
        excep = "Reading Error" 
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False


    return {'energy' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff}
  
def get_emeter_3(file):

    try:
        data = file["/AwakeEventData/EMETER03/Acq/"]
        dt = data["value"][:]
        timestamp = data.attrs['acqStamp']
        excep = data.attrs['exception']
    except:
        dt = []
        timestamp = "Reading Error"
        excep = "Reading Error" 
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False


    return {'energy' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff}
  
def get_emeter_4(file):
        
    try:
        data = file["/AwakeEventData/EMETER04/Acq/"]
        dt = data["value"][:]
        timestamp = data.attrs['acqStamp']
        excep = data.attrs['exception']
    except:
        dt = []
        timestamp = "Reading Error"
        excep = "Reading Error" 
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False

    return {'energy' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff}
  
def get_emeter_5(file):
     
        
    try:
        data = file["/AwakeEventData/EMETER05/Acq/"]
        dt = data["value"][:]
        timestamp = data.attrs['acqStamp']
        excep = data.attrs['exception']
    except:
        dt = []
        timestamp = "Reading Error"
        excep = "Reading Error" 
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False
    
    
    return {'energy' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff}


# =============================================================================
# get virtual line cameras
# =============================================================================

def get_laser_camera_1_pxi(file):
    """
    Access and convert the data from BOVWA.01TCV4.CAM8 to a dictionary.
    """

    try:
        image = file["/AwakeEventData/BOVWA.01TCV4.CAM8/ExtractionImage/"]
        dt = image["imageRawData"][:]
        dt = _rebin(dt, [i // 4 for i in dt.shape])
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }

def get_laser_camera_2_pxi(file):
    """
    Access and convert the data from BOVWA.02TSG40.CAM7 to a dictionary.
    """

   
    try:
        image = file["/AwakeEventData/BOVWA.02TSG40.CAM7/ExtractionImage/"]
        dt = image["imageRawData"][:]
        dt = _rebin(dt, [i // 4 for i in dt.shape])
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }


def get_laser_camera_3_pxi(file):
    """
    Access and convert the data from BOVWA.03TT41.CAM3 to a dictionary.
    """

    try:
        image = file["/AwakeEventData/BOVWA.03TT41.CAM3/ExtractionImage/"]
        dt = image["imageRawData"][:]
        dt = _rebin(dt, [i // PIXELS['vlc'][2] for i in dt.shape])
        max_hor_amp = image["horProjAmp"][0]  
        max_ver_amp = image["verProjAmp"][0]   
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
        
    except :
        dt = [[]]
        max_hor_amp = 0
        max_ver_amp = 0   
        timestamp = "Reading Error"
        excep = "Reading Error"
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 
    print(excep)

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff, 'max_hor_amp' : max_hor_amp, 'max_ver_amp' : max_ver_amp}




def get_laser_camera_4_pxi(file):
    """
    Access and convert the data from BOVWA.04TT41.CAM4 to a dictionary.
    """

    try:
        image = file["/AwakeEventData/BOVWA.04TT41.CAM4/ExtractionImage/"]
        dt = image["imageRawData"][:]
        dt = _rebin(dt, [i // PIXELS['vlc'][2] for i in dt.shape])
        max_hor_amp = image["horProjAmp"][0]  
        max_ver_amp = image["verProjAmp"][0]   
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        max_hor_amp = 0
        max_ver_amp = 0   
        timestamp = "Reading Error"
        excep = "Reading Error"
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 


    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff, 'max_hor_amp' : max_hor_amp, 'max_ver_amp' : max_ver_amp}



def get_laser_camera_5_pxi(file):
    """
    Access and convert the data from BOVWA.05TT41.CAM5 to a dictionary.
    """


    try:
        image = file["/AwakeEventData/BOVWA.05TT41.CAM5/ExtractionImage/"]
        dt = image["imageRawData"][:]
        dt = _rebin(dt, [i // PIXELS['vlc'][2] for i in dt.shape])
        max_hor_amp = image["horProjAmp"][0]  
        max_ver_amp = image["verProjAmp"][0]   
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        max_hor_amp = 0
        max_ver_amp = 0   
        timestamp = "Reading Error"
        excep = "Reading Error"
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 


    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff, 'max_hor_amp' : max_hor_amp, 'max_ver_amp' : max_ver_amp}




def get_laser_camera_1_digi(file):
    """
    Access and convert the data from BOVWA.01TCV4.CAM8 to a dictionary.
    """        
        
    try:
        image = file["/AwakeEventData/TT41.VLC1.DigiCam/ExtractionImage/"]
        dt = image["image2D"][:]
        dt = _rebin(dt, [i // 4 for i in dt.shape])
        max_hor_amp = image["horProjAmp"][0]  
        max_ver_amp = image["verProjAmp"][0]   
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        max_hor_amp = 0
        max_ver_amp = 0   
        timestamp = "Reading Error"
        excep = "Reading Error"
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 


    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff, 'max_hor_amp' : max_hor_amp, 'max_ver_amp' : max_ver_amp }



def get_laser_camera_2_digi(file):
    """
    Access and convert the data from BOVWA.02TSG40.CAM7 to a dictionary.
    """

    try:
        image = file["/AwakeEventData/TT41.VLC2.DigiCam/ExtractionImage/"]
        dt = image["image2D"][:]
        dt = _rebin(dt, [i // 4 for i in dt.shape])
        max_hor_amp = image["horProjAmp"][0]  
        max_ver_amp = image["verProjAmp"][0]   
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        max_hor_amp = 0
        max_ver_amp = 0   
        timestamp = "Reading Error"
        excep = "Reading Error"
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff, 'max_hor_amp' : max_hor_amp, 'max_ver_amp' : max_ver_amp }

def get_laser_camera_3_digi(file):
    """
    Access and convert the data from BOVWA.03TT41.CAM3 to a dictionary.
    """

    try:
        image = file["/AwakeEventData/TT41.VLC3.DigiCam/ExtractionImage/"]
        dt = image["image2D"][:]
        dt = _rebin(dt, [i // PIXELS['vlc_3'][2] for i in dt.shape])
        max_hor_amp = image["horProjAmp"][0]  
        max_ver_amp = image["verProjAmp"][0]   
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        max_hor_amp = 0
        max_ver_amp = 0   
        timestamp = "Reading Error"
        excep = "Reading Error"
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 
    
    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff, 'max_hor_amp' : max_hor_amp, 'max_ver_amp' : max_ver_amp }

        
def get_laser_camera_4_digi(file):
    """
    Access and convert the data from BOVWA.04TT41.CAM4 to a dictionary.
    """
        
    try:
        image = file["/AwakeEventData/TT41.VLC4.DigiCam/ExtractionImage/"]
        dt = image["image2D"][:]
        dt = _rebin(dt, [i // PIXELS['vlc_4'][2] for i in dt.shape])
        max_hor_amp = image["horProjAmp"][0]  
        max_ver_amp = image["verProjAmp"][0]   
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        max_hor_amp = 0
        max_ver_amp = 0   
        timestamp = "Reading Error"
        excep = "Reading Error"
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff, 'max_hor_amp' : max_hor_amp, 'max_ver_amp' : max_ver_amp }


def get_laser_camera_5_digi(file):
    """
    Access and convert the data from BOVWA.05TT41.CAM5 to a dictionary.
    """

    try:
        image = file["/AwakeEventData/TT41.VLC5.DigiCam/ExtractionImage/"]
        dt = image["image2D"][:]
        dt = _rebin(dt, [i // PIXELS['vlc_5'][2] for i in dt.shape])
        max_hor_amp = image["horProjAmp"][0]  
        max_ver_amp = image["verProjAmp"][0]   
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        max_hor_amp = 0
        max_ver_amp = 0   
        timestamp = "Reading Error"
        excep = "Reading Error"
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff, 'max_hor_amp' : max_hor_amp, 'max_ver_amp' : max_ver_amp }


# =============================================================================
# get DMD cameras
# =============================================================================



def get_dmd_1_digi(file):
    """
    Access and convert the data from TCC4.DMD1.DigiCam to a dictionary.
    
    """

    try:
        image = file["/AwakeEventData/TCC4.DMD1.DigiCam/ExtractionImage/"]
        dt =image["image2D"][:]
        dt = _rebin(dt, [i // 2 for i in dt.shape])
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff}


def get_dmd_1_pxi(file):
    """
    Access and convert the data from AWAKECAM01 (DMD 1) to a dictionary.
    """
    try:
        image = file["/AwakeEventData/BOVWA.01TCC4.AWAKECAM01/ExtractionImage/"]
        dt =image["imageRawData"][:]
        dt = _rebin(dt, [i // 2 for i in dt.shape])
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff}


def get_dmd_2_digi(file):
    """
    Access and convert the data from TCC4.DMD2.DigiCam to a dictionary.

    """

    try:
        image = file["/AwakeEventData/TCC4.DMD2.DigiCam/ExtractionImage/"]
        dt =image["image2D"][:]
        dt = _rebin(dt, [i // 2 for i in dt.shape])
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff}


def get_dmd_2_pxi(file):
    """
    Access and convert the data from AWAKECAM12 (DMD 2) to a dictionary.
    """

    try:
        image = file["/AwakeEventData/BOVWA.12TCC4.AWAKECAM12/ExtractionImage/"]
        dt =image["imageRawData"][:]
        dt = _rebin(dt, [i // 2 for i in dt.shape])
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff}




# =============================================================================
# get plasma diagnostics
# =============================================================================
def get_spectrometer(file):
    """
    Access and convert the data from XUCL-SPECTRO to a dictionary.
    """

    try:
        image = file["/AwakeEventData/XUCL-SPECTRO/ImageAcq/"]
        dt = image["imageData"][:].reshape(512, 2048)
        dt = _rebin(dt, [i // 2 for i in dt.shape])
        timestamp = image.attrs['acqStamp']
        excep = image.attrs['exception']
    except :
        dt = [[]]
        timestamp = "Reading Error"
        excep = "Reading Error"
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'data' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }




def get_plasma_light(file):

    try:
        data = file["AwakeEventData/XMPP-SPECTRO/ImageAcq/"]
        dt = np.array(data["imageData"], dtype = float)
        up = np.sum(dt[350:550])-4*np.sum(dt[50:100])
        down =  np.sum(dt[150:350])-4*np.sum(dt[50:100])
        timestamp = data.attrs['acqStamp']
        excep = data.attrs['exception']

    except:
        dt = [[]]
        up = []
        down = []
        timestamp = "Reading Error"
        excep = "Reading Error"        
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'image' : dt, 'point'  : {'upstream' : up, 'downstream' : down} , 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }
    

# =============================================================================
# get proton intensity
# =============================================================================


def get_population_bctfi(file):
    
    try:
        data = file["/AwakeEventData/TT40.BCTFI.400344/CaptureAcquisition/"]
        dt = data["totalIntensity"][:]
        timestamp = data.attrs['acqStamp']
        excep = data.attrs['exception']

    except:
        dt = []
        timestamp = "Reading Error"
        excep = "Reading Error"
    try: 
        etimestamp = file_timestamp
        timediff = round(np.abs(timestamp/1E9 - etimestamp), ndigits=3) 
    except:
        timediff = False 

    return {'charge' : dt, 'timestamp' : timestamp, 'excep' : excep, 'timediff' : timediff }


def get_population_bctf(file):
    """
    Get bunch population.
    """

    try:
        population = file["/AwakeEventData/TT41.BCTF.412340/CaptureAcquisition/totalIntensity"][0]
    except:
        population = 0.0

    return population



# =============================================================================
# File properties
# =============================================================================

def get_timestamp(file):
    """
    Get event timestamp and convert it to time.
    """

    try:
        event_timestamp = round(file["/AwakeEventInfo/timestamp/"][...]/1E9, ndigits=5)
        event_time = time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(event_timestamp))
    except:
        event_timestamp = "Reading Error"
        event_time = "Reading Error"

    return (event_timestamp, event_time)

def get_event_number(file):
    """
    Get event number.
    """

    try:
        event_number = file["/AwakeEventInfo/eventNumber/"][...]
    except:
        event_number = "Reading Error"

    return event_number


# =============================================================================
# Filename properties
# =============================================================================

def _merge_fold_paths(path1, path2):
    """
    Append the month and day directories in path2 to path1 
    """

    tmp1 = path2.split('/')[-2] + '/' + path2.split('/')[-1]
    return os.path.join(path1, tmp1)

def find_lastest_subdirectory(path):
    """
    Find the most recently updated folder.
    """
    list_folders_1 = [i for i in os.listdir(path)  if os.path.isdir(os.path.join(path,i)) and (i[0] != '.') ]
    newest_folder = max([os.path.join(path,d) for d in list_folders_1], key=os.path.getmtime)

    list_folders_2 = [i for i in os.listdir(newest_folder)  if os.path.isdir(os.path.join(newest_folder,i)) and (i[0] != '.') and (i != '16') ]
    newest_folder_2 = max([os.path.join(newest_folder,d) for d in list_folders_2], key=os.path.getmtime)

    return newest_folder_2

def det_run_number(ename):
    return int(ename.split('_')[2]) 

# =============================================================================
# Misc.
# =============================================================================

def _rebin(a, shape): #'shape' is smaller version but same ratios
    sh = shape[0],a.shape[0]//shape[0],shape[1],a.shape[1]//shape[1]
    return a.reshape(sh).mean(-1).mean(1) #sh shape must be compatible with a shape

def nearest(items, pivot):
    return min(items, key=lambda x: abs(x - pivot))


#loop_events(update_interval = 1)
def await_run():
    await_run_start = 10

    while True:
        
        loop_events(update_interval = 1)
        time.sleep(await_run_start)
        print('Awaiting new run ...') 
    return True
    
    
await_run()
