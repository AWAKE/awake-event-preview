#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  8 09:53:31 2022

@author: vasylhafyc, francescaelverson, gzevi
"""
import sys
sys.path.insert(0,'/user/awakeop/AwakeEventPreview/src/')
from functions import *


# Get current date
COLLECT_DATA_TIME_NORM = (datetime.now()-timedelta(seconds = 30)).strftime('%Y-%m-%d %H:%M:%S')
#COLLECT_DATA_TIME_NORM = "2021-10-24 12:00:00" # input date in format "YYYY-mm-dd HH:MM:SS" between quotation marks                                                                                                                           
COLLECT_DATA_TIME_EPOCH = 0000000000
# no
if bool(COLLECT_DATA_TIME_NORM) == True:
    collect_data_time_object = datetime.strptime(COLLECT_DATA_TIME_NORM, '%Y-%m-%d %H:%M:%S')
    COLLECT_DATA_TIME_EPOCH = int(str(collect_data_time_object.timestamp())[0:10])

# Warning sound
SOUND_1= '/user/awakeop/felverson/quick_tools/246332__kwahmah-02__five-beeps.wav' 

# Directory path containing the h5 files; insert manually the relevant year
DATA_YEAR_PATH = '/user/awakeop/event_data/2022'
#DATA_YEAR_PATH = '/user/awakeop/AwakeEventPreview/patricPanel/testfiles'


# Directory path where all historical graphics are stored; insert manually the relevant year
SAVE_PATH =  "/user/awakeop/AwakeEventPreview/patricPanel/events-preview/2022"

# Graphic font parameters
plt.style.use('dark_background')
SMALL_SIZE = 7
MEDIUM_SIZE = 9
BIGGER_SIZE = 10
plt.rc("font", size=SMALL_SIZE)
plt.rc("axes", titlesize=MEDIUM_SIZE) 
plt.rc("axes", labelsize=MEDIUM_SIZE) 
plt.rc("xtick", labelsize=SMALL_SIZE) 
plt.rc("ytick", labelsize=SMALL_SIZE) 
plt.rc("legend", fontsize=SMALL_SIZE) 
plt.rc("figure", titlesize=BIGGER_SIZE)
plt.style.use('dark_background')

start_time = [] 

file_timestamp = 0


# File paths where the most recently updated preview graphics are stored
PREVIEW_PATHS = { 'MINI_IMGS_PATH' : "/user/awakeop/AwakeEventPreview/patricPanel/events-preview/img.pdf", # Mini diagnostics DQM (all detectors)
                  'MINI_IMGS_LASER_PATH' : "/user/awakeop/AwakeEventPreview/fran/events-preview/raw_imgs_laser.pdf", # Mini diagnostics DQM (laser-relevant detectors)
                  'TIMELINE_PROTONS_PATH' : "/user/awakeop/AwakeEventPreview/fran/events-preview/timeline-protons.pdf", # Proton BTV timeline (absolute trajectory),
                  'TIMELINE_LASER_PATH' : "/user/awakeop/AwakeEventPreview/fran/events-preview/timeline-laser.pdf", # Laser VLC timeline (absolute trajectory),
                  'TIMELINE_BPM_PROTONS_PATH' : "/user/awakeop/AwakeEventPreview/fran/events-preview/timeline-bpm-protons.pdf", # Proton BPM timeline (absolute trajectory),
                  'TIMELINE_BPM_ELECTRONS_PATH' : "/user/awakeop/AwakeEventPreview/fran/events-preview/timeline-bpm-electrons.pdf", # Electron BPM timeline (absolute trajectory),
                  'TIMELINE_PLASMA_PATH' : "/user/awakeop/AwakeEventPreview/fran/events-preview/timeline-plasma.pdf", # Plasma light timeline (absolute measurement),
                  'TIMELINE_AVERAGE_PROTONS_PATH' : "/user/awakeop/AwakeEventPreview/fran/events-preview/timeline-average-protons.pdf", #  Proton BTV timeline (average trajectory),
                  'TIMELINE_AVERAGE_LASER_PATH' : "/user/awakeop/AwakeEventPreview/fran/events-preview/timeline-average-laser.pdf", # Laser VLC timeline (average trajectory)
                  'TIMELINE_AVERAGE_BPM_PROTONS_PATH' : "/user/awakeop/AwakeEventPreview/fran/events-preview/timeline-average-bpm-protons.pdf", # Proton BPM timeline (average trajectory)
                  'TIMELINE_AVERAGE_BPM_ELECTRONS_PATH' : "/user/awakeop/AwakeEventPreview/fran/events-preview/timeline-average-bpm-electrons.pdf", # Electron BPM timeline (average trajectory)
                  #'TIMELINE_AVERAGE_PLASMA_PATH' : "/user/awakeop/AwakeEventPreview/fran/events-preview/timeline-average-plasma.pdf", # Plasma light timeline (average measurement)
                  'TIMELINE_ALIGNMENT_PATH' :  "/user/awakeop/AwakeEventPreview/fran/events-preview/timeline-alignment.pdf", # Electron BPM timeline (average trajectory)
                  }



def loop_events(update_interval = 5, ): 
    """
    The function plots a summary of individual events and the timeline plot from the most recently updated folder. The folder is updated every `update_interval=10` seconds, and new events are plotted if they appear. If the newest folder is created, it will be automatically considered for further updates.
    """  
    global file_timestamp

    # Find the latest month and day
    newest_folder_old = find_lastest_subdirectory(DATA_YEAR_PATH) 
    print('newest_folder_old',newest_folder_old)
    
    # Get the most recent run number; include constraints on the h5 data 
    try:
        #print(os.listdir(newest_folder_old))
        run_number_old = max([det_run_number(i) for i in os.listdir(newest_folder_old)
                          if _isvalid(i)  and _recentdata(i)]) 
    except Exception as e:
        print (e)
        return
    # Create empty save folder corresponding to the most recent day of data
    # (within a folder corresponding to the corresponding month i.e. SAVE_PATH/month/day/)
    save_folder = merge_fold_paths(SAVE_PATH, newest_folder_old) 
    if not os.path.exists(save_folder): os.makedirs(save_folder)
    
    files_all_old = [] # Empty list to store all filenames in directory
    ref_time = []
    ref_time_old = []
    ref_times_all = []
    ref = constr_ref()

    # Initialise dictionaries which will hold timeline data 
    timeline_data = constr_timeline_data()
    timeline_average = constr_timeline_data()
    alignment_tl = constr_alignment_tl()
    
    counter = 0
    
    # Initialise dictionaries which will hold the delta position data for two 
    # detectors per beam species
    curr_delta, avg_delta = constr_deltas()
    
    # Warnings
    warning_bool_new = constr_warning_bool()
    sound_bool = copy.deepcopy(warning_bool_new)
    warning_bool_prev = copy.deepcopy(warning_bool_new)

    while True:
        tic = time.perf_counter() # Start performance counter
       
        # Find the most recent month, and day, of data collection
        newest_folder = find_lastest_subdirectory(DATA_YEAR_PATH)
        # Get the most recent run number
        try: 
            run_number = max([det_run_number(i) for i in os.listdir(newest_folder) 
                                     if _isvalid(i)  and  _recentdata(i)])
        except: 
            run_number = False
        
        # Check if new HDF5 files correspond to a new run or a new day  
        # If so, redefine/update the save folders, latest day/run of data and
        # reset timeline arrays and HDF5 files list 
        if (newest_folder != newest_folder_old) or (run_number != run_number_old):
            newest_folder_old = copy.deepcopy(newest_folder)
            run_number_old = copy.deepcopy(run_number)
            # Create new empty save folder if new HDF5 files correspond to a new day  
            save_folder = merge_fold_paths(SAVE_PATH, newest_folder)
            if not os.path.exists(save_folder): os.makedirs(save_folder)
            # Initialise new dictionaries which will hold new timeline data 
            timeline_data = constr_timeline_data() 
            timeline_average = copy.deepcopy(timeline_data)
            alignment_tl = constr_alignment_tl()
            counter = 0
            ref_time = []
            ref_time_old = []
            files_all_old = []
            ref = constr_ref()


        # Get all filenames in directory
        files_all_new = [i for i in os.listdir(newest_folder)
                           if _isvalid(i)  and _recentdata(i) and _isrunid(i, run_number)] 
        # Get only newly added filenames in directory (sorted in time order)
        files_new = sorted(list(set(files_all_new).difference(set(files_all_old)))) 
       
        
        
        # Get paths for new files in directory
        files_new_path = [os.path.join(newest_folder, i) for i in files_new]

        # Print an update of the status of new HDF5 files
        print('New files detected ' + str(len(files_new_path)), flush=True) 
        t = time.localtime() 
        current_time = time.strftime("%H:%M:%S", t)
        print('Lastest update: ' + current_time,  flush=True)
        #print(files_new_path)
        for event in files_new_path:
            event_data = get_event_data(event) # Get the event data for new files
            #print(event_data)
            print('GotEventData')
            if type(event_data) != bool: 
                print('HDF5 event is: ' + event)
                
                # Get timestamp of current file as 'end time'
                end_time = datetime.strptime(copy.deepcopy(event_data['time']), '%Y-%m-%d %H:%M:%S') 
                
                # 'Start time' corresponds to timestamp of first file of the current day,
                # or timestamp of first file of the current run, or 6 hours prior if data
                # was being collected then, (whichever is most recent)
                if counter == 0 :
                    start_time = datetime.strptime(copy.deepcopy(event_data['time']), '%Y-%m-%d %H:%M:%S')
                counter += 1
                if  end_time - start_time > timedelta(hours =6):
                    start_time = end_time - timedelta(hours =6) 
                  
                # Add event data to the timeline dictionaries    
                update_timeline(timeline_data, event_data)
                update_timeline_average(timeline_average, timeline_data, event_data)
               
                
                # Assign two detectors to characterise each beam species
                proton_data = (timeline_data['bpm_proton_1'], timeline_data['bpm_proton_3'])
                electron_data = (timeline_data['bpm_electron_1'], timeline_data['bpm_electron_2'])
                laser_data = (timeline_data['laser_3'], timeline_data['laser_5'])
                #print('proton_data', laser_data)

                # Get the time info for the reference beam from the txt file                
                try:
                    ref_time = get_reference_time('/user/awakeop/AwakeEventPreview/patricPanel/src_ref_time.txt') 
                except: print('REFERENCE TIME HAS WRONG FORMAT!!!')
                if ref_time != ref_time_old:
                    ref_times_all.append(ref_time)
                #print('ref_time ', ref_time)
                    
                # # Get the beam reference trajectories for all detectors used in itmeline plots
                print("Get new reference? ",ref_time, ref_time_old, len(timeline_data['bpm_proton_1']['x']))
#                if ref_time != ref_time_old or {'x': [], 'y': []} in ref.values():
                if ref_time != ref_time_old or len(timeline_data['bpm_proton_1']['x']) < 5:
                    print("New Reference!")
                    get_beam_reference(timeline_data, ref_time, ref)

                ref_alignment = update_ref_alignment(ref)
                ref_time_old = copy.deepcopy(ref_time)
                print('ref alignment: ', ref_alignment)                
                
                # Get position deltas from reference trajectory
                delta_curr, delta_avg = get_deltas(timeline_data, event_data, ref_alignment, curr_delta, avg_delta)
                print("delta_curr ", delta_curr)

                # Get the alignment
                update_alignment_tl(event_data, delta_curr, alignment_tl)

                # Create the mini diagnostics DQM graphic for the current event, and store with unique HDF5 reference
                save_figure_path =  os.path.join(save_folder, os.path.split(event)[-1][0:-3] + str("-preview.pdf"))
                #plot_event_data(event_data, save_figure_path, closeplot=True, store=True) 
                save_figure_path2 =  os.path.join(save_folder, os.path.split(event)[-1][0:-3] + str("-previewAlign.pdf"))
                plot_alignment_singleEvent(event_data, alignment_tl, delta_curr, delta_avg, closeplot=True, store=True, savepath=save_figure_path2, starttime = start_time, endtime = end_time, reftime = ref_time, ref_times_all = ref_times_all)                    
                shutil.copyfile(save_figure_path2, PREVIEW_PATHS['MINI_IMGS_PATH'])

                # if event == files_new_path[-1]: 
                if event == files_new_path[-1]:
                    
                    # Play warning sound if there are any errors with the data
                    # warning_bool_prev = play_warning(event_data, warning_bool_new, warning_bool_prev, sound_bool)
                  
                    for detector in warning_bool_new.keys():
                        if event_data[detector]['excep'] == True or event_data[detector]['timediff']>10:
                            warning_bool_new[detector] = True
                            sound_bool[detector] = True
                            # Only sound the warning if the error is a new one
                            # if warning_bool_prev[detector] == True:
                            #     sound_bool[detector] = False
                        else:
                            warning_bool_new[detector] = False
                            sound_bool[detector] = False

                    warning_bool_prev = copy.deepcopy(warning_bool_new)
                    if True in sound_bool.values():
                        os.system('aplay --device=hdmi --channels=3 ' + SOUND_1)
                  

                    # Plot the event data and the timeline only for the last event 
                    # Initialise list of paths for the timeline figures
                    save_paths = {'save_preview_laser':[],
                                  'save_timeline_protons':[], 'save_timeline_laser':[],'save_timeline_protons_bpm':[],
                                  'save_timeline_electrons_bpm':[],'save_timeline_plasma':[],
                                  'save_average_timeline_protons':[], 'save_average_timeline_laser':[], 'save_average_timeline_protons_bpm':[],
                                  'save_average_timeline_electrons_bpm':[],'save_average_timeline_plasma':[], 'save_alignment':[],
                                   }
                    for path in save_paths: 
                        if bool(COLLECT_DATA_TIME_EPOCH) == False:
                           # name the file based upon the key in 'save_paths' dictionary 
                            save_paths[path] = os.path.join(save_folder, "Run-" + str(run_number) + '-'.join(path.split('_')[1:])+'.pdf')
                        if bool(COLLECT_DATA_TIME_EPOCH) == True:
                            save_paths[path] = os.path.join(save_folder, "Run-" + str(run_number)+'-'+ COLLECT_DATA_TIME_NORM[-8:] +'-' + '-'.join(path.split('_')[1:])+'.pdf')
                    
                    # Create the mini diagnostics DQM graphic for the current event, and store with unique HDF5 reference in save folder
                    
                    # Create the timeline DQM graphics for the current event, and store with general run number reference in save folder 
                    #plot_mini_imgs_laser(event_data, save_paths['save_preview_laser'], closeplot=True, store=True) 
                    #print('About to save all timeline files')
                    #plot_timeline_data_protons(timeline_data, closeplot=True, store=True, savepath=save_paths['save_timeline_protons'], starttime = start_time, endtime = end_time, ref = ref, ref_times_all = ref_times_all) 
                    #plot_timeline_data_laser(timeline_data, closeplot=True, store=True, savepath=save_paths['save_timeline_laser'], starttime = start_time, endtime = end_time, ref = ref, ref_times_all = ref_times_all)                    
                    #plot_timeline_bpm_protons(timeline_data, closeplot=True, store=True,  savepath=save_paths['save_timeline_protons_bpm'], starttime = start_time, endtime = end_time, ref = ref, ref_times_all = ref_times_all)                    
                    ##plot_timeline_bpm_electrons(timeline_data, closeplot=True, store=True, savepath=save_paths['save_timeline_electrons_bpm'], starttime = start_time, endtime = end_time)                    
                    #plot_timeline_plasma(timeline_data, closeplot=True, store=True, savepath=save_paths['save_timeline_plasma'], starttime = start_time, endtime = end_time, ref = ref, ref_times_all = ref_times_all) 
                    ##plot_timeline_average_protons(timeline_data, timeline_average, proton_beam_size_average, closeplot=True, store=True, savepath=save_paths['save_average_timeline_protons'], starttime = start_time, endtime = end_time)
                    ##plot_timeline_average_laser(timeline_data, timeline_average, closeplot=True, store=True, savepath=save_paths['save_average_timeline_laser'], starttime = start_time, endtime = end_time)
                    #plot_timeline_average_bpm_protons(timeline_data, timeline_average, closeplot=True, store=True, savepath=save_paths['save_average_timeline_protons_bpm'], starttime = start_time, endtime = end_time, ref = ref, ref_times_all = ref_times_all)
                    #plot_timeline_average_bpm_electrons(timeline_data, timeline_average, closeplot=True, store=True, savepath=save_paths['save_average_timeline_electrons_bpm'], starttime = start_time, endtime = end_time, ref = ref, ref_times_all = ref_times_all)
                    #plot_timeline_average_plasma(timeline_data, timeline_average, closeplot=True, store=True, savepath=save_paths['save_average_timeline_plasma'], starttime = start_time, endtime = end_time, ref = ref, ref_times_all = ref_times_all)   
                    #plot_alignment(alignment_tl, delta_curr, delta_avg, closeplot=True, store=True,  savepath=save_paths['save_alignment'], starttime = start_time, endtime = end_time, reftime = ref_time, ref_times_all = ref_times_all)                    

                    # Copy stored figures to most recent event preview folder
                    print('About to copy images to events-preview')
                    # Mini imgs DQM
                    #shutil.copyfile(save_figure_path, PREVIEW_PATHS['MINI_IMGS_PATH'])
                    # Remainder of DQM pdfs
                    
#                    for paths in [
#                            ('save_preview_laser', 'MINI_IMGS_LASER_PATH'),
#                            ('save_timeline_protons','TIMELINE_PROTONS_PATH'), 
#                            ('save_timeline_laser', 'TIMELINE_LASER_PATH'), 
#                            ('save_timeline_protons_bpm', 'TIMELINE_BPM_PROTONS_PATH'),
#                            # ('save_timeline_electrons_bpm', 'TIMELINE_BPM_ELECTRONS_PATH'), 
#                            # ('save_average_timeline_protons', 'TIMELINE_AVERAGE_PROTONS_PATH'),
#                            # ('save_average_timeline_plasma', 'TIMELINE_AVERAGE_PLASMA_PATH'),
#                            # ('save_average_timeline_laser', 'TIMELINE_AVERAGE_LASER_PATH'),
#                            ('save_average_timeline_protons_bpm', 'TIMELINE_AVERAGE_BPM_PROTONS_PATH'), 
#                            ('save_average_timeline_electrons_bpm', 'TIMELINE_AVERAGE_BPM_ELECTRONS_PATH'),
#                            ('save_alignment', 'TIMELINE_ALIGNMENT_PATH')
#
#                    ]:
#                        
#                        
#                        shutil.copyfile(save_paths[paths[0]], PREVIEW_PATHS[paths[1]])
            
        files_all_old.extend(files_new) # Add on the new files to the previous file array
        toc = time.perf_counter() # End performance counter
        print('Timing:', tic, toc, update_interval,  toc - tic , update_interval - toc + tic)
        if toc - tic < update_interval: # Search for new HDF5 files at most every 5 seconds
            time.sleep(update_interval - toc + tic)
    return True 

 
def _isvalid(ename):
    """
    Set restrictions on H5 file names
    """
#    return bool((ename.split('_')[1] == 'Type0')* (ename[-2:] == 'h5'))# * (int(ename[:10])<1654683906))
    # Only look every NMOD events
    nmod = 1
    if ename[-2:] == 'h5':
        #print(ename)
        #print('Checking modulo: ', ename.split('_')[3][:-3], np.mod(int(ename.split('_')[3][:-3]),5))
        return bool((ename.split('_')[1] == 'Type0')* (ename[-2:] == 'h5') * (np.mod(int(ename.split('_')[3][:-3]),int(nmod))==0))
    else: return False
def _recentdata(ename):
    """
    Find H5 files which have a timestamp after the reference timestamp
    """
    #print('recentdata: ',int(ename[0:10]), COLLECT_DATA_TIME_EPOCH)
    return bool((int(ename[0:10])) >= COLLECT_DATA_TIME_EPOCH)

def _isrunid(ename, runid):
    return bool( (det_run_number(ename) == runid) )


def get_event_data(path):
    """
    Get data from all needed diagnostics. Can be extended with additional instances if needed.
    """
    global file_timestamp

    try:
        f = h5py.File(path,'r')
        file_timestamp = get_timestamp(f)[0]
        print('setting file_timestamp to: ', file_timestamp)
        edata = {

            'timestamp' : get_timestamp(f)[0],
            'time' : get_timestamp(f)[1],
            'btv_50' : get_btv_50_digi(f, file_timestamp),
            'btv_53' : get_btv_53_analogue(f, file_timestamp),
            'btv_54' : get_btv_54_digi(f, file_timestamp),
            'btv_26' : get_btv_26_analogue(f, file_timestamp),
            'is1_core' : get_btv_is1_core_digi(f, file_timestamp),
            'is1_halo' : get_btv_is1_halo_digi(f, file_timestamp),
            'is2_core' : get_btv_is2_core_digi(f, file_timestamp),
            'is2_halo' : get_btv_is2_halo_digi(f, file_timestamp),
            'dmd_1' : get_dmd_1_digi(f, file_timestamp),
            'dmd_2' : get_dmd_2_digi(f, file_timestamp),
            # 'otr_angle': get_otr_angle_pxi(f, file_timestamp),
            'exp_vol_1': get_exp_vol_1_digi(f, file_timestamp),
            'streak_1' : get_streak_1_analogue(f, file_timestamp),
            'streak_2' : get_streak_2_analogue(f, file_timestamp),
            'MPP_STREAK' : get_MPP_streak_digi(f, file_timestamp),
            'BTV_STREAK' : get_BTV_streak_digi(f, file_timestamp),
            'laser_1' : get_laser_camera_1_pxi(f, file_timestamp),
            'laser_2' : get_laser_camera_2_pxi(f, file_timestamp),
            'laser_3' : get_laser_camera_3_digi(f, file_timestamp),
            'laser_4' : get_laser_camera_4_digi(f, file_timestamp),
            'laser_5' : get_laser_camera_5_digi(f, file_timestamp),
            'spectro' : get_spectrometer(f, file_timestamp),
            'bpm_electron_1' : get_bpm_electron_1(f, file_timestamp),
            'bpm_electron_2' : get_bpm_electron_2(f, file_timestamp),
            'bpm_proton_all': get_bpm_proton_all(f, file_timestamp), 
            'emeter_02' : get_emeter_2(f, file_timestamp),
            'emeter_03' : get_emeter_3(f, file_timestamp),
            'emeter_04' : get_emeter_4(f, file_timestamp),
            'emeter_05' : get_emeter_5(f, file_timestamp),
            'plasma_light' : get_plasma_light(f, file_timestamp),
            'proton_pop_bctfi' : get_population_bctfi(f, file_timestamp),
            'proton_pop_bctf' : get_population_bctf(f, file_timestamp),
            'enumber' : get_event_number(f, file_timestamp), 
        }
        
        edata['bpm_proton_1'] = edata['bpm_proton_all']['1']
        edata['bpm_proton_2'] = edata['bpm_proton_all']['2']
        edata['bpm_proton_3'] = edata['bpm_proton_all']['3']
        edata['bpm_proton_4'] = edata['bpm_proton_all']['4']
        edata['bpm_proton_5'] = edata['bpm_proton_all']['5']

        f.close()
        
    except Exception as e:

        edata = False
        print(e)

    return edata

def plot_alignment_singleEvent(full_data, data, delta_curr, delta_avg, starttime, endtime, reftime, ref_times_all, store=False, savepath=[], closeplot=False,  ):
    fig = plt.figure(figsize=(15,12),)
    

    # Improvements: History, Circles, Circles based on history

    """
    Trigonometry in-house
    We just want the offset of the beam, transported from the BPMs to the plasma entrance/exit
    offset_entrance = DeltaBPM1 + (DeltaBPM2 - DeltaBPM1) / Dist_BPM1_BPM2 * Dist_BPM1_entrance
    offset_entrance = detector_1 + (detector_2 - detector_1) / Dist_BPM1_BPM2 * DIMENSIONS_DICT['upstream']['e/p']['detector_1']
    offset_entrance = detector_1 + (detector_2 - detector_1) / scale_up
    offset_entrance = (detector_1(scale_up - 1) + detector_2) / scale_up
    """
    dist_BPMs_e = DIMENSIONS_DICT['upstream']['electron']['detector_2'] - DIMENSIONS_DICT['upstream']['electron']['detector_1']
    scale_electron_up = dist_BPMs_e / np.abs(DIMENSIONS_DICT['upstream']['electron']['detector_1'])
    scale_electron_dn = dist_BPMs_e / np.abs(DIMENSIONS_DICT['downstream']['electron']['detector_1'])
    dist_BPMs_p = DIMENSIONS_DICT['upstream']['proton']['detector_2'] - DIMENSIONS_DICT['upstream']['proton']['detector_1']
    scale_proton_up = dist_BPMs_p / np.abs(DIMENSIONS_DICT['upstream']['proton']['detector_1'])
    scale_proton_dn = dist_BPMs_p / np.abs(DIMENSIONS_DICT['downstream']['proton']['detector_1'])

    #print("scales: ",scale_electron_up, scale_electron_dn, scale_proton_up, scale_proton_dn)

    delta_laser_up_y = 	delta_curr["laser"]["detector_1"]["x"]  # Rotate 1: right is up, left is down
    delta_laser_up_x = -1 * delta_curr["laser"]["detector_1"]["y"]
    delta_laser_dn_y = 	delta_curr["laser"]["detector_2"]["x"]
    delta_laser_dn_x = -1 * delta_curr["laser"]["detector_2"]["y"]
    if (math.isnan(delta_laser_up_x)) : delta_laser_up_x = 9
    if (math.isnan(delta_laser_up_y)) : delta_laser_up_y = 9
    if (math.isnan(delta_laser_dn_x)) : delta_laser_dn_x = 9
    if (math.isnan(delta_laser_dn_y)) : delta_laser_dn_y = 9

    delta_e = delta_curr["electron"]

    delta_electron_up_x = ( delta_e["detector_1"]["x"] * (scale_electron_up -1) + delta_e["detector_2"]["x"] ) / scale_electron_up 
    delta_electron_up_y = ( delta_e["detector_1"]["y"] * (scale_electron_up -1) + delta_e["detector_2"]["y"] ) / scale_electron_up 
    delta_electron_dn_x = ( delta_e["detector_1"]["x"] * (scale_electron_dn -1) + delta_e["detector_2"]["x"] ) / scale_electron_dn 
    delta_electron_dn_y = ( delta_e["detector_1"]["y"] * (scale_electron_dn -1) + delta_e["detector_2"]["y"] ) / scale_electron_dn 
    print('XXXXXXXXX value here is: ',delta_electron_up_x)

    if (math.isnan(delta_electron_up_x) or delta_electron_up_x == 0 or
        math.isnan(delta_electron_up_y) or delta_electron_up_y == 0 or
        math.isnan(delta_electron_dn_x) or delta_electron_dn_x == 0 or
        math.isnan(delta_electron_dn_y) or delta_electron_dn_y == 0):
        delta_electron_up_x = 9
        delta_electron_up_y = 9
        delta_electron_dn_x = 9
        delta_electron_dn_y = 9

    print('YYYYYYYYY value here is: ',delta_electron_up_x)

    delta_p = delta_curr["proton"]
    delta_proton_up_x = ( delta_p["detector_1"]["x"] * (scale_proton_up -1) + delta_p["detector_2"]["x"] ) / scale_proton_up 
    delta_proton_up_y = ( delta_p["detector_1"]["y"] * (scale_proton_up -1) + delta_p["detector_2"]["y"] ) / scale_proton_up 
    delta_proton_dn_x = ( delta_p["detector_1"]["x"] * (scale_proton_dn -1) + delta_p["detector_2"]["x"] ) / scale_proton_dn 
    delta_proton_dn_y = ( delta_p["detector_1"]["y"] * (scale_proton_dn -1) + delta_p["detector_2"]["y"] ) / scale_proton_dn 
    if (math.isnan(delta_proton_up_x)) : delta_proton_up_x = 9
    if (math.isnan(delta_proton_up_y)) : delta_proton_up_y = 9
    if (math.isnan(delta_proton_dn_x)) : delta_proton_dn_x = 9
    if (math.isnan(delta_proton_dn_y)) : delta_proton_dn_y = 9

    print('Delta UP X', delta_proton_up_x, delta_electron_up_x, delta_laser_up_x)


    print('Electrons: ', delta_electron_up_x)
    print('Laser: ', delta_laser_up_x)
    print('Protons: ', delta_proton_up_x)
  
    #gs0 = fig.add_gridspec(ncols = 1, nrows = 7, hspace = 0.2, height_ratios = [0.2,2,0.01,0.5,4.3,0.5,4.3])
    gs0 = fig.add_gridspec(ncols = 1, nrows = 5, hspace = 0.2, height_ratios = [0.2,2,0.01,0.5,3.1])
    
    # Sub layout for individual diagnostic
    gs00 = gs0[0].subgridspec(2, 5, hspace = 0, wspace = 0.45) # Title of detector deltas
    gs01 = gs0[1].subgridspec(3, 15, hspace = 0, wspace = 0.45) # Detector deltas table
    gs02 = gs0[2].subgridspec(2, 5, hspace = 0, wspace = 0.45) # Title of alignment deltas
    gs03_t = gs0[3].subgridspec(2, 5, hspace = 0, wspace = 0.45) # Subtitle proton-electron
    gs03 = gs0[4].subgridspec(2, 5, hspace = 0, wspace = 0.45) # Proton-Electron alignment deltas 
#    gs04_t = gs0[5].subgridspec(2, 5, hspace = 0, wspace = 0.45) # Subtitle proton-laser
#    gs04 = gs0[6].subgridspec(2, 5, hspace = 0, wspace = 0.45) # Proton-Laser alignment deltas 
    
    """
    gs00: Title for the position deltas at each of the relevant detectors used 
    in the alignment calculations
    
    """
    ax01 = fig.add_subplot(gs00[:, :])
    ax01.text(x = 0, y = 0, s = f'NEW PANEL: Reference timestamp: {reftime}', ha = 'left', va = 'center', transform = ax01.transAxes, fontsize = 18, fontweight = 'bold', color = 'r')
    ax01.axis('off')
    
    """
    gs01: Position deltas
    
    """
    ax11 = fig.add_subplot(gs01[0, 1:14])
    ax11.axis('off')
    ax12 = fig.add_subplot(gs01[1, 1:14])
    ax12.axis('off')
    ax13 = fig.add_subplot(gs01[2, 1:14])
    ax13.axis('off')


    ax11.text(x = 0.22, y = 0.5, s = 'Proton', ha = 'left', va = 'bottom', transform = ax11.transAxes, fontsize = 14, weight = 'bold')
    ax11.text(x = 0.49, y = 0.5, s = 'Electron', ha = 'left', va = 'bottom', transform = ax11.transAxes, fontsize = 14, weight = 'bold')
    ax11.text(x = 0.76, y = 0.5, s = 'Laser', ha = 'left', va = 'bottom', transform = ax11.transAxes, fontsize = 14, weight = 'bold')


    # Table spines formatting
    ax11.axvline(x = 0.2,)
    ax11.axvline(x = 0.47,)
    ax11.axvline(x = 0.74,)
    
    ax12.axvline(x = 0.2,)
    ax12.axvline(x = 0.47,)
    ax12.axvline(x = 0.74,)
    
    ax13.axvline(x = 0.2,)
    ax13.axvline(x = 0.47, )
    ax13.axvline(x = 0.74, )
    
    ax13.set_ylim(0, 1)
    ax13.axhline(y = 1)

    ax12.text(x = 0, y = 0.5, s = 'Current Δ(pos.), mm', ha = 'left', va = 'center', transform = ax12.transAxes, fontsize = 13)

    
    try:
        ax12.text(x = 0.22, y = 0.5, s = 
                  (f'BPM 339: {delta_curr["proton"]["detector_1"]["x"]:.2f} $\it{{x}}$, {delta_curr["proton"]["detector_1"]["y"]:.2f} $\it{{y}}$\nBPM 411: {delta_curr["proton"]["detector_2"]["x"]:.2f} $\it{{x}}$, {delta_curr["proton"]["detector_2"]["y"]:.2f} $\it{{y}}$'), 
                  ha = 'left', va = 'center', transform = ax12.transAxes, fontsize = 13)
    except:
        ax12.text(x = 0.22, y = 0.5, s = f'BPM 339: error\nBPM 411: error', ha = 'left', va = 'center', transform = ax12.transAxes, fontsize = 13)

    try:
        ax12.text(x = 0.49, y = 0.5, s = 
                  (f'BPM 349: {delta_curr["electron"]["detector_1"]["x"]:.2f} $\it{{x}}$, {delta_curr["electron"]["detector_1"]["y"]:.2f} $\it{{y}}$\nBPM 351: {delta_curr["electron"]["detector_2"]["x"]:.2f} $\it{{x}}$, {delta_curr["electron"]["detector_2"]["y"]:.2f} $\it{{y}}$'), 
                  ha = 'left', va = 'center', transform = ax12.transAxes, fontsize = 13)
    except Exception as e:
        print("CANNOT PRINT ELECTRON DELTAS")
        print(e)
        ax12.text(x = 0.49, y = 0.5, s = f'BPM 349: error\nBPM 351: error', ha = 'left', va = 'center', transform = ax12.transAxes, fontsize = 13)

    try:     
        ax12.text(x = 0.76, y = 0.5, s =
                  (f'VLC 3: {delta_curr["laser"]["detector_1"]["x"]:.2f} $\it{{x}}$, {delta_curr["laser"]["detector_1"]["y"]:.2f} $\it{{y}}$\nVLC 5: {delta_curr["laser"]["detector_2"]["x"]:.2f} $\it{{x}}$, {delta_curr["laser"]["detector_2"]["y"]:.2f} $\it{{y}}$'), 
                  ha = 'left', va = 'center', transform = ax12.transAxes, fontsize = 13)
    except:
        ax12.text(x = 0.76, y = 0.5, s = f'VLC 3: error\nVLC 5: error', ha = 'left', va = 'center', transform = ax12.transAxes, fontsize = 13)

    ax13.text(x = 0, y = 0.5, s = 'Average Δ(pos.), mm', ha = 'left', va = 'center', transform = ax13.transAxes, fontsize = 13)
    ax13.text(x = 0.22, y = 0.5, s = f'BPM 339: {delta_avg["proton"]["detector_1"]["x"]:.2f} $\it{{x}}$, {delta_avg["proton"]["detector_1"]["y"]:.2f} $\it{{y}}$ \nBPM 411: {delta_avg["proton"]["detector_2"]["x"]:.2f} $\it{{x}}$, {delta_avg["proton"]["detector_2"]["y"]:.2f} $\it{{y}}$',
              ha = 'left', va = 'center', transform = ax13.transAxes, fontsize = 13)
    ax13.text(x = 0.49, y = 0.5, s = f'BPM 349: {delta_avg["electron"]["detector_1"]["x"]:.2f} $\it{{x}}$, {delta_avg["electron"]["detector_1"]["y"]:.2f} $\it{{y}}$ \nBPM 351: {delta_avg["electron"]["detector_2"]["x"]:.2f} $\it{{x}}$, {delta_avg["electron"]["detector_2"]["y"]:.2f} $\it{{y}}$',
              ha = 'left', va = 'center', transform = ax13.transAxes, fontsize = 13)
    ax13.text(x = 0.76, y = 0.5, s = f'VLC 3: {delta_avg["laser"]["detector_1"]["x"]:.2f} $\it{{x}}$, {delta_avg["laser"]["detector_1"]["y"]:.2f} $\it{{y}}$ \nVLC 5: {delta_avg["laser"]["detector_2"]["x"]:.2f} $\it{{x}}$, {delta_avg["laser"]["detector_2"]["y"]:.2f} $\it{{y}}$',
              ha = 'left', va = 'center', transform = ax13.transAxes, fontsize = 13)


    """
    gs02: Title for the position deltas at the start of the plasma cell between
    the two specified beam species
    
    """
#    ax21 = fig.add_subplot(gs02[:, :])
#    # ax21.text(x = 0, y = 0, s = 'Δ(pos.) between beams at start of plasma cell',  ha = 'left', va = 'bottom', transform = ax21.transAxes, fontsize = 17, weight = 'bold')
#    ax21.text(x = 0, y = 0, s = 'Beam alignment at start of plasma cell',  ha = 'left', va = 'bottom', transform = ax21.transAxes, fontsize = 17, weight = 'bold')
#    ax21.axis('off')
    
    """
    gs03_t: Title for the position deltas at the start of the plasma cell between
    the two specified beam species
    
    """
    ax3_t1 = fig.add_subplot(gs03_t[0, :])
    ax3_t2 = fig.add_subplot(gs03_t[1, :1])
    ax3_t3 = fig.add_subplot(gs03_t[1, 1:3])
    ax3_t4 = fig.add_subplot(gs03_t[1, 3:5])
    ax3_t1.text(x = 0, y = 0, s = 'Proton-Electron-Laser', fontsize = 15, weight = 'bold')
    ax3_t1.axis('off')
    ax3_t2.text(x = 0, y = 0, s = 'Laser Energies', fontsize = 11, weight = 'bold')
    ax3_t2.axis('off')
    ax3_t3.text(x = 0, y = 0, s = 'Upstream (w.r.t Ref)', fontsize = 11, weight = 'bold')
    ax3_t3.axis('off')
    ax3_t4.text(x = 0, y = 0, s = 'Downstream (w.r.t. Ref)', fontsize = 11, weight = 'bold')
    ax3_t4.axis('off')
    

    """
    gs03: Electron-Proton alignment timeline (start of plasma cell, position difference
    in x and y)
        gs03 = gs0[4].subgridspec(2, 5, hspace = 0, wspace = 0.45) # Proton-Electron alignment deltas 

    """
    ax30 = fig.add_subplot(gs03[0, :1])
    ax30.axis('off')

    ax31= fig.add_subplot(gs03[1, :1])
    ax31.axis('off')

    # Need to modify 32 and 33 to turn them into square scatter plots
    #ax32 = fig.add_subplot(gs03[0, 1:])
    #ax33 = fig.add_subplot(gs03[1, 1:])
    ax32 = fig.add_subplot(gs03[:, 1:3])
    ax33 = fig.add_subplot(gs03[:, 3:5])
   
    x_pos = data['alignment_p_e']['upstream']['rel_pos']['x'][-1] 
    x_angle = data['alignment_p_e']['upstream']['angle']['x'][-1]
    y_pos = data['alignment_p_e']['upstream']['rel_pos']['y'][-1]
    y_angle = data['alignment_p_e']['upstream']['angle']['y'][-1]
   
    # Text
    #ax30.text(x = 0, y = 0.5, s = f'Δ($pos._x$): {x_pos:.2f} mm', ha = 'left', va = 'bottom',  fontsize = 14,  )
    #ax30.text(x = 0, y = 0.5, s = f'Δ($θ_x$): {x_angle:.2f} μrad', ha = 'left', va = 'top', fontsize = 14,   )
    #ax31.text(x = 0, y = 0.5, s = f'Δ($pos._y$): {y_pos:.2f} mm', ha = 'left', va = 'bottom', fontsize = 14,)
    #ax31.text(x = 0, y = 0.5, s = f'Δ($θ_y$): {y_angle:.2f} μrad', ha = 'left', va = 'top',  fontsize = 14, )

    UV = full_data['emeter_05']['energy'][-1]*1E9
    IR = full_data['emeter_04']['energy'][-1]*1E3
    ax30.text(x = 0, y = 0.5, s = f'IR Energy', ha = 'left', va = 'bottom',  fontsize = 14,  )
    ax30.text(x = 0, y = 0.5, s = f'{IR:.2f} mJ', ha = 'left', va = 'top', fontsize = 14,   )
    ax31.text(x = 0, y = 0.5, s = f'UV Energy', ha = 'left', va = 'bottom', fontsize = 14,)
    ax31.text(x = 0, y = 0.5, s = f'{UV:.2f} nJ', ha = 'left', va = 'top',  fontsize = 14, )
    #print('UV Energy', full_data['emeter_05']['energy'][-1])
    
    ax32.set_xlabel("Δ($pos._x$), mm")   
    ax32.set_ylabel("Δ($pos._y$), mm")
    ax33.set_xlabel("Δ($pos._x$), mm")                  
    ax33.set_ylabel("Δ($pos._y$), mm") 

    
    # # Hide the ticks for the 1D x plot
#    ax32.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=False, right=False, left=False, labelleft=False)

    # Plot the proton - electron alignment

    timedata = data['alignment_p_e']['time']


    #if timedata.size> 1:

    size = 60
    c1=ax32.scatter(delta_proton_up_x, delta_proton_up_y, alpha=1, s=size, c='b', label='Proton')
    c2=ax32.scatter(delta_electron_up_x, delta_electron_up_y, alpha=1, s=size, c='y', label='Electron')
    c3=ax32.scatter(delta_laser_up_x, delta_laser_up_y, alpha=1, s=size, c='r', label='Laser')
    circle1 = plt.Circle((delta_laser_up_x, delta_laser_up_y), radius=0.7, color='r', alpha=0.3)
    circle2 = plt.Circle((delta_proton_up_x, delta_proton_up_y), radius=0.2, color='b', alpha=0.3)
    circle3 = plt.Circle((delta_electron_up_x, delta_electron_up_y), radius=0.2, color='y', alpha=0.3)
    ax32.add_patch(circle1)
    ax32.add_patch(circle2)
    ax32.add_patch(circle3)
    ax32.legend(handles=[c1,c2,c3], loc = 'upper right', prop={'size':10})
    ax32.grid(linestyle='--', linewidth = 0.5)
    c1=ax33.scatter(delta_proton_dn_x, delta_proton_dn_y, alpha=1, s=size, c='b', label='Proton')
    c2=ax33.scatter(delta_electron_dn_x, delta_electron_dn_y, alpha=1, s=size, c='y', label='Electron')
    c3=ax33.scatter(delta_laser_dn_x, delta_laser_dn_y, alpha=1, s=size, c='r', label='Laser')
    circle1 = plt.Circle((delta_laser_dn_x, delta_laser_dn_y), radius=0.7, color='r', alpha=0.3)
    circle2 = plt.Circle((delta_proton_dn_x, delta_proton_dn_y), radius=0.2, color='b', alpha=0.3)
    circle3 = plt.Circle((delta_electron_dn_x, delta_electron_dn_y), radius=0.2, color='y', alpha=0.3)
    ax33.add_patch(circle1)
    ax33.add_patch(circle2)
    ax33.add_patch(circle3)
    ax33.grid(linestyle='--', linewidth = 0.5)
    ax33.legend(handles=[c1,c2,c3], loc = 'upper right', prop={'size':10})


        #ax33.scatter(timedata[1:], ydata[1:])
        #ax32.scatter(xdata[-1], ydata[-1], alpha=0.4, s=15,)
        #ax32.scatter(xdata[-1], ydata[-1], alpha=0.4, s=15,)
            
    #ax32.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M')) # covert datetime format into 
    #ax33.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M')) # covert datetime format into 
 
    #ax32.set_xlim(starttime, endtime)
    #ax33.set_xlim(starttime, endtime)
    limit = 1
    ax32.set_xlim(-1*limit, 1*limit)
    ax33.set_xlim(-1.5*limit, 1.5*limit)
    ax32.set_ylim(-1*limit, 1*limit)
    ax33.set_ylim(-1.5*limit, 1.5*limit)
    
    # create markers for when reference time changed
    #try:
    #    for i in ref_times_all:
    #        ax32.axvline(x = i, alpha = 0.3, color = 'red')
    #        ax33.axvline(x = i, alpha = 0.3, color = 'red')
    #except:
    #    pass
    
    
    # Get zero at centre of y-axis as perfect alignment reference point
    #yabs_max = abs(max(ax32.get_ylim(), key=abs))
    #ax32.set_ylim(ymin=-yabs_max, ymax=yabs_max)
    #ax32.axhline(y = 0, ls = 'dashed', alpha = 0.6)
    #yabs_max = abs(max(ax33.get_ylim(), key=abs))
    #ax33.set_ylim(ymin=-yabs_max, ymax=yabs_max)
    #ax33.axhline(y = 0, ls = 'dashed', alpha = 0.6)
    
    if store:
        fig.savefig(savepath)#, bbox_inches = "tight")
    if closeplot:
        plt.close(fig)
    


#loop_events(update_interval = 1)
def await_run():
    await_run_start = 10

    while True:
        
        loop_events(update_interval = 1)
        time.sleep(await_run_start)
        print('Awaiting new run ...') 
    return True

# Start running the code
await_run()
